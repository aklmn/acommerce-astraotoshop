package com.astra.astraotoshop.dummies;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.astra.astraotoshop.R;

/**
 * Created by Henra Setia Nugraha on 11/30/2017.
 */

public class SelectorAdapter extends BaseAdapter {
    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_selector, parent, false);
        }
        return convertView;
    }
}
