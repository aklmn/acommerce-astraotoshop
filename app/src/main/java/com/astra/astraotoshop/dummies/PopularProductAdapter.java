package com.astra.astraotoshop.dummies;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.astra.astraotoshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 11/16/2017.
 */

public class PopularProductAdapter extends RecyclerView.Adapter<PopularProductAdapter.PopularProductViewHolder> {

    PopularListener listener;

    public PopularProductAdapter() {
    }

    public PopularProductAdapter(PopularListener listener) {
        this.listener = listener;
    }

    @Override
    public PopularProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PopularProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_popular_package, parent, false));
    }

    @Override
    public void onBindViewHolder(PopularProductViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 9;
    }

    class PopularProductViewHolder extends RecyclerView.ViewHolder {
        public PopularProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.container)
        public void itemClick(){
            try {
                listener.onItemPopularClick();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface PopularListener {
        void onItemPopularClick();
    }
}
