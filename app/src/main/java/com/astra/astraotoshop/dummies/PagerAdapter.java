package com.astra.astraotoshop.dummies;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 11/16/2017.
 */

public class PagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragmentList;
    ArrayList<String> titles;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentList = new ArrayList<>();
        titles = new ArrayList<>();
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    public void addFragment(Fragment fragment) {
        addFragment(fragment,"");
    }

    public void addFragment(Fragment fragment, String name) {
        fragmentList.add(fragment);
        titles.add(name);
        notifyDataSetChanged();
    }

    public void removeFragment(int position) {
        if (fragmentList.size() > 0) {
            fragmentList.remove(position);
            titles.remove(position);
            notifyDataSetChanged();
        }
    }
}
