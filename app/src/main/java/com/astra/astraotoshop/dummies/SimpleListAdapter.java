package com.astra.astraotoshop.dummies;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.CustomFontFace;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;

/**
 * Created by Henra Setia Nugraha on 11/16/2017.
 */

public class SimpleListAdapter extends RecyclerView.Adapter<SimpleListAdapter.SimpleViewHolder> {

    private String sampleText;
    private List<String> strings = new ArrayList<>();
    private ListListener listener;
    private int resId = -1;
    int sampleCount = 0;

    public SimpleListAdapter(ListListener listener) {
        this.listener = listener;
    }

    public SimpleListAdapter(ListListener listener, int resId) {
        this.listener = listener;
        this.resId = resId;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SimpleViewHolder(LayoutInflater.from(parent.getContext()).inflate(resId == -1 ? android.R.layout.simple_list_item_1 : resId, parent, false));
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        if (holder.textView != null) {
            holder.textView.setText(strings.get(position));
        }
        if (holder.label != null) {
            holder.label.setText(strings.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return strings.size();
    }

    public void setSampleText(String sampleText) {
        this.sampleText = sampleText;
    }

    public void setSampleCount(int sampleCount) {
        this.sampleCount = sampleCount;
    }

    public void setStrings(List<String> strings) {
        this.strings = strings;
        notifyDataSetChanged();
    }

    class SimpleViewHolder extends RecyclerView.ViewHolder {
        @Nullable
        @BindView(android.R.id.text1)
        TextView textView;
        @BindView(R.id.text1)
        CustomFontFace label;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Optional
        @OnClick({android.R.id.text1, R.id.text1})
        public void clicked() {
            try {
                listener.onItemClick(1, getAdapterPosition(), strings.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
