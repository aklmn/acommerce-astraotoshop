package com.astra.astraotoshop.dummies;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.astra.astraotoshop.R;

/**
 * Created by Henra Setia Nugraha on 11/29/2017.
 */

public class SimpleListViewAdapter extends BaseAdapter {

    Context context;

    public SimpleListViewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 7;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.item_outlet, parent, false);
        }
        return convertView;
    }
}
