package com.astra.astraotoshop.dummies;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.home.provider.StaticBlocksItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 11/16/2017.
 */

public class BrandAdapter extends RecyclerView.Adapter<BrandAdapter.BrandViewHolder> {

    private Context context;
    private List<StaticBlocksItem> images;
    private ListListener listener;

    public BrandAdapter(Context context, @Nullable ListListener listener) {
        this.context = context;
        this.listener = listener;
        images = new ArrayList<>();
    }

    @Override
    public BrandViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BrandViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_brand_image, parent, false));
    }

    @Override
    public void onBindViewHolder(BrandViewHolder holder, int position) {

//        if (position == getItemCount() - 1) {
//            int padding = (int) (16 * context.getResources().getDisplayMetrics().density);
//            ViewGroup.MarginLayoutParams layoutParams= (ViewGroup.MarginLayoutParams) holder.container.getLayoutParams();
//            layoutParams.setMarginEnd(padding);
//            holder.container.setLayoutParams(layoutParams);
//        }

        if (images.get(position) != null) {
            Glide.with(context).load(images.get(position).getImgUrl()).into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void setImages(List<StaticBlocksItem> images) {
        this.images = images;
        notifyDataSetChanged();
    }

    class BrandViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.brandImage)
        ImageView imageView;
        @BindView(R.id.container)
        LinearLayout container;

        BrandViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.brandImage)
        public void itemClick() {
            try {
                listener.onItemClick(R.id.brandItem, getAdapterPosition(), images.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
