package com.astra.astraotoshop.product.product.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.product.provider.TierPriceEntity;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 1/12/2018.
 */

public class GroceryAdapter extends RecyclerView.Adapter<GroceryAdapter.GroceryViewHolder> {

    List<TierPriceEntity> groceries;

    public GroceryAdapter() {
        groceries = new ArrayList<>();
    }

    @Override
    public GroceryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new GroceryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_grocery, parent, false));
    }

    @Override
    public void onBindViewHolder(GroceryViewHolder holder, int position) {
        TierPriceEntity entity = groceries.get(position);
        holder.price.setText(groceryFormat(entity.getPriceQty(), entity.getPrice()));
        holder.percent.setText("hemat " + entity.getSave());
    }

    @Override
    public int getItemCount() {
        return groceries.size();
    }

    public void addData(List<TierPriceEntity> groceries) {
        this.groceries.addAll(groceries);
        notifyDataSetChanged();
    }

    private String groceryFormat(String qty, String price) {
        String grocery = "Beli ";
        grocery += String.valueOf(qty) + " seharga ";
        grocery += StringFormat.addCurrencyFormat(price) + " dan ";
        return grocery;
    }

    class GroceryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.price)
        TextView price;
        @BindView(R.id.percent)
        TextView percent;

        public GroceryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
