package com.astra.astraotoshop.product.category.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/8/2018.
 */

public class SubCategoryAdapter extends BaseAdapter {

    private Context context;
    private List<CategoryEntity> categories = new ArrayList<>();

    public SubCategoryAdapter(Context context, List<CategoryEntity> categories) {
        this.context = context;
        this.categories = categories;
    }


    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return categories.get(position).getId();
    }

    public void setCategories(List<CategoryEntity> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.item_selector, parent, false);
        }

        TextView item = convertView.findViewById(R.id.item);
        item.setText(categories.get(position).getName());
        return convertView;
    }
}
