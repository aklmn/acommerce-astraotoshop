package com.astra.astraotoshop.product.search.provider;

import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("product")
    private String product;

    @SerializedName("uenc")
    private String uenc;

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProduct() {
        return product;
    }

    public void setUenc(String uenc) {
        this.uenc = uenc;
    }

    public String getUenc() {
        return uenc;
    }

    @Override
    public String toString() {
        return
                "Data{" +
                        "product = '" + product + '\'' +
                        ",uenc = '" + uenc + '\'' +
                        "}";
    }
}