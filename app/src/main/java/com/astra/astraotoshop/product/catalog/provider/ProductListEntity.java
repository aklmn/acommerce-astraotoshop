package com.astra.astraotoshop.product.catalog.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductListEntity implements Parcelable {

    @SerializedName("items")
    private List<ProductItemEntity> items;

    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("brand")
    private List<BrandEntity> brands;

    @SerializedName("price")
    private PriceEntity price;

    protected ProductListEntity(Parcel in) {
        items = in.createTypedArrayList(ProductItemEntity.CREATOR);
        totalCount = in.readInt();
    }

    public static final Creator<ProductListEntity> CREATOR = new Creator<ProductListEntity>() {
        @Override
        public ProductListEntity createFromParcel(Parcel in) {
            return new ProductListEntity(in);
        }

        @Override
        public ProductListEntity[] newArray(int size) {
            return new ProductListEntity[size];
        }
    };

    public PriceEntity getPrice() {
        return price;
    }

    public void setPrice(PriceEntity price) {
        this.price = price;
    }

    public List<BrandEntity> getBrands() {
        return brands;
    }

    public void setBrands(List<BrandEntity> brands) {
        this.brands = brands;
    }

    public List<ProductItemEntity> getItems() {
        return items;
    }

    public void setItems(List<ProductItemEntity> items) {
        this.items = items;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(items);
        dest.writeInt(totalCount);
    }
}