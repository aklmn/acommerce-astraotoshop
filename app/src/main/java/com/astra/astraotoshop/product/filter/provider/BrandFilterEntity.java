package com.astra.astraotoshop.product.filter.provider;

import com.google.gson.annotations.SerializedName;

public class BrandFilterEntity{

	@SerializedName("label")
	private String label;

	@SerializedName("value")
	private String value;

	@SerializedName("isSelected")
	private boolean isSelected;

	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean selected) {
		isSelected = selected;
	}

	public void setLabel(String label){
		this.label = label;
	}

	public String getLabel(){
		return label;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"BrandFilterEntity{" + 
			"label = '" + label + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}