package com.astra.astraotoshop.product.review.provider;

import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.Map;

/**
 * Created by Henra Setia Nugraha on 1/15/2018.
 */

public interface ReviewContract {
    interface Review{
        void setData(ReviewEntity review);
        void showError();
    }
    interface ReviewPresenter{
        ReviewEntity getReview();
        void requestProductReview(String productId);
        void onReviewReceived(ReviewEntity review);
        void onFailedReceivingReview(Throwable throwable);
    }
    interface ReviewInteractor{
        void requestProductReview(NetworkHandler networkHandler, String productId, Map<String,String> filter);
    }
}
