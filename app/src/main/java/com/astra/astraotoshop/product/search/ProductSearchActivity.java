package com.astra.astraotoshop.product.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartPresenter;
import com.astra.astraotoshop.product.catalog.product.ProductAdapter;
import com.astra.astraotoshop.product.catalog.provider.BrandEntity;
import com.astra.astraotoshop.product.catalog.provider.PriceEntity;
import com.astra.astraotoshop.product.catalog.provider.ProductContract;
import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;
import com.astra.astraotoshop.product.catalog.provider.ProductListPresenter;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.filter.FilterFragment;
import com.astra.astraotoshop.product.filter.provider.FilterContract;
import com.astra.astraotoshop.product.filter.provider.FilterPresenter;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.user.profile.ProfileFragment;
import com.astra.astraotoshop.utils.base.BaseActionActivity;
import com.astra.astraotoshop.utils.view.FontIcon;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductSearchActivity extends BaseActionActivity implements SwipeRefreshLayout.OnRefreshListener, ProductContract.ProductList, ListListener, CartContract.ExternalCart {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.itemCount)
    TextView itemCount;
    @BindView(R.id.productList)
    RecyclerView productList;
    @BindView(R.id.swipe)
    SwipeRefreshLayout swipe;
    @BindView(R.id.loadingIcon)
    FontIcon loadingIcon;
    @BindView(R.id.bottomSheet)
    LinearLayout bottomSheet;
    private ProductAdapter adapter;
    private LinearLayoutManager layoutManager;
    private ProductContract.ProductListPresenter presenter;
    private FilterContract.FilterPresenter filterPresenter;
    private CartContract.CartPresenter cartPresenter;
    private BottomSheetBehavior<LinearLayout> sheetBehavior;
    private FilterFragment filterFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        ButterKnife.bind(this);

        String title = getIntent().getStringExtra("name");
        if (title != null)
            super.actionBarSetup(title);
        else
            super.actionBarSetup();

        bottomLoadingSetup();

        adapter = new ProductAdapter(this, this, getViewVisibility(), getViewVisibility());
        layoutManager = new LinearLayoutManager(this);
        productList.setLayoutManager(layoutManager);
        productList.setAdapter(adapter);
        productList.addOnScrollListener(onScrollListener);

        presenter = new ProductListPresenter(this);
        filterPresenter = new FilterPresenter(null, presenter);
        cartPresenter = new CartPresenter(null, this, getStoreCode());
        requestProducts();

        sheetBehavior = BottomSheetBehavior.from(bottomSheet);
        swipe.setRefreshing(true);
        swipe.setOnRefreshListener(this);
    }

    private int getViewVisibility() {
        if (getState() == getResources().getInteger(R.integer.state_product_service))
            return View.GONE;
        return View.VISIBLE;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        registerCustomMenu(menu, R.id.action_cart);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            case R.id.action_cart: {
                openCart();
                break;
            }
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRefresh() {
        loadingIcon.clearAnimation();
        requestProducts();
        hideLoading();
    }

    @Override
    public void setProductList(List<ProductItemEntity> productList, boolean isAddMore, int totalCount) {
        adapter.addProducts(productList, isAddMore);
        isLoad = false;
        swipe.setRefreshing(false);
        hideLoading();
        if (totalCount > 0)
            itemCount.setText(totalCount + " produk ditemukan");
        else itemCount.setText("Produk tidak ditemukan");
    }

    @Override
    public void showError() {
        swipe.setRefreshing(false);
        hideLoading();
    }

    @Override
    public void onFilterApply(Map<String, String> filter) {
        if (filter != null) {
            presenter.setFilters(filter);
        }
        requestProducts();
        swipe.setRefreshing(true);
    }

    @Override
    public void setFilterData(List<BrandEntity> brands, PriceEntity price) {

    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        if (viewId == R.id.container) {
            Intent intent = new Intent(this, ProductDetailActivity.class);
            intent.putExtra(STATE, getState());
            intent.putExtra("id", (String) data);
            //TODO put name
            startActivity(intent);
        } else if (viewId == R.id.buy) {
            if (isLogin()) {
                cartPresenter.addToCart(((ProductItemEntity) data).getSku(), 1);
//                Loading.showLoading(this);
            } else {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                Fragment fragment = getSupportFragmentManager().findFragmentByTag("account");
                if (fragment != null) transaction.remove(fragment);
                transaction.commit();

                ProfileFragment profileFragment = ProfileFragment.newInstance(getState());
                profileFragment.show(getSupportFragmentManager(), "account");
//                Intent intent = new Intent(this, LoginActivity.class);
//                intent.putExtra(STATE, getState());
//                startActivity(intent);
            }
        }
    }

    public void showFilter() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (filterFragment == null)
            filterFragment = FilterFragment.newInstance();
        Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("fragment_filter");
        if (fragment1 != null) {
            transaction.remove(filterFragment);
            transaction.commit();
        } else {
            filterFragment.show(getSupportFragmentManager(), "fragment_filter");
        }
        filterFragment.injectFilter(presenter, this);
        filterFragment.injectPresenter(filterPresenter);
    }

    private void hideLoading() {
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        loadingIcon.clearAnimation();
    }

    private void showLoading() {
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomLoadingSetup();
    }

    private void bottomLoadingSetup() {
        AnimationSet animationSet = new AnimationSet(true);
        animationSet.setFillAfter(true);
        animationSet.setInterpolator(new DecelerateInterpolator());
        animationSet.setFillEnabled(true);

        RotateAnimation rotateAnimation = new RotateAnimation(0, 360,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotateAnimation.setDuration(1000);
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        rotateAnimation.setFillAfter(true);
        animationSet.addAnimation(rotateAnimation);
        loadingIcon.startAnimation(animationSet);
    }

    private void requestProducts() {
        presenter.requestProductList(
                getStoreCode(),
                String.valueOf(getIntent().getIntExtra("catId", 0))
        );
    }

    public ProductContract.ProductListPresenter getPresenter() {
        return presenter;
    }

    boolean isLoad = false;

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (layoutManager.findFirstVisibleItemPosition() + recyclerView.getChildCount() == layoutManager.getItemCount()) {
                if (!isLoad) {
                    requestProducts();
                    isLoad = true;
                    showLoading();
                }
            }
        }
    };

    @Override
    public void onItemAdded() {
//        Loading.hideLoading();
        setCartBadgeVisibility(true);
        Toast.makeText(this, "Produk di tambahkan ke keranjang", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onItemFailedToAdd(String message) {
        Toast.makeText(this, "Gagal menambah produk", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemUpdated() {

    }

    @Override
    public void onItemFailedToUpdate(String message) {

    }

    @Override
    public void onItemDeleted() {

    }

    @Override
    public void onItemFailedToDelete() {

    }
}
