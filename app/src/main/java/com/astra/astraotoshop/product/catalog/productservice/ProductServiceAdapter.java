package com.astra.astraotoshop.product.catalog.productservice;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 11/15/2017.
 */

public class ProductServiceAdapter extends RecyclerView.Adapter<ProductServiceAdapter.ListViewHolder> {

    ItemClickListener listener;

    public ProductServiceAdapter() {
    }

    public ProductServiceAdapter(ItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_service, parent, false));
    }

    @Override
    public void onBindViewHolder(ListViewHolder holder, int position) {
        ViewGroup.MarginLayoutParams params = null;
        try {
            params = (ViewGroup.MarginLayoutParams)holder.parent.getLayoutParams();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (position % 2 == 1 && params != null) {
                params.setMarginStart(2);
            } else if (position % 2 == 0 && params != null) {
                params.setMarginEnd(2);
            }

            if (params != null) {
                params.bottomMargin=4;
                holder.parent.setLayoutParams(params);
            }
        }
    }

    @Override
    public int getItemCount() {
        return 9;
    }

    class ListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.parent)
        ConstraintLayout parent;

        ListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.parent)
        public void itemClicked(){
            if (listener != null) {
                listener.onItemClick();
            }
        }
    }

    public interface ItemClickListener{
        void onItemClick();
        void onItemClick(int viewId);
    }
}
