package com.astra.astraotoshop.product.search.provider;

import com.astra.astraotoshop.product.search.SearchContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 6/8/2018.
 */

public class SearchInteractor extends BaseInteractor implements SearchContract.SearchInteractor {

    private SearchContract.SearchPresenter presenter;

    public SearchInteractor(SearchContract.SearchPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void search(String storeCode, String query) {
        getGeneralNetworkManager()
                .getSuggest(new NetworkHandler(), storeCode, query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        suggestion -> presenter.onSuggestReceived(suggestion),
                        error -> presenter.onSuggestError(error)
                );
    }
}
