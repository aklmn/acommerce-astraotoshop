package com.astra.astraotoshop.product.catalog.productservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.CartFragment;
import com.astra.astraotoshop.user.wishlist.WishlistAdapter;
import com.astra.astraotoshop.product.filter.FilterFragment;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductServicesActivity extends BaseActivity implements ProductServiceAdapter.ItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_product);
        ButterKnife.bind(this);
        super.actionBarSetup();

        ProductServiceAdapter productServiceAdapter = new ProductServiceAdapter(this);
        WishlistAdapter adapter = new WishlistAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
//        GridLayoutManager layoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
            case R.id.action_cart: {
                CartFragment cartFragment = CartFragment.newInstance(getResources().getInteger(R.integer.state_product));
                cartFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
                cartFragment.show(getSupportFragmentManager(), "Cart");
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void showFilter() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        FilterFragment fragment = FilterFragment.newInstance();
        Fragment fragment1 = getSupportFragmentManager().findFragmentByTag("fragment_filter");
        if (fragment1 != null) {
            transaction.remove(fragment);
        } else
            fragment.show(getSupportFragmentManager(), "fragment_filter");
    }

    @Override
    public void onItemClick() {
        Intent intent = new Intent(this, ProductDetailActivity.class);
        intent.putExtra(STATE, getState());
        startActivity(intent);
    }

    @Override
    public void onItemClick(int viewId) {

    }
}
