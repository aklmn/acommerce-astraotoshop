package com.astra.astraotoshop.product.category.view;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.productservice.ProductServiceAdapter;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 1/5/2018.
 */

public class MainCategoryAdapter extends RecyclerView.Adapter<MainCategoryAdapter.MainCategoryViewHolder> {

    private List<CategoryEntity> categories = new ArrayList<>();
    private Context context;
    private int[] images = {R.drawable.cat_car, R.drawable.cat_bike};
    private ProductServiceAdapter.ItemClickListener listener;
    private ListListener itemClickListener;
    int current = 0;

    public MainCategoryAdapter(List<CategoryEntity> categories, Context context, ProductServiceAdapter.ItemClickListener listener, ListListener itemClickListener) {
        this.categories = categories;
        this.context = context;
        this.listener = listener;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MainCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MainCategoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_category, parent, false));
    }

    @Override
    public void onBindViewHolder(MainCategoryViewHolder holder, int position) {
        if (categories.get(position).isIsActive()
                && (categories.get(position).getName().equalsIgnoreCase("Motor")
                || categories.get(position).getName().equalsIgnoreCase("Mobil")
                || categories.get(position).getName().equalsIgnoreCase("paket mobil")
                || categories.get(position).getName().equalsIgnoreCase("Paket Motor"))) {

            holder.card.setVisibility(View.VISIBLE);
            holder.title.setText(categories.get(position).getName());
            if (images.length > current)
                Glide.with(context).load(images[current]).into(holder.image);
            ++current;
        } else holder.card.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class MainCategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.card)
        CardView card;

        public MainCategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.card)
        public void onItemClick() {
            try {
                itemClickListener.onItemClick(R.id.card, getAdapterPosition(), categories.get(getAdapterPosition()).getId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
