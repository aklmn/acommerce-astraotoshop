package com.astra.astraotoshop.product.product.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductEntity implements Parcelable{

	@SerializedName("short_description")
	private String shortDescription;

	@SerializedName("title_warehouse")
	private String titleWarehouse;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("description")
	private String description;

	@SerializedName("updated_in")
	private String updatedIn;

	@SerializedName("is_service")
	private String isService;

	@SerializedName("attribute_set_id")
	private String attributeSetId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("code_warehouse")
	private String codeWarehouse;

	@SerializedName("applicable_for_multiple")
	private String applicableForMultiple;

	@SerializedName("nama_pasar")
	private String namaPasar;

	@SerializedName("price")
	private String price;

	@SerializedName("special_from_date")
	private String specialFromDate;

	@SerializedName("sku")
	private String sku;

	@SerializedName("brand")
	private String brand;

	@SerializedName("tier_price")
	private List<TierPriceEntity> tierPrice;

	@SerializedName("image")
	private List<String> image;

	@SerializedName("type_id")
	private String typeId;

	@SerializedName("qty_warehouse")
	private String qtyWarehouse;

	@SerializedName("has_options")
	private String hasOptions;

	@SerializedName("weight")
	private String weight;

	@SerializedName("entity_id")
	private String entityId;

	@SerializedName("oem_number")
	private String oemNumber;

	@SerializedName("required_options")
	private String requiredOptions;

	@SerializedName("special_price")
	private String  specialPrice;

	@SerializedName("qty")
	private String qty;

	@SerializedName("name")
	private String name;

	@SerializedName("row_id")
	private String rowId;

	@SerializedName("appointment_at_home")
	private String appointmentAtHome;

	@SerializedName("created_in")
	private String createdIn;

	@SerializedName("warehouse_id")
	private String warehouseId;

	@SerializedName("status")
	private String status;

	@SerializedName("url_product")
	private String urlProduct;

	protected ProductEntity(Parcel in) {
		shortDescription = in.readString();
		titleWarehouse = in.readString();
		createdAt = in.readString();
		description = in.readString();
		updatedIn = in.readString();
		isService = in.readString();
		attributeSetId = in.readString();
		updatedAt = in.readString();
		codeWarehouse = in.readString();
		applicableForMultiple = in.readString();
		namaPasar = in.readString();
		price = in.readString();
		specialFromDate = in.readString();
		sku = in.readString();
		brand = in.readString();
		tierPrice = in.createTypedArrayList(TierPriceEntity.CREATOR);
		image = in.createStringArrayList();
		typeId = in.readString();
		qtyWarehouse = in.readString();
		hasOptions = in.readString();
		weight = in.readString();
		entityId = in.readString();
		oemNumber = in.readString();
		requiredOptions = in.readString();
		specialPrice = in.readString();
		qty = in.readString();
		name = in.readString();
		rowId = in.readString();
		appointmentAtHome = in.readString();
		createdIn = in.readString();
		warehouseId = in.readString();
		status = in.readString();
		urlProduct = in.readString();
	}

	public static final Creator<ProductEntity> CREATOR = new Creator<ProductEntity>() {
		@Override
		public ProductEntity createFromParcel(Parcel in) {
			return new ProductEntity(in);
		}

		@Override
		public ProductEntity[] newArray(int size) {
			return new ProductEntity[size];
		}
	};

	public void setShortDescription(String shortDescription){
		this.shortDescription = shortDescription;
	}

	public String getShortDescription(){
		return shortDescription;
	}

	public void setTitleWarehouse(String titleWarehouse){
		this.titleWarehouse = titleWarehouse;
	}

	public String getTitleWarehouse(){
		return titleWarehouse;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setUpdatedIn(String updatedIn){
		this.updatedIn = updatedIn;
	}

	public String getUpdatedIn(){
		return updatedIn;
	}

	public void setIsService(String isService){
		this.isService = isService;
	}

	public String getIsService(){
		return isService;
	}

	public void setAttributeSetId(String attributeSetId){
		this.attributeSetId = attributeSetId;
	}

	public String getAttributeSetId(){
		return attributeSetId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setCodeWarehouse(String codeWarehouse){
		this.codeWarehouse = codeWarehouse;
	}

	public String getCodeWarehouse(){
		return codeWarehouse;
	}

	public void setApplicableForMultiple(String applicableForMultiple){
		this.applicableForMultiple = applicableForMultiple;
	}

	public String getApplicableForMultiple(){
		return applicableForMultiple;
	}

	public void setNamaPasar(String namaPasar){
		this.namaPasar = namaPasar;
	}

	public String getNamaPasar(){
		return namaPasar;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setSpecialFromDate(String specialFromDate){
		this.specialFromDate = specialFromDate;
	}

	public String getSpecialFromDate(){
		return specialFromDate;
	}

	public void setSku(String sku){
		this.sku = sku;
	}

	public String getSku(){
		return sku;
	}

	public void setBrand(String brand){
		this.brand = brand;
	}

	public String getBrand(){
		return brand;
	}

	public void setTierPrice(List<TierPriceEntity> tierPrice){
		this.tierPrice = tierPrice;
	}

	public List<TierPriceEntity> getTierPrice(){
		return tierPrice;
	}

	public void setImage(List<String> image){
		this.image = image;
	}

	public List<String> getImage(){
		return image;
	}

	public void setTypeId(String typeId){
		this.typeId = typeId;
	}

	public String getTypeId(){
		return typeId;
	}

	public void setQtyWarehouse(String qtyWarehouse){
		this.qtyWarehouse = qtyWarehouse;
	}

	public String getQtyWarehouse(){
		return qtyWarehouse;
	}

	public void setHasOptions(String hasOptions){
		this.hasOptions = hasOptions;
	}

	public String getHasOptions(){
		return hasOptions;
	}

	public void setWeight(String weight){
		this.weight = weight;
	}

	public String getWeight(){
		return weight;
	}

	public void setEntityId(String entityId){
		this.entityId = entityId;
	}

	public String getEntityId(){
		return entityId;
	}

	public void setOemNumber(String oemNumber){
		this.oemNumber = oemNumber;
	}

	public String  getOemNumber(){
		return oemNumber;
	}

	public void setRequiredOptions(String requiredOptions){
		this.requiredOptions = requiredOptions;
	}

	public String getRequiredOptions(){
		return requiredOptions;
	}

	public void setSpecialPrice(String specialPrice){
		this.specialPrice = specialPrice;
	}

	public String getSpecialPrice(){
		return specialPrice;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRowId(String rowId){
		this.rowId = rowId;
	}

	public String getRowId(){
		return rowId;
	}

	public void setAppointmentAtHome(String appointmentAtHome){
		this.appointmentAtHome = appointmentAtHome;
	}

	public String getAppointmentAtHome(){
		return appointmentAtHome;
	}

	public void setCreatedIn(String createdIn){
		this.createdIn = createdIn;
	}

	public String getCreatedIn(){
		return createdIn;
	}

	public void setWarehouseId(String warehouseId){
		this.warehouseId = warehouseId;
	}

	public String getWarehouseId(){
		return warehouseId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public String getUrlProduct() {
		return urlProduct;
	}

	public void setUrlProduct(String urlProduct) {
		this.urlProduct = urlProduct;
	}

	@Override
 	public String toString(){
		return 
			"ProductEntity{" + 
			"short_description = '" + shortDescription + '\'' + 
			",title_warehouse = '" + titleWarehouse + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",description = '" + description + '\'' + 
			",updated_in = '" + updatedIn + '\'' + 
			",is_service = '" + isService + '\'' + 
			",attribute_set_id = '" + attributeSetId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",code_warehouse = '" + codeWarehouse + '\'' + 
			",applicable_for_multiple = '" + applicableForMultiple + '\'' + 
			",nama_pasar = '" + namaPasar + '\'' + 
			",price = '" + price + '\'' + 
			",special_from_date = '" + specialFromDate + '\'' + 
			",sku = '" + sku + '\'' + 
			",brand = '" + brand + '\'' + 
			",tier_price = '" + tierPrice + '\'' + 
			",image = '" + image + '\'' + 
			",type_id = '" + typeId + '\'' + 
			",qty_warehouse = '" + qtyWarehouse + '\'' + 
			",has_options = '" + hasOptions + '\'' + 
			",weight = '" + weight + '\'' + 
			",entity_id = '" + entityId + '\'' + 
			",oem_number = '" + oemNumber + '\'' + 
			",required_options = '" + requiredOptions + '\'' + 
			",special_price = '" + specialPrice + '\'' + 
			",qty = '" + qty + '\'' + 
			",name = '" + name + '\'' + 
			",row_id = '" + rowId + '\'' + 
			",appointment_at_home = '" + appointmentAtHome + '\'' + 
			",created_in = '" + createdIn + '\'' + 
			",warehouse_id = '" + warehouseId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(shortDescription);
        dest.writeString(titleWarehouse);
        dest.writeString(createdAt);
        dest.writeString(description);
        dest.writeString(updatedIn);
        dest.writeString(isService);
        dest.writeString(attributeSetId);
        dest.writeString(updatedAt);
        dest.writeString(codeWarehouse);
        dest.writeString(applicableForMultiple);
        dest.writeString(namaPasar);
        dest.writeString(price);
        dest.writeString(specialFromDate);
        dest.writeString(sku);
        dest.writeString(brand);
        dest.writeTypedList(tierPrice);
        dest.writeStringList(image);
        dest.writeString(typeId);
        dest.writeString(qtyWarehouse);
        dest.writeString(hasOptions);
        dest.writeString(weight);
        dest.writeString(entityId);
        dest.writeString(oemNumber);
        dest.writeString(requiredOptions);
        dest.writeString(specialPrice);
        dest.writeString(qty);
        dest.writeString(name);
        dest.writeString(rowId);
        dest.writeString(appointmentAtHome);
        dest.writeString(createdIn);
        dest.writeString(warehouseId);
        dest.writeString(status);
		dest.writeString(urlProduct);
    }
}