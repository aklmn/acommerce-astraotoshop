package com.astra.astraotoshop.product.category.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.category.provider.CategoryContract;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.product.category.provider.SubCatPresenter;
import com.astra.astraotoshop.utils.base.BaseFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/16/2017.
 */

public class CategoryFragment extends BaseFragment {

    public static final String DATA_KEY = "data";
    Unbinder unbinder;
    CategoryContract.SubCatPresenter presenter;

    public static CategoryFragment newInstance(int state, CategoryEntity cateData) {

        Bundle args = new Bundle();
        args.putInt(STATE, state);
        args.putParcelable(DATA_KEY, cateData);
        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new SubCatPresenter();
        presenter.collectIntentData(getArguments().getParcelable(DATA_KEY));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        unbinder = ButterKnife.bind(this, view);
        setState(getArguments().getInt(STATE));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.carPackage)
    public void onViewClicked() {
        Intent intent = new Intent(getActivity(), CategoryActivity.class);
        intent.putExtra(STATE, getState());
        startActivity(intent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface SubCategoryListener {
        void onItemSelected(int position);
    }
}
