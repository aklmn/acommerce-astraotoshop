package com.astra.astraotoshop.product.review.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReviewEntity {

    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("rating")
    private int rating;

    @SerializedName("store")
    private String store;

    @SerializedName("items")
    private List<ReviewItemEntity> items;

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getStore() {
        return store;
    }

    public void setItems(List<ReviewItemEntity> items) {
        this.items = items;
    }

    public List<ReviewItemEntity> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return
                "ReviewEntity{" +
                        "total_count = '" + totalCount + '\'' +
                        ",rating = '" + rating + '\'' +
                        ",store = '" + store + '\'' +
                        ",items = '" + items + '\'' +
                        "}";
    }
}