package com.astra.astraotoshop.product.search.provider;

import com.google.gson.annotations.SerializedName;

public class TextSuggestItem {

    @SerializedName("title")
    private String title;

    @SerializedName("num_results")
    private String numResults;

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setNumResults(String numResults) {
        this.numResults = numResults;
    }

    public String getNumResults() {
        return numResults;
    }

    @Override
    public String toString() {
        return
                "TextSuggestItem{" +
                        "title = '" + title + '\'' +
                        ",num_results = '" + numResults + '\'' +
                        "}";
    }
}