package com.astra.astraotoshop.product.search.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IndicesItem {

    @SerializedName("identifier")
    private String identifier;

    @SerializedName("isShowTotals")
    private boolean isShowTotals;

    @SerializedName("totalItems")
    private int totalItems;

    @SerializedName("title")
    private String title;

    @SerializedName("items")
    private List<ItemsItem> items;

    @SerializedName("order")
    private int order;

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIsShowTotals(boolean isShowTotals) {
        this.isShowTotals = isShowTotals;
    }

    public boolean isIsShowTotals() {
        return isShowTotals;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setItems(List<ItemsItem> items) {
        this.items = items;
    }

    public List<ItemsItem> getItems() {
        return items;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getOrder() {
        return order;
    }

    @Override
    public String toString() {
        return
                "IndicesItem{" +
                        "identifier = '" + identifier + '\'' +
                        ",isShowTotals = '" + isShowTotals + '\'' +
                        ",totalItems = '" + totalItems + '\'' +
                        ",title = '" + title + '\'' +
                        ",items = '" + items + '\'' +
                        ",order = '" + order + '\'' +
                        "}";
    }
}