package com.astra.astraotoshop.product.review.provider;

import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/15/2018.
 */

public class ReviewInteractor extends BaseInteractor  implements ReviewContract.ReviewInteractor {

    private ReviewContract.ReviewPresenter presenter;

    public ReviewInteractor(ReviewContract.ReviewPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestProductReview(NetworkHandler networkHandler, String productId, Map<String, String> filter) {
        getGeneralNetworkManager()
                .requestProductReview(
                        new NetworkHandler(),
                        productId,
                        filter)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        review->presenter.onReviewReceived(review),
                        error->presenter.onFailedReceivingReview(error)
                );
    }
}
