package com.astra.astraotoshop.product.search.provider;

import com.astra.astraotoshop.product.search.SearchContract;
import com.astra.astraotoshop.utils.pref.Pref;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 6/8/2018.
 */

public class SearchPresenter implements SearchContract.SearchPresenter {
    private final String PREF_KEY = "elasticSuggest";
    private final int MAX_RECENT = 5;

    private SearchContract.SearchView view;
    private String storeCode;
    private SearchContract.SearchInteractor interactor;

    public SearchPresenter(SearchContract.SearchView view, String storeCode) {
        this.view = view;
        this.storeCode = storeCode;
        interactor = new SearchInteractor(this);
    }


    @Override
    public void search(String query) {
        interactor.search(storeCode, query);
    }

    @Override
    public void onSuggestReceived(Suggestion suggestion) {
        view.showSuggest(suggestion);
    }

    @Override
    public void onSuggestError(Throwable throwable) {
        view.showError();
    }

    @Override
    public void addRecent(String value) {
        List<String> suggestion = loadRecent();
        if (suggestion.size() > MAX_RECENT - 1) {
            suggestion.add(0, value);
            suggestion.remove(MAX_RECENT);
        } else if (!suggestion.contains(value)) {
            suggestion.add(value);
        }
        Pref.getPreference().putString(PREF_KEY, new Gson().toJson(suggestion));
    }

    @Override
    public void deleteRecent() {
        Pref.getPreference().remove(PREF_KEY);
    }

    @Override
    public List<String> loadRecent() {
        List<String> suggestion;
        String suggest = Pref.getPreference().getString(PREF_KEY);
        suggestion = new Gson().fromJson(suggest, new TypeToken<List<String>>() {
        }.getType());
        if (suggestion == null)
            suggestion = new ArrayList<>();
        return suggestion;
    }
}
