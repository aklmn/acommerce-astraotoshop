package com.astra.astraotoshop.product.category.provider;

import com.astra.astraotoshop.utils.network.handler.NetworkHandler;
import com.astra.astraotoshop.utils.network.service.CategoryService;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/5/2018.
 */

public class CatInteractor implements CategoryContract.CatInteractor {
    CategoryService service;
    CategoryContract.CatPresenter presenter;

    public CatInteractor(CategoryContract.CatPresenter presenter) {
        service = new CategoryService();
        this.presenter = presenter;
    }

    @Override
    public void requestCategory(NetworkHandler networkHandler, String storeCode) {
        service.requestCategory(networkHandler, storeCode)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        categories -> {
                            presenter.onCategoryReceived(categories,storeCode);
                        },
                        error -> {
                            presenter.onRequestCatFailed();
                        });
    }
}
