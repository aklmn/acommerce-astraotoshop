package com.astra.astraotoshop.product.category.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.PagerAdapter;
import com.astra.astraotoshop.product.catalog.product.ProductsActivity;
import com.astra.astraotoshop.product.category.provider.CategoryContract;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoryActivity extends BaseActivity implements CategoryContract.CategoryView {

    public static final String DATA_KEY = "catData";
    public static final String TITLE_KEY = "catTitle";

    @BindView(R.id.categoryPager)
    ViewPager categoryPager;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private PagerAdapter pagerAdapter;
    CategoryContract.SubCatPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);
        ButterKnife.bind(this);
        super.actionBarSetup("Paket Mobil");

        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        categoryPager.setAdapter(pagerAdapter);

        //TODO should be in presenter
        createFragment(
                getIntent().getExtras().getParcelable(DATA_KEY),
                getIntent().getExtras().getString(TITLE_KEY)
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return false;
    }

    @Override
    public void onBackPressed() {
        if (categoryPager.getCurrentItem() != 0) {
            int currentPosition = categoryPager.getCurrentItem();
            categoryPager.setCurrentItem(currentPosition - 1, true);
            setCustomTitle(((SubCategoryFragment)pagerAdapter.getItem(currentPosition-1)).getTitle());
        } else finish();
    }

    @Override
    public void onItemSelect(CategoryEntity category, String title) {
        if (category != null) {
            if (category.getChildrenData().size() > 0)
                if (categoryPager.getCurrentItem() < pagerAdapter.getCount() - 1) {
                    nextPage(category,title);
                } else
                    createFragment(category, title);
            else {
                Intent intent = new Intent(this, ProductsActivity.class);
                intent.putExtra(STATE, getState());
                intent.putExtra("catId", category.getId());
                intent.putExtra("name", category.getName());
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    private void nextPage(CategoryEntity category, String title) {
        ((SubCategoryFragment) pagerAdapter.getItem(categoryPager.getCurrentItem() + 1)).updateList(category);
        categoryPager.setCurrentItem(categoryPager.getCurrentItem() + 1);
        setCustomTitle(title);
    }

    private void createFragment(CategoryEntity category, String title) {
        if (!TextUtils.isEmpty(title))
            try {
                getSupportActionBar().setTitle(title);
            } catch (Exception e) {
                e.printStackTrace();
            }
        if (category != null) {
            SubCategoryFragment fragment = SubCategoryFragment.newInstance(category);
            fragment.addListener(this);
            pagerAdapter.addFragment(fragment, title == null ? "" : title);
            categoryPager.setCurrentItem(categoryPager.getCurrentItem() + 1, true);
        }
    }
}
