package com.astra.astraotoshop.product.review.provider;

import com.google.gson.annotations.SerializedName;

public class ReviewItemEntity {

	@SerializedName("review_id")
	private String reviewId;

	@SerializedName("status_id")
	private String statusId;

	@SerializedName("entity_pk_value")
	private String entityPkValue;

	@SerializedName("entity_code")
	private String entityCode;

	@SerializedName("nickname")
	private String nickname;

	@SerializedName("rating")
	private int rating;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("detail")
	private String detail;

	@SerializedName("entity_id")
	private String entityId;

	@SerializedName("detail_id")
	private String detailId;

	@SerializedName("title")
	private String title;

	@SerializedName("customer_id")
	private String  customerId;

	public void setReviewId(String reviewId){
		this.reviewId = reviewId;
	}

	public String getReviewId(){
		return reviewId;
	}

	public void setStatusId(String statusId){
		this.statusId = statusId;
	}

	public String getStatusId(){
		return statusId;
	}

	public void setEntityPkValue(String entityPkValue){
		this.entityPkValue = entityPkValue;
	}

	public String getEntityPkValue(){
		return entityPkValue;
	}

	public void setEntityCode(String entityCode){
		this.entityCode = entityCode;
	}

	public String getEntityCode(){
		return entityCode;
	}

	public void setNickname(String nickname){
		this.nickname = nickname;
	}

	public String getNickname(){
		return nickname;
	}

	public void setRating(int rating){
		this.rating = rating;
	}

	public int getRating(){
		return rating;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	public void setEntityId(String entityId){
		this.entityId = entityId;
	}

	public String getEntityId(){
		return entityId;
	}

	public void setDetailId(String detailId){
		this.detailId = detailId;
	}

	public String getDetailId(){
		return detailId;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setCustomerId(String customerId){
		this.customerId = customerId;
	}

	public String getCustomerId(){
		return customerId;
	}

	@Override
 	public String toString(){
		return 
			"ReviewItemEntity{" +
			"review_id = '" + reviewId + '\'' + 
			",status_id = '" + statusId + '\'' + 
			",entity_pk_value = '" + entityPkValue + '\'' + 
			",entity_code = '" + entityCode + '\'' + 
			",nickname = '" + nickname + '\'' + 
			",rating = '" + rating + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",detail = '" + detail + '\'' + 
			",entity_id = '" + entityId + '\'' + 
			",detail_id = '" + detailId + '\'' + 
			",title = '" + title + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			"}";
		}
}