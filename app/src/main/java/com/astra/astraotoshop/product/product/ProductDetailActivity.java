package com.astra.astraotoshop.product.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.PagerAdapter;
import com.astra.astraotoshop.order.appointment.AppointmentActivity;
import com.astra.astraotoshop.order.appointment.AppointmentFragment;
import com.astra.astraotoshop.order.cart.provider.AppointmentParamItem;
import com.astra.astraotoshop.order.cart.provider.AppointmentsItemCart;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartPresenter;
import com.astra.astraotoshop.product.product.provider.ProductDetailContract;
import com.astra.astraotoshop.product.product.provider.ProductDetailPresenter;
import com.astra.astraotoshop.product.product.provider.ProductEntity;
import com.astra.astraotoshop.product.review.provider.ReviewContract;
import com.astra.astraotoshop.product.review.provider.ReviewEntity;
import com.astra.astraotoshop.product.review.provider.ReviewPresenter;
import com.astra.astraotoshop.user.profile.ProfileFragment;
import com.astra.astraotoshop.utils.base.BaseActionActivity;
import com.astra.astraotoshop.utils.pref.Pref;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.Loading;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductDetailActivity extends BaseActionActivity implements ProductDetailContract.ProductDetailView, ViewPager.OnPageChangeListener, ReviewContract.Review, CartContract.ExternalCart {

    private final int RC_ADD = 1;
    private final int RC_EDIT = 2;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.tab)
    TabLayout tab;
    @BindView(R.id.buy)
    Button buy;
    @BindView(R.id.addAppointment)
    CustomFontFace addAppointment;

    private ProductDetailFragment productDetailFragment;
    private ProductDescFragment productDescFragment;
    private ReviewContract.ReviewPresenter reviewPresenter;
    private ProductDetailContract.ProductDetailPresenter presenter;
    private ProductReviewsFragment productReviewsFragment;
    private CartContract.CartPresenter cartPresenter;
    private String sku;
    private boolean isEditAppointment = false;
    private boolean isUpdated = false;
    private int currentPosition;
    private int itemId;
    private Loading loading;

    List<String> appointmentList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        ButterKnife.bind(this);
        actionBarSetup();

        isEditAppointment = getIntent().getBooleanExtra(getString(R.string.intent_edit_appointment), false);
        itemId = getIntent().getIntExtra(getString(R.string.intent_item_id), -1);

        presenter = new ProductDetailPresenter(this);
        reviewPresenter = new ReviewPresenter(this);
        cartPresenter = new CartPresenter(null, this, "product", false);
        presenter.requestProduct(getStoreCode(), getIntent().getStringExtra("id"));
        reviewPresenter.requestProductReview(getIntent().getStringExtra("id"));

//        buttonInit();
        productDetailFragment = ProductDetailFragment.newInstance(getState());
        productDescFragment = ProductDescFragment.newInstance();
        productReviewsFragment = ProductReviewsFragment.newInstance();
        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(productDetailFragment, "PRODUK");
        pagerAdapter.addFragment(productDescFragment, "DESKRIPSI");
        pagerAdapter.addFragment(productReviewsFragment, "ULASAN");
        pager.addOnPageChangeListener(this);
        pager.setAdapter(pagerAdapter);
        tab.setupWithViewPager(pager);
        loading = new Loading(this, getLayoutInflater());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        registerCustomMenu(menu, R.id.action_cart);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                onBackPressed();
                break;
            }

            case R.id.action_cart: {
                openCart();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isEditAppointment) {
            try {
                if (isUpdated ||
                        (presenter.getAppointment(new ArrayList<>(), null).get(0).getAddress().size() == 0)) {

                    showAlert();
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RC_ADD: {
                    presenter.addAppointmentData(data.getParcelableExtra(getString(R.string.intent_appointment)));
                    productDetailFragment.setAppointmentData(presenter.getAppointment(new ArrayList<>(), null));
                    addAppointment.setVisibility(View.GONE);
                    break;
                }
                case RC_EDIT: {
                    AppointmentsItemCart item = data.getParcelableExtra(getString(R.string.intent_appointment));
                    productDetailFragment.setAppointmentByPosition(currentPosition, item);
                    presenter.editAppointment(currentPosition, item);
                    System.out.println();
                    break;
                }
            }
            isUpdated = true;
        }
    }

    @Override
    public void setProduct(ProductEntity product) {
        if (product != null) {
            sku = product.getSku();
            productDetailFragment.setProductData(product);
            productDescFragment.setData(product);
            setCustomTitle(product.getName());
            if (Double.parseDouble(product.getQty()) > 0)
                buy.setEnabled(true);
            else buy.setText("Stok tidak tersedia");
            addAppointment.setEnabled(true);
            reviewPresenter.requestProductReview(getIntent().getStringExtra("id"));
        }
    }

    @Override
    public void setData(ReviewEntity review) {
//        productReviewsFragment.setReviewEntity(review);
        System.out.println("det");
        productDetailFragment.setRating(
                presenter.getProduct().getName(),
                presenter.getProduct().getSku(),
                review.getRating(),
                review.getItems().size());

    }

    @Override
    public void showError() {
        Toast.makeText(this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void appointmentComplete(List<AppointmentParamItem> paramItems) {
        cartPresenter.addToCart(sku, presenter.getAppointment(new ArrayList<>(), null).size(), paramItems);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            if (presenter.getProduct() != null) {
                productDetailFragment.setProductData(presenter.getProduct());
                productDetailFragment.setRating(
                        presenter.getProduct().getName(), presenter.getProduct().getSku(), reviewPresenter.getReview().getRating(),
                        reviewPresenter.getReview().getItems().size());
                if (isEditAppointment) {
                    productDetailFragment.setAppointmentData(
                            presenter.getAppointment(super.cartPresenter.getCartsItem(), sku));
                } else {
                    productDetailFragment.setAppointmentData(
                            presenter.getAppointment(new ArrayList<>(), null)
                    );
                }
            }
        } else if (position == 2) {
            if (reviewPresenter.getReview() != null) {
                if (reviewPresenter.getReview().getItems().size() > 0) {
                    productReviewsFragment.setReviewEntity(
                            reviewPresenter.getReview(),
                            presenter.getProduct().getName(),
                            presenter.getProduct().getSku());
                } else {
                    productReviewsFragment.setReviewEntity(
                            presenter.getProduct().getName(),
                            presenter.getProduct().getSku()
                    );
                }
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onItemAdded() {
        setCartBadgeVisibility(true);
        showToast("Berhasil menambahkan barang");
//        productDetailFragment.setAppointmentData(new ArrayList<>());
//        presenter.clearAppointment();
//        if (getState() == getResources().getInteger(R.integer.state_product_service))
//            addAppointment.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemFailedToAdd(String message) {
        showToast(message);
    }

    @Override
    public void onItemUpdated() {
        showToast("Tempat dan waktu berhasil diubah");
        finish();
    }

    @Override
    public void onItemFailedToUpdate(String message) {
        showToast("Terjadi kesalahan, silakan coba lagi");
    }

    @Override
    public void onItemDeleted() {
        finish();
    }

    @Override
    public void onItemFailedToDelete() {
        Toast.makeText(this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.addToCart)
    public void onViewClicked() {
        if (isLogin()) {
            cartPresenter.addToCart(sku, 1);
        } else {
            showAccount();
        }
    }

    @OnClick(R.id.addAppointment)
    public void onAddAppointmentClicked() {
        addProductAppointment(null, -1);
    }

    @Override
    protected void onCartLoaded() {
        if (super.cartPresenter.getCartsItem().size() > 0) {
            productDetailFragment.setAppointmentData(
                    presenter.getAppointment(super.cartPresenter.getCartsItem(), sku));
            addAppointment.setVisibility(View.GONE);
        } else if (getState() == getResources().getInteger(R.integer.state_product_service) && presenter.getAppointment(new ArrayList<>(), null).size() == 0) {
//            addAppointment.setVisibility(View.VISIBLE);
        }
    }

    private void buttonInit() {
        if (getState() == getResources().getInteger(R.integer.state_product_service) && cartPresenter.getCartsItem().size() == 0 && !isEditAppointment)
            addAppointment.setVisibility(View.VISIBLE);
        else if (getState() == getResources().getInteger(R.integer.state_product))
            addAppointment.setVisibility(View.GONE);
        if (isEditAppointment)
            buy.setText(R.string.label_save);
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void changePage(int position) {
        pager.setCurrentItem(position, true);
    }

    public void addProductAppointment(AppointmentsItemCart appointment, int position) {
//        Intent intent = getIntent(this, AppointmentActivity.class);
//        intent.putExtra("id", presenter.getProduct().getEntityId());
//        intent.putExtra("sku", sku);
//        intent.putExtra(getString(R.string.intent_appointment), appointment);
//        intent.putExtra(getString(R.string.intent_index_position), position);
//        if (appointment != null) {
//            currentPosition = position;
//            startActivityForResult(intent, RC_EDIT);
//        } else startActivityForResult(intent, RC_ADD);

        AppointmentFragment appointmentFragment = AppointmentFragment.newInstance(presenter.getProduct().getEntityId(), presenter.getProduct().getSku());
        appointmentFragment.show(getSupportFragmentManager(), ProductDetailActivity.class.getName() + AppointmentFragment.class.getName());
    }

    public void onAppointmentItemDelete(int position) {
        presenter.deleteItem(position);
        if (presenter.getAppointment(new ArrayList<>(), null).size() > 0) {
            if (presenter.getAppointment(new ArrayList<>(), null).get(0).getApDate().size() == 0) {
//                addAppointment.setVisibility(View.VISIBLE);
            }
        }
        isUpdated = true;
    }

    private void showAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage("Anda sudah melakukan perubahan tempat dan jadwal")
                .setNegativeButton(R.string.label_ignore, (dialog, which) -> {
                    isEditAppointment = false;
                    onBackPressed();
                })
                .setPositiveButton(R.string.label_appointment_save, ((dialog, which) -> {
                    if (presenter.getAppointment(new ArrayList<>(), null).get(0).getAddress().size() <= 0) {
                        cartPresenter.deleteCart(String.valueOf(itemId));
                        setCartBadgeVisibility(false);
                    } else {
                        cartPresenter.updateCart(presenter.getCartItem(sku, itemId, Pref.getPreference().getInt("quoteId" + getStoreCode())));
                    }
//                    isEditAppointment = false;
//                    onBackPressed();
                    loading.show("Harap Tunggu");
                }));
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
