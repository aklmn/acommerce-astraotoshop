package com.astra.astraotoshop.product.catalog.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.astra.astraotoshop.product.product.provider.TierPriceEntity;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductItemEntity implements Parcelable{

	@SerializedName("short_description")
	private String shortDescription;

	@SerializedName("title_warehouse")
	private String titleWarehouse;

	@SerializedName("rating")
	private int rating;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("description")
	private String description;

	@SerializedName("updated_in")
	private String updatedIn;

	@SerializedName("is_service")
	private String isService;

	@SerializedName("attribute_set_id")
	private String attributeSetId;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("code_warehouse")
	private String codeWarehouse;

	@SerializedName("price")
	private String price;

	@SerializedName("special_from_date")
	private String specialFromDate;

	@SerializedName("sku")
	private String sku;

	@SerializedName("brand")
	private String brand;

	@SerializedName("tier_price")
	private List<TierPriceEntity> tierPrice;

	@SerializedName("image")
	private String image;

	@SerializedName("type_id")
	private String typeId;

	@SerializedName("qty_warehouse")
	private String qtyWarehouse;

	@SerializedName("has_options")
	private String hasOptions;

	@SerializedName("weight")
	private String weight;

	@SerializedName("cat_index_position")
	private String catIndexPosition;

	@SerializedName("entity_id")
	private String entityId;

	@SerializedName("brand_id")
	private String brandId;

	@SerializedName("required_options")
	private String requiredOptions;

	@SerializedName("special_price")
	private String specialPrice;

	@SerializedName("qty")
	private String qty;

	@SerializedName("name")
	private String name;

	@SerializedName("row_id")
	private String rowId;

	@SerializedName("appointment_at_home")
	private String appointmentAtHome;

	@SerializedName("created_in")
	private String createdIn;

	@SerializedName("warehouse_id")
	private String warehouseId;

	@SerializedName("status")
	private String status;

	protected ProductItemEntity(Parcel in) {
		shortDescription = in.readString();
		titleWarehouse = in.readString();
		rating = in.readInt();
		createdAt = in.readString();
		description = in.readString();
		updatedIn = in.readString();
		isService = in.readString();
		attributeSetId = in.readString();
		updatedAt = in.readString();
		codeWarehouse = in.readString();
		price = in.readString();
		specialFromDate = in.readString();
		sku = in.readString();
		brand = in.readString();
		tierPrice = in.createTypedArrayList(TierPriceEntity.CREATOR);
		image = in.readString();
		typeId = in.readString();
		qtyWarehouse = in.readString();
		hasOptions = in.readString();
		weight = in.readString();
		catIndexPosition = in.readString();
		entityId = in.readString();
		brandId = in.readString();
		requiredOptions = in.readString();
		specialPrice = in.readString();
		qty = in.readString();
		name = in.readString();
		rowId = in.readString();
		appointmentAtHome = in.readString();
		createdIn = in.readString();
		warehouseId = in.readString();
		status = in.readString();
	}

	public static final Creator<ProductItemEntity> CREATOR = new Creator<ProductItemEntity>() {
		@Override
		public ProductItemEntity createFromParcel(Parcel in) {
			return new ProductItemEntity(in);
		}

		@Override
		public ProductItemEntity[] newArray(int size) {
			return new ProductItemEntity[size];
		}
	};

	public void setShortDescription(String shortDescription){
		this.shortDescription = shortDescription;
	}

	public String getShortDescription(){
		return shortDescription;
	}

	public void setTitleWarehouse(String titleWarehouse){
		this.titleWarehouse = titleWarehouse;
	}

	public String getTitleWarehouse(){
		return titleWarehouse;
	}

	public void setRating(int rating){
		this.rating = rating;
	}

	public int getRating(){
		return rating;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setUpdatedIn(String updatedIn){
		this.updatedIn = updatedIn;
	}

	public String getUpdatedIn(){
		return updatedIn;
	}

	public void setIsService(String isService){
		this.isService = isService;
	}

	public String getIsService(){
		return isService;
	}

	public void setAttributeSetId(String attributeSetId){
		this.attributeSetId = attributeSetId;
	}

	public String getAttributeSetId(){
		return attributeSetId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setCodeWarehouse(String codeWarehouse){
		this.codeWarehouse = codeWarehouse;
	}

	public String getCodeWarehouse(){
		return codeWarehouse;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setSpecialFromDate(String specialFromDate){
		this.specialFromDate = specialFromDate;
	}

	public String getSpecialFromDate(){
		return specialFromDate;
	}

	public void setSku(String sku){
		this.sku = sku;
	}

	public String getSku(){
		return sku;
	}

	public void setBrand(String brand){
		this.brand = brand;
	}

	public String getBrand(){
		return brand;
	}

	public void setTierPrice(List<TierPriceEntity> tierPrice){
		this.tierPrice = tierPrice;
	}

	public List<TierPriceEntity> getTierPrice(){
		return tierPrice;
	}

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setTypeId(String typeId){
		this.typeId = typeId;
	}

	public String getTypeId(){
		return typeId;
	}

	public void setQtyWarehouse(String qtyWarehouse){
		this.qtyWarehouse = qtyWarehouse;
	}

	public String getQtyWarehouse(){
		return qtyWarehouse;
	}

	public void setHasOptions(String hasOptions){
		this.hasOptions = hasOptions;
	}

	public String getHasOptions(){
		return hasOptions;
	}

	public void setWeight(String weight){
		this.weight = weight;
	}

	public String getWeight(){
		return weight;
	}

	public void setCatIndexPosition(String catIndexPosition){
		this.catIndexPosition = catIndexPosition;
	}

	public String getCatIndexPosition(){
		return catIndexPosition;
	}

	public void setEntityId(String entityId){
		this.entityId = entityId;
	}

	public String getEntityId(){
		return entityId;
	}

	public void setBrandId(String brandId){
		this.brandId = brandId;
	}

	public String getBrandId(){
		return brandId;
	}

	public void setRequiredOptions(String requiredOptions){
		this.requiredOptions = requiredOptions;
	}

	public String getRequiredOptions(){
		return requiredOptions;
	}

	public void setSpecialPrice(String specialPrice){
		this.specialPrice = specialPrice;
	}

	public String getSpecialPrice(){
		return specialPrice;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRowId(String rowId){
		this.rowId = rowId;
	}

	public String getRowId(){
		return rowId;
	}

	public void setAppointmentAtHome(String appointmentAtHome){
		this.appointmentAtHome = appointmentAtHome;
	}

	public String getAppointmentAtHome(){
		return appointmentAtHome;
	}

	public void setCreatedIn(String createdIn){
		this.createdIn = createdIn;
	}

	public String getCreatedIn(){
		return createdIn;
	}

	public void setWarehouseId(String warehouseId){
		this.warehouseId = warehouseId;
	}

	public String getWarehouseId(){
		return warehouseId;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ProductItemEntity{" + 
			"short_description = '" + shortDescription + '\'' + 
			",title_warehouse = '" + titleWarehouse + '\'' + 
			",rating = '" + rating + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",description = '" + description + '\'' + 
			",updated_in = '" + updatedIn + '\'' + 
			",is_service = '" + isService + '\'' + 
			",attribute_set_id = '" + attributeSetId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",code_warehouse = '" + codeWarehouse + '\'' + 
			",price = '" + price + '\'' + 
			",special_from_date = '" + specialFromDate + '\'' + 
			",sku = '" + sku + '\'' + 
			",brand = '" + brand + '\'' + 
			",tier_price = '" + tierPrice + '\'' + 
			",image = '" + image + '\'' + 
			",type_id = '" + typeId + '\'' + 
			",qty_warehouse = '" + qtyWarehouse + '\'' + 
			",has_options = '" + hasOptions + '\'' + 
			",weight = '" + weight + '\'' + 
			",cat_index_position = '" + catIndexPosition + '\'' + 
			",entity_id = '" + entityId + '\'' + 
			",brand_id = '" + brandId + '\'' + 
			",required_options = '" + requiredOptions + '\'' + 
			",special_price = '" + specialPrice + '\'' + 
			",qty = '" + qty + '\'' + 
			",name = '" + name + '\'' + 
			",row_id = '" + rowId + '\'' + 
			",appointment_at_home = '" + appointmentAtHome + '\'' + 
			",created_in = '" + createdIn + '\'' + 
			",warehouse_id = '" + warehouseId + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(shortDescription);
		dest.writeString(titleWarehouse);
		dest.writeInt(rating);
		dest.writeString(createdAt);
		dest.writeString(description);
		dest.writeString(updatedIn);
		dest.writeString(isService);
		dest.writeString(attributeSetId);
		dest.writeString(updatedAt);
		dest.writeString(codeWarehouse);
		dest.writeString(price);
		dest.writeString(specialFromDate);
		dest.writeString(sku);
		dest.writeString(brand);
		dest.writeTypedList(tierPrice);
		dest.writeString(image);
		dest.writeString(typeId);
		dest.writeString(qtyWarehouse);
		dest.writeString(hasOptions);
		dest.writeString(weight);
		dest.writeString(catIndexPosition);
		dest.writeString(entityId);
		dest.writeString(brandId);
		dest.writeString(requiredOptions);
		dest.writeString(specialPrice);
		dest.writeString(qty);
		dest.writeString(name);
		dest.writeString(rowId);
		dest.writeString(appointmentAtHome);
		dest.writeString(createdIn);
		dest.writeString(warehouseId);
		dest.writeString(status);
	}
}