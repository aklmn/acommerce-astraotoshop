package com.astra.astraotoshop.product.search.provider;

import com.google.gson.annotations.SerializedName;

public class ItemsItem {

    @SerializedName("image")
    private String image;

    @SerializedName("price")
    private int price;

    @SerializedName("name")
    private String name;

    @SerializedName("rating")
    private String rating;

    @SerializedName("description")
    private String description;

    @SerializedName("sku")
    private String sku;

    @SerializedName("url")
    private String url;

    @SerializedName("cart")
    private Cart cart;

    public void setImage(String  image) {
        this.image = image;
    }

    public String  getImage() {
        return image;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSku() {
        return sku;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public Cart getCart() {
        return cart;
    }

    @Override
    public String toString() {
        return
                "ItemsItem{" +
                        "image = '" + image + '\'' +
                        ",price = '" + price + '\'' +
                        ",name = '" + name + '\'' +
                        ",rating = '" + rating + '\'' +
                        ",description = '" + description + '\'' +
                        ",sku = '" + sku + '\'' +
                        ",url = '" + url + '\'' +
                        ",cart = '" + cart + '\'' +
                        "}";
    }
}