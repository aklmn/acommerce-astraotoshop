package com.astra.astraotoshop.product.catalog.provider;

import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 7/5/2018.
 */

public class ProductSearchInteractor extends BaseInteractor implements ProductContract.ProductListInteractor {

    private ProductContract.ProductListPresenter presenter;
    private String query = "";

    public ProductSearchInteractor(ProductContract.ProductListPresenter presenter, String query) {
        super(null);
        this.presenter = presenter;
        this.query = query;
    }

    @Override
    public void requestProductList(NetworkHandler networkHandler, String storeCode, String catId, Map<String, String> filters) {
        getGeneralNetworkManager()
                .requestProductSearch(new NetworkHandler(), query, storeCode, filters)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        productListEntity -> presenter.onProductListReceived(productListEntity),
                        error -> presenter.onFailedReceivingProduct(error)
                );
    }
}
