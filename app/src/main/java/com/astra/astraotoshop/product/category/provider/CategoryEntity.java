package com.astra.astraotoshop.product.category.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryEntity implements Parcelable {

	@SerializedName("is_active")
	private boolean isActive;

	@SerializedName("level")
	private int level;

	@SerializedName("parent_id")
	private int parentId;

	@SerializedName("product_count")
	private int productCount;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("position")
	private int position;

	@SerializedName("children_data")
	private List<CategoryEntity> childrenData;

	protected CategoryEntity(Parcel in) {
		isActive = in.readByte() != 0;
		level = in.readInt();
		parentId = in.readInt();
		productCount = in.readInt();
		name = in.readString();
		id = in.readInt();
		position = in.readInt();
		childrenData = in.createTypedArrayList(CategoryEntity.CREATOR);
	}

    public CategoryEntity() {
    }

	public CategoryEntity(String name, int id, List<CategoryEntity> childrenData) {
		this.name = name;
		this.id = id;
		this.childrenData = childrenData;
	}

	public CategoryEntity(boolean isActive, int level, int parentId, int productCount, String name, int id, int position, List<CategoryEntity> childrenData) {
		this.isActive = isActive;
		this.level = level;
		this.parentId = parentId;
		this.productCount = productCount;
		this.name = name;
		this.id = id;
		this.position = position;
		this.childrenData = childrenData;
	}

	public static final Creator<CategoryEntity> CREATOR = new Creator<CategoryEntity>() {
		@Override
		public CategoryEntity createFromParcel(Parcel in) {
			return new CategoryEntity(in);
		}

		@Override
		public CategoryEntity[] newArray(int size) {
			return new CategoryEntity[size];
		}
	};

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setLevel(int level){
		this.level = level;
	}

	public int getLevel(){
		return level;
	}

	public void setParentId(int parentId){
		this.parentId = parentId;
	}

	public int getParentId(){
		return parentId;
	}

	public void setProductCount(int productCount){
		this.productCount = productCount;
	}

	public int getProductCount(){
		return productCount;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setPosition(int position){
		this.position = position;
	}

	public int getPosition(){
		return position;
	}

	public void setChildrenData(List<CategoryEntity> childrenData){
		this.childrenData = childrenData;
	}

	public List<CategoryEntity> getChildrenData(){
		return childrenData;
	}

	@Override
 	public String toString(){
		return 
			"CategoryEntity{" +
			"is_active = '" + isActive + '\'' + 
			",level = '" + level + '\'' + 
			",parent_id = '" + parentId + '\'' + 
			",product_count = '" + productCount + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",position = '" + position + '\'' + 
			",children_data = '" + childrenData + '\'' + 
			"}";
		}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
		dest.writeByte((byte) (isActive ? 1 : 0));
		dest.writeInt(level);
		dest.writeInt(parentId);
		dest.writeInt(productCount);
		dest.writeString(name);
		dest.writeInt(id);
		dest.writeInt(position);
		dest.writeTypedList(childrenData);
	}
}