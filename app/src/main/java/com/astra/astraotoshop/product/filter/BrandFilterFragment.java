package com.astra.astraotoshop.product.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckedTextView;
import android.widget.ListView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.filter.provider.BrandFilterEntity;
import com.astra.astraotoshop.product.filter.provider.BrandListAdapter;
import com.astra.astraotoshop.product.filter.provider.FilterContract;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 11/29/2017.
 */

public class BrandFilterFragment extends DialogFragment implements AdapterView.OnItemClickListener {

    @BindView(R.id.brands)
    ListView brandList;
    Unbinder unbinder;
    private BrandListAdapter adapter;
    private List<BrandFilterEntity> brands = new ArrayList<>();
    private List<String> selectedBrand = new ArrayList<>();
    private FilterContract.FilterView filterView;

    public static BrandFilterFragment newInstance() {

        Bundle args = new Bundle();
        BrandFilterFragment fragment = new BrandFilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
        adapter = new BrandListAdapter();
        adapter.setData(brands);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter_brand, container, false);
        unbinder = ButterKnife.bind(this, view);
        brandList.setAdapter(adapter);
        brandList.setOnItemClickListener(this);
        remap();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (((CheckedTextView) view).isChecked())
            selectedBrand.add(brands.get(position).getValue());
        else
            selectedBrand.remove(brands.get(position).getValue());

        brands.get(position).setSelected(((CheckedTextView) view).isChecked());
    }

    @OnClick(R.id.applyFilter)
    public void applyFilter() {
        filterView.onFilterApply(selectedBrand, brands);
        dismiss();
    }

    @OnClick(R.id.clearFilter)
    public void clearFilter() {
        selectedBrand = new ArrayList<>();
        brandList.setAdapter(adapter);
        clearCheck();
    }

    @OnClick(R.id.close)
    public void close() {
        dismiss();
    }

    private void clearCheck() {
        for (BrandFilterEntity entity : brands)
            entity.setSelected(false);
    }

    @OnTextChanged(R.id.search)
    public void onSearch(CharSequence charSequence, int i, int j, int k) {
        if (charSequence.length() == 0) {
            resetAll();
        } else if (charSequence.length() > 2) {
            brandFilter(charSequence.toString());
        }
    }

    private void resetAll() {
        adapter.setData(brands);
        remap(brands);
    }

    private void brandFilter(String value) {
        Observable.from(brands)
                .filter(brandFilterEntity -> {
                    if (brandFilterEntity.getLabel().length() < value.length()) return false;
                    return ((String) brandFilterEntity.getLabel().subSequence(0, value.length())).equalsIgnoreCase(value);
                })
                .toList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(brandFilterEntities -> {
                            adapter.setData(brandFilterEntities);
                            remap(brandFilterEntities);
                        },
                        error -> {
                        });
    }

    public void setFilterView(FilterContract.FilterView filterView) {
        this.filterView = filterView;
    }

    public void setBrands(List<BrandFilterEntity> brands) {
        this.brands = brands;
    }

    private void remap() {
        for (int i = 0; i < brands.size(); i++) {
            brandList.setItemChecked(i, brands.get(i).isSelected());
        }

        if (selectedBrand.size() < 1) {
            for (int i = 0; i < brands.size(); i++)
                if (brands.get(i).isSelected())
                    selectedBrand.add(brands.get(i).getValue());
        }
    }

    private void remap(List<BrandFilterEntity> brands) {
        for (int i = 0; i < brands.size(); i++) {
            brandList.setItemChecked(i, brands.get(i).isSelected());
        }
    }
}
