package com.astra.astraotoshop.product.catalog.provider;

import android.content.Context;
import android.support.annotation.Nullable;

import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/8/2018.
 */

public class ProductListInteractor extends BaseInteractor implements ProductContract.ProductListInteractor {

    ProductContract.ProductListPresenter presenter;

    public ProductListInteractor(@Nullable Context context, ProductContract.ProductListPresenter presenter) {
        super(context);
        this.presenter = presenter;
    }

    @Override
    public void requestProductList(NetworkHandler networkHandler, String storeCode, String catId, Map<String, String> filters) {
        getGeneralNetworkManager()
                .requestProductList(
                        networkHandler,
                        storeCode,
                        catId,
                        filters
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        productList ->
                            presenter.onProductListReceived(productList),
                        error ->
                            presenter.onFailedReceivingProduct(error));
    }
}
