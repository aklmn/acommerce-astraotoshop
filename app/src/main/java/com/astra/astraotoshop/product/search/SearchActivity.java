package com.astra.astraotoshop.product.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.SimpleListAdapter;
import com.astra.astraotoshop.product.catalog.product.ProductsActivity;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.product.search.provider.IndicesItem;
import com.astra.astraotoshop.product.search.provider.ItemsItem;
import com.astra.astraotoshop.product.search.provider.SearchPresenter;
import com.astra.astraotoshop.product.search.provider.Suggestion;
import com.astra.astraotoshop.product.search.provider.TextSuggestItem;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.FontIconic;
import com.astra.astraotoshop.utils.view.FontTextView;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;

public class SearchActivity extends BaseActivity implements SearchContract.SearchView, ListListener, TextView.OnEditorActionListener {

    @BindView(R.id.historyActionContainer)
    LinearLayout historyActionContainer;
    @BindView(R.id.searchHistory)
    RecyclerView searchHistory;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.productSuggestList)
    RecyclerView productSuggestList;
    @BindView(R.id.productCount)
    CustomFontFace productCount;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recentList)
    RecyclerView recentList;
    @BindView(R.id.deleteIcon)
    CustomFontFace deleteIcon;
    @BindView(R.id.recentTitle)
    TextView recentTitle;
    @BindView(R.id.suggest)
    TextView suggest;
    @BindView(R.id.recentTitleContainer)
    RelativeLayout recentTitleContainer;
    @BindView(R.id.labelPopularSearch)
    FontTextView labelPopularSearch;
    @BindView(R.id.iconClose)
    FontIconic iconClose;
    @BindView(R.id.allProduct)
    Button allProduct;
    private SearchContract.SearchPresenter presenter;
    private ProductSuggestAdapter productAdapter;
    private SearchView searchView;
    private PopularSuggestionAdapter adapter;
    private SimpleListAdapter recentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        actionBarSetup();
        presenter = new SearchPresenter(this, getStoreCode());

        adapter = new PopularSuggestionAdapter(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        recentAdapter = new SimpleListAdapter(this, R.layout.item_search_recent);

        productAdapter = new ProductSuggestAdapter(this, this);
        productSuggestList.setLayoutManager(layoutManager);
        productSuggestList.setAdapter(productAdapter);
        productSuggestList.setNestedScrollingEnabled(true);
        productSuggestList.setHasFixedSize(true);

        searchHistory.setLayoutManager(new LinearLayoutManager(this));
        searchHistory.setNestedScrollingEnabled(true);
        searchHistory.setHasFixedSize(true);
        searchHistory.setAdapter(adapter);
        searchSetup();
        search.setOnEditorActionListener(this);

        recentList.setLayoutManager(new LinearLayoutManager(this));
        recentList.setHasFixedSize(true);
        recentList.setAdapter(recentAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (presenter.loadRecent().size() > 0) {
            recentTitleContainer.setVisibility(View.VISIBLE);
        }
        recentAdapter.setStrings(presenter.loadRecent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_search, menu);
//        MenuItem menuItem = menu.findItem(search);
//        searchView = (SearchView) menuItem.getActionView();
//        if (searchView != null) {
//            searchView.setIconifiedByDefault(false);
//            searchView.setIconified(false);
//            searchView.clearFocus();
//            searchSetup();
//        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.search) {
            search.setText("");
            return false;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showSuggest(Suggestion suggestion) {
        progressBar.setVisibility(View.GONE);
        recentTitle.setVisibility(View.VISIBLE);
        labelPopularSearch.setVisibility(View.VISIBLE);
        adapter.setTextSuggestItems(suggestion.getItems().getTextSuggest());
        for (IndicesItem item : suggestion.getItems().getProdSuggest().getIndices()) {
            if (item.getIdentifier().contains("catalog_product")) {
                productAdapter.setSuggestItem(item.getItems());
                if (item.getTotalItems() > 0) {
                    productCount.setVisibility(View.VISIBLE);
                    productCount.setText(String.format("Produk (%d)", item.getTotalItems()));
                    allProduct.setText(String.format("TAMPILKAN SEMUA %d HASIL", item.getTotalItems()));
                } else {
                    productCount.setText("Produk (0)");
                }
                break;
            }
        }
    }

    @Override
    public void showError() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        if (viewId == 0) {
            ItemsItem item = (ItemsItem) data;
            presenter.addRecent(item.getName());
            Intent intent = getIntent(this, ProductDetailActivity.class);
            intent.putExtra("id", item.getCart().getParams().getData().getProduct());
            startActivity(intent);
        } else if (viewId == android.R.id.text1) {
            TextSuggestItem suggestItem = (TextSuggestItem) data;
            if (data != null) {
                searchProduct(suggestItem.getTitle());
            }
        } else if (viewId == 1) {
            String query = (String) data;
            if (query != null) {
                search.setText(query);
            }
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (v.getText().length() > 1) {
            searchProduct(v.getText().toString());
            return true;
        }
        return false;
    }

    @OnClick(R.id.deleteIcon)
    public void onRecentDelete() {
        presenter.deleteRecent();
        recentAdapter.setStrings(presenter.loadRecent());
    }

    @OnClick(R.id.iconClose)
    public void onClearClick() {
        search.setText("");
    }

    @OnEditorAction(R.id.search)
    public boolean onActionSearch(TextView textView, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            exeSearch(textView.getText().toString());
            return true;
        }
        return false;
    }

    private void searchSetup() {
        Observable.unsafeCreate((Observable.OnSubscribe<String>) subscriber -> {
            search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() > 1) {
                        subscriber.onNext(s.toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        })
                .filter(s -> s.length() > 1)
                .distinctUntilChanged()
                .debounce(900, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    if (search.getText().toString().length() > 1) {
                        exeSearch(s);
                    }
                });
    }

    private void exeSearch(String s) {
        presenter.search(s);
        progressBar.setVisibility(View.VISIBLE);
        recentAdapter.setStrings(presenter.loadRecent());
    }

    private void searchProduct(String query) {
        Intent intent = getIntent(this, ProductsActivity.class);
        intent.putExtra("name", query);
        intent.putExtra("searchQuery", query);
        startActivity(intent);
    }

    @OnClick(R.id.allProduct)
    public void onViewAllClicked() {
        searchProduct(search.getText().toString());
    }
}
