package com.astra.astraotoshop.product.product;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.astra.astraotoshop.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 11/20/2017.
 */

public class ProductGalleryAdapter extends RecyclerView.Adapter<ProductGalleryAdapter.ProductGalleryViewHolder> {

    private List<String> images;
    private Context context;

    public ProductGalleryAdapter(Context context) {
        this.context = context;
        images = new ArrayList<>();
    }

    @Override
    public ProductGalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductGalleryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_gallery, parent, false));
    }

    @Override
    public void onBindViewHolder(ProductGalleryViewHolder holder, int position) {
        Glide.with(context).load(images.get(position)).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void setImages(List<String> images) {
        this.images = images;
        notifyDataSetChanged();
    }

    class ProductGalleryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        public ProductGalleryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
