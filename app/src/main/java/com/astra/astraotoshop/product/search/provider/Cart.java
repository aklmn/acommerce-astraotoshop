package com.astra.astraotoshop.product.search.provider;

import com.google.gson.annotations.SerializedName;

public class Cart {

    @SerializedName("visible")
    private boolean visible;

    @SerializedName("label")
    private String label;

    @SerializedName("params")
    private Params params;

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    public Params getParams() {
        return params;
    }

    @Override
    public String toString() {
        return
                "Cart{" +
                        "visible = '" + visible + '\'' +
                        ",label = '" + label + '\'' +
                        ",params = '" + params + '\'' +
                        "}";
    }
}