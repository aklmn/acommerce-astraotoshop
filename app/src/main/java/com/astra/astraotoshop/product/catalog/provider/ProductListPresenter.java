package com.astra.astraotoshop.product.catalog.provider;

import com.astra.astraotoshop.product.catalog.product.FilterStateModel;
import com.astra.astraotoshop.product.filter.provider.BrandFilterEntity;
import com.astra.astraotoshop.utils.base.BasePresenter;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Henra Setia Nugraha on 1/8/2018.
 */

public class ProductListPresenter extends BasePresenter implements ProductContract.ProductListPresenter {

    private ProductContract.ProductListInteractor interactor;
    private ProductContract.ProductList view;
    NetworkHandler networkHandler = new NetworkHandler();
    private List<BrandFilterEntity> brands;
    private FilterStateModel stateModel;
    private List<BrandEntity> productBrand;

    public ProductListPresenter(ProductContract.ProductList view) {
        this.view = view;
        interactor = new ProductListInteractor(null, this);
        brands = new ArrayList<>();
    }

    public ProductListPresenter(ProductContract.ProductList view, String query) {
        this.view = view;
        interactor = new ProductSearchInteractor(this, query);
        brands = new ArrayList<>();
    }

    @Override
    public void updateFilter(String key, String value) {
        super.updateFilter(key, value);
    }

    @Override
    public void deleteFilterItem(String key) {
        super.deleteFilterItem(key);
    }

    @Override
    public void resetFilter() {
        getDefaultFilters();
    }

    @Override
    public Map<String, String> getFilters() {
        return getQueries();
    }

    @Override
    public void setFilters(Map<String, String> filters) {
        if (filters != null) {
            filters.put("page", "1");
            super.setAddMore(false);
            super.setQueries(filters);
        }
    }

    @Override
    public Map<String, String> getQueries() {
        return super.getQueries();
    }

    @Override
    public void setQueries(Map<String, String> queries) {
        queries.put("page", "1");
        super.setQueries(queries);
    }

    @Override
    public boolean isAddMore() {
        return super.isAddMore();
    }

    @Override
    public void requestProductList(String storeCode, String catId) {
        if (isAvailableToLoad()) {
            interactor.requestProductList(
                    networkHandler,
                    storeCode,
                    catId,
                    getQueries());
        }
    }

    @Override
    public void onProductListReceived(ProductListEntity productList) {
        if (productList != null) {
            productBrand = productList.getBrands();
            setTotalItem(productList.getTotalCount());
            addCurrentTotal(productList.getItems().size());
            increasePage();
            view.setProductList(productList.getItems(), isAddMore(), productList.getTotalCount());
            view.setFilterData(productList.getBrands(), productList.getPrice());
        }
    }

    @Override
    public void onFailedReceivingProduct(Throwable error) {
        view.showError();
    }

    @Override
    public void setFilter(Map<String, String> filters) {
        super.setQueries(filters);
    }

    @Override
    public void setFilterState(FilterStateModel stateModel) {
        this.stateModel = stateModel;
        StringBuilder brand = new StringBuilder();
        if (stateModel.getBrandIndex().size() > 0) {
            for (int i : stateModel.getBrandIndex()) {
                brand.append(productBrand.get(i).getValue()).append(",");
            }
            updateFilter("searchCriteria[filter][brand]", brand.toString());
        }
    }

    @Override
    public FilterStateModel getStateModel() {
        if (stateModel == null) stateModel = new FilterStateModel();
        return stateModel;
    }

    @Override
    public void setResetState() {
        stateModel = new FilterStateModel();
        deleteFilterItem("searchCriteria[filter][brand]");
    }
}
