package com.astra.astraotoshop.product.category.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.product.ProductsActivity;
import com.astra.astraotoshop.product.catalog.productservice.ProductServiceAdapter;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.category.provider.CatPresenter;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.pref.Pref;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainCategoryActivity extends BaseActivity implements ProductServiceAdapter.ItemClickListener, ListListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.catList)
    RecyclerView catList;
    private CategoryEntity categories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_category);
        ButterKnife.bind(this);
        actionBarSetup();

        categories = new CategoryEntity();
        try {
            String storeCode = getResources().getStringArray(R.array.storeCode)[getState()];
            String category = Pref.getPreference().getString("cat-" + storeCode);
            categories = new Gson().fromJson(category, CategoryEntity.class);
        } catch (JsonSyntaxException e) {
            e.printStackTrace();
        }

        if (categories != null) {
            if (categories.getName() != null) {
                MainCategoryAdapter adapter = new MainCategoryAdapter(categories.getChildrenData(), this, this, this);
                LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                catList.setLayoutManager(layoutManager);
                catList.setAdapter(adapter);
            }
        }else new CatPresenter(Pref.getPreference()).requestCategories(getStoreCode());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return true;
    }

    @Override
    public void onItemClick() {

    }

    @Override
    public void onItemClick(int viewId) {

    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        Intent intent = new Intent(this, CategoryActivity.class);
        intent.putExtra(STATE, getState());
        intent.putExtra("catData", categories.getChildrenData().get(position));
        intent.putExtra("catTitle", categories.getChildrenData().get(position).getName());
        startActivityForResult(intent, 99);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Intent intent = new Intent(this, ProductsActivity.class);
            intent.putExtra(STATE, getState());
            intent.putExtra("catId", data.getIntExtra("catId", 0));
            intent.putExtra("name", data.getStringExtra("name"));
            startActivity(intent);
            finish();
        }
    }
}
