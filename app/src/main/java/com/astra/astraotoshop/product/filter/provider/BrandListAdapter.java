package com.astra.astraotoshop.product.filter.provider;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckedTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/14/2018.
 */

public class BrandListAdapter extends BaseAdapter {

    private List<BrandFilterEntity> brands = new ArrayList<>();
    private boolean isChecked = false;

    public BrandListAdapter(List<BrandFilterEntity> brands) {
        this.brands = brands;
    }

    public BrandListAdapter() {
    }

    @Override
    public int getCount() {
        return brands.size();
    }

    @Override
    public Object getItem(int position) {
        return brands.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setData(List<BrandFilterEntity> brands) {
        this.brands = brands;
        notifyDataSetChanged();
    }

    public void setChecked(boolean checked) {
        isChecked = !isChecked;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(android.R.layout.simple_list_item_multiple_choice, parent, false);
        }

        CheckedTextView textView = convertView.findViewById(android.R.id.text1);
        textView.setText(brands.get(position).getLabel());

        return convertView;
    }
}
