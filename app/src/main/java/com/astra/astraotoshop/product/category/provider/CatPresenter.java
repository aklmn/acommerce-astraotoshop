package com.astra.astraotoshop.product.category.provider;

import com.astra.astraotoshop.utils.network.handler.NetworkHandler;
import com.astra.astraotoshop.utils.pref.Pref;
import com.astra.astraotoshop.utils.pref.PrefHelper;
import com.google.gson.Gson;

/**
 * Created by Henra Setia Nugraha on 1/5/2018.
 */

public class CatPresenter implements CategoryContract.CatPresenter {

    private final PrefHelper preferences;
    private final CategoryContract.CatInteractor interactor;

    public CatPresenter(PrefHelper preferences) {
        this.preferences = preferences;
        interactor = new CatInteractor(this);
    }

    @Override
    public void requestCategories(String storeCode) {
        interactor.requestCategory(new NetworkHandler(), storeCode);
    }

    @Override
    public void onCategoryReceived(CategoryEntity categoryEntity, String storeCode) {
        Pref.getPreference().putInt("parent" + storeCode, categoryEntity.getId());
        try {
            preferences.putString("cat-" + storeCode, new Gson().toJson(categoryEntity));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestCatFailed() {

    }
}
