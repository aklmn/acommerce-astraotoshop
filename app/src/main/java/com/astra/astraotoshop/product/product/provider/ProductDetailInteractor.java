package com.astra.astraotoshop.product.product.provider;

import android.content.Context;

import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/10/2018.
 */

public class ProductDetailInteractor extends BaseInteractor implements ProductDetailContract.ProductDetailInteractor {

    ProductDetailContract.ProductDetailPresenter presenter;

    public ProductDetailInteractor(Context context, ProductDetailContract.ProductDetailPresenter presenter) {
        super(context);
        this.presenter = presenter;
    }

    @Override
    public void requestProduct(NetworkHandler networkHandler, String storeCode, String id) {
        getGeneralNetworkManager()
                .requestProduct(networkHandler, storeCode, id)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        product -> presenter.onProductReceived(product),
                        error -> presenter.onFailedReceivingProduct(error)
                );
    }
}
