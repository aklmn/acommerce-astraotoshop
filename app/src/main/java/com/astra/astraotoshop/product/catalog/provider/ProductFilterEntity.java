package com.astra.astraotoshop.product.catalog.provider;

/**
 * Created by Henra Setia Nugraha on 1/8/2018.
 */

public class ProductFilterEntity {
    String order;
    String dir;
    String limit;
    String page;
    String priceFrom;
    String priceTo;
    String keyword;
    String brand;

    public ProductFilterEntity(String order, String dir, String limit, String page, String priceFrom, String priceTo, String keyword, String brand) {
        this.order = order;
        this.dir = dir;
        this.limit = limit;
        this.page = page;
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
        this.keyword = keyword;
        this.brand = brand;
    }

    public ProductFilterEntity() {
        order = "position";
        dir = "desc";
        limit = "10";
        page = "1";
        priceFrom = "";
        priceTo = "";
        keyword = "";
        brand = "";
    }
}
