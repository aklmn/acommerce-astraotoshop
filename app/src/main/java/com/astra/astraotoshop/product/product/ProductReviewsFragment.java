package com.astra.astraotoshop.product.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.review.provider.ReviewEntity;
import com.astra.astraotoshop.user.review.ProductReviewAdapter;
import com.astra.astraotoshop.utils.view.CustomFontFace;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/20/2017.
 */

public class ProductReviewsFragment extends Fragment {

    @BindView(R.id.reviewList)
    RecyclerView reviewList;
    @BindView(R.id.count)
    TextView count;
    @BindView(R.id.productName)
    CustomFontFace productName;
    @BindView(R.id.sku)
    CustomFontFace sku;
    @BindView(R.id.rate)
    RatingBar rate;
    private ReviewEntity reviewEntity;
    Unbinder unbinder;
    private ProductReviewAdapter adapter;

    public static ProductReviewsFragment newInstance() {

        Bundle args = new Bundle();

        ProductReviewsFragment fragment = new ProductReviewsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_reviews, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ProductReviewAdapter("product");
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        reviewList.setLayoutManager(layoutManager);
        reviewList.setAdapter(adapter);
    }

    public void setReviewEntity(ReviewEntity reviewEntity, String name, String mSku) {
        if (reviewEntity != null) {
            this.reviewEntity = reviewEntity;
            adapter.setReviews(reviewEntity.getItems());
            count.setText(adapter.getItemCount() + " Ulasan");
            rate.setRating((float) reviewEntity.getRating());
        }
        productName.setText(name);
        sku.setText(mSku);
    }

    public void setReviewEntity(String name, String mSku) {
        setReviewEntity(null, name, mSku);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
