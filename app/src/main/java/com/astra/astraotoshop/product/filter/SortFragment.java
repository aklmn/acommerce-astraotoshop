package com.astra.astraotoshop.product.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.astra.astraotoshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 2/1/2018.
 */

public class SortFragment extends DialogFragment implements AdapterView.OnItemClickListener {

    @BindView(R.id.list)
    ListView list;
    Unbinder unbinder;

    private String[] sortValue;
    private String[] sortKey;
    private String[] sortDir;
    private int positionState;

    public static SortFragment newInstance(int positionState) {

        Bundle args = new Bundle();

        SortFragment fragment = new SortFragment();
        fragment.positionState=positionState;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);

        sortKey = getResources().getStringArray(R.array.sortKey);
        sortValue = getResources().getStringArray(R.array.sortValue);
        sortDir = getResources().getStringArray(R.array.sortDir);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sort, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_single_choice, sortKey);
        list.setAdapter(adapter);
        list.setOnItemClickListener(this);

        try {
            list.setItemChecked(positionState,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Intent intent = new Intent();
//        intent.putExtra("sort", sortValue[position]);
//        intent.putExtra("dir", sortDir[position]);

        ((FilterFragment) getTargetFragment()).onSortSelected(
                sortValue[position],
                sortDir[position],
                position);

//        getTargetFragment().onActivityResult(3, Activity.RESULT_OK, intent);
        dismiss();
    }

    @OnClick(R.id.icon)
    public void onViewClicked() {
        dismiss();
    }

    public void setPositionState(int positionState) {
        this.positionState = positionState;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
