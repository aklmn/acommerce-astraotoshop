package com.astra.astraotoshop.product.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.product.provider.ProductEntity;
import com.astra.astraotoshop.utils.view.CustomFontFace;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/20/2017.
 */

public class ProductDescFragment extends Fragment {

    @BindView(R.id.productName)
    CustomFontFace productName;
    @BindView(R.id.sku)
    CustomFontFace sku;
    @BindView(R.id.desc)
    TextView desc;
    @BindView(R.id.marketName)
    TextView marketName;
    @BindView(R.id.oem)
    TextView oem;
    @BindView(R.id.applicable)
    TextView applicable;
    Unbinder unbinder;

    public static ProductDescFragment newInstance() {

        Bundle args = new Bundle();

        ProductDescFragment fragment = new ProductDescFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_desc, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setData(ProductEntity product) {
        if (product != null) {
            productName.setText(product.getName());
            sku.setText(product.getSku());
            desc.setText(Html.fromHtml(
                    product.getDescription() != null ? product.getDescription() : "-"));
            marketName.setText(product.getNamaPasar() != null ? product.getNamaPasar() : "-");
            oem.setText(product.getOemNumber() != null ? product.getOemNumber() : "-");
            applicable.setText(product.getApplicableForMultiple() != null ? product.getApplicableForMultiple() : "-");
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
