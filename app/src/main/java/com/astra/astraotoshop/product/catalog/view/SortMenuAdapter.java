package com.astra.astraotoshop.product.catalog.view;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 1/14/2018.
 */

public class SortMenuAdapter extends RecyclerView.Adapter<SortMenuAdapter.SortMenuViewHolder> {

    private List<KeyValueEntity> data;
    private ListListener listener;
    private String key = "";

    public SortMenuAdapter(ListListener listener) {
        this.listener = listener;
        data = new ArrayList<>();
    }

    @Override
    public SortMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SortMenuViewHolder(LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false));
    }

    @Override
    public void onBindViewHolder(SortMenuViewHolder holder, int position) {
        holder.textView.setText(data.get(position).getValue());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<KeyValueEntity> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setItemSelected(String key) {
        this.key = key;
        notifyDataSetChanged();
    }

    class SortMenuViewHolder extends ViewHolder {
        @BindView(android.R.id.text1)
        TextView textView;

        SortMenuViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14f);
        }

        @OnClick(android.R.id.text1)
        public void onItemClick() {
            try {
                listener.onItemClick(android.R.id.text1, getAdapterPosition(), data.get(getAdapterPosition()).getKey());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
