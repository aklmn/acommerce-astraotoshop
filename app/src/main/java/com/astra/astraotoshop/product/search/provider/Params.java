package com.astra.astraotoshop.product.search.provider;

import com.google.gson.annotations.SerializedName;

public class Params {

    @SerializedName("data")
    private Data data;

    @SerializedName("action")
    private String action;

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    @Override
    public String toString() {
        return
                "Params{" +
                        "data = '" + data + '\'' +
                        ",action = '" + action + '\'' +
                        "}";
    }
}