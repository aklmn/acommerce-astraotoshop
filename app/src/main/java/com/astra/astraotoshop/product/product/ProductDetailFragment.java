package com.astra.astraotoshop.product.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.appointment.AppointmentActivity;
import com.astra.astraotoshop.order.appointment.AppointmentAdapter_old;
import com.astra.astraotoshop.order.cart.CartAppointmentDetailAdapter;
import com.astra.astraotoshop.order.cart.provider.AppointmentsItemCart;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartPresenter;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.product.provider.ProductEntity;
import com.astra.astraotoshop.product.product.view.GroceryAdapter;
import com.astra.astraotoshop.user.profile.ProfileFragment;
import com.astra.astraotoshop.user.wishlist.WishlistContract;
import com.astra.astraotoshop.user.wishlist.provider.WishlistPresenter;
import com.astra.astraotoshop.utils.base.BaseFragment;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/20/2017.
 */

public class ProductDetailFragment extends BaseFragment implements AppointmentAdapter_old.AppointmentListener, CartContract.ExternalCart, WishlistContract.AddWishlist, ListListener {

    Unbinder unbinder;
    @BindView(R.id.groceries)
    RecyclerView groceries;
    @BindView(R.id.productName)
    CustomFontFace productName;
    @BindView(R.id.productRating)
    RatingBar productRating;
    @BindView(R.id.productReviews)
    CustomFontFace productReviews;
    @BindView(R.id.productSKU)
    CustomFontFace productSKU;
    @BindView(R.id.firstPrice)
    CustomFontFace firstPrice;
    @BindView(R.id.secondPrice)
    CustomFontFace secondPrice;
    @BindView(R.id.productGallery)
    RecyclerView productGallery;
    @BindView(R.id.indicator)
    IndefinitePagerIndicator indicator;
    @BindView(R.id.discount)
    CustomFontFace discount;
    @BindView(R.id.appointmentList)
    RecyclerView appointmentList;
    @BindView(R.id.scheduleContainer)
    LinearLayout scheduleContainer;
//    @BindView(R.id.buy)
//    Button buy;

    private LinearLayoutManager appointmentLayoutManager;
    private GroceryAdapter groceryAdapter;
    private ProductGalleryAdapter adapter;
    private CartContract.CartPresenter cartPresenter;
    private WishlistContract.WishlistPresenter presenter;
    private String productId;
    private CartAppointmentDetailAdapter appointmentAdapter;
    private String urlProduct;

    public static ProductDetailFragment newInstance(int state) {

        Bundle args = new Bundle();
        args.putInt(STATE, state);

        ProductDetailFragment fragment = new ProductDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product_detail1, container, false);
        unbinder = ButterKnife.bind(this, view);
        setState(getArguments().getInt(STATE));
        presenter = new WishlistPresenter(null, this, getStoreCode());
        cartPresenter = new CartPresenter(null, (ProductDetailActivity) getActivity(),
                getResources().getStringArray(R.array.storeCode)[getState()]);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        System.out.println("frag");
        if (getState() == getResources().getInteger(R.integer.state_product_service))
            setupProductService();
        else if (getState() == getResources().getInteger(R.integer.state_product)) {
            setupProduct();
        }

        groceryAdapter = new GroceryAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        groceries.setLayoutManager(layoutManager);
        groceries.setAdapter(groceryAdapter);

        adapter = new ProductGalleryAdapter(getContext());
        LinearLayoutManager galleryLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        productGallery.setLayoutManager(galleryLayoutManager);
        productGallery.setAdapter(adapter);
        indicator.attachToRecyclerView(productGallery);

        SnapHelper snapHelperStart = new GravitySnapHelper(Gravity.START);
        snapHelperStart.attachToRecyclerView(productGallery);

        appointmentAdapter = new CartAppointmentDetailAdapter(true, this);
        appointmentList.setLayoutManager(new LinearLayoutManager(getContext()));
        appointmentList.setAdapter(appointmentAdapter);
    }

    private void setupProductService() {
        appointmentLayoutManager = new LinearLayoutManager(getContext());
        AppointmentAdapter_old appointmentAdapterOld = new AppointmentAdapter_old(this);
//        appointmentContainer.setVisibility(View.VISIBLE);
//        appointmentList.setLayoutManager(appointmentLayoutManager);
//        appointmentList.setAdapter(appointmentAdapterOld);
    }

    private void setupProduct() {
//        countContainer.setVisibility(View.VISIBLE);
    }

//    @OnClick(R.id.order)
//    public void order() {
//        CartFragment cartFragment = CartFragment.newInstance(getResources().getInteger(R.integer.state_product));
//        cartFragment.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
//        cartFragment.show(getActivity().getSupportFragmentManager(), "Cart");
//        Intent intent = new Intent(getActivity(), CartActivity.class);
//        intent.putExtra(STATE, getState());
//        startActivity(intent);
//    }

    @Override
    public void onItemClick() {
        startActivity(new Intent(getContext(), AppointmentActivity.class));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE, getState());
    }

    @Override
    public void onItemAdded() {
        Toast.makeText(getContext(), "Produk telah masuk ke keranjang", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemFailedToAdd(String message) {
        Toast.makeText(getContext(), "Gagal menambah produk ke keranjang", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemUpdated() {

    }

    @Override
    public void onItemFailedToUpdate(String message) {

    }

    @Override
    public void onItemDeleted() {

    }

    @Override
    public void onItemFailedToDelete() {

    }

    @Override
    public void showSuccessMessage() {
        Toast.makeText(getContext(), "Sukses memfavoritkan produk", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFailedMessage() {
        Toast.makeText(getContext(), "Produk gagal ditambahkan", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.rateContainer)
    public void onRateClicked() {
        ((ProductDetailActivity) getActivity()).changePage(2);
    }

//    @OnClick(R.id.buy)
//    public void buy() {
//        cartPresenter.addToCart(productSKU.getText().toString(), 1);
//    }

    @OnClick({R.id.addWishlist, R.id.addWishlistIcon})
    public void addToWishlist() {
        if (isLogin()) {
            try {
                presenter.addWishlist(Integer.parseInt(productId));
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        } else {
            FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
            Fragment fragment = getActivity().getSupportFragmentManager().findFragmentByTag("account");
            if (fragment != null) transaction.remove(fragment);
            transaction.commit();

            ProfileFragment profileFragment = ProfileFragment.newInstance(getState());
            profileFragment.show(getActivity().getSupportFragmentManager(), "account");
        }
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        if (viewId == R.id.deleteIcon) {
            if (getActivity() instanceof ProductDetailActivity) {
                ((ProductDetailActivity) getActivity()).onAppointmentItemDelete(position);
                try {
                    if (((Integer) data) <= 0)
                        scheduleContainer.setVisibility(View.GONE);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (viewId == R.id.editIcon) {
            if (data instanceof AppointmentsItemCart) {
                AppointmentsItemCart item = (AppointmentsItemCart) data;
                openAppointment(item, position);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.addAppointment)
    public void onAddAppointmentClicked() {
        openAppointment(null, -1);
    }

    public void setProductData(ProductEntity productData) {
//        if (Integer.valueOf(StringFormat.clearCurrencyFormat(productData.getQty())) == 0) {
//            buy.setEnabled(false);
//            buy.setText("Stok tidak tersedia");
//        }
        if (productData != null) {
            urlProduct = productData.getUrlProduct();
            productId = productData.getEntityId();
            if (productData.getTierPrice() != null) {
                groceryAdapter.addData(productData.getTierPrice());
            }
            productName.setText(productData.getName());
            productSKU.setText(productData.getSku());

            StringFormat.applyCurrencyFormat(firstPrice, productData.getPrice());

            if (productData.getSpecialPrice() != null) {
                StringFormat.applyCurrencyFormat(firstPrice, productData.getSpecialPrice());
                secondPrice.setVisibility(View.VISIBLE);
                StringFormat.applyCurrencyFormat(secondPrice, productData.getPrice());
                discount.setText(discountCalculate(productData.getPrice(), productData.getSpecialPrice()));
                discount.setVisibility(View.VISIBLE);

            }
            adapter.setImages(productData.getImage());
        }
    }

    public void setAppointmentData(List<AppointmentsItemCart> extensionAttributes) {
        if (extensionAttributes.size() > 0) {
            appointmentAdapter.setItem(extensionAttributes.get(0));
            appointmentList.setAdapter(appointmentAdapter);
            scheduleContainer.setVisibility(View.VISIBLE);
        } else {
            scheduleContainer.setVisibility(View.GONE);
            appointmentAdapter.setItem(new AppointmentsItemCart());
        }
    }

    public void setAppointmentByPosition(int position, AppointmentsItemCart item) {
        appointmentAdapter.updateItem(position, item);
    }

    public void setRating(String name, String sku, int rating, int count) {
        productRating.setRating(rating);
        productReviews.setText(String.valueOf(count) + " Ulasan");
    }

    private String discountCalculate(String price, String specialPrice) {
        Double dPrice = Double.parseDouble(price);
        Double dSpecialPrice = Double.parseDouble(specialPrice);

        double cutPrice = (dPrice - dSpecialPrice) / dPrice;
        int discount = (int) Math.ceil(cutPrice * 100);
        return String.valueOf(discount + "%");
    }

    private void openAppointment(AppointmentsItemCart appointment, int position) {
        ((ProductDetailActivity) getActivity()).addProductAppointment(appointment, position);
    }

    @OnClick({R.id.shareIcon, R.id.shareLabel})
    public void onShareClicked() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, urlProduct.replace("https://", "http://"));
        Intent chooser = Intent.createChooser(intent, "Bagikan Ke Teman");
        startActivity(chooser);
    }
}
