package com.astra.astraotoshop.product.catalog.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartPresenter;
import com.astra.astraotoshop.product.catalog.provider.BrandEntity;
import com.astra.astraotoshop.product.catalog.provider.PriceEntity;
import com.astra.astraotoshop.product.catalog.provider.ProductContract;
import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;
import com.astra.astraotoshop.product.catalog.provider.ProductListPresenter;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.utils.base.BaseActionActivity;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductsActivity extends BaseActionActivity implements SwipeRefreshLayout.OnRefreshListener, ProductContract.ProductList, ListListener, CartContract.ExternalCart, DrawerLayout.DrawerListener {


    @BindView(R.id.productList)
    RecyclerView productList;
    @BindView(R.id.navigation_view_container)
    NavigationView navigationViewContainer;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.itemCount)
    TextView itemCount;
    private ProductAdapterRevamp adapter;
    private LinearLayoutManager layoutManager;
    private boolean isLoad;
    private ProductContract.ProductListPresenter presenter;
    private CartContract.CartPresenter cartPresenter;
    private float prev;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plp_revamp);
        ButterKnife.bind(this);
        drawerLayout.addDrawerListener(this);

        String title = getIntent().getStringExtra("name");
        if (title != null)
            super.actionBarSetup(title);
        else
            super.actionBarSetup();

        adapter = new ProductAdapterRevamp(this);
        layoutManager = new LinearLayoutManager(this);
        productList.setHasFixedSize(true);
        productList.setLayoutManager(layoutManager);
        productList.setAdapter(adapter);
        productList.addOnScrollListener(onScrollListener);

        if (getIntent().hasExtra("searchQuery"))
            presenter = new ProductListPresenter(this, getIntent().getStringExtra("searchQuery"));
        else
            presenter = new ProductListPresenter(this);
        cartPresenter = new CartPresenter(null, this, "product");


        requestProducts();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cart, menu);
        registerCustomMenu(menu, R.id.action_cart);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart: {
                openCart();
                break;
            }
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return true;
    }

    @Override
    public void onRefresh() {

    }

    @Override
    public void onItemAdded() {
        setCartBadgeVisibility(true);
        Toast.makeText(this, "Produk di tambahkan ke keranjang", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemFailedToAdd(String message) {

    }

    @Override
    public void onItemUpdated() {

    }

    @Override
    public void onItemFailedToUpdate(String message) {

    }

    @Override
    public void onItemDeleted() {

    }

    @Override
    public void onItemFailedToDelete() {

    }

    @Override
    public void setProductList(List<ProductItemEntity> productList, boolean isAddMore, int totalCount) {
        adapter.addProducts(productList, isAddMore);
        if (totalCount > 0)
            itemCount.setText(totalCount + " produk ditemukan");
        else itemCount.setText("Produk tidak ditemukan"
        );
        isLoad = false;
    }

    @Override
    public void onFilterApply(Map<String, String> presenter) {

    }

    @Override
    public void setFilterData(List<BrandEntity> brands, PriceEntity price) {
        FilterFragment frag = (FilterFragment) getSupportFragmentManager().findFragmentById(R.id.filterFragment);
        if (frag != null) {
            frag.setBrands(brands);
            frag.setPriceRange(price);
        }
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        if (viewId == 0) {
            Intent intent = getIntent(this, ProductDetailActivity.class);
            intent.putExtra("id", ((ProductItemEntity) data).getEntityId());
            startActivity(intent);
        } else if (viewId == R.id.btnBuy) {
            if (isLogin()) {
                cartPresenter.addToCart(((ProductItemEntity) data).getSku(), 1);
            } else {
                showAccount();
            }
        }
    }

    @OnClick({R.id.filter, R.id.filterIcon})
    public void onFilterClicked() {
        drawerLayout.openDrawer(Gravity.END);
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (layoutManager.findFirstVisibleItemPosition() + recyclerView.getChildCount() == layoutManager.getItemCount()) {
                if (!isLoad) {
                    requestProducts();
                    isLoad = true;
                    showLoading();
                }
            }
        }
    };

    private void showLoading() {

    }

    private void requestProducts() {
        presenter.requestProductList(
                getStoreCode(),
                String.valueOf(getIntent().getIntExtra("catId", 0))
        );
    }

    public void saveState(FilterStateModel stateModel) {
        presenter.setFilterState(stateModel);
        drawerLayout.closeDrawer(Gravity.END);
        requestProducts();
    }

    public void setReset() {
        presenter.setResetState();
        drawerLayout.closeDrawer(Gravity.END);
    }

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
        if (slideOffset > 0.1 && slideOffset < 0.2 && prev < slideOffset) {
            try {
                ((FilterFragment) getSupportFragmentManager().findFragmentById(R.id.filterFragment)).setStateModel(presenter.getStateModel());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        prev = slideOffset;
    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {
//        try {
//            ((FilterFragment) getSupportFragmentManager().findFragmentById(R.id.filterFragment)).setStateModel(presenter.getStateModel());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {
        Log.i("TAG", "onDrawerClosed:");
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }
}
