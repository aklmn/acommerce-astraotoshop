package com.astra.astraotoshop.product.catalog.product;

public interface filterCallback {
    void onApply();
    void onReset();
}
