package com.astra.astraotoshop.product.product.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class TierPriceEntity implements Parcelable{

	@SerializedName("price_qty")
	private String priceQty;

	@SerializedName("price")
	private String price;

	@SerializedName("save")
	private String save;

	protected TierPriceEntity(Parcel in) {
		priceQty = in.readString();
		price = in.readString();
		save = in.readString();
	}

	public static final Creator<TierPriceEntity> CREATOR = new Creator<TierPriceEntity>() {
		@Override
		public TierPriceEntity createFromParcel(Parcel in) {
			return new TierPriceEntity(in);
		}

		@Override
		public TierPriceEntity[] newArray(int size) {
			return new TierPriceEntity[size];
		}
	};

	public void setPriceQty(String priceQty){
		this.priceQty = priceQty;
	}

	public String getPriceQty(){
		return priceQty;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setSave(String save){
		this.save = save;
	}

	public String getSave(){
		return save;
	}

	@Override
 	public String toString(){
		return 
			"TierPriceEntity{" + 
			"price_qty = '" + priceQty + '\'' + 
			",price = '" + price + '\'' + 
			",save = '" + save + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(priceQty);
		dest.writeString(price);
		dest.writeString(save);
	}
}