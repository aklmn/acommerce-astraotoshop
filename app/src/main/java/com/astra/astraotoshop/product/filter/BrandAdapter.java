package com.astra.astraotoshop.product.filter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.filter.provider.BrandFilterEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 11/30/2017.
 */

public class BrandAdapter extends RecyclerView.Adapter<BrandAdapter.BrandViewHolder> {

    private ListListener listener;
    private List<BrandFilterEntity> brandFilterEntities;

    public BrandAdapter(ListListener listener) {
        this.listener = listener;
        brandFilterEntities = new ArrayList<>();
    }

    @Override
    public BrandViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BrandViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chips, parent, false));
    }

    @Override
    public void onBindViewHolder(BrandViewHolder holder, int position) {
        if (brandFilterEntities.get(position).isSelected())
            holder.cardView.setVisibility(View.VISIBLE);
        else
            holder.cardView.setVisibility(View.GONE);
        holder.textView.setText(brandFilterEntities.get(position).getLabel());
    }

    @Override
    public int getItemCount() {
        return brandFilterEntities.size();
    }

    public void setBrandFilterEntities(List<BrandFilterEntity> brandFilterEntities) {
        this.brandFilterEntities = brandFilterEntities;
        notifyDataSetChanged();
    }

    class BrandViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView textView;
        @BindView(R.id.container)
        LinearLayout cardView;

        BrandViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.title)
        public void title() {
            try {
                listener.onItemClick(R.id.container, getAdapterPosition(), brandFilterEntities.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.delete)
        public void delete() {
            try {
                listener.onItemClick(R.id.delete, getAdapterPosition(), brandFilterEntities.get(getAdapterPosition()));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
