package com.astra.astraotoshop.product.filter.provider;

import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.ArrayList;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/14/2018.
 */

public class FilterInteractor extends BaseInteractor implements FilterContract.FilterInteractor {

    private FilterContract.FilterPresenter presenter;

    public FilterInteractor(FilterContract.FilterPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestBrand() {
        getGeneralNetworkManager()
                .requestBrandFilter(new NetworkHandler())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        brands -> presenter.onBrandsReceived(brands != null ? brands : new ArrayList<>()),
                        error -> presenter.onFailedReceivingBrands(error)
                );
    }
}
