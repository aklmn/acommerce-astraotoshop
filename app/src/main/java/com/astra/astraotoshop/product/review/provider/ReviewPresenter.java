package com.astra.astraotoshop.product.review.provider;

import com.astra.astraotoshop.utils.base.BasePresenter;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

/**
 * Created by Henra Setia Nugraha on 1/15/2018.
 */

public class ReviewPresenter extends BasePresenter implements ReviewContract.ReviewPresenter {

    private ReviewContract.ReviewInteractor interactor;
    private ReviewContract.Review view;
    private ReviewEntity review;

    public ReviewPresenter(ReviewContract.Review view) {
        this.view = view;
        interactor = new ReviewInteractor(this);
    }

    @Override
    public ReviewEntity getReview() {
        return review;
    }

    @Override
    public void requestProductReview(String productId) {
        interactor.requestProductReview(
                new NetworkHandler(),
                productId,
                getQueries()
        );
    }

    @Override
    public void onReviewReceived(ReviewEntity review) {
        this.review = review;
        view.setData(review);
        System.out.println();
    }

    @Override
    public void onFailedReceivingReview(Throwable throwable) {
        System.out.println();
    }
}
