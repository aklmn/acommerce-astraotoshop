package com.astra.astraotoshop.product.search.provider;

import com.google.gson.annotations.SerializedName;

public class Suggestion {

    @SerializedName("items")
    private Items items;

    public void setItems(Items items) {
        this.items = items;
    }

    public Items getItems() {
        return items;
    }

    @Override
    public String toString() {
        return
                "Suggestion{" +
                        "items = '" + items + '\'' +
                        "}";
    }
}