package com.astra.astraotoshop.product.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.provider.ProductContract;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.filter.provider.BrandFilterEntity;
import com.astra.astraotoshop.product.filter.provider.FilterContract;
import com.astra.astraotoshop.utils.view.FontIcon;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/29/2017.
 */

public class FilterFragment extends DialogFragment implements ListListener, FilterContract.FilterView {

    //    @BindView(R.id.brand)
//    Switch brand;
    Unbinder unbinder;
    @BindView(R.id.brands)
    RecyclerView brands;
    @BindView(R.id.sortArrowIcon)
    FontIcon sortArrowIcon;
    @BindView(R.id.brandArrowIcon)
    FontIcon brandArrowIcon;
    @BindView(R.id.minPrice)
    EditText minPrice;
    @BindView(R.id.maxPrice)
    EditText maxPrice;
    @BindView(R.id.currentSort)
    TextView currentSort;
    private BrandAdapter chipsAdapter;
    private ProductContract.ProductList productView;
    private ProductContract.ProductListPresenter presenter;
    private FilterContract.FilterPresenter filterPresenter;
    private BrandFilterFragment brandFragment;
    private SortFragment sortFragment;

    public static FilterFragment newInstance() {

        Bundle args = new Bundle();

        FilterFragment fragment = new FilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
        chipsAdapter = new BrandAdapter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_filter2, container, false);
        unbinder = ButterKnife.bind(this, view);
        brands.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        sortFragment = SortFragment.newInstance(filterPresenter.getSortState());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        brands.setAdapter(chipsAdapter);
        setPriceValue();
        currentSort.setText(getResources().getStringArray(R.array.sortKey)[filterPresenter.getSortState()]);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (filterPresenter.getBrands().size() > 0) {
            chipsAdapter.setBrandFilterEntities(filterPresenter.getBrands());
        }
    }

//    @OnCheckedChanged(R.id.sort)
//    public void sort(CompoundButton button, boolean isChecked) {
//        if (isChecked) {
//            sortList.setVisibility(View.VISIBLE);
//            sortArrowIcon.setText(getString(R.string.chevron_down));
//        } else {
//            sortList.setVisibility(View.GONE);
//            sortArrowIcon.setText(getString(R.string.chevron_right));
//        }
//    }

//    @OnCheckedChanged(R.id.brand)
//    public void brand(CompoundButton button, boolean isChecked) {
//        if (isChecked) {
//            brands.setVisibility(View.VISIBLE);
//            brandArrowIcon.setText(getString(R.string.chevron_down));
//        } else {
//            brands.setVisibility(View.GONE);
//            brandArrowIcon.setText(getString(R.string.chevron_right));
//        }
//    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        if (viewId == R.id.delete) {
            filterPresenter.removeBrandValue((BrandFilterEntity) data);
            filterPresenter.updateFilter(getString(R.string.filter_brand), new Gson().toJson(filterPresenter.getBrandValue()));
            chipsAdapter.setBrandFilterEntities(filterPresenter.getBrands());
        }
        switch (viewId) {
            case android.R.id.text1: {
                presenter.updateFilter("order", (String) data);
                break;
            }
            case R.id.delete: {
                filterPresenter.removeBrandValue((BrandFilterEntity) data);
                System.out.println();
            }
        }

    }

    public void injectFilter(ProductContract.ProductListPresenter presenter, ProductContract.ProductList productView) {
        this.presenter = presenter;
        this.productView = productView;
    }

    public void injectPresenter(FilterContract.FilterPresenter filterPresenter) {
        this.filterPresenter = filterPresenter;
        filterPresenter.attachView(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.applyFilter, R.id.clearFilter, R.id.close})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.applyFilter:
//                collectAllFilterData();
                break;
            case R.id.clearFilter:
                clearAllFilterData();
                break;
            case R.id.close:
                dismiss();
                break;
        }
    }

    private void collectAllFilterData() {
        int[] priceState = new int[2];
        if (!TextUtils.isEmpty(minPrice.getText().toString())) {
            filterPresenter.updateFilter(
                    getString(R.string.filter_price_from),
                    StringFormat.clearCurrencyFormat(minPrice.getText().toString()));
            priceState[0] = StringFormat.getIntFromString(minPrice.getText().toString());
        } else {
            filterPresenter.removeFilterItem(getString(R.string.filter_price_from));
        }

        if (!TextUtils.isEmpty(maxPrice.getText().toString())) {
            filterPresenter.updateFilter(
                    getString(R.string.filter_price_to),
                    StringFormat.clearCurrencyFormat(maxPrice.getText().toString()));
            priceState[1] = StringFormat.getIntFromString(minPrice.getText().toString());
        } else {
            filterPresenter.removeFilterItem(getString(R.string.filter_price_to));
        }

        filterPresenter.updatePriceState(priceState);
    }

    // TODO: 2/3/2018 resetAllData
    private void clearAllFilterData() {
        maxPrice.setText("");
        minPrice.setText("");
        sortFragment.setPositionState(0);
        chipsAdapter = new BrandAdapter(this);
        brands.setAdapter(chipsAdapter);
        // TODO: 2/4/2018 reset sortview
        filterPresenter.updateSortState(0);
        currentSort.setText(getResources().getStringArray(R.array.sortKey)[filterPresenter.getSortState()]);
        filterPresenter.updateBrands(filterBrands());
        filterPresenter.resetFilter();
        filterPresenter.updatePriceState(new int[2]);
    }

    @Override
    public void setBrand(List<BrandFilterEntity> brands) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        if (brandFragment == null)
            brandFragment = BrandFilterFragment.newInstance();
        Fragment fragment1 = getChildFragmentManager().findFragmentByTag("fragment_filter");
        if (fragment1 != null)
            transaction.remove(brandFragment);
        else {
//            brandFragment.show(getChildFragmentManager(), "fragment_filter");
            brandFragment.setBrands(brands);
            brandFragment.setFilterView(this);
        }
    }

    @Override
    public void showError() {

    }

    @Override
    public void onFilterApply(List<String> brandValues, List<BrandFilterEntity> brands) {
        filterPresenter.updateBrands(brands);
        filterPresenter.updateBrandValue(brandValues);
        String cleanBrand = (new Gson().toJson(brandValues)).replace("[", "");
        cleanBrand = cleanBrand.replace("]", "");
        cleanBrand = cleanBrand.replace("\"", "");
        if (!cleanBrand.equals(""))
            filterPresenter.removeFilterItem(getString(R.string.filter_brand));
        else
            presenter.deleteFilterItem(getString(R.string.filter_brand));
        chipsAdapter.setBrandFilterEntities(brands);
    }

    private List<BrandFilterEntity> filterBrands() {
        List<BrandFilterEntity> brandFilterEntities = new ArrayList<>();
        for (BrandFilterEntity entity : filterPresenter.getBrands()) {
            entity.setSelected(false);
            brandFilterEntities.add(entity);
        }
        return brandFilterEntities;
    }

    @OnClick(R.id.applyFilter)
    public void applyFilter() {
        collectAllFilterData();
        cleanBrand();
        filterPresenter.updateFilter();
        productView.onFilterApply(null);
        dismiss();
    }

    private void cleanBrand() {
        String brand = "";
        for (BrandFilterEntity entity : filterPresenter.getBrands()) {
            if (entity.isSelected()) {
                brand += entity.getValue() + ",";
            }
        }

        if (!brand.equals("")) {
            filterPresenter.updateFilter(getString(R.string.filter_brand),brand);
        }else {
            filterPresenter.removeFilterItem(getString(R.string.filter_brand));
        }
    }

    @OnClick(R.id.sortContainer)
    public void onSortClicked() {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        if (sortFragment == null)
            sortFragment = SortFragment.newInstance(0);

        Fragment fragment = getChildFragmentManager().findFragmentByTag("sort_fragment");
        if (fragment != null) {
            transaction.remove(sortFragment);
        } else {
            sortFragment.setTargetFragment(this, 3);
            sortFragment.show(this.getFragmentManager(), "sort_fragment");
            sortFragment.setPositionState(filterPresenter.getSortState());
        }
    }

    @OnClick(R.id.brandContainer)
    public void onBrandClicked() {
        if (filterPresenter.getBrands().size() > 0) {
            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            brandFragment = BrandFilterFragment.newInstance();
            brandFragment.setBrands(filterPresenter.getBrands());
            Fragment fragment1 = getChildFragmentManager().findFragmentByTag("fragment_filter");
            if (fragment1 != null)
                transaction.remove(fragment1);
            else {
                brandFragment.show(getChildFragmentManager(), "fragment_filter");
                brandFragment.setBrands(filterPresenter.getBrands());
                brandFragment.setFilterView(this);
            }
        } else {

        }
    }

    private void setPriceValue() {
        try {
            minPrice.setText(filterPresenter.getPriceState()[0]);
            maxPrice.setText(filterPresenter.getPriceState()[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSortSelected(String order, String dir, int position) {
        filterPresenter.updateFilter(getString(R.string.filter_order), order);
        filterPresenter.updateFilter(getString(R.string.filter_dir), dir);
        filterPresenter.updateSortState(position);
        currentSort.setText(getResources().getStringArray(R.array.sortKey)[position]);
    }
}
