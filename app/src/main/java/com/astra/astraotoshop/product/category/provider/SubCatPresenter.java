package com.astra.astraotoshop.product.category.provider;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/5/2018.
 */

public class SubCatPresenter implements CategoryContract.SubCatPresenter {

    private CategoryEntity category;

    @Override
    public String getTitle() {
        try {
            if (category.getName() != null)
                return category.getName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String getChildTitle(int position) {
        return getCategoryByPosition(position).getName();
    }

    @Override
    public int getId() {
        try {
            if (category.getName() != null) {
                return category.getId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public List<CategoryEntity> getCategories() {
        return category.getChildrenData();
    }

    @Override
    public CategoryEntity getCategoryByPosition(int position) {
        try {
            if (category.getChildrenData().size() > 0)
                return category.getChildrenData().get(position);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void collectIntentData(CategoryEntity data) {
        if (data != null) {
            category = data;
            if (getCategories().get(0).getId() != data.getId()) {
                CategoryEntity category = new CategoryEntity(
                        data.isIsActive(),
                        data.getLevel(),
                        data.getParentId(),
                        data.getProductCount(),
                        "Semua " + data.getName(),
                        data.getId(),
                        data.getPosition(),
                        new ArrayList<>());

                getCategories().add(0, category);
            }
        }
    }
}
