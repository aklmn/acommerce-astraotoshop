package com.astra.astraotoshop.product.category.provider;

import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/5/2018.
 */

public interface CategoryContract {
    interface CategoryView{
        void onItemSelect(CategoryEntity category, String title);
    }

    interface CatPresenter {
        void requestCategories(String storeCode);

        void onCategoryReceived(CategoryEntity categoryEntity, String storeCode);

        void onRequestCatFailed();
    }

    interface CatInteractor {
        void requestCategory(NetworkHandler networkHandler, String storeCode);
    }

    interface SubCatPresenter {

        String getTitle();
        String getChildTitle(int position);

        int getId();

        List<CategoryEntity> getCategories();

        CategoryEntity getCategoryByPosition(int position);

        void collectIntentData(CategoryEntity data);
    }
}
