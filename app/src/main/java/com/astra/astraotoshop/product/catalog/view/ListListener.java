package com.astra.astraotoshop.product.catalog.view;

/**
 * Created by Henra Setia Nugraha on 1/11/2018.
 */

public interface ListListener {
    void onItemClick(int viewId, int position, Object data);
}
