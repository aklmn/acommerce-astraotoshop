package com.astra.astraotoshop.product.search;

import android.content.Context;
import android.util.Log;

import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;
import com.google.gson.Gson;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 11/3/2017.
 */

public class TestInteractor extends BaseInteractor implements Contracts.Interactor{

    private static final String TAG = TestInteractor.class.getSimpleName();

    public TestInteractor(Context context) {
        super(context);
    }

    @Override
    public void request(NetworkHandler networkHandler) {
        getGeneralNetworkManager().request(networkHandler)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                    Log.i(TAG, "request: "+new Gson().toJson(response));
                });
    }
}
