package com.astra.astraotoshop.user.subsricption.provider;

import com.google.gson.annotations.SerializedName;

public class Subscription {

    @SerializedName("is_subscribed")
    private boolean isSubscribed;

    public void setIsSubscribed(boolean isSubscribed) {
        this.isSubscribed = isSubscribed;
    }

    public boolean isIsSubscribed() {
        return isSubscribed;
    }

    @Override
    public String toString() {
        return
                "Subscription{" +
                        "is_subscribed = '" + isSubscribed + '\'' +
                        "}";
    }
}