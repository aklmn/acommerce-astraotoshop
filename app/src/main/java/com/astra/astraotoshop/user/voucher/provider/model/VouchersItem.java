package com.astra.astraotoshop.user.voucher.provider.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VouchersItem {

    @SerializedName("data")
    private List<VoucherData> data;

    public void setData(List<VoucherData> data) {
        this.data = data;
    }

    public List<VoucherData> getData() {
        return data;
    }

    @Override
    public String toString() {
        return
                "VouchersItem{" +
                        "data = '" + data + '\'' +
                        "}";
    }
}