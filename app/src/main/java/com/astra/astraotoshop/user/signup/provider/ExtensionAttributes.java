package com.astra.astraotoshop.user.signup.provider;

import com.google.gson.annotations.SerializedName;

public class ExtensionAttributes {

    @SerializedName("is_subscribed")
    private boolean isSubscribed;

    public ExtensionAttributes(boolean isSubscribed) {
        this.isSubscribed = isSubscribed;
    }

    public void setIsSubscribed(boolean isSubscribed) {
        this.isSubscribed = isSubscribed;
    }

    public boolean isIsSubscribed() {
        return isSubscribed;
    }

    @Override
    public String toString() {
        return
                "AppExtensionAttributes{" +
                        "is_subscribed = '" + isSubscribed + '\'' +
                        "}";
    }
}