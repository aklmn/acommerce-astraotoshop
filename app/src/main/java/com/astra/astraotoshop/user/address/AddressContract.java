package com.astra.astraotoshop.user.address;

import com.astra.astraotoshop.user.address.provider.AddAddressBody;
import com.astra.astraotoshop.user.address.provider.CitiesItem;
import com.astra.astraotoshop.user.address.provider.CityEntity;
import com.astra.astraotoshop.user.address.provider.RegionEntity;
import com.astra.astraotoshop.user.address.provider.RegionsItem;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 2/18/2018.
 */

public interface AddressContract {
    interface AddressView{
        void onAddressSaved();
        void onSaveFailed();
        void onAddressDeleted();
        void onDeleteAddressFailed();

        void onRegionReceived(List<String> regions);
        void onCityReceived(List<String> cities);
        void onReceivingFailed(String message);
    }

    interface AddressPresenter{
        void saveAddress(String firstName,
                         String lastName,
                         String postCode,
                         String city,
                         String regionId,
                         String telephone,
                         List<String> street,
                         String district,
                         String rtRw,
                         boolean isDefault,
                         String addressId,
                         boolean isNew);
        void onAddressSaved();
        void onSaveFailed();

        void requestRegion(String countryId);
        void requestCity(String regionId);
        void onRegionReceived(RegionEntity region);
        void onCityReceived(CityEntity city);
        void onReceivingFailed();

        void deleteAddress(String addressId);
        void onAddressDeleted();
        void onDeleteAddressFailed();

        RegionsItem getRegion(int position);
        CitiesItem getCity(int position);
    }
    interface AddressInteractor{
        void saveAddress(String storeCode, AddAddressBody addressBody);
        void updateAddress(String storeCode, String addressId, AddAddressBody addressBody);
        void deleteAddress(String addressId, String id);
        void requestRegion(String storeCode,String countryId);
        void requestCity(String storeCode,String regionId);
    }
}
