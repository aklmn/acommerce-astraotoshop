package com.astra.astraotoshop.user.profile.provider;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.utils.pref.Pref;

/**
 * Created by Henra Setia Nugraha on 5/17/2018.
 */

public class UpdateProfilePresenter implements ProfileContract.UpdateProfilePresenter {
    private ProfileContract.UpdateProfileView view;
    private ProfileContract.UpdateProfileInteractor interactor;
    private ProfileEntity profile;
    private String storeCode;

    public UpdateProfilePresenter(ProfileContract.UpdateProfileView view, String storeCode) {
        this.view = view;
        this.storeCode = storeCode;
        interactor = new UpdateProfileInteractor(this);
    }

    @Override
    public void setProfile(ProfileEntity profile) {
        this.profile = profile;
    }

    @Override
    public void updateProfile(String storeCode, String firstName, String lastName, String email) {
        profile.setFirstname(firstName);
        profile.setLastname(lastName);
        profile.setEmail(email);
        interactor.updateProfile(storeCode, new UpdateProfile(profile));
    }

    @Override
    public void profileUpdated(ProfileEntity profileEntity) {
        Pref.getPreference().putString(BuildConfig.UID, String.valueOf(profileEntity.getId()));
        Pref.getPreference().putString("username", profileEntity.getFirstname() + " " + profileEntity.getLastname());
        Pref.getPreference().putString("email" + storeCode, profileEntity.getEmail());
        view.showUpdateProfileMessage(true);
    }

    @Override
    public void failedUpdate() {
        view.showUpdateProfileMessage(false);
    }
}
