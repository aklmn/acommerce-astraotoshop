package com.astra.astraotoshop.user.voucher;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.user.voucher.provider.VoucherListPresenter;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherData;
import com.astra.astraotoshop.utils.base.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VoucherActivity extends BaseActivity implements VoucherContract.VoucherListView, ListListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.itemCount)
    TextView itemCount;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.placeHolder)
    LinearLayout placeHolder;

    private VoucherContract.VoucherListPresenter presenter;
    private VoucherAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_list);
        ButterKnife.bind(this);
        actionBarSetup();
        placeHolder.setVisibility(View.VISIBLE);

        presenter = new VoucherListPresenter(this, getStoreCode());
        adapter = new VoucherAdapter(this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.getVoucherList();
    }

    @Override
    public void showVouchers(List<VoucherData> vouchers) {
        adapter.setVouchers(vouchers);
        if (adapter.getItemCount() > 0) {
            itemCount.setText(adapter.getItemCount() + " voucher ditemukan");
            placeHolder.setVisibility(View.GONE);
        } else {
            itemCount.setText("Voucher tidak ditemukan");
        }
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        VoucherData voucherData = (VoucherData) data;
        if (voucherData != null)
            startActivity((getIntent(this, VoucherDetailActivity.class)).putExtra("voucherId", voucherData.getVoucherId()));
    }
}
