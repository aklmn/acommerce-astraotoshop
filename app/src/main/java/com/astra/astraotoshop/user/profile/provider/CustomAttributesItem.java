package com.astra.astraotoshop.user.profile.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CustomAttributesItem implements Parcelable{

    @SerializedName("value")
    private String value;

    @SerializedName("attribute_code")
    private String attributeCode;

    protected CustomAttributesItem(Parcel in) {
        value = in.readString();
        attributeCode = in.readString();
    }

    public static final Creator<CustomAttributesItem> CREATOR = new Creator<CustomAttributesItem>() {
        @Override
        public CustomAttributesItem createFromParcel(Parcel in) {
            return new CustomAttributesItem(in);
        }

        @Override
        public CustomAttributesItem[] newArray(int size) {
            return new CustomAttributesItem[size];
        }
    };

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setAttributeCode(String attributeCode) {
        this.attributeCode = attributeCode;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    @Override
    public String toString() {
        return
                "CustomAttributesItem{" +
                        "value = '" + value + '\'' +
                        ",attribute_code = '" + attributeCode + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(value);
        dest.writeString(attributeCode);
    }
}