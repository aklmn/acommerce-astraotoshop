package com.astra.astraotoshop.user.wishlist.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WishlistEntity {

    @SerializedName("items")
    private List<WishlistItem> items;

    public void setItems(List<WishlistItem> items) {
        this.items = items;
    }

    public List<WishlistItem> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return
                "WishlistEntity{" +
                        "items = '" + items + '\'' +
                        "}";
    }
}