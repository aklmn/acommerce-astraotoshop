package com.astra.astraotoshop.user.voucher.provider.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VoucherList {

    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("items")
    private List<VoucherData> vouchersItem;

    public List<VoucherData> getVouchersItem() {
        return vouchersItem;
    }

    public void setVouchersItem(List<VoucherData> vouchersItem) {
        this.vouchersItem = vouchersItem;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    @Override
    public String toString() {
        return
                "VoucherList{" +
                        "total_count = '" + totalCount + '\'' +
                        ",vouchersItem = '" + vouchersItem + '\'' +
                        "}";
    }
}