package com.astra.astraotoshop.user.address;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.PagerAdapter;
import com.astra.astraotoshop.product.catalog.productservice.ProductServiceAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddressSelectorActivity extends AppCompatActivity implements ProductServiceAdapter.ItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.result)
    TextView result;
    @BindView(R.id.pager)
    ViewPager pager;
    private PagerAdapter pagerAdapter;
    int position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_selector);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        addFragment();
        addFragment();
        addFragment();
        pager.setAdapter(pagerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        --position;
    }

    @Override
    public void onItemClick() {
        if (pagerAdapter.getCount() == position + 1) {
            finish();
            return;
        }
        pager.setCurrentItem(++position);
    }

    @Override
    public void onItemClick(int viewId) {

    }

    private void addFragment() {
        AddressSelectorFragment fragment = AddressSelectorFragment.newInstance();
        fragment.setListener(this);
        pagerAdapter.addFragment(fragment, "");
    }
}
