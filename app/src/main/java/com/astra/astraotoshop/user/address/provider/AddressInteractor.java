package com.astra.astraotoshop.user.address.provider;

import com.astra.astraotoshop.user.address.AddressContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 2/18/2018.
 */

public class AddressInteractor extends BaseInteractor implements AddressContract.AddressInteractor {
    AddressContract.AddressPresenter presenter;

    public AddressInteractor(AddressContract.AddressPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void saveAddress(String storeCode, AddAddressBody addressBody) {
        getGeneralNetworkManager()
                .addNewAddress(
                        new NetworkHandler(),
                        storeCode,
                        addressBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.onAddressSaved(),
                        e -> presenter.onSaveFailed()
                );
    }

    @Override
    public void updateAddress(String storeCode, String addressId, AddAddressBody addressBody) {
        getGeneralNetworkManager()
                .updateAddress(
                        new NetworkHandler(),
                        addressId,
                        storeCode,
                        addressBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.onAddressSaved(),
                        e -> presenter.onSaveFailed()
                );
    }

    @Override
    public void deleteAddress(String storeCode, String addressId) {
        getGeneralNetworkManager()
                .deleteAddress(
                        new NetworkHandler(),
                        storeCode,
                        addressId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.onAddressDeleted(),
                        e->presenter.onDeleteAddressFailed()
                );
    }

    @Override
    public void requestRegion(String storeCode, String countryId) {
        getGeneralNetworkManager()
                .getRegion(
                        new NetworkHandler(),
                        storeCode,
                        countryId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        regionEntity -> presenter.onRegionReceived(regionEntity),
                        error -> presenter.onReceivingFailed()
                );
    }

    @Override
    public void requestCity(String storeCode, String regionId) {
        getGeneralNetworkManager()
                .getCity(
                        new NetworkHandler(),
                        storeCode,
                        regionId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        cityEntity -> presenter.onCityReceived(cityEntity),
                        error -> presenter.onReceivingFailed()
                );
    }
}
