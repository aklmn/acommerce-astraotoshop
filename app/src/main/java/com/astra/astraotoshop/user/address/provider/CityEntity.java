package com.astra.astraotoshop.user.address.provider;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CityEntity{

	@SerializedName("items")
	private List<CitiesItem> items;

	public void setItems(List<CitiesItem> items){
		this.items = items;
	}

	public List<CitiesItem> getItems(){
		return items;
	}

	@Override
 	public String toString(){
		return 
			"CityEntity{" + 
			"items = '" + items + '\'' + 
			"}";
		}
}