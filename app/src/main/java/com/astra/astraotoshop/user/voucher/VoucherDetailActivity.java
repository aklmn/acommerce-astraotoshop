package com.astra.astraotoshop.user.voucher;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.voucher.provider.VoucherPresenter;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherDetail;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VoucherDetailActivity extends BaseActivity implements VoucherContract.VoucherView {

    @BindView(R.id.orderId)
    TextView orderId;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.expDate)
    TextView expDate;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.productName)
    TextView productName;
    @BindView(R.id.voucherCode)
    TextView voucherCode;
    @BindView(R.id.outlet)
    TextView outlet;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.productImage)
    ImageView productImage;
    @BindView(R.id.firstPrice)
    TextView firstPrice;
    private VoucherContract.VoucherPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voucher_detail);
        ButterKnife.bind(this);
        actionBarSetup();
        presenter = new VoucherPresenter(this, getStoreCode());
        if (getIntent().hasExtra("voucherId")) {
            presenter.getVoucher(getIntent().getStringExtra("voucherId"));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    public void showVouchers(VoucherDetail detail) {
        StringFormat.setText(orderId, "#" + detail.getOrder().getIncrementId());
        StringFormat.setText(date, StringFormat.timeConverter(detail.getOrder().getCreatedAt()));
        StringFormat.setText(expDate, StringFormat.timeConverter(detail.getVoucherExpiredAt()));
        StringFormat.setText(status, detail.getVoucherStatus());
        StringFormat.setText(productName, detail.getOrder().getItem().getName());
        StringFormat.setText(voucherCode, detail.getVoucherCode());
        StringFormat.setText(outlet, detail.getOrder().getAppointmentDetail().getLocation().getTitle());
        StringFormat.setText(address, detail.getOrder().getAppointmentDetail().getLocation().getAddress());
        StringFormat.setText(time, detail.getOrder().getAppointmentDetail().getSlot().getDate() + ", jam " +
                detail.getOrder().getAppointmentDetail().getSlot().getFromHour() + " - " +
                detail.getOrder().getAppointmentDetail().getSlot().getToHour());
        StringFormat.applyCurrencyFormat(firstPrice, detail.getOrder().getItem().getPrice());
        if (detail.getOrder().getImage() != null) {
            Glide.with(this).load(detail.getOrder().getImage()).into(productImage);
        }
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
    }
}
