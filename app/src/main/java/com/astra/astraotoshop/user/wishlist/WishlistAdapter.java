package com.astra.astraotoshop.user.wishlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.user.wishlist.provider.WishlistItem;
import com.astra.astraotoshop.utils.view.FontIcon;
import com.astra.astraotoshop.utils.view.ImageSquared;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 12/4/2017.
 */

public class WishlistAdapter extends RecyclerView.Adapter<WishlistAdapter.WishlistViewHolder> {

    private ListListener listener;
    private List<WishlistItem> wishlistItems;
    private Context context;

    public WishlistAdapter() {
    }

    public WishlistAdapter(ListListener listener, Context context) {
        this.listener = listener;
        this.context = context;
        wishlistItems = new ArrayList<>();
    }

    @Override
    public WishlistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new WishlistViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wishlist, parent, false));
    }

    @Override
    public void onBindViewHolder(WishlistViewHolder holder, int position) {
        holder.name.setText(wishlistItems.get(position).getName());
        holder.review.setRating(wishlistItems.get(position).getRating());
        Glide.with(context).load(wishlistItems.get(position).getImage()).into(holder.image);
        String price = StringFormat.addCurrencyFormat(wishlistItems.get(position).getPrice());
        String sPrice = StringFormat.addCurrencyFormat(wishlistItems.get(position).getFinalPrice());
        if (!price.equals(sPrice)) {
            holder.secondPrice.setText(price);
            holder.discount.setText(discountCalculate(price, sPrice));
            holder.discount.setVisibility(View.VISIBLE);
            holder.secondPrice.setVisibility(View.VISIBLE);
        } else {
            holder.discount.setVisibility(View.GONE);
            holder.secondPrice.setVisibility(View.GONE);
        }
        holder.firstPrice.setText(sPrice);
        holder.sku.setText(wishlistItems.get(position).getSku());
    }

    private String discountCalculate(String price, String specialPrice) {
        Double dPrice = Double.parseDouble(StringFormat.clearCurrencyFormat(price));
        Double dSpecialPrice = Double.parseDouble(StringFormat.clearCurrencyFormat(specialPrice));

        double cutPrice = (dPrice - dSpecialPrice) / dPrice;
        int discount = (int) Math.ceil(cutPrice * 100);
        return String.valueOf(discount + "%");
    }

    @Override
    public int getItemCount() {
        return wishlistItems.size();
    }

    public void setWishlistItems(List<WishlistItem> wishlistItems) {
        this.wishlistItems = wishlistItems;
        notifyDataSetChanged();
    }

    class WishlistViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.image)
        ImageSquared image;
        @BindView(R.id.discount)
        TextView discount;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.sku)
        TextView sku;
        @BindView(R.id.firstPrice)
        TextView firstPrice;
        @BindView(R.id.secondPrice)
        TextView secondPrice;
        @BindView(R.id.grocery)
        TextView grocery;
        @BindView(R.id.review)
        RatingBar review;
        @BindView(R.id.iconDelete)
        FontIcon iconDelete;
        @BindView(R.id.container)
        LinearLayout container;

        public WishlistViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                listener.onItemClick(0, getAdapterPosition(), wishlistItems.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.iconDelete)
        public void onDelete() {
            listener.onItemClick(R.id.iconDelete, getAdapterPosition(), wishlistItems.get(getAdapterPosition()));
        }
    }
}
