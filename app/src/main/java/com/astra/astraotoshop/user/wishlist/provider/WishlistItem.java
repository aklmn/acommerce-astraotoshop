package com.astra.astraotoshop.user.wishlist.provider;

import com.google.gson.annotations.SerializedName;

public class WishlistItem {

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("image")
    private String image;

    @SerializedName("rating")
    private int rating;

    @SerializedName("description")
    private Object description;

    @SerializedName("wishlist_id")
    private String wishlistId;

    @SerializedName("product_name")
    private String productName;

    @SerializedName("added_at")
    private String addedAt;

    @SerializedName("final_price")
    private int finalPrice;

    @SerializedName("price")
    private String price;

    @SerializedName("wishlist_item_id")
    private String wishlistItemId;

    @SerializedName("product_id")
    private String productId;

    @SerializedName("qty")
    private String qty;

    @SerializedName("name")
    private String name;

    @SerializedName("sku")
    private String sku;

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getRating() {
        return rating;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getDescription() {
        return description;
    }

    public void setWishlistId(String wishlistId) {
        this.wishlistId = wishlistId;
    }

    public String getWishlistId() {
        return wishlistId;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setAddedAt(String addedAt) {
        this.addedAt = addedAt;
    }

    public String getAddedAt() {
        return addedAt;
    }

    public void setFinalPrice(int finalPrice) {
        this.finalPrice = finalPrice;
    }

    public int getFinalPrice() {
        return finalPrice;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setWishlistItemId(String wishlistItemId) {
        this.wishlistItemId = wishlistItemId;
    }

    public String getWishlistItemId() {
        return wishlistItemId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQty() {
        return qty;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSku() {
        return sku;
    }

    @Override
    public String toString() {
        return
                "WishlistItem{" +
                        "store_id = '" + storeId + '\'' +
                        ",image = '" + image + '\'' +
                        ",rating = '" + rating + '\'' +
                        ",description = '" + description + '\'' +
                        ",wishlist_id = '" + wishlistId + '\'' +
                        ",product_name = '" + productName + '\'' +
                        ",added_at = '" + addedAt + '\'' +
                        ",final_price = '" + finalPrice + '\'' +
                        ",price = '" + price + '\'' +
                        ",wishlist_item_id = '" + wishlistItemId + '\'' +
                        ",product_id = '" + productId + '\'' +
                        ",qty = '" + qty + '\'' +
                        ",name = '" + name + '\'' +
                        ",sku = '" + sku + '\'' +
                        "}";
    }
}