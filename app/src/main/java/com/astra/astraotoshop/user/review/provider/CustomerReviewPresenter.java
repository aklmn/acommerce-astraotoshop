package com.astra.astraotoshop.user.review.provider;

import com.astra.astraotoshop.user.review.CustomerReviewContract;

/**
 * Created by Henra Setia Nugraha on 3/27/2018.
 */

public class CustomerReviewPresenter implements CustomerReviewContract.ReviewPresenter {

    private CustomerReviewContract.View view;
    private CustomerReviewContract.ReviewInteractor interactor;

    public CustomerReviewPresenter(CustomerReviewContract.View view) {
        this.view = view;
        interactor=new CustomerReviewInteractor(this);
    }

    @Override
    public void requestReview(String storeCode) {
        interactor.requestReview(storeCode);
    }

    @Override
    public void onReviewReceived(CustomerReview review) {
        view.showReviews(review.getItems());
    }

    @Override
    public void onFailedReceive(Throwable e) {
        view.showError(e.getMessage());
    }
}
