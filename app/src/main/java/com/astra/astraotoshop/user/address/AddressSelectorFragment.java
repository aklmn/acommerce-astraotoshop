package com.astra.astraotoshop.user.address;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.SelectorAdapter;
import com.astra.astraotoshop.product.catalog.productservice.ProductServiceAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/30/2017.
 */

public class AddressSelectorFragment extends Fragment implements AdapterView.OnItemClickListener {

    @BindView(R.id.simpleListView)
    ListView simpleListView;
    ProductServiceAdapter.ItemClickListener listener;
    Unbinder unbinder;

    public static AddressSelectorFragment newInstance() {

        Bundle args = new Bundle();

        AddressSelectorFragment fragment = new AddressSelectorFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.simple_list_view, container, false);
        unbinder = ButterKnife.bind(this, view);
        SelectorAdapter adapter = new SelectorAdapter();
        simpleListView.setAdapter(adapter);
        simpleListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        simpleListView.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void setListener(ProductServiceAdapter.ItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            listener.onItemClick();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
