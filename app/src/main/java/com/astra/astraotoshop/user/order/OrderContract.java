package com.astra.astraotoshop.user.order;

import com.astra.astraotoshop.order.history.provider.OrderHistoriesItem;
import com.astra.astraotoshop.order.history.provider.OrderHistory;
import com.astra.astraotoshop.user.order.provider.OrderDetail;
import com.astra.astraotoshop.user.order.provider.OrderList;
import com.astra.astraotoshop.user.order.provider.OrdersItem;

import java.util.List;
import java.util.Map;

/**
 * Created by Henra Setia Nugraha on 2/14/2018.
 */

public interface OrderContract {
    interface OrderDetailView{
        void showOrder(OrderDetail orderDetail);
        void onError();
    }

    interface OrderDetailPresenter{
        void requestOrderDetail(String orderId,String storeCode);
        void onOrderDetailReceived(OrderDetail orderDetail);
        void onFailedReceivingOrderDetail(Throwable error);
    }
    interface OrderDetailInteractor{
        void requestOrderDetail(String orderId,String storeCode);
    }

    interface OrderListVIew{
        void showOrderList(List<OrderHistoriesItem> ordersItems, int totalCount);
        void showError(String message);
    }
    interface OrderListPresenter{
        void reset();
        void requestOrderList();
        void onOrderListReceived(OrderHistory orderList);
        void onFailedReceivingOrderList(Throwable error);
        boolean isAddMore();
    }

    interface OrderListInteractor{
        void requestOrderList(String storeCode, Map<String, String> queries);
    }
}
