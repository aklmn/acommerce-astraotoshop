package com.astra.astraotoshop.user.signin;

import com.astra.astraotoshop.user.signin.provider.SocialLogin;
import com.facebook.AccessToken;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by Henra Setia Nugraha on 1/22/2018.
 */

public interface LoginContract {
    interface LoginView {
        void onLoginSuccess();

        void onLoginFailed(String media);
    }

    interface LoginPresenter {
        void requestFB(String tokenFB, String firstName, String lastName, String email);
        void FBSuccess(Object socialLogin);
        void FBFailed();

        void requestG(GoogleSignInAccount account, String token);
        void GSuccess(Object socialLogin);
        void GFailed(Throwable throwable);

        void getFBData(LoginResult loginResult);
    }

    interface LoginInteractor {
        void loginFB(String tokenFB, String firstName, String lastName, String email);

        void loginG(String idToken, String firstName, String lastName, String email);
    }
}
