package com.astra.astraotoshop.user.order.provider;

import com.google.gson.annotations.SerializedName;

public class InfoBuyRequest {

    @SerializedName("product")
    private String product;

    @SerializedName("uenc")
    private String uenc;

    @SerializedName("qty")
    private String qty;

    public void setProduct(String product) {
        this.product = product;
    }

    public String getProduct() {
        return product;
    }

    public void setUenc(String uenc) {
        this.uenc = uenc;
    }

    public String getUenc() {
        return uenc;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getQty() {
        return qty;
    }

    @Override
    public String toString() {
        return
                "InfoBuyRequest{" +
                        "product = '" + product + '\'' +
                        ",uenc = '" + uenc + '\'' +
                        ",qty = '" + qty + '\'' +
                        "}";
    }
}