package com.astra.astraotoshop.user.order.provider;

import com.astra.astraotoshop.user.order.OrderContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;
import com.google.gson.Gson;

import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 2/15/2018.
 */

public class OrderListInteractor extends BaseInteractor implements OrderContract.OrderListInteractor {
    private OrderContract.OrderListPresenter presenter;

    public OrderListInteractor(OrderContract.OrderListPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestOrderList(String storeCode, Map<String, String> queries) {
        getGeneralNetworkManager()
                .requestOrderList(
                        new NetworkHandler(),
                        storeCode,
                        queries)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        orderList -> presenter.onOrderListReceived(orderList),//presenter.onOrderListReceived(orderList),
                        error -> presenter.onFailedReceivingOrderList(error)
                );
    }
}
