package com.astra.astraotoshop.user.voucher.provider.model;

import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("price")
    private String price;

    @SerializedName("name")
    private String name;

    @SerializedName("sku")
    private String sku;

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getSku() {
        return sku;
    }

    @Override
    public String toString() {
        return
                "Item{" +
                        "price = '" + price + '\'' +
                        ",name = '" + name + '\'' +
                        ",sku = '" + sku + '\'' +
                        "}";
    }
}