package com.astra.astraotoshop.user.signin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.astra.astraotoshop.Application.AppPresenter;
import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartListItem;
import com.astra.astraotoshop.order.cart.provider.CartPresenter;
import com.astra.astraotoshop.user.password.ResetPasswordActivity;
import com.astra.astraotoshop.user.profile.provider.ProfileContract;
import com.astra.astraotoshop.user.profile.provider.ProfileEntity;
import com.astra.astraotoshop.user.profile.provider.ProfilePresenter;
import com.astra.astraotoshop.user.signin.provider.LoginPresenter;
import com.astra.astraotoshop.user.token.TokenContract;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.AlertManager;
import com.astra.astraotoshop.utils.view.Loading;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends BaseActivity implements TokenContract.TokenView, ProfileContract.ProfileView, CartContract.CartView, LoginContract.LoginView {

    private static final int RC_SIGN_IN = 333;
    private static final int REQUEST_AUTHORIZATION = 33;
    private final String EMAIL = "email";
    TokenContract.TokenPresenter tokenPresenter;
    ProfileContract.ProfilePresenter profilePresenter;
    CartContract.CartPresenter cartPresenter;

    @BindView(R.id.userName)
    TextInputEditText userName;
    @BindView(R.id.password)
    TextInputEditText password;
    @BindView(R.id.contUsername)
    TextInputLayout contUsername;
    @BindView(R.id.contPassword)
    TextInputLayout contPassword;
    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.sign_in_button)
    SignInButton signInButton;
    private Loading loading;
    private CallbackManager callbackManager;
    private LoginContract.LoginPresenter presenter;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        ButterKnife.bind(this);
        super.actionBarSetup();
        tokenPresenter = new AppPresenter(this, this);
        profilePresenter = new ProfilePresenter(this);
        cartPresenter = new CartPresenter(this, null, getStoreCode());
        loading = new Loading(this, getLayoutInflater());

        loginButton.setOnClickListener(v -> {
            //do some stuff
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleSignInClient.signOut();
        mGoogleSignInClient.revokeAccess();

        presenter = new LoginPresenter(this,
                getResources().getStringArray(R.array.storeCode)[0] + BuildConfig.CTKN,
                getResources().getStringArray(R.array.storeCode)[1] + BuildConfig.CTKN);
        callbackManager = CallbackManager.Factory.create();
        loginButton.setReadPermissions(Collections.singletonList(EMAIL));
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                presenter.getFBData(loginResult);
                loading.show(R.string.message_please_wait);
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException e) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN && resultCode == RESULT_OK) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Observable.unsafeCreate((Observable.OnSubscribe<String>) subscriber -> {
                    try {
                        String token = GoogleAuthUtil.getToken(LoginActivity.this, task.getResult().getAccount(), "oauth2:" + Scopes.PLUS_ME);
                        presenter.requestG(account, token);
                        if (subscriber.isUnsubscribed()) {
                            subscriber.onNext(token);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (UserRecoverableAuthException e) {
                        startActivityForResult(e.getIntent(), REQUEST_AUTHORIZATION);
                    } catch (GoogleAuthException e) {
                        e.printStackTrace();
                    }
                })
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                s -> {
                                },
                                e -> {
                                }
                        );
            } catch (ApiException e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_AUTHORIZATION) {
            System.out.println();
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        setResult(RESULT_CANCELED);
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.forgotPassword)
    public void forgotPassword() {
        startActivity(getIntent(this, ResetPasswordActivity.class));
    }

    @OnClick(R.id.submit)
    public void submit() {
        if (TextUtils.isEmpty(userName.getText())) contUsername.setError("Email wajib diisi");
        else if (!Patterns.EMAIL_ADDRESS.matcher(userName.getText().toString()).matches())
            contUsername.setError("Format email salah");
        else contUsername.setError(null);
        if (TextUtils.isEmpty(password.getText())) contPassword.setError("Kata sandi wajib diisi");
        else contPassword.setError("");

        if (TextUtils.isEmpty(userName.getText()) || TextUtils.isEmpty(password.getText())) return;

        tokenPresenter.requestToken(
                userName.getText().toString(),
                password.getText().toString(),
                getResources().getStringArray(R.array.storeCode)[0] + BuildConfig.CTKN,
                "customer");

        tokenPresenter.requestToken(
                userName.getText().toString(),
                password.getText().toString(),
                getResources().getStringArray(R.array.storeCode)[1] + BuildConfig.CTKN,
                "customer");
        loading.show(R.string.message_please_wait);
    }

    @Override
    public void onRequestComplete() {
        profilePresenter.requestProfile(getStoreCode());
        loading.dismiss();
    }

    @Override
    public void onFailedLogin(String message) {
        AlertManager.showSimpleMessage(this, message, null);
        loading.dismiss();
    }

    @Override
    public void setProfileData(ProfileEntity profileEntity) {
//        cartPresenter.requestCartList();
        finish();
    }

    @Override
    public void setCartList(List<CartListItem> items) {
        finish();
    }

    @Override
    public void onCartItemDeleted() {

    }

    @Override
    public void onCartUpdated() {

    }

    @Override
    public void showError() {
        loading.dismiss();
    }

    @Override
    public void showError(String message) {
        loading.dismiss();
    }

    @Override
    public void onLoginSuccess() {
        profilePresenter.requestProfile(getStoreCode());
    }

    @Override
    public void onLoginFailed(String media) {
        loading.dismiss();
        LoginManager.getInstance().logOut();
        showToast(media + " login gagal");
    }

    @OnClick(R.id.fbLogin)
    public void onFbLoginClicked() {
        loginButton.performClick();
    }

    @OnClick(R.id.gLogin)
    public void onGLoginClicked() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
