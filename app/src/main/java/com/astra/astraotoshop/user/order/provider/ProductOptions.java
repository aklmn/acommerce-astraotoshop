package com.astra.astraotoshop.user.order.provider;

import com.google.gson.annotations.SerializedName;

public class ProductOptions {

    @SerializedName("giftcard_email_template")
    private Object giftcardEmailTemplate;

    @SerializedName("info_buyRequest")
    private InfoBuyRequest infoBuyRequest;

    @SerializedName("giftcard_is_redeemable")
    private int giftcardIsRedeemable;

    @SerializedName("giftcard_lifetime")
    private Object giftcardLifetime;

    @SerializedName("giftcard_type")
    private Object giftcardType;

    public void setGiftcardEmailTemplate(Object giftcardEmailTemplate) {
        this.giftcardEmailTemplate = giftcardEmailTemplate;
    }

    public Object getGiftcardEmailTemplate() {
        return giftcardEmailTemplate;
    }

    public void setInfoBuyRequest(InfoBuyRequest infoBuyRequest) {
        this.infoBuyRequest = infoBuyRequest;
    }

    public InfoBuyRequest getInfoBuyRequest() {
        return infoBuyRequest;
    }

    public void setGiftcardIsRedeemable(int giftcardIsRedeemable) {
        this.giftcardIsRedeemable = giftcardIsRedeemable;
    }

    public int getGiftcardIsRedeemable() {
        return giftcardIsRedeemable;
    }

    public void setGiftcardLifetime(Object giftcardLifetime) {
        this.giftcardLifetime = giftcardLifetime;
    }

    public Object getGiftcardLifetime() {
        return giftcardLifetime;
    }

    public void setGiftcardType(Object giftcardType) {
        this.giftcardType = giftcardType;
    }

    public Object getGiftcardType() {
        return giftcardType;
    }

    @Override
    public String toString() {
        return
                "ProductOptions{" +
                        "giftcard_email_template = '" + giftcardEmailTemplate + '\'' +
                        ",info_buyRequest = '" + infoBuyRequest + '\'' +
                        ",giftcard_is_redeemable = '" + giftcardIsRedeemable + '\'' +
                        ",giftcard_lifetime = '" + giftcardLifetime + '\'' +
                        ",giftcard_type = '" + giftcardType + '\'' +
                        "}";
    }
}