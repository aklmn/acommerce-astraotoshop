package com.astra.astraotoshop.user.password.provider;

import com.google.gson.annotations.SerializedName;

public class ChangePassword {

    @SerializedName("newPassword")
    private String newPassword;

    @SerializedName("currentPassword")
    private String currentPassword;

    public ChangePassword() {
    }

    public ChangePassword(String newPassword, String currentPassword) {
        this.newPassword = newPassword;
        this.currentPassword = currentPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    @Override
    public String toString() {
        return
                "ChangePassword{" +
                        "newPassword = '" + newPassword + '\'' +
                        ",currentPassword = '" + currentPassword + '\'' +
                        "}";
    }
}