package com.astra.astraotoshop.user.signin.provider;

import com.astra.astraotoshop.user.signin.LoginContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 7/10/2018.
 */

public class LoginInteractor extends BaseInteractor implements LoginContract.LoginInteractor {

    private LoginContract.LoginPresenter presenter;

    public LoginInteractor(LoginContract.LoginPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void loginFB(String tokenFB, String firstName, String lastName, String email) {
        getGeneralNetworkManager()
                .loginFB(new NetworkHandler(), tokenFB, firstName, lastName, email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        socialLogin -> presenter.FBSuccess(socialLogin),
                        e -> presenter.FBFailed()
                );

    }

    @Override
    public void loginG(String idToken, String firstName, String lastName, String email) {
        getGeneralNetworkManager()
                .loginG(new NetworkHandler(),
                        idToken,
                        firstName,
                        lastName,
                        email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        socialLogin -> presenter.GSuccess(socialLogin),
                        error->presenter.GFailed(error)
                );
    }
}
