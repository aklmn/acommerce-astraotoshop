package com.astra.astraotoshop.user.password.provider;

import com.google.gson.annotations.SerializedName;

public class IsEmailAvailable {

    @SerializedName("websiteId")
    private int websiteId;

    @SerializedName("customerEmail")
    private String customerEmail;

    public IsEmailAvailable() {
    }

    public IsEmailAvailable(int websiteId, String customerEmail) {
        this.websiteId = websiteId;
        this.customerEmail = customerEmail;
    }

    public void setWebsiteId(int websiteId) {
        this.websiteId = websiteId;
    }

    public int getWebsiteId() {
        return websiteId;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    @Override
    public String toString() {
        return
                "IsEmailAvailable{" +
                        "websiteId = '" + websiteId + '\'' +
                        ",customerEmail = '" + customerEmail + '\'' +
                        "}";
    }
}