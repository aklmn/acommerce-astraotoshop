package com.astra.astraotoshop.user.address.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddAddressBody {

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("city")
    private String city;

    @SerializedName("street")
    private List<String> street;

    @SerializedName("postcode")
    private String postcode;

    @SerializedName("region_id")
    private String regionId;

    @SerializedName("telephone")
    private String telephone;

    @SerializedName("default_billing")
    private boolean defaultBilling;

    @SerializedName("country_id")
    private String countryId;

    @SerializedName("kelurahan")
    private String kelurahan;

    @SerializedName("rt_rw")
    private String rtRw;

    @SerializedName("default_shipping")
    private boolean defaultShipping;

    @SerializedName("lastname")
    private String lastname;

    public AddAddressBody() {
    }

    public AddAddressBody(String firstname, String city, List<String> street, String postcode, String regionId, String telephone, boolean defaultBilling, String countryId, String kelurahan, String rtRw, boolean defaultShipping, String lastname) {
        this.firstname = firstname;
        this.city = city;
        this.street = street;
        this.postcode = postcode;
        this.regionId = regionId;
        this.telephone = telephone;
        this.defaultBilling = defaultBilling;
        this.countryId = countryId;
        this.kelurahan = kelurahan;
        this.rtRw = rtRw;
        this.defaultShipping = defaultShipping;
        this.lastname = lastname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setStreet(List<String> street) {
        this.street = street;
    }

    public List<String> getStreet() {
        return street;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setDefaultBilling(boolean defaultBilling) {
        this.defaultBilling = defaultBilling;
    }

    public boolean getDefaultBilling() {
        return defaultBilling;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setRtRw(String rtRw) {
        this.rtRw = rtRw;
    }

    public String getRtRw() {
        return rtRw;
    }

    public void setDefaultShipping(boolean defaultShipping) {
        this.defaultShipping = defaultShipping;
    }

    public boolean getDefaultShipping() {
        return defaultShipping;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    @Override
    public String toString() {
        return
                "AddAddressBody{" +
                        "firstname = '" + firstname + '\'' +
                        ",city = '" + city + '\'' +
                        ",street = '" + street + '\'' +
                        ",postcode = '" + postcode + '\'' +
                        ",region_id = '" + regionId + '\'' +
                        ",telephone = '" + telephone + '\'' +
                        ",default_billing = '" + defaultBilling + '\'' +
                        ",country_id = '" + countryId + '\'' +
                        ",kelurahan = '" + kelurahan + '\'' +
                        ",rt_rw = '" + rtRw + '\'' +
                        ",default_shipping = '" + defaultShipping + '\'' +
                        ",lastname = '" + lastname + '\'' +
                        "}";
    }
}