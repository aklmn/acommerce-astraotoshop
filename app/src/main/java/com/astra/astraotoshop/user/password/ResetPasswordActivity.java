package com.astra.astraotoshop.user.password;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.password.provider.ResetPasswordPresenter;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


// TODO: 5/16/2018 check email (false for exist)
public class ResetPasswordActivity extends BaseActivity implements PasswordContract.ResetPasswordView {

    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.emailContainer)
    TextInputLayout emailContainer;
    private PasswordContract.ResetPasswordPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        actionBarSetup();
        presenter = new ResetPasswordPresenter(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @OnClick(R.id.submit)
    public void onSubmitClicked() {
        if (TextUtils.isEmpty(email.getText())) {
            emailContainer.setError("Email wajib diisi");
            return;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            emailContainer.setError("Format email salah");
            return;
        } else {
            emailContainer.setError(null);
        }

        presenter.resetPassword(email.getText().toString());
    }

    @Override
    public void showResetPasswordMessage(boolean isSuccess) {
        if (isSuccess) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setMessage(String.format("" +
                                    "Jika ada akun yang terkait dengan %s Anda akan menerima email dengan tautan untuk mereset kata sandi Anda.",
                            email.getText().toString()))
                    .setPositiveButton("OK", ((dialog, which) -> finish()));
            builder.show();
        } else showToastMessage("Terjadi Kesalahan");
    }

    @Override
    public void showEmailNotExistMessage() {
        showToastMessage("Email yang anda masukan tidak terdaftar");
    }

    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}