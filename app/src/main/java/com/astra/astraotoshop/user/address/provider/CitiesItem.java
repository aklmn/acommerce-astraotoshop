package com.astra.astraotoshop.user.address.provider;

import com.google.gson.annotations.SerializedName;

public class CitiesItem {

	@SerializedName("region_id")
	private String regionId;

	@SerializedName("name")
	private String name;

	@SerializedName("city_id")
	private String cityId;

	public void setRegionId(String regionId){
		this.regionId = regionId;
	}

	public String getRegionId(){
		return regionId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setCityId(String cityId){
		this.cityId = cityId;
	}

	public String getCityId(){
		return cityId;
	}

	@Override
 	public String toString(){
		return 
			"CitiesItem{" +
			"region_id = '" + regionId + '\'' + 
			",name = '" + name + '\'' + 
			",city_id = '" + cityId + '\'' + 
			"}";
		}
}