package com.astra.astraotoshop.user.address.provider;

import com.google.gson.annotations.SerializedName;

public class RegionsItem {

    @SerializedName("default_name")
    private String defaultName;

    @SerializedName("code")
    private String code;

    @SerializedName("region_id")
    private String regionId;

    @SerializedName("country_id")
    private String countryId;

    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    public String getDefaultName() {
        return defaultName;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    @Override
    public String toString() {
        return
                "RegionsItem{" +
                        "default_name = '" + defaultName + '\'' +
                        ",code = '" + code + '\'' +
                        ",region_id = '" + regionId + '\'' +
                        ",country_id = '" + countryId + '\'' +
                        "}";
    }
}