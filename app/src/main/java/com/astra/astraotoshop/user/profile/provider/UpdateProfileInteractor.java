package com.astra.astraotoshop.user.profile.provider;

import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 5/17/2018.
 */

public class UpdateProfileInteractor extends BaseInteractor implements ProfileContract.UpdateProfileInteractor {
    private ProfileContract.UpdateProfilePresenter presenter;

    public UpdateProfileInteractor(ProfileContract.UpdateProfilePresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void updateProfile(String storeCode, UpdateProfile profile) {
        getGeneralNetworkManager()
                .updateProfile(new NetworkHandler(), storeCode, profile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.profileUpdated(o),
                        e -> presenter.failedUpdate()
                );
    }
}
