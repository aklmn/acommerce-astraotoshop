package com.astra.astraotoshop.user.wishlist;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.user.wishlist.provider.WishlistItem;
import com.astra.astraotoshop.user.wishlist.provider.WishlistPresenter;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.Loading;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteActivity extends BaseActivity implements ListListener, WishlistContract.WishlistView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.itemCount)
    TextView itemCount;
    @BindView(R.id.list)
    RecyclerView list;

    private WishlistContract.WishlistPresenter presenter;
    private WishlistAdapter adapter;
    private Loading loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_list);
        ButterKnife.bind(this);
        actionBarSetup();
        presenter = new WishlistPresenter(this, null, getStoreCode());
        loading = new Loading(this, getLayoutInflater());
        presenter.requestWishlist();
        loading.show(getString(R.string.message_please_wait));

        adapter = new WishlistAdapter(this, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        list.setLayoutManager(layoutManager);
        list.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        WishlistItem wishlistItem = (WishlistItem) data;
        if (data != null) {
            if (viewId == 0) {
                startActivity(getIntent(this, ProductDetailActivity.class).putExtra("id", wishlistItem.getProductId()));
            } else if (viewId == R.id.iconDelete) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setMessage("Apakah anda yakin untuk menghapus?")
                        .setNegativeButton("Tidak", (dialog, which) -> dialog.dismiss())
                        .setPositiveButton("Ya", ((dialog, which) -> {
                            presenter.deleteItemWishlist(wishlistItem);
                            dialog.dismiss();
                            loading.show("Harap Tunggu");
                        }));
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }
        }
    }

    @Override
    public void showWishlist(List<WishlistItem> wishlistItems) {
        adapter.setWishlistItems(wishlistItems);
        if (wishlistItems.size() > 0) itemCount.setText(wishlistItems.size() + " Produk ditemukan");
        else itemCount.setText("Produk tidak ditemukan");
        loading.dismiss();
    }

    @Override
    public void showMessage(boolean isDelete, String message) {
        loading.dismiss();
        if (isDelete) presenter.requestWishlist();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
