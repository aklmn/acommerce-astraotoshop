package com.astra.astraotoshop.user.order.provider;

import com.astra.astraotoshop.order.history.provider.OrderHistory;
import com.astra.astraotoshop.user.order.OrderContract;
import com.astra.astraotoshop.utils.base.BasePresenter;
import com.astra.astraotoshop.utils.pref.Pref;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henra Setia Nugraha on 2/15/2018.
 */

public class OrderListPresenter extends BasePresenter implements OrderContract.OrderListPresenter {

    private OrderContract.OrderListVIew view;
    private String storeCode;
    private OrderContract.OrderListInteractor interactor;
    private Map<String, String> queries;

    public OrderListPresenter(OrderContract.OrderListVIew view, String storeCode) {
        super(new HashMap<>());
        this.view = view;
        this.storeCode = storeCode;
        queries = new HashMap<>();
        setQueryValue();
        super.setQueries(queries);
        interactor = new OrderListInteractor(this);
    }

    @Override
    public void reset() {
        setQueryValue();
        super.setQueries(queries);
        interactor.requestOrderList(storeCode, getQueries());
    }

    @Override
    public void requestOrderList() {
        interactor.requestOrderList(storeCode, getQueries());
    }

    @Override
    public void onOrderListReceived(OrderHistory orderList) {
        increasePage();
        view.showOrderList(orderList.getItems(), orderList.getTotalCount());
    }

    @Override
    public void onFailedReceivingOrderList(Throwable error) {
        view.showError("Terjadi kesalahan");
    }

    @Override
    public boolean isAddMore() {
        return super.isAddMore();
    }

    private void setQueryValue() {
        queries.put("searchCriteria[pageSize]", "10");
        queries.put("searchCriteria[currentPage]", "1");
        queries.put("searchCriteria[filter_groups][0][filters][0][field]", "customer_email");
        queries.put("searchCriteria[filter_groups][0][filters][0][value]", Pref.getPreference().getString("email"));
    }
}
