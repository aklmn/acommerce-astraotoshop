package com.astra.astraotoshop.user.order.provider;

import com.google.gson.annotations.SerializedName;

public class MethodDescription{

	@SerializedName("COST")
	private int cOST;

	@SerializedName("IMAGE")
	private String iMAGE;

	@SerializedName("WAREHOUSE")
	private String wAREHOUSE;

	@SerializedName("QTY")
	private int qTY;

	@SerializedName("ITEM_ID")
	private String iTEMID;

	@SerializedName("SKU")
	private String sKU;

	@SerializedName("is_additional_shippment")
	private boolean isAdditionalShippment;

	@SerializedName("ESTIMATE")
	private String eSTIMATE;

	@SerializedName("NAME")
	private String nAME;

	public void setCOST(int cOST){
		this.cOST = cOST;
	}

	public int getCOST(){
		return cOST;
	}

	public void setIMAGE(String iMAGE){
		this.iMAGE = iMAGE;
	}

	public String getIMAGE(){
		return iMAGE;
	}

	public void setWAREHOUSE(String wAREHOUSE){
		this.wAREHOUSE = wAREHOUSE;
	}

	public String getWAREHOUSE(){
		return wAREHOUSE;
	}

	public void setQTY(int qTY){
		this.qTY = qTY;
	}

	public int getQTY(){
		return qTY;
	}

	public void setITEMID(String iTEMID){
		this.iTEMID = iTEMID;
	}

	public String getITEMID(){
		return iTEMID;
	}

	public void setSKU(String sKU){
		this.sKU = sKU;
	}

	public String getSKU(){
		return sKU;
	}

	public void setIsAdditionalShippment(boolean isAdditionalShippment){
		this.isAdditionalShippment = isAdditionalShippment;
	}

	public boolean isIsAdditionalShippment(){
		return isAdditionalShippment;
	}

	public void setESTIMATE(String eSTIMATE){
		this.eSTIMATE = eSTIMATE;
	}

	public String getESTIMATE(){
		return eSTIMATE;
	}

	public void setNAME(String nAME){
		this.nAME = nAME;
	}

	public String getNAME(){
		return nAME;
	}

	@Override
 	public String toString(){
		return 
			"MethodDescription{" + 
			"cOST = '" + cOST + '\'' + 
			",iMAGE = '" + iMAGE + '\'' + 
			",wAREHOUSE = '" + wAREHOUSE + '\'' + 
			",qTY = '" + qTY + '\'' + 
			",iTEM_ID = '" + iTEMID + '\'' + 
			",sKU = '" + sKU + '\'' + 
			",is_additional_shippment = '" + isAdditionalShippment + '\'' + 
			",eSTIMATE = '" + eSTIMATE + '\'' + 
			",nAME = '" + nAME + '\'' + 
			"}";
		}
}