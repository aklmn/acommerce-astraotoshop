package com.astra.astraotoshop.user.signup;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.signup.provider.RegisterPresenter;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.AlertManager;
import com.astra.astraotoshop.utils.view.Loading;
import com.astra.astraotoshop.utils.view.PasswordStrength;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;
import butterknife.OnTouch;

public class RegisterActivity extends BaseActivity implements RegisterContract.RegisterView, DialogInterface.OnDismissListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.firstName)
    EditText firstName;
    @BindView(R.id.lastName)
    EditText lastName;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.firstNameContainer)
    TextInputLayout firstNameContainer;
    @BindView(R.id.lastNameContainer)
    TextInputLayout lastNameContainer;
    @BindView(R.id.emailContainer)
    TextInputLayout emailContainer;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.passwordContainer)
    TextInputLayout passwordContainer;
    @BindView(R.id.confirmPassword)
    EditText confirmPassword;
    @BindView(R.id.confirmPasswordContainer)
    TextInputLayout confirmPasswordContainer;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.password_strength)
    TextView passwordStrength;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.scroll)
    ScrollView scroll;
    @BindView(R.id.npwpLabel)
    TextView npwpLabel;
    @BindView(R.id.npwp)
    EditText npwp;
    @BindView(R.id.ktp)
    EditText ktp;
    @BindView(R.id.address)
    EditText address;
    Loading loading;
    @BindView(R.id.invoiceSpinner)
    Spinner invoiceSpinner;
    @BindView(R.id.newsletter)
    CheckBox newsletter;
    List<Boolean> valid;
    private RegisterContract.RegisterPresenter presenter;
    private PasswordStrength strength;
    private int result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        ButterKnife.bind(this);
        actionBarSetup();
        presenter = new RegisterPresenter(this);
        loading = new Loading(this, getLayoutInflater());
        result = RESULT_CANCELED;
    }

    @OnClick(R.id.submit)
    public void submit() {
        if (isValid()) {
            presenter.submit(
                    getStoreCode(),
                    email.getText().toString(),
                    firstName.getText().toString(),
                    lastName.getText().toString(),
                    password.getText().toString(),
                    invoiceSpinner.getSelectedItemPosition(),
                    npwp.getText().toString(),
                    ktp.getText().toString(),
                    address.getText().toString(),
                    newsletter.isChecked()
            );
            loading.show("Harap tunggu");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        setResult(result);
        finish();
        return true;
    }

    @Override
    public void onRegisterSuccess() {
        loading.dismiss();
        AlertManager.showSimpleMessage(this, "Registrasi berhasil silakan cek email untuk aktifasi", this);
    }

    @Override
    public void onRegisterFailed() {
        loading.dismiss();
        Toast.makeText(this, "Registrasi gagal silakan coba lagi", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        result = RESULT_OK;
        setResult(result);
        finish();
    }

    @OnFocusChange(R.id.password)
    public void onPasswordFocus(View view, boolean isFocus) {
        if (isFocus) {
            scroll.post(() -> scroll.smoothScrollTo(0, passwordStrength.getTop()));
        }
    }

    @OnItemSelected(R.id.invoiceSpinner)
    public void onInvoiceSelected(AdapterView<?> adapter, View view, int position, long id) {
        if (position == 1) {
            npwpLabel.setVisibility(View.VISIBLE);
            npwp.setVisibility(View.VISIBLE);
        } else {
            npwpLabel.setVisibility(View.GONE);
            npwp.setVisibility(View.GONE);
        }
    }

    @OnTextChanged(R.id.password)
    public void passwordTextChanged(CharSequence charSequence, int start, int count, int after) {
        if (charSequence.length() == 0) {
            passwordStrength.setText("Weak");
            progressBar.setProgress(0);
            return;
        }

        strength = PasswordStrength.calculateStrength(charSequence.toString());
        passwordStrength.setText(strength.getText(this));
        passwordStrength.setTextColor(strength.getColor());

        progressBar.getProgressDrawable().setColorFilter(strength.getColor(), PorterDuff.Mode.SRC_IN);
        if (strength.getText(this).equals("Weak")) {
            progressBar.setProgress(25);
        } else if (strength.getText(this).equals("Medium")) {
            progressBar.setProgress(50);
        } else if (strength.getText(this).equals("Strong")) {
            progressBar.setProgress(75);
        } else if (strength.getText(this).equals("Very Strong")) {
            progressBar.setProgress(100);
        }

        passwordContainer.setError(strength.getCurrentScore() < 1 ? "Password belum memenuhi syarat" : "");
    }

    private boolean isValid() {
        valid = new ArrayList<>();
        if (isEmpty(firstName)) {
            firstNameContainer.setError("Nama depan wajib diisi");
            valid.add(false);
            firstNameContainer.requestFocus();
        } else firstNameContainer.setError("");
        if (isEmpty(lastName)) {
            lastNameContainer.setError("Nama belakang wajib diisi");
            valid.add(false);
            lastNameContainer.requestFocus();
        } else lastNameContainer.setError("");
        if (isEmpty(email)) {
            emailContainer.setError("Email wajib diisi");
            valid.add(false);
            emailContainer.requestFocus();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            valid.add(false);
            emailContainer.setError("Format email salah");
        } else emailContainer.setError("");
        if (isEmpty(password)) {
            passwordContainer.setError("Password wajib diisi");
            valid.add(false);
        } else {
            if (strength.getCurrentScore() < 1) {
                valid.add(false);
                passwordContainer.setError("Password belum memenuhi syarat");
            } else passwordContainer.setError("");
        }
        if (isEmpty(confirmPassword)) {
            confirmPasswordContainer.setError("Password wajib diisi");
            valid.add(false);
        } else confirmPasswordContainer.setError("");
        if (invoiceSpinner.getSelectedItemPosition() == 1) {
            if (isEmpty(npwp)) {
                valid.add(false);
                npwp.setError("No NPWP wajib diisi");
            }
            if (isEmpty(ktp)) {
                valid.add(false);
                ktp.setError("No KTP wajib diisi");
            } else if (ktp.getText().length() != 16) {
                valid.add(false);
                ktp.setError("No KTP harus 16 digit angka");
            }
            if (isEmpty(address)) {
                valid.add(false);
                address.setError("Alamat wajib diisi");
            }
        }

        if (valid.contains(false)) {
            return false;
        }

        if (!password.getText().toString().equals(confirmPassword.getText().toString())) {
            Toast.makeText(this, "Password tidak sama", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean isEmpty(EditText firstName) {
        return TextUtils.isEmpty(firstName.getText().toString());
    }

    @OnFocusChange({R.id.lastName, R.id.firstName, R.id.npwp, R.id.ktp, R.id.address, R.id.email, R.id.password, R.id.confirmPassword})
    public void fc(View view, boolean isFocused) {
        lastName.setCursorVisible(isFocused);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!isFocused)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @OnTouch(R.id.invoiceSpinner)
    public boolean onInvoiceClick(View view, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP) {
            invoiceSpinner.requestFocus();
            invoiceSpinner.performClick();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        return true;
    }
}
