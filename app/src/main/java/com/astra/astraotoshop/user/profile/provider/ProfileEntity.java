package com.astra.astraotoshop.user.profile.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileEntity {

    @SerializedName("store_id")
    private int storeId;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("addresses")
    private List<Addresses> addresses;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("custom_attributes")
    private List<CustomAttributesItem> customAttributes;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("disable_auto_group_change")
    private int disableAutoGroupChange;

    @SerializedName("group_id")
    private int groupId;

    @SerializedName("id")
    private int id;

    @SerializedName("website_id")
    private int websiteId;

    @SerializedName("email")
    private String email;

    @SerializedName("created_in")
    private String createdIn;


    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setAddresses(List<Addresses> addresses) {
        this.addresses = addresses;
    }

    public List<Addresses> getAddresses() {
        return addresses;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setCustomAttributes(List<CustomAttributesItem> customAttributes) {
        this.customAttributes = customAttributes;
    }

    public List<CustomAttributesItem> getCustomAttributes() {
        return customAttributes;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setDisableAutoGroupChange(int disableAutoGroupChange) {
        this.disableAutoGroupChange = disableAutoGroupChange;
    }

    public int getDisableAutoGroupChange() {
        return disableAutoGroupChange;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setWebsiteId(int websiteId) {
        this.websiteId = websiteId;
    }

    public int getWebsiteId() {
        return websiteId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setCreatedIn(String createdIn) {
        this.createdIn = createdIn;
    }

    public String getCreatedIn() {
        return createdIn;
    }

    @Override
    public String toString() {
        return
                "ProfileEntity{" +
                        "store_id = '" + storeId + '\'' +
                        ",firstname = '" + firstname + '\'' +
                        ",addresses = '" + addresses + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",lastname = '" + lastname + '\'' +
                        ",custom_attributes = '" + customAttributes + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",disable_auto_group_change = '" + disableAutoGroupChange + '\'' +
                        ",group_id = '" + groupId + '\'' +
                        ",id = '" + id + '\'' +
                        ",website_id = '" + websiteId + '\'' +
                        ",email = '" + email + '\'' +
                        ",created_in = '" + createdIn + '\'' +
                        "}";
    }
}