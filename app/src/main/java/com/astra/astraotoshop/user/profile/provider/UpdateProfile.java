package com.astra.astraotoshop.user.profile.provider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Henra Setia Nugraha on 5/17/2018.
 */

public class UpdateProfile {
    @SerializedName("customer")
    private ProfileEntity entity;

    public UpdateProfile() {
    }

    public UpdateProfile(ProfileEntity entity) {
        this.entity = entity;
    }

    public ProfileEntity getEntity() {
        return entity;
    }

    public void setEntity(ProfileEntity entity) {
        this.entity = entity;
    }
}
