package com.astra.astraotoshop.user.voucher.provider;

import com.astra.astraotoshop.user.voucher.VoucherContract;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherList;

/**
 * Created by Henra Setia Nugraha on 4/9/2018.
 */

public class VoucherListPresenter implements VoucherContract.VoucherListPresenter {

    private VoucherContract.VoucherListView view;
    private VoucherContract.VoucherListInteractor interactor;
    private String storeCode;

    public VoucherListPresenter(VoucherContract.VoucherListView view, String storeCode) {
        this.view = view;
        this.storeCode = storeCode;
        interactor = new VoucherListInteractor(this);
    }

    @Override
    public void getVoucherList() {
        interactor.getVoucherList(storeCode);
    }

    @Override
    public void onVouchersReceived(VoucherList voucherList) {
        view.showVouchers(voucherList.getVouchersItem());
    }

    @Override
    public void onFailedReceiveVouchers(Throwable e) {
        view.showErrorMessage();
    }
}
