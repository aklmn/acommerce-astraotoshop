package com.astra.astraotoshop.user.profile;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.astra.astraotoshop.Application.AOPApplication;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.history.OrderHistoryActivity;
import com.astra.astraotoshop.user.address.AddressListActivity;
import com.astra.astraotoshop.user.password.ChangeChangePasswordActivity;
import com.astra.astraotoshop.user.review.ReviewActivity;
import com.astra.astraotoshop.user.subsricption.SubscriptionActivity;
import com.astra.astraotoshop.user.voucher.VoucherActivity;
import com.astra.astraotoshop.user.wishlist.FavoriteActivity;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.pref.Pref;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileMenuActivity extends BaseActivity implements ProfileMenuAdapter.OnItemClickListener {

    @BindView(R.id.profile_name)
    TextView profileName;

    @BindView(R.id.profile_email)
    TextView profileEmail;

    @BindView(R.id.profile_update)
    TextView profileUpdate;

    @BindView(R.id.profile_list_menu)
    RecyclerView profileListMenu;

    @BindView(R.id.profile_signout)
    TextView profileSignout;

    @BindView(R.id.scroll)
    NestedScrollView scroll;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_menu);
        ButterKnife.bind(this);
        actionBarSetup();

        String[] icon;
        String[] label;
        int[] ids;
        if (getState() == getResources().getInteger(R.integer.state_product)) {
            TypedArray typedArray = getResources().obtainTypedArray(R.array.product_menu_icon);
            TypedArray typedArrayId = getResources().obtainTypedArray(R.array.product_menu_id);
            icon = typedArray.getResources().getStringArray(R.array.product_menu_icon);
            ids = getAllId(typedArrayId);
            typedArrayId.recycle();
            typedArray.recycle();

            label = getResources().getStringArray(R.array.product_menu_label);
        } else {
            TypedArray typedArray = getResources().obtainTypedArray(R.array.product_service_menu_icon);
            TypedArray typedArrayId = getResources().obtainTypedArray(R.array.product_service_menu_id);
            icon = typedArray.getResources().getStringArray(R.array.product_service_menu_icon);
            ids = getAllId(typedArrayId);
            typedArray.recycle();

            label = getResources().getStringArray(R.array.product_service_menu_label);
        }
        ProfileMenuAdapter adapter = new ProfileMenuAdapter(label, icon, ids, this, this);
        profileListMenu.setLayoutManager(new LinearLayoutManager(this));
        profileListMenu.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    protected void onResume() {
        scroll.fullScroll(View.FOCUS_UP);
        setValue();
        super.onResume();
    }

    @Override
    public void onItemClick(int id) {
        if (id == R.id.idAddress)
            this.startActivityForResult((getIntent(this, AddressListActivity.class)).putExtra("isFromCheckout", false), 3);
        else if (id == R.id.idOrderHistory)
            startActivity(getIntent(this, OrderHistoryActivity.class));
        else if (id == R.id.idVoucher)
            startActivity(getIntent(this, VoucherActivity.class));
        else if (id == R.id.idSubscription)
            startActivity(getIntent(this, SubscriptionActivity.class));
        else if (id == R.id.idReview)
            startActivity(getIntent(this, ReviewActivity.class));
        else if (id == R.id.idFavorite)
            startActivity(getIntent(this, FavoriteActivity.class));
        else if (id == R.id.idChangePassword)
            startActivity(getIntent(this, ChangeChangePasswordActivity.class));
    }

    @OnClick(R.id.profile_update)
    public void editProfilePressed() {
        startActivity(getIntent(this, EditProfileActivity.class));
    }

    @OnClick(R.id.profile_signout)
    public void sigOut() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage("Apakah anda yakin?")
                .setPositiveButton("YA", (dialog, which) -> {
                    AOPApplication.forceLogout(getStoreCode());
                    finish();
                })
                .setNegativeButton("TIDAK", (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    private void setValue() {
        profileName.setText(Pref.getPreference().getString("username"));
        profileEmail.setText(Pref.getPreference().getString("email"));
    }

    private int[] getAllId(TypedArray typedArray) {
        int[] data = new int[typedArray.length()];
        for (int i = 0; i < typedArray.length(); i++) {
            data[i] = typedArray.getResourceId(i, 0);
        }
        return data;
    }
}
