package com.astra.astraotoshop.user.signup.provider;

import com.astra.astraotoshop.user.signup.RegisterContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 2/15/2018.
 */

public class RegisterInteractor extends BaseInteractor implements RegisterContract.RegisterInteractor {
    private RegisterContract.RegisterPresenter presenter;

    public RegisterInteractor(RegisterContract.RegisterPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void submit(String storeCode, RegisterBody registerBody) {
        getGeneralNetworkManager()
                .register(
                        new NetworkHandler(),
                        storeCode,
                        registerBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        register -> presenter.onRegisterSuccess(),
                        error -> presenter.onRegisterFailed()
                );
    }
}
