package com.astra.astraotoshop.user.voucher.provider.model;

import com.google.gson.annotations.SerializedName;

public class ShippingAddress {

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("ar_overdue")
    private Object arOverdue;

    @SerializedName("city")
    private String city;

    @SerializedName("prefix")
    private String prefix;

    @SerializedName("suffix")
    private Object suffix;

    @SerializedName("division")
    private Object division;

    @SerializedName("vat_request_date")
    private Object vatRequestDate;

    @SerializedName("vat_request_id")
    private Object vatRequestId;

    @SerializedName("street")
    private String street;

    @SerializedName("mobile_phone")
    private String mobilePhone;

    @SerializedName("vat_request_success")
    private Object vatRequestSuccess;

    @SerializedName("vat_id")
    private Object vatId;

    @SerializedName("company")
    private Object company;

    @SerializedName("fax")
    private String fax;

    @SerializedName("email")
    private String email;

    @SerializedName("rt_rw")
    private String rtRw;

    @SerializedName("partner_function")
    private Object partnerFunction;

    @SerializedName("customer_address_id")
    private String customerAddressId;

    @SerializedName("address_type")
    private String addressType;

    @SerializedName("ktp")
    private String ktp;

    @SerializedName("dist_channel")
    private Object distChannel;

    @SerializedName("region_id")
    private String regionId;

    @SerializedName("postcode")
    private String postcode;

    @SerializedName("npwp")
    private String npwp;

    @SerializedName("middlename")
    private String middlename;

    @SerializedName("ktp_address")
    private String ktpAddress;

    @SerializedName("telephone")
    private String telephone;

    @SerializedName("djp")
    private String djp;

    @SerializedName("entity_id")
    private String entityId;

    @SerializedName("kelurahan")
    private String kelurahan;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("giftregistry_item_id")
    private Object giftregistryItemId;

    @SerializedName("parent_id")
    private String parentId;

    @SerializedName("block_flag")
    private Object blockFlag;

    @SerializedName("vat_is_valid")
    private Object vatIsValid;

    @SerializedName("sales_org")
    private Object salesOrg;

    @SerializedName("customer_id")
    private String customerId;

    @SerializedName("region")
    private String region;

    @SerializedName("price_list_type")
    private Object priceListType;

    @SerializedName("quote_address_id")
    private Object quoteAddressId;

    @SerializedName("country_id")
    private String countryId;

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setArOverdue(Object arOverdue) {
        this.arOverdue = arOverdue;
    }

    public Object getArOverdue() {
        return arOverdue;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setSuffix(Object suffix) {
        this.suffix = suffix;
    }

    public Object getSuffix() {
        return suffix;
    }

    public void setDivision(Object division) {
        this.division = division;
    }

    public Object getDivision() {
        return division;
    }

    public void setVatRequestDate(Object vatRequestDate) {
        this.vatRequestDate = vatRequestDate;
    }

    public Object getVatRequestDate() {
        return vatRequestDate;
    }

    public void setVatRequestId(Object vatRequestId) {
        this.vatRequestId = vatRequestId;
    }

    public Object getVatRequestId() {
        return vatRequestId;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet() {
        return street;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setVatRequestSuccess(Object vatRequestSuccess) {
        this.vatRequestSuccess = vatRequestSuccess;
    }

    public Object getVatRequestSuccess() {
        return vatRequestSuccess;
    }

    public void setVatId(Object vatId) {
        this.vatId = vatId;
    }

    public Object getVatId() {
        return vatId;
    }

    public void setCompany(Object company) {
        this.company = company;
    }

    public Object getCompany() {
        return company;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFax() {
        return fax;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setRtRw(String rtRw) {
        this.rtRw = rtRw;
    }

    public String getRtRw() {
        return rtRw;
    }

    public void setPartnerFunction(Object partnerFunction) {
        this.partnerFunction = partnerFunction;
    }

    public Object getPartnerFunction() {
        return partnerFunction;
    }

    public void setCustomerAddressId(String customerAddressId) {
        this.customerAddressId = customerAddressId;
    }

    public String getCustomerAddressId() {
        return customerAddressId;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setKtp(String ktp) {
        this.ktp = ktp;
    }

    public String getKtp() {
        return ktp;
    }

    public void setDistChannel(Object distChannel) {
        this.distChannel = distChannel;
    }

    public Object getDistChannel() {
        return distChannel;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setKtpAddress(String ktpAddress) {
        this.ktpAddress = ktpAddress;
    }

    public String getKtpAddress() {
        return ktpAddress;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setDjp(String djp) {
        this.djp = djp;
    }

    public String getDjp() {
        return djp;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setGiftregistryItemId(Object giftregistryItemId) {
        this.giftregistryItemId = giftregistryItemId;
    }

    public Object getGiftregistryItemId() {
        return giftregistryItemId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setBlockFlag(Object blockFlag) {
        this.blockFlag = blockFlag;
    }

    public Object getBlockFlag() {
        return blockFlag;
    }

    public void setVatIsValid(Object vatIsValid) {
        this.vatIsValid = vatIsValid;
    }

    public Object getVatIsValid() {
        return vatIsValid;
    }

    public void setSalesOrg(Object salesOrg) {
        this.salesOrg = salesOrg;
    }

    public Object getSalesOrg() {
        return salesOrg;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setPriceListType(Object priceListType) {
        this.priceListType = priceListType;
    }

    public Object getPriceListType() {
        return priceListType;
    }

    public void setQuoteAddressId(Object quoteAddressId) {
        this.quoteAddressId = quoteAddressId;
    }

    public Object getQuoteAddressId() {
        return quoteAddressId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    @Override
    public String toString() {
        return
                "ShippingAddress{" +
                        "firstname = '" + firstname + '\'' +
                        ",ar_overdue = '" + arOverdue + '\'' +
                        ",city = '" + city + '\'' +
                        ",prefix = '" + prefix + '\'' +
                        ",suffix = '" + suffix + '\'' +
                        ",division = '" + division + '\'' +
                        ",vat_request_date = '" + vatRequestDate + '\'' +
                        ",vat_request_id = '" + vatRequestId + '\'' +
                        ",street = '" + street + '\'' +
                        ",mobile_phone = '" + mobilePhone + '\'' +
                        ",vat_request_success = '" + vatRequestSuccess + '\'' +
                        ",vat_id = '" + vatId + '\'' +
                        ",company = '" + company + '\'' +
                        ",fax = '" + fax + '\'' +
                        ",email = '" + email + '\'' +
                        ",rt_rw = '" + rtRw + '\'' +
                        ",partner_function = '" + partnerFunction + '\'' +
                        ",customer_address_id = '" + customerAddressId + '\'' +
                        ",address_type = '" + addressType + '\'' +
                        ",ktp = '" + ktp + '\'' +
                        ",dist_channel = '" + distChannel + '\'' +
                        ",region_id = '" + regionId + '\'' +
                        ",postcode = '" + postcode + '\'' +
                        ",npwp = '" + npwp + '\'' +
                        ",middlename = '" + middlename + '\'' +
                        ",ktp_address = '" + ktpAddress + '\'' +
                        ",telephone = '" + telephone + '\'' +
                        ",djp = '" + djp + '\'' +
                        ",entity_id = '" + entityId + '\'' +
                        ",kelurahan = '" + kelurahan + '\'' +
                        ",lastname = '" + lastname + '\'' +
                        ",giftregistry_item_id = '" + giftregistryItemId + '\'' +
                        ",parent_id = '" + parentId + '\'' +
                        ",block_flag = '" + blockFlag + '\'' +
                        ",vat_is_valid = '" + vatIsValid + '\'' +
                        ",sales_org = '" + salesOrg + '\'' +
                        ",customer_id = '" + customerId + '\'' +
                        ",region = '" + region + '\'' +
                        ",price_list_type = '" + priceListType + '\'' +
                        ",quote_address_id = '" + quoteAddressId + '\'' +
                        ",country_id = '" + countryId + '\'' +
                        "}";
    }
}