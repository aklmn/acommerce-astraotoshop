package com.astra.astraotoshop.user.order.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderList {

    @SerializedName("items")
    private List<OrdersItem> items;

    public void setItems(List<OrdersItem> items) {
        this.items = items;
    }

    public List<OrdersItem> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return
                "OrderList{" +
                        "items = '" + items + '\'' +
                        "}";
    }
}