package com.astra.astraotoshop.user.password.provider;

import com.astra.astraotoshop.user.password.PasswordContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 5/15/2018.
 */

public class ChangePasswordInteractor extends BaseInteractor implements PasswordContract.ChangePasswordInteractor {

    private PasswordContract.ChangePasswordPresenter presenter;

    public ChangePasswordInteractor(PasswordContract.ChangePasswordPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void changePassword(String storeCode, ChangePassword changePassword) {
        getGeneralNetworkManager()
                .changePassword(new NetworkHandler(), storeCode, changePassword)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.onPasswordChanged(),
                        e -> presenter.onPasswordUnchanged()
                );
    }
}
