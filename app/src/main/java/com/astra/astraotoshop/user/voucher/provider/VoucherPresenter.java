package com.astra.astraotoshop.user.voucher.provider;

import com.astra.astraotoshop.user.voucher.VoucherContract;
import com.astra.astraotoshop.user.voucher.provider.VoucherInteractor;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherDetail;

/**
 * Created by Henra Setia Nugraha on 4/9/2018.
 */

public class VoucherPresenter implements VoucherContract.VoucherPresenter {
    private VoucherContract.VoucherView view;
    private VoucherContract.VoucherInteractor interactor;
    private String storeCode;

    public VoucherPresenter(VoucherContract.VoucherView view, String storeCode) {
        this.view = view;
        this.storeCode = storeCode;
        interactor = new VoucherInteractor(this);
    }

    @Override
    public void getVoucher(String voucherId) {
        interactor.getVoucher(storeCode, voucherId);
    }

    @Override
    public void onVouchersReceived(VoucherDetail detail) {
        view.showVouchers(detail);
    }

    @Override
    public void onFailedReceiveVouchers(Throwable e) {
        view.showErrorMessage();
    }
}
