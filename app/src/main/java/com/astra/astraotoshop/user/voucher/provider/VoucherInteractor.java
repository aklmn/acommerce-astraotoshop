package com.astra.astraotoshop.user.voucher.provider;

import com.astra.astraotoshop.user.voucher.VoucherContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 4/9/2018.
 */

public class VoucherInteractor extends BaseInteractor implements VoucherContract.VoucherInteractor {
    private VoucherContract.VoucherPresenter presenter;

    public VoucherInteractor(VoucherContract.VoucherPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void getVoucher(String storeCode, String voucherId) {
        getGeneralNetworkManager()
                .requestVoucher(new NetworkHandler(), storeCode, voucherId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        voucherDetail -> presenter.onVouchersReceived(voucherDetail),
                        e -> presenter.onFailedReceiveVouchers(e)
                );
    }
}
