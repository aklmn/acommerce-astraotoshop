package com.astra.astraotoshop.user.voucher.provider.model;

import com.google.gson.annotations.SerializedName;

public class Slot {

    @SerializedName("from_hour")
    private String fromHour;

    @SerializedName("to_hour")
    private String toHour;

    @SerializedName("name")
    private String name;

    @SerializedName("date")
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setFromHour(String fromHour) {
        this.fromHour = fromHour;
    }

    public String getFromHour() {
        return fromHour;
    }

    public void setToHour(String toHour) {
        this.toHour = toHour;
    }

    public String getToHour() {
        return toHour;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return
                "Slot{" +
                        "from_hour = '" + fromHour + '\'' +
                        ",to_hour = '" + toHour + '\'' +
                        ",name = '" + name + '\'' +
                        "}";
    }
}