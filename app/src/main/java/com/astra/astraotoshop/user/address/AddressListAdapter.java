package com.astra.astraotoshop.user.address;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.user.profile.provider.Addresses;
import com.astra.astraotoshop.utils.view.FontIcon;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 11/17/2017.
 */

public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.AddressListViewHolder> {

    private Context context;
    private List<Addresses> addresses;
    private ListListener listener;

    public AddressListAdapter(Context context) {
        this.context = context;
    }

    public AddressListAdapter(Context context, ListListener listener) {
        this.context = context;
        this.listener = listener;
        addresses = new ArrayList<>();
    }

    @Override
    public AddressListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddressListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address_list, parent, false));
    }

    @Override
    public void onBindViewHolder(AddressListViewHolder holder, int position) {
        holder.name.setText(addresses.get(position).getFirstname() + " " + addresses.get(position).getLastname());
        holder.address.setText(getAddress(addresses.get(position)));

        if (addresses.get(position).isDefaultShipping())
            holder.check.setText(context.getString(R.string.circle_check));
        else holder.check.setText(context.getString(R.string.circle));
    }

    private String getAddress(Addresses addresses) {
        String address = "";
        address = addressBuilder(address, addresses.getStreet().get(0));
        try {
            address = addressBuilder(address, addresses.getCustomAttributes().get(1).getValue());
            address = addressBuilder(address, addresses.getCustomAttributes().get(0).getValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        address = addressBuilder(address, addresses.getCity());
        address = addressBuilder(address, addresses.getRegion().getRegion());
        address = addressBuilder(address, addresses.getPostcode());
        try {
            address=addressBuilder(address,addresses.getCustomAttributes().get(2).getValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return address;
    }

    private String addressBuilder(String exist, String value) {
        if (value != null) exist += value + ", ";
        return exist;
    }

    @Override
    public int getItemCount() {
        return addresses.size();
    }

    public void setAddresses(List<Addresses> addresses) {
        this.addresses = addresses;
        notifyDataSetChanged();
    }

    class AddressListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.check)
        FontIcon check;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.address)
        TextView address;

        public AddressListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @OnClick(R.id.check)
        public void onChecked() {
            try {
                listener.onItemClick(R.id.check, getAdapterPosition(), addresses.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onClick(View v) {
            try {
                listener.onItemClick(v.getId(),getAdapterPosition(),addresses.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
