package com.astra.astraotoshop.user.voucher;

import com.astra.astraotoshop.user.voucher.provider.model.VoucherData;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherDetail;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherList;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 4/9/2018.
 */

public interface VoucherContract {
    interface VoucherListView{
        void showVouchers(List<VoucherData> vouchers);
        void showErrorMessage();
    }

    interface VoucherListPresenter{
        void getVoucherList();
        void onVouchersReceived(VoucherList voucherList);
        void onFailedReceiveVouchers(Throwable e);
    }

    interface VoucherListInteractor{
        void getVoucherList(String storeCode);
    }

    interface VoucherView{
        void showVouchers(VoucherDetail detail);
        void showErrorMessage();
    }

    interface VoucherPresenter{
        void getVoucher(String voucherId);
        void onVouchersReceived(VoucherDetail detail);
        void onFailedReceiveVouchers(Throwable e);
    }

    interface VoucherInteractor{
        void getVoucher(String storeCode,String voucherId);
    }
}
