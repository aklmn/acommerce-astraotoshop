package com.astra.astraotoshop.user.subsricption;

import com.astra.astraotoshop.user.subsricption.provider.Subscription;

/**
 * Created by Henra Setia Nugraha on 4/3/2018.
 */

public interface SubscriptionContract {
    interface SubscriptionView{
        void subscription(boolean isSubscribe);
        void showMessage(String message);
    }

    interface SubscriptionPresenter{
        void getSubscription();
        void onSubscriptionReceived(Subscription subscription);
        void onFailedReceivingSubscription(Throwable e);

        void changeSubscription(boolean state);
        void onSubscriptionChanged();
        void onFailedChangeSubscription(Throwable e);
    }

    interface SubscriptionInteractor{
        void changeSubscription(String state,String storeCode);
        void getSubscription(String storeCode);
    }
}
