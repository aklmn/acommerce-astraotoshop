package com.astra.astraotoshop.user.review;

import com.astra.astraotoshop.user.review.provider.CustomerReview;
import com.astra.astraotoshop.user.review.provider.ReviewsItem;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 3/27/2018.
 */

public interface CustomerReviewContract {
    interface View{
        void showReviews(List<ReviewsItem> reviewsItems);
        void showError(String message);
    }

    interface ReviewPresenter{
        void requestReview(String storeCode);
        void onReviewReceived(CustomerReview review);
        void onFailedReceive(Throwable e);
    }

    interface ReviewInteractor{
        void requestReview(String storeCode);
    }
}
