package com.astra.astraotoshop.user.wishlist.provider;

import com.astra.astraotoshop.user.wishlist.WishlistContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 2/22/2018.
 */

public class WishlistInteractor extends BaseInteractor implements WishlistContract.WishlistInteractor {

    private WishlistContract.WishlistPresenter presenter;

    public WishlistInteractor(WishlistContract.WishlistPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestWishlist(String storeCode) {
        getGeneralNetworkManager()
                .getWishlist(
                        new NetworkHandler(),
                        storeCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        wishlistEntity -> presenter.onWishlistReceived(wishlistEntity),
                        e -> presenter.onFailedReceivingWishlist(e)
                );
    }

    @Override
    public void addWishlist(String storeCode, AddWishlistBody wishlistBody) {
        getGeneralNetworkManager()
                .addWishlist(
                        new NetworkHandler(),
                        storeCode,
                        wishlistBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.onAddSucceed(),
                        e -> presenter.onAddFailed(e)
                );
    }

    @Override
    public void deleteItemWishlist(String storeCode, String wishlistItemId) {
        getGeneralNetworkManager()
                .deleteWishlist(
                        new NetworkHandler(),
                        storeCode,
                        wishlistItemId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.onDeleteSucceed(),
                        e -> presenter.onDeleteFailed(e)
                );
    }
}
