package com.astra.astraotoshop.user.address.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegionEntity {

    @SerializedName("items")
    private List<RegionsItem> items;

    public void setItems(List<RegionsItem> items) {
        this.items = items;
    }

    public List<RegionsItem> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return
                "RegionEntity{" +
                        "items = '" + items + '\'' +
                        "}";
    }
}