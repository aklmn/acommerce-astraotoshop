package com.astra.astraotoshop.user.signin.provider;

import android.os.Bundle;

import com.astra.astraotoshop.user.signin.LoginContract;
import com.astra.astraotoshop.utils.pref.Pref;
import com.facebook.GraphRequest;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.gson.Gson;

import org.json.JSONException;

/**
 * Created by Henra Setia Nugraha on 7/10/2018.
 */

public class LoginPresenter implements LoginContract.LoginPresenter {

    private LoginContract.LoginView view;
    private String storeProduct;
    private String storeProductService;
    private LoginContract.LoginInteractor interactor;

    public LoginPresenter(LoginContract.LoginView view, String storeProduct, String storeProductService) {
        this.view = view;
        this.storeProduct = storeProduct;
        this.storeProductService = storeProductService;
        interactor = new LoginInteractor(this);

    }

    @Override
    public void requestFB(String tokenFB, String firstName, String lastName, String email) {
        interactor.loginFB(tokenFB, firstName, lastName, email);
    }

    @Override
    public void FBSuccess(Object data) {
        extractData(data, "Facebook");
    }

    @Override
    public void FBFailed() {
        view.onLoginFailed("Facebook");
    }

    @Override
    public void requestG(GoogleSignInAccount account, String token) {
        interactor.loginG(
                token,
                account.getGivenName(),
                account.getFamilyName(),
                account.getEmail());
    }

    @Override
    public void GSuccess(Object data) {
        extractData(data, "Google");
    }

    @Override
    public void GFailed(Throwable throwable) {
        view.onLoginFailed("Google");
    }

    @Override
    public void getFBData(LoginResult loginResult) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),
                (jsonObject, graphResponse) -> {
                    try {
                        if (jsonObject.getString("email") == null) return;
                        interactor.loginFB(
                                loginResult.getAccessToken().getToken(),
                                jsonObject.getString("first_name"),
                                jsonObject.getString("last_name"),
                                jsonObject.getString("email"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
        );
        Bundle params = new Bundle();
        params.putString("fields", "first_name,middle_name,last_name,email");
        graphRequest.setParameters(params);
        graphRequest.executeAsync();
    }

    private void extractData(Object data, String platform) {
        if (data instanceof String) {
            SocialLogin socialLogin;
            try {
                socialLogin = new Gson().fromJson(data.toString(), SocialLogin.class);
                saveCustomerData(socialLogin);
                view.onLoginSuccess();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            view.onLoginFailed(platform);
        }
    }

    private void saveCustomerData(SocialLogin socialLogin) {
        Pref.getPreference().putString(storeProduct, socialLogin.getCustomerToken());
        Pref.getPreference().putString(storeProductService, socialLogin.getCustomerToken());
    }
}
