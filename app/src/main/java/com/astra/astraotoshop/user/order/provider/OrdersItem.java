package com.astra.astraotoshop.user.order.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OrdersItem implements Parcelable{

    @SerializedName("shipping_amount")
    private String shippingAmount;

    @SerializedName("status_label")
    private String statusLabel;

    @SerializedName("increment_id")
    private String incrementId;

    @SerializedName("subtotal")
    private String subtotal;

    @SerializedName("discount_amount")
    private String discountAmount;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("grand_total")
    private String grandTotal;

    @SerializedName("entity_id")
    private String entityId;

    @SerializedName("status")
    private String status;

    protected OrdersItem(Parcel in) {
        shippingAmount = in.readString();
        statusLabel = in.readString();
        incrementId = in.readString();
        subtotal = in.readString();
        discountAmount = in.readString();
        createdAt = in.readString();
        grandTotal = in.readString();
        entityId = in.readString();
        status = in.readString();
    }

    public static final Creator<OrdersItem> CREATOR = new Creator<OrdersItem>() {
        @Override
        public OrdersItem createFromParcel(Parcel in) {
            return new OrdersItem(in);
        }

        @Override
        public OrdersItem[] newArray(int size) {
            return new OrdersItem[size];
        }
    };

    public void setShippingAmount(String shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    public String getShippingAmount() {
        return shippingAmount;
    }

    public void setStatusLabel(String statusLabel) {
        this.statusLabel = statusLabel;
    }

    public String getStatusLabel() {
        return statusLabel;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return
                "OrdersItem{" +
                        "shipping_amount = '" + shippingAmount + '\'' +
                        ",status_label = '" + statusLabel + '\'' +
                        ",increment_id = '" + incrementId + '\'' +
                        ",subtotal = '" + subtotal + '\'' +
                        ",discount_amount = '" + discountAmount + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",grand_total = '" + grandTotal + '\'' +
                        ",entity_id = '" + entityId + '\'' +
                        ",status = '" + status + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(shippingAmount);
        dest.writeString(statusLabel);
        dest.writeString(incrementId);
        dest.writeString(subtotal);
        dest.writeString(discountAmount);
        dest.writeString(createdAt);
        dest.writeString(grandTotal);
        dest.writeString(entityId);
        dest.writeString(status);
    }
}