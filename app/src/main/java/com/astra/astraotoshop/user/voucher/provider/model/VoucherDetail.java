package com.astra.astraotoshop.user.voucher.provider.model;

import com.google.gson.annotations.SerializedName;

public class VoucherDetail {

    @SerializedName("voucher_status")
    private String voucherStatus;

    @SerializedName("voucher_code")
    private String voucherCode;

    @SerializedName("voucher_expired_at")
    private String voucherExpiredAt;

    @SerializedName("order")
    private Order order;

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherExpiredAt(String voucherExpiredAt) {
        this.voucherExpiredAt = voucherExpiredAt;
    }

    public String getVoucherExpiredAt() {
        return voucherExpiredAt;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    @Override
    public String toString() {
        return
                "VoucherDetail{" +
                        "voucher_status = '" + voucherStatus + '\'' +
                        ",voucher_code = '" + voucherCode + '\'' +
                        ",voucher_expired_at = '" + voucherExpiredAt + '\'' +
                        ",order = '" + order + '\'' +
                        "}";
    }
}