package com.astra.astraotoshop.user.subsricption.provider;

import com.astra.astraotoshop.user.subsricption.SubscriptionContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 4/3/2018.
 */

public class SubscriptionInteractor extends BaseInteractor implements SubscriptionContract.SubscriptionInteractor {

    private SubscriptionContract.SubscriptionPresenter presenter;

    public SubscriptionInteractor(SubscriptionContract.SubscriptionPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void changeSubscription(String state, String storeCode) {
        getGeneralNetworkManager()
                .changeSubscription(new NetworkHandler(), state, storeCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        s -> presenter.onSubscriptionChanged(),
                        e -> presenter.onFailedChangeSubscription(e)
                );
    }

    @Override
    public void getSubscription(String storeCode) {
        getGeneralNetworkManager()
                .getSubscription(new NetworkHandler(), storeCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        subscription -> presenter.onSubscriptionReceived(subscription),
                        e -> presenter.onFailedReceivingSubscription(e)
                );
    }
}
