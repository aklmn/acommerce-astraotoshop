package com.astra.astraotoshop.user.signup.provider;

import com.astra.astraotoshop.user.signup.RegisterContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 2/15/2018.
 */

public class RegisterPresenter implements RegisterContract.RegisterPresenter {

    private RegisterContract.RegisterView view;
    private RegisterContract.RegisterInteractor interactor;

    public RegisterPresenter(RegisterContract.RegisterView view) {
        this.view = view;
        interactor = new RegisterInteractor(this);
    }

    @Override
    public void submit(String storeCode, String email, String firstName, String lastName, String password, int djp, String npwp, String ktp, String address, boolean isSubscribe) {

        List<CustomAttributesItem> attributesItems = new ArrayList<>();
        CustomAttributesItem item;
        item = new CustomAttributesItem("djp", String.valueOf(djp));
        attributesItems.add(item);
        if (djp == 1) {
            item = new CustomAttributesItem("npwp", npwp);
            attributesItems.add(item);
            item = new CustomAttributesItem("address_npwp", address);
            attributesItems.add(item);
        }

        Customer customer = new Customer();
        customer.setEmail(email);
        customer.setFirstname(firstName);
        customer.setLastname(lastName);
        customer.setGroupId(1);
        customer.setCustomAttributes(attributesItems);
        customer.setExtensionAttributes(new ExtensionAttributes(isSubscribe));

        interactor.submit(
                storeCode,
                new RegisterBody(
                        password,
                        customer
                ));
    }

    @Override
    public void onRegisterSuccess() {
        view.onRegisterSuccess();
    }

    @Override
    public void onRegisterFailed() {
        view.onRegisterFailed();
    }
}
