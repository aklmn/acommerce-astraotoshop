package com.astra.astraotoshop.user.voucher.provider;

import com.astra.astraotoshop.user.voucher.VoucherContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 4/9/2018.
 */

public class VoucherListInteractor extends BaseInteractor implements VoucherContract.VoucherListInteractor {

    private VoucherContract.VoucherListPresenter presenter;

    public VoucherListInteractor(VoucherContract.VoucherListPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void getVoucherList(String storeCode) {
        getGeneralNetworkManager()
                .requestVoucherList(new NetworkHandler(), storeCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        voucherList ->presenter.onVouchersReceived(voucherList),
                        e->presenter.onFailedReceiveVouchers(e)
                );
    }
}
