package com.astra.astraotoshop.user.signin.provider;

import com.google.gson.annotations.SerializedName;

public class SocialLogin {

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("lock_expires")
    private Object lockExpires;

    @SerializedName("gender")
    private Object gender;

    @SerializedName("prefix")
    private Object prefix;

    @SerializedName("failures_num")
    private Object failuresNum;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("rp_token")
    private String rpToken;

    @SerializedName("suffix")
    private Object suffix;

    @SerializedName("default_shipping")
    private Object defaultShipping;

    @SerializedName("first_failure")
    private Object firstFailure;

    @SerializedName("productservice_group")
    private String productserviceGroup;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("increment_id")
    private Object incrementId;

    @SerializedName("password_hash")
    private Object passwordHash;

    @SerializedName("product_group")
    private String productGroup;

    @SerializedName("email")
    private String email;

    @SerializedName("taxvat")
    private Object taxvat;

    @SerializedName("store_id")
    private String storeId;

    @SerializedName("rewards_subscription")
    private String rewardsSubscription;

    @SerializedName("rp_token_created_at")
    private String rpTokenCreatedAt;

    @SerializedName("is_active")
    private String isActive;

    @SerializedName("middlename")
    private Object middlename;

    @SerializedName("service_group")
    private String serviceGroup;

    @SerializedName("confirmation")
    private Object confirmation;

    @SerializedName("djp")
    private String djp;

    @SerializedName("entity_id")
    private String entityId;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("disable_auto_group_change")
    private String disableAutoGroupChange;

    @SerializedName("group_id")
    private String groupId;

    @SerializedName("dob")
    private Object dob;

    @SerializedName("customer_token")
    private String customerToken;

    @SerializedName("default_billing")
    private Object defaultBilling;

    @SerializedName("website_id")
    private String websiteId;

    @SerializedName("created_in")
    private String createdIn;

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setLockExpires(Object lockExpires) {
        this.lockExpires = lockExpires;
    }

    public Object getLockExpires() {
        return lockExpires;
    }

    public void setGender(Object gender) {
        this.gender = gender;
    }

    public Object getGender() {
        return gender;
    }

    public void setPrefix(Object prefix) {
        this.prefix = prefix;
    }

    public Object getPrefix() {
        return prefix;
    }

    public void setFailuresNum(Object failuresNum) {
        this.failuresNum = failuresNum;
    }

    public Object getFailuresNum() {
        return failuresNum;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setRpToken(String rpToken) {
        this.rpToken = rpToken;
    }

    public String getRpToken() {
        return rpToken;
    }

    public void setSuffix(Object suffix) {
        this.suffix = suffix;
    }

    public Object getSuffix() {
        return suffix;
    }

    public void setDefaultShipping(Object defaultShipping) {
        this.defaultShipping = defaultShipping;
    }

    public Object getDefaultShipping() {
        return defaultShipping;
    }

    public void setFirstFailure(Object firstFailure) {
        this.firstFailure = firstFailure;
    }

    public Object getFirstFailure() {
        return firstFailure;
    }

    public void setProductserviceGroup(String productserviceGroup) {
        this.productserviceGroup = productserviceGroup;
    }

    public String getProductserviceGroup() {
        return productserviceGroup;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setIncrementId(Object incrementId) {
        this.incrementId = incrementId;
    }

    public Object getIncrementId() {
        return incrementId;
    }

    public void setPasswordHash(Object passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Object getPasswordHash() {
        return passwordHash;
    }

    public void setProductGroup(String productGroup) {
        this.productGroup = productGroup;
    }

    public String getProductGroup() {
        return productGroup;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setTaxvat(Object taxvat) {
        this.taxvat = taxvat;
    }

    public Object getTaxvat() {
        return taxvat;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setRewardsSubscription(String rewardsSubscription) {
        this.rewardsSubscription = rewardsSubscription;
    }

    public String getRewardsSubscription() {
        return rewardsSubscription;
    }

    public void setRpTokenCreatedAt(String rpTokenCreatedAt) {
        this.rpTokenCreatedAt = rpTokenCreatedAt;
    }

    public String getRpTokenCreatedAt() {
        return rpTokenCreatedAt;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setMiddlename(Object middlename) {
        this.middlename = middlename;
    }

    public Object getMiddlename() {
        return middlename;
    }

    public void setServiceGroup(String serviceGroup) {
        this.serviceGroup = serviceGroup;
    }

    public String getServiceGroup() {
        return serviceGroup;
    }

    public void setConfirmation(Object confirmation) {
        this.confirmation = confirmation;
    }

    public Object getConfirmation() {
        return confirmation;
    }

    public void setDjp(String djp) {
        this.djp = djp;
    }

    public String getDjp() {
        return djp;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setDisableAutoGroupChange(String disableAutoGroupChange) {
        this.disableAutoGroupChange = disableAutoGroupChange;
    }

    public String getDisableAutoGroupChange() {
        return disableAutoGroupChange;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setDob(Object dob) {
        this.dob = dob;
    }

    public Object getDob() {
        return dob;
    }

    public void setCustomerToken(String customerToken) {
        this.customerToken = customerToken;
    }

    public String getCustomerToken() {
        return customerToken;
    }

    public void setDefaultBilling(Object defaultBilling) {
        this.defaultBilling = defaultBilling;
    }

    public Object getDefaultBilling() {
        return defaultBilling;
    }

    public void setWebsiteId(String websiteId) {
        this.websiteId = websiteId;
    }

    public String getWebsiteId() {
        return websiteId;
    }

    public void setCreatedIn(String createdIn) {
        this.createdIn = createdIn;
    }

    public String getCreatedIn() {
        return createdIn;
    }

    @Override
    public String toString() {
        return
                "SocialLogin{" +
                        "firstname = '" + firstname + '\'' +
                        ",lock_expires = '" + lockExpires + '\'' +
                        ",gender = '" + gender + '\'' +
                        ",prefix = '" + prefix + '\'' +
                        ",failures_num = '" + failuresNum + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",rp_token = '" + rpToken + '\'' +
                        ",suffix = '" + suffix + '\'' +
                        ",default_shipping = '" + defaultShipping + '\'' +
                        ",first_failure = '" + firstFailure + '\'' +
                        ",productservice_group = '" + productserviceGroup + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",increment_id = '" + incrementId + '\'' +
                        ",password_hash = '" + passwordHash + '\'' +
                        ",product_group = '" + productGroup + '\'' +
                        ",email = '" + email + '\'' +
                        ",taxvat = '" + taxvat + '\'' +
                        ",store_id = '" + storeId + '\'' +
                        ",rewards_subscription = '" + rewardsSubscription + '\'' +
                        ",rp_token_created_at = '" + rpTokenCreatedAt + '\'' +
                        ",is_active = '" + isActive + '\'' +
                        ",middlename = '" + middlename + '\'' +
                        ",service_group = '" + serviceGroup + '\'' +
                        ",confirmation = '" + confirmation + '\'' +
                        ",djp = '" + djp + '\'' +
                        ",entity_id = '" + entityId + '\'' +
                        ",lastname = '" + lastname + '\'' +
                        ",disable_auto_group_change = '" + disableAutoGroupChange + '\'' +
                        ",group_id = '" + groupId + '\'' +
                        ",dob = '" + dob + '\'' +
                        ",customer_token = '" + customerToken + '\'' +
                        ",default_billing = '" + defaultBilling + '\'' +
                        ",website_id = '" + websiteId + '\'' +
                        ",created_in = '" + createdIn + '\'' +
                        "}";
    }
}