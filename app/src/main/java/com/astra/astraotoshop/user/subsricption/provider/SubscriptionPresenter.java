package com.astra.astraotoshop.user.subsricption.provider;

import com.astra.astraotoshop.user.subsricption.SubscriptionContract;

/**
 * Created by Henra Setia Nugraha on 4/3/2018.
 */

public class SubscriptionPresenter implements SubscriptionContract.SubscriptionPresenter {

    private SubscriptionContract.SubscriptionInteractor interactor;
    private SubscriptionContract.SubscriptionView view;
    private String storeCode;

    private final String subscribe = "subscribe";
    private final String unSubscribe = "unsubscribe";
    private final String subscribeMessage = "Anda telah berhasil berlangganan";
    private final String unSubscribeMessage = "Anda telah berhenti berlangganan";
    private boolean isSubscribe;

    public SubscriptionPresenter(SubscriptionContract.SubscriptionView view, String storeCode) {
        this.view = view;
        this.storeCode = storeCode;
        interactor = new SubscriptionInteractor(this);
    }

    @Override
    public void getSubscription() {
        interactor.getSubscription(storeCode);
    }

    @Override
    public void onSubscriptionReceived(Subscription subscription) {
        view.subscription(subscription.isIsSubscribed());
    }

    @Override
    public void onFailedReceivingSubscription(Throwable e) {
        view.showMessage("Terjadi kesalahan");
    }

    @Override
    public void changeSubscription(boolean isSubscribe) {
        this.isSubscribe = isSubscribe;
        interactor.changeSubscription(isSubscribe ? subscribe : unSubscribe, storeCode);
    }

    @Override
    public void onSubscriptionChanged() {
        view.showMessage(isSubscribe ? subscribeMessage : unSubscribeMessage);
    }

    @Override
    public void onFailedChangeSubscription(Throwable e) {
        view.showMessage("Terjadi kesalahan");
    }
}
