package com.astra.astraotoshop.user.signup.provider;

import com.astra.astraotoshop.order.checkout.provider.address.CustomAttributes;
import com.google.gson.annotations.SerializedName;

public class RegisterBody {

    @SerializedName("password")
    private String password;

    @SerializedName("customer")
    private Customer customer;

    @SerializedName("custom_attributes")
    private CustomeAttribute attributes;

    public RegisterBody(String password, Customer customer) {
        this.password = password;
        this.customer = customer;
        this.attributes = attributes;
    }

    public CustomeAttribute getAttributes() {
        return attributes;
    }

    public void setAttributes(CustomeAttribute attributes) {
        this.attributes = attributes;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    @Override
    public String toString() {
        return
                "RegisterBody{" +
                        "password = '" + password + '\'' +
                        ",customer = '" + customer + '\'' +
                        "}";
    }
}