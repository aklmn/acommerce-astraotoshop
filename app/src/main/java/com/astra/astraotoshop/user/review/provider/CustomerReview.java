package com.astra.astraotoshop.user.review.provider;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CustomerReview{

	@SerializedName("items")
	private List<ReviewsItem> items;

	public void setItems(List<ReviewsItem> items){
		this.items = items;
	}

	public List<ReviewsItem> getItems(){
		return items;
	}

	@Override
 	public String toString(){
		return 
			"CustomerReview{" + 
			"items = '" + items + '\'' + 
			"}";
		}
}