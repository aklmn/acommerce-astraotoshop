package com.astra.astraotoshop.user.password;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.password.provider.ChangePasswordPresenter;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.Loading;
import com.astra.astraotoshop.utils.view.PasswordStrength;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class ChangeChangePasswordActivity extends BaseActivity implements PasswordContract.ChangePasswordView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.currentPass)
    EditText currentPass;
    @BindView(R.id.newPass)
    EditText newPass;
    @BindView(R.id.confirmPass)
    EditText confirmPass;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.password_strength)
    TextView passwordStrength;
    @BindView(R.id.currentPassContainer)
    TextInputLayout currentPassContainer;
    @BindView(R.id.newPassContainer)
    TextInputLayout newPassContainer;
    @BindView(R.id.confirmPassContainer)
    TextInputLayout confirmPassContainer;
    private ChangePasswordPresenter presenter;
    private Loading loading;
    private PasswordStrength strength;
    ArrayList<Boolean> validations = new ArrayList<Boolean>() {{
        add(false);
        add(false);
        add(false);
    }};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.change_password);
        ButterKnife.bind(this);
        actionBarSetup();
        presenter = new ChangePasswordPresenter(this);
        loading = new Loading(this, getLayoutInflater());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    public void showPasswordMessage(boolean isSuccess) {
        loading.dismiss();
        if (isSuccess) {
            showToastMessage("Password berhasil diubah");
            finish();
        } else showToastMessage("Password gagal diubah");
    }

    private void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @OnTextChanged(R.id.newPass)
    public void onNewPassTextChange(CharSequence charSequence, int start, int count, int after) {
        if (charSequence.length() == 0) {
            passwordStrength.setText("Weak");
            progressBar.setProgress(0);
            return;
        }

        strength = PasswordStrength.calculateStrength(charSequence.toString());
        passwordStrength.setText(strength.getText(this));
        passwordStrength.setTextColor(strength.getColor());

        progressBar.getProgressDrawable().setColorFilter(strength.getColor(), PorterDuff.Mode.SRC_IN);
        if (strength.getText(this).equals("Weak")) {
            progressBar.setProgress(25);
        } else if (strength.getText(this).equals("Medium")) {
            progressBar.setProgress(50);
        } else if (strength.getText(this).equals("Strong")) {
            progressBar.setProgress(75);
        } else if (strength.getText(this).equals("Very Strong")) {
            progressBar.setProgress(100);
        }

        newPassContainer.setError(strength.getCurrentScore() < 2 ? "Password belum memenuhi syarat" : "");
        validations.set(0, strength.getCurrentScore() > 1);
    }

    @OnClick(R.id.save)
    public void onSaveClicked() {
        if (isFormValid()) {
            loading.show(getString(R.string.message_please_wait));
            presenter.changePassword(getStoreCode(), currentPass.getText().toString(), newPass.getText().toString());
        }
    }

    private boolean isFormValid() {
        validations.set(0, textCheck(newPass, newPassContainer, "Password baru wajib diisi") && strength.getCurrentScore() >= 1);
        validations.set(1, textCheck(currentPass, currentPassContainer, "Password wajib diisi"));
        validations.set(2, textCheck(confirmPass, confirmPassContainer, "Konfirmasi password wajib diisi"));
        validations.set(2, confirmPass.getText().toString().equals(newPass.getText().toString()));
        if (!confirmPass.getText().toString().equals(newPass.getText().toString())) {
            newPassContainer.setError("Password tidak sama");
            confirmPassContainer.setError("Password tidak sama");
        } else {
            newPassContainer.setError(null);
            confirmPassContainer.setError(null);
        }
        return !validations.contains(false);
    }

    public boolean textCheck(EditText editText, @NonNull TextInputLayout inputEditText, String errorMessage) {
        if (TextUtils.isEmpty(editText.getText())) {
            inputEditText.setError(errorMessage);
            return false;
        } else inputEditText.setError(null);
        return true;
    }
}
