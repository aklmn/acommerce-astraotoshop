package com.astra.astraotoshop.user.profile.provider;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.utils.pref.Pref;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/22/2018.
 */

public class ProfilePresenter implements ProfileContract.ProfilePresenter {

    private ProfileContract.ProfileView view;
    ProfileContract.ProfileInteractor interactor;

    public ProfilePresenter(ProfileContract.ProfileView view) {
        this.view = view;
        interactor = new ProfileInteractor(this);
    }

    @Override
    public void requestProfile(String storeCode) {
        interactor.requestProfile(storeCode);
    }

    @Override
    public void onProfileReceived(ProfileEntity profileEntity, String storeCode) {
        Pref.getPreference().putString(BuildConfig.UID, String.valueOf(profileEntity.getId()));
        Pref.getPreference().putString("username", profileEntity.getFirstname() + " " + profileEntity.getLastname());
        Pref.getPreference().putString("email", profileEntity.getEmail());
        saveDefaultAddresses(profileEntity.getAddresses());
        view.setProfileData(profileEntity);
    }

    @Override
    public void onFailedReceivingProfile() {
        view.showError("Email atau Password anda salah");
    }

    private void saveDefaultAddresses(List<Addresses> addresses) {
        if (addresses != null) {
            for (Addresses item : addresses) {
                if (item.isDefaultShipping()) {
                    Pref.getPreference().putString("address", new Gson().toJson(item));
                    return;
                }
            }
        }
    }
}
