package com.astra.astraotoshop.user.profile.provider;

import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/22/2018.
 */

public class ProfileInteractor extends BaseInteractor implements ProfileContract.ProfileInteractor {

    ProfileContract.ProfilePresenter presenter;

    public ProfileInteractor( ProfileContract.ProfilePresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestProfile(String storeCode) {
        getGeneralNetworkManager()
                .requestProfile(new NetworkHandler(), storeCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        profileEntity -> {
                            presenter.onProfileReceived(profileEntity,storeCode);
                        },
                        error->{
                            presenter.onFailedReceivingProfile();
                        }
                );
    }
}
