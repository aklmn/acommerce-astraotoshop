package com.astra.astraotoshop.user.wishlist.provider;

import com.astra.astraotoshop.user.wishlist.WishlistContract;

/**
 * Created by Henra Setia Nugraha on 2/22/2018.
 */

public class WishlistPresenter implements WishlistContract.WishlistPresenter {

    private WishlistContract.WishlistView view;
    private WishlistContract.AddWishlist wishlist;
    private WishlistContract.WishlistInteractor interactor;
    private String storeCode;

    public WishlistPresenter(WishlistContract.WishlistView view, WishlistContract.AddWishlist wishlist, String storeCode) {
        this.view = view;
        this.wishlist = wishlist;
        this.storeCode = storeCode;
        interactor = new WishlistInteractor(this);
    }


    @Override
    public void requestWishlist() {
        interactor.requestWishlist(storeCode);
    }

    @Override
    public void addWishlist(int productId) {
        interactor.addWishlist(storeCode, new AddWishlistBody(productId));
    }

    @Override
    public void deleteItemWishlist(WishlistItem wishlistItemId) {
        interactor.deleteItemWishlist(storeCode, String.valueOf(wishlistItemId.getWishlistItemId()));
    }

    @Override
    public void onWishlistReceived(WishlistEntity wishlistEntity) {
        view.showWishlist(wishlistEntity.getItems());
    }

    @Override
    public void onFailedReceivingWishlist(Throwable error) {
        view.showMessage(false, "Terjadi kesalahan");
    }

    @Override
    public void onAddSucceed() {
        wishlist.showSuccessMessage();
    }

    @Override
    public void onAddFailed(Throwable error) {
        wishlist.showFailedMessage();
    }

    @Override
    public void onDeleteSucceed() {
        view.showMessage(true,"Sukses menonfavoritkan produk");
    }

    @Override
    public void onDeleteFailed(Throwable error) {
        view.showMessage(true,"Gagal menghapus");
    }
}
