package com.astra.astraotoshop.user.voucher;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherData;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 12/4/2017.
 */

public class VoucherAdapter extends RecyclerView.Adapter<VoucherAdapter.VoucherViewHolder> {

    private List<VoucherData> vouchers;
    private ListListener listener;

    public VoucherAdapter(ListListener listener) {
        this.listener = listener;
        vouchers = new ArrayList<>();
    }

    @Override
    public VoucherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VoucherViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket, parent, false));
    }

    @Override
    public void onBindViewHolder(VoucherViewHolder holder, int position) {
        holder.orderId.setText("ORDER #" + vouchers.get(position).getOrderIncrementId());
        holder.productName.setText(vouchers.get(position).getItemName());
        holder.voucherStatus.setText(vouchers.get(position).getVoucherStatus());
        holder.voucherExpDate.setText(StringFormat.timeConverter(vouchers.get(position).getVoucherExpiredAt()));
        holder.voucherCode.setText(vouchers.get(position).getVoucherCode());
    }

    @Override
    public int getItemCount() {
        return vouchers.size();
    }

    public void setVouchers(List<VoucherData> vouchers) {
        this.vouchers = vouchers;
        notifyDataSetChanged();
    }

    class VoucherViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.orderId)
        TextView orderId;
        @BindView(R.id.productName)
        TextView productName;
        @BindView(R.id.voucherStatus)
        TextView voucherStatus;
        @BindView(R.id.voucherExpDate)
        TextView voucherExpDate;
        @BindView(R.id.voucherCode)
        TextView voucherCode;

        VoucherViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                listener.onItemClick(v.getId(), getAdapterPosition(), vouchers.get(getAdapterPosition()));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
