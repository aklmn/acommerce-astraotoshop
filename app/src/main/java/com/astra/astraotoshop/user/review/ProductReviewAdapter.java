package com.astra.astraotoshop.user.review;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.review.provider.ReviewItemEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 11/20/2017.
 */

public class ProductReviewAdapter extends RecyclerView.Adapter<ProductReviewAdapter.ReviewViewHolder> {

    String state;
    private List<ReviewItemEntity> reviews;

    public ProductReviewAdapter(String state) {
        this.state = state;
        reviews = new ArrayList<>();
    }

    @Override
    public ReviewViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ReviewViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_review, parent, false));
    }

    @Override
    public void onBindViewHolder(ReviewViewHolder holder, int position) {
        try {
            if (state.equals("user")) {
                holder.productName.setVisibility(View.VISIBLE);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        holder.shortReview.setText(reviews.get(position).getTitle());
        holder.rate.setRating((float) reviews.get(position).getRating());
        holder.reviewer.setText(reviews.get(position).getNickname());
        holder.review.setText(reviews.get(position).getDetail());
        holder.date.setText(reviews.get(position).getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    public void setReviews(List<ReviewItemEntity> reviews) {
        this.reviews = reviews;
        notifyDataSetChanged();
    }

    class ReviewViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.divider)
        View divider;
        @BindView(R.id.productName)
        TextView productName;
        @BindView(R.id.rate)
        RatingBar rate;
        @BindView(R.id.review)
        TextView review;
        @BindView(R.id.reviewer)
        TextView reviewer;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.shortReview)
        TextView shortReview;

        public ReviewViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (getAdapterPosition() == getItemCount() - 1) {
                divider.setVisibility(View.GONE);
            }
        }
    }
}
