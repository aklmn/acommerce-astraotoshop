package com.astra.astraotoshop.user.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.signin.LoginActivity;
import com.astra.astraotoshop.user.signup.RegisterActivity;
import com.astra.astraotoshop.utils.base.BaseDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/16/2017.
 */

public class ProfileFragment extends BaseDialogFragment {

    Unbinder unbinder;
    @Nullable
    @BindView(R.id.profile_name)
    TextView profileName;

    @Nullable
    @BindView(R.id.profile_email)
    TextView profileEmail;

    @Nullable
    @BindView(R.id.profile_update)
    TextView profileUpdate;

    @Nullable
    @BindView(R.id.profile_list_menu)
    RecyclerView profileListMenu;

    @Nullable
    @BindView(R.id.profile_signout)
    TextView profileSignout;

    @Nullable
    @BindView(R.id.scroll)
    NestedScrollView scroll;

    ProfileStatusChange listener;

    public static ProfileFragment newInstance(int state) {

        Bundle args = new Bundle();
        args.putInt(STATE, state);
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String[] items = {"panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang", "panjang"};
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            profileListMenu.setFocusableInTouchMode(false);
            profileListMenu.setFocusable(false);
            profileListMenu.clearFocus();
            scroll.fullScroll(View.FOCUS_UP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Optional
    @OnClick(R.id.profile_update)
    public void editProfilePressed() {
        startActivity(new Intent(getActivity(), EditProfileActivity.class));
    }

    @Optional
    @OnClick(R.id.register)
    public void register() {
        Intent intent = new Intent(getActivity(), RegisterActivity.class);
        intent.putExtra(STATE, getState());
        getActivity().startActivityForResult(intent,33);
        dismiss();
    }

    @Optional
    @OnClick(R.id.login)
    public void login() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.putExtra(STATE, getState());
        getActivity().startActivity(intent);
        dismiss();
    }

    @Optional
    @OnClick(R.id.profile_signout)
    public void sigOut() {
        try {
            listener.onSignOut();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void onDetach(String state) {
        getArguments().putString("STATE", state);
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setListener(ProfileStatusChange listener) {
        this.listener = listener;
    }

    public interface ProfileStatusChange {
        void onSignOut();
    }
}
