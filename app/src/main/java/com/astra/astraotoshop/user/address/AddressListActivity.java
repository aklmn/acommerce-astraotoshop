package com.astra.astraotoshop.user.address;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.user.profile.provider.Addresses;
import com.astra.astraotoshop.user.profile.provider.ProfileContract;
import com.astra.astraotoshop.user.profile.provider.ProfileEntity;
import com.astra.astraotoshop.user.profile.provider.ProfilePresenter;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressListActivity extends BaseActivity implements ProfileContract.ProfileView, ListListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.addresses)
    RecyclerView addresses;

    ProfileContract.ProfilePresenter presenter;
    private AddressListAdapter adapter;
    private boolean isFromCheckout = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        ButterKnife.bind(this);
        actionBarSetup();
        presenter = new ProfilePresenter(this);
        isFromCheckout = getIntent().getBooleanExtra("isFromCheckout", false);

        adapter = new AddressListAdapter(this, this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        addresses.setLayoutManager(layoutManager);
        addresses.setAdapter(adapter);
        presenter.requestProfile(getStoreCode());

    }

    @OnClick(R.id.createAddress)
    public void createAddress() {
        startActivityForResult((new Intent(this, CreateAddressActivity.class)).putExtra(STATE, getState()), 3);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            presenter.requestProfile(getStoreCode());
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void setProfileData(ProfileEntity profileEntity) {
        adapter.setAddresses(profileEntity.getAddresses());
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        if (isFromCheckout) {
            Intent intent = new Intent();
            intent.putExtra("data", (Addresses) data);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            Intent intent = getIntent(this, CreateAddressActivity.class);
            intent.putExtra("isUpdate", true);
            intent.putExtra("address", (Addresses) data);
            startActivityForResult(intent, 3);
        }
    }
}
