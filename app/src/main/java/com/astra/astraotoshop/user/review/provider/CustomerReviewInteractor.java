package com.astra.astraotoshop.user.review.provider;

import android.content.Context;

import com.astra.astraotoshop.user.review.CustomerReviewContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 3/27/2018.
 */

public class CustomerReviewInteractor extends BaseInteractor implements CustomerReviewContract.ReviewInteractor {

    CustomerReviewContract.ReviewPresenter presenter;

    public CustomerReviewInteractor(CustomerReviewContract.ReviewPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestReview(String storeCode) {
        getGeneralNetworkManager()
                .requestCustomerReview(new NetworkHandler(),storeCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        review -> presenter.onReviewReceived(review),
                        e->presenter.onFailedReceive(e)
                );
    }
}
