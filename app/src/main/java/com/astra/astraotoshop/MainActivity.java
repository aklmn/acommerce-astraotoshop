package com.astra.astraotoshop;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.astra.astraotoshop.order.checkout.doku.AOPWebClient;
import com.astra.astraotoshop.order.checkout.doku.WebViewListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements WebViewListener {


    @BindView(R.id.web)
    WebView web;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.testhtml);
        ButterKnife.bind(this);
        webSetup();
//        web.loadData("<html>" +
//                "<head>\n" +
//                "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://shop.adidas.co.id/skin/frontend/adidas/default/css/style.css\" media=\"all\">" +
//                "</head>" +
//                "<body>\n" +
//                "\n" +
//                "<div class=\"page-title\">\n" +
//                "   <h2>About adidas Products</h2>\n" +
//                "</div>\n" +
//                "\n" +
//                "\n" +
//                "<!--\n" +
//                "{{config path=\"trans_email/ident_general/email\"}}\n" +
//                "{{config path=\"general/store_information/phone\"}}\n" +
//                "{{config path=\"web/unsecure/base_url\"}}\n" +
//                "-->\n" +
//                "\n" +
//                "<div class=\"content-page\">\n" +
//                "    <div class=\"section\">\n" +
//                "        adidas Online Store offers the latest and greatest gear that adidas has to offer at every moment. When you shop with adidas, you can rest assured that for more than seven decades, adidas has been synonymous with excellence. Our hallmarks are the very best in quality standards, technology and innovation.        \n" +
//                "    </div>\n" +
//                "\n" +
//                "    <div class=\"section\">\n" +
//                "        <h5>Originals</h5>\n" +
//                "        <p>adidas Originals: Reflecting the timeless adidas heritage. Once innovative, now classic and always authentic, adidas Originals products are identified by the Trefoil launched at the 1972 Olympics in Munich.</p>\n" +
//                "    </div>\n" +
//                "\n" +
//                "    <div class=\"section\">\n" +
//                "        <h5>Sports Performance</h5>\n" +
//                "        <p>All adidas products not only look great but also offer unsurpassed performance. In the Performance division, we strive to offer the most functional and best performing sporting products to athletes in all sports categories. Our passion and motivation is to help athletes who demand performance ahead of any other considerations. Performance products are designed using the very latest technical breakthroughs.</p>\n" +
//                "    </div>\n" +
//                "\n" +
//                "    <div class=\"section\">\n" +
//                "        <h5>Technologies</h5>\n" +
//                "        <p>Our award-winning product innovations constantly strive to support athletes aiming to achieve ever better levels of performance. Our clothes and shoes use a lot of different technologies, which are described on the product page of each Sports product.</p>\n" +
//                "    </div>\n" +
//                "\n" +
//                "\n" +
//                "    <div class=\"section\">\n" +
//                "        <h5>Sizing</h5>\n" +
//                "        <p>We provide size charts on Product Detail Page that’ll help you compare the sizes displayed on the product page to the sizes you might be more familiar with. If you receive the product, and it's the wrong size, you can send it back and order it again in a different size. Please read our Return Policy for more details.</p>\n" +
//                "    </div>\n" +
//                "</div></body></html>","text/html","utf-8");

        web.loadUrl("https://shop.adidas.co.id/about-adidas-products/");

    }

    private void webSetup() {
        web.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        web.getSettings().setJavaScriptEnabled(true);
        web.setWebChromeClient(new WebChromeClient());
        web.setWebViewClient(new AOPWebClient(this, this));
        web.getSettings().setDomStorageEnabled(true);
        web.getSettings().setBuiltInZoomControls(false);
        web.getSettings().setBuiltInZoomControls(false);
        web.clearHistory();
    }

    @Override
    public void onSuccess() {

    }

    @Override
    public void onFailed() {

    }

    @Override
    public void onLoaded() {

    }
}
