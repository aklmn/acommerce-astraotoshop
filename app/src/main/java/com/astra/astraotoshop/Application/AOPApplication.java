package com.astra.astraotoshop.Application;

import android.app.Application;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.view.View;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.home.productservice.MainPageActivity;
import com.astra.astraotoshop.home.revamp.HomePageActivity;
import com.astra.astraotoshop.product.category.provider.CatPresenter;
import com.astra.astraotoshop.product.category.provider.CategoryContract;
import com.astra.astraotoshop.user.token.TokenContract;
import com.astra.astraotoshop.utils.pref.Pref;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import prv.aop.Translate;

/**
 * Created by Henra Setia Nugraha on 10/18/2017.
 */

public class AOPApplication extends Application implements TokenContract.TokenView {

    private TokenContract.TokenPresenter presenter;
    private static AOPApplication instance;
    public SplashListener splashListener;
    private Object adminToken;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        if (instance == null)
            instance = this;
//        Fabric.with(this, new Crashlytics());
//        FacebookSdk.sdkInitialize(getApplicationContext());
        Pref.setPreference(this, AOPApplication.class.getSimpleName());
        presenter = new AppPresenter(this, this);

//        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
//        Toast.makeText(getApplicationContext(), String.valueOf(
//                (networkInfo != null && networkInfo.isConnectedOrConnecting())),
//                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestComplete() {
        getCategories();
        try {
            splashListener.onPrepareFinished();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailedLogin(String message) {
        splashListener.onPrepareFailed();
    }

    public synchronized static AOPApplication getInstance() {
        return instance;
    }

    public static void forceLogout(String storeCode) {
        Pref.getPreference().remove(BuildConfig.UID);
        Pref.getPreference().remove("username");
        Pref.getPreference().remove("email" + storeCode);
        Pref.getPreference().remove(BuildConfig.CTKN);
        Pref.getPreference().putInt("cartVisibility", View.GONE);
        Pref.getPreference().remove("quoteId" + storeCode);
    }

    public static void start() {
        getInstance().getApplicationContext().startActivity(new Intent(
                getInstance().getApplicationContext(),
                HomePageActivity.class
        ));
    }

    public void isTokenExpired() {
        long savedTime = Pref.getPreference().getLong("tokenTime");
        long currentTime = (Calendar.getInstance().getTime()).getTime();

        long minute = (TimeUnit.MINUTES).convert(currentTime - savedTime, TimeUnit.MILLISECONDS);
        if (Pref.getPreference().getString("cat-product") == null) {
            getCategories();
        }
        if (minute <= 1440 && savedTime != 0 && Pref.getPreference().getString(BuildConfig.ATKN) != null) {
            try {
                splashListener.onPrepareFinished();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            getAdminToken();
        }
    }

    public void getAdminToken() {
        presenter.requestToken(Translate.getInstance().getAdmin(), Translate.getInstance().getPassword(), BuildConfig.ATKN, "admin");
    }

    private void getCategories() {
        CategoryContract.CatPresenter presenter = new CatPresenter(Pref.getPreference());
        presenter.requestCategories(
                getResources().getStringArray(R.array.storeCode)
                        [getResources().getInteger(R.integer.state_product)]
        );
        presenter.requestCategories(
                getResources().getStringArray(R.array.storeCode)
                        [getResources().getInteger(R.integer.state_product_service)]
        );
    }
}
