package com.astra.astraotoshop.Application;

import android.content.Context;
import android.text.TextUtils;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.provider.entity.TokenEntity;
import com.astra.astraotoshop.user.token.TokenAdminInteractor;
import com.astra.astraotoshop.user.token.TokenContract;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;
import com.astra.astraotoshop.utils.pref.Pref;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Henra Setia Nugraha on 1/4/2018.
 */

public class AppPresenter implements TokenContract.TokenPresenter {

    private TokenContract.TokenInteractor interactor;
    private TokenContract.TokenView tokenView;
    private Context context;
    private String role;

    public AppPresenter(Context context, TokenContract.TokenView tokenView) {
        this.tokenView = tokenView;
        interactor = new TokenAdminInteractor(context, this);
        this.context = context;
    }

    @Override
    public void requestToken(String userName, String password, String role, String rolePath) {
        interactor.requestToken(new NetworkHandler(), rolePath, new TokenEntity(userName, password), role);
    }

    @Override
    public void onTokenReceived(String response, String role) {
        if (!TextUtils.isEmpty(response))
            try {
                if (role.equals(BuildConfig.ATKN)) {
                    Calendar calendar = Calendar.getInstance();
                    Date date = calendar.getTime();
                    Pref.getPreference().putLong("tokenTime", date.getTime());
                }

                Pref.getPreference().putString(role, response);
//                if (role.equalsIgnoreCase(BuildConfig.CTKN))
                tokenView.onRequestComplete();
            } catch (Exception e) {
                e.printStackTrace();
            }
    }

    @Override
    public void onRequestTokenFailed() {
        tokenView.onFailedLogin("Login tidak berhasil, periksa lagi email/password Anda");
    }
}
