package com.astra.astraotoshop.Application;

/**
 * Created by Henra Setia Nugraha on 5/30/2018.
 */

public interface SplashListener {
    void onPrepareFinished();
    void onPrepareFailed();
}
