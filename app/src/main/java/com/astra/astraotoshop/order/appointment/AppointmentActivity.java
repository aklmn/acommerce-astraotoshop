package com.astra.astraotoshop.order.appointment;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.appointment.provider.AppointmentPresenter;
import com.astra.astraotoshop.order.appointment.provider.AppointmentsItem;
import com.astra.astraotoshop.order.appointment.provider.SlotItem;
import com.astra.astraotoshop.order.cart.provider.AppointmentsItemCart;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartPresenter;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.FontIconic;
import com.astra.astraotoshop.utils.view.Loading;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AppointmentActivity extends BaseActivity implements CartContract.ExternalCart, AppointmentContract.AppointmentExternal {
    @BindView(R.id.close)
    FontIconic close;
    @BindView(R.id.toolbar)
    LinearLayout toolbar;
    @BindView(R.id.divider)
    View divider;
    @BindView(R.id.save)
    Button save;
    @BindView(R.id.homeService)
    CheckBox homeService;
    @BindView(R.id.outlet)
    RelativeLayout outlet;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.addressContainer)
    LinearLayout addressContainer;
    @BindView(R.id.time)
    RelativeLayout time;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.clock)
    TextView clock;
    @BindView(R.id.timeContainer)
    ConstraintLayout timeContainer;

    private OutletFragment outletFragment;
    private BackListener backListener;
    private List<SlotItem> slotItems;
    private AppointmentsItem selectedAppointment = new AppointmentsItem();
    private TimeFragment timeFragment;
    private SlotItem slotItem;
    private String selectedTime;
    private Calendar dateTime;
    private CartContract.CartPresenter cartPresenter;
    private Loading loading;
    private AppointmentsItemCart item;
    private AppointmentContract.AppointmentPresenter appointmentPresenter;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment2);
        ButterKnife.bind(this);
        loading = new Loading(this, getLayoutInflater());
        String productId = getIntent().getStringExtra("id");
        outletFragment = OutletFragment.newInstance(productId);
        timeFragment = TimeFragment.newInstance();
        backListener = outletFragment.getOnBackListener();
        appointmentPresenter = new AppointmentPresenter(null, productId, this);
        cartPresenter = new CartPresenter(null, this, getStoreCode());
        setDefaultValue();
    }

    @Override
    public void onBackPressed() {
        if (backListener != null) {
            backListener.onBackPressed();
        }
    }

    @Override
    public void onItemAdded() {
        Toast.makeText(this, "Kamu telah menambahkan produk ke keranjang Anda", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onItemFailedToAdd(String message) {
        loading.dismiss();
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemUpdated() {

    }

    @Override
    public void onItemFailedToUpdate(String message) {

    }

    @Override
    public void onItemDeleted() {

    }

    @Override
    public void onItemFailedToDelete() {

    }

    @OnClick(R.id.close)
    public void onClose() {
        finish();
    }

    @Override
    public void setSlot(List<SlotItem> slots) {
        slotItems = slots;
    }

    @Override
    public void showSlotErrorMessage() {

    }

    @OnClick({R.id.addressContainer, R.id.outlet})
    public void onAddressClicked() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("addressFragment");
        if (fragment != null) transaction.remove(fragment);
        outletFragment.show(getSupportFragmentManager(), "addressFragment");
    }

    @OnClick({R.id.timeContainer, R.id.time})
    public void onTimeClicked() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("timeFragment");
        if (fragment != null) transaction.remove(fragment);
        transaction.commit();
        timeFragment.setSlotItems(slotItems);
        timeFragment.show(getSupportFragmentManager(), "timeFragment");
    }

    @OnClick(R.id.save)
    public void onSaveClicked() {
        if (isLogin()) {
//            cartPresenter.addToCart(getIntent().getExtras().getString("sku"), 1, appointmentMap(selectedAppointment, slotItem));
            Intent intent = new Intent();
            intent.putExtra(getString(R.string.intent_appointment), appointmentMap());
            setResult(RESULT_OK, intent);
            finish();
        } else showAccount();
    }

    private void setDefaultValue() {
        if (getIntent().hasExtra(getString(R.string.intent_appointment))) {

            item = getIntent().getParcelableExtra(getString(R.string.intent_appointment));
            if (item != null) {
                String id = getIntent().getStringExtra("id");
                position = getIntent().getIntExtra(getString(R.string.intent_index_position), 0);
                address.setText(item.getAddress().get(position).replace("|| ", "\n"));
                clock.setText(item.getApHours().get(position));
                date.setText(StringFormat.getCalendarString(
                        StringFormat.timeConverter(item.getApDate().get(position), "yyyy-MM-dd"),
                        "EEEE, dd MMMM yyyy"
                ));
                appointmentPresenter.getSlotItems(item.getRegion().get(position), item.getCity().get(position), item.getAddress().get(position));
                addressContainer.setVisibility(View.VISIBLE);
                timeContainer.setVisibility(View.VISIBLE);
                selectedAppointment.setAddress(Collections.singletonList(item.getAddress().get(position)));
                selectedAppointment.setRegion(Collections.singletonList(item.getRegion().get(position)));
                selectedAppointment.setCity(Collections.singletonList(item.getCity().get(position)));
            }
        }
    }

    private AppointmentsItemCart appointmentMap() {
        AppointmentsItemCart appointmentsItemCart;
        appointmentsItemCart = new AppointmentsItemCart();
        appointmentsItemCart.setAddress(selectedAppointment.getAddress());
        appointmentsItemCart.setRegion(selectedAppointment.getRegion());
        appointmentsItemCart.setCity(selectedAppointment.getCity());
        appointmentsItemCart.setApHours(Collections.singletonList(slotItem.getClock()));
        appointmentsItemCart.setSlotName(Collections.singletonList(slotItem.getSlotName()));
        appointmentsItemCart.setWhsId(Collections.singletonList(slotItem.getWhsId()));
        appointmentsItemCart.setMechanicComeToHome(Collections.singletonList("0"));
        if (dateTime == null && item != null)
            appointmentsItemCart.setApDate(Collections.singletonList(item.getApDate().get(position)));
        else
            appointmentsItemCart.setApDate(Collections.singletonList(StringFormat.getCalendarString(dateTime, "yyyy-MM-dd")));
        return appointmentsItemCart;
    }

    public void setOutlet(List<SlotItem> slotItems, String outlet, AppointmentsItem appointment) {
        this.slotItems = slotItems;
        selectedAppointment = appointment;
        address.setText(outlet.replace("|| ", "\n"));
        addressContainer.setVisibility(View.VISIBLE);
        resetTime();
    }

    public void setTime(SlotItem slotItem, Calendar dateTime) {
        this.slotItem = slotItem;
        this.dateTime = dateTime;
        selectedTime = StringFormat.getCalendarString(dateTime, null);
        if (slotItem != null) {
            save.setEnabled(true);
            date.setText(StringFormat.getCalendarString(dateTime, "EEEE, dd MMMM yyyy"));
            clock.setText(slotItem.getClock());
            timeContainer.setVisibility(View.VISIBLE);
        }
    }

    private void resetTime() {
        save.setEnabled(false);
        timeContainer.setVisibility(View.GONE);
        setTime(null, null);
    }
}
