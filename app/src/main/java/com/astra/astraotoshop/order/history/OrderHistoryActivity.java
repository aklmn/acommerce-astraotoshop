package com.astra.astraotoshop.order.history;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.history.provider.OrderHistoriesItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.user.order.OrderContract;
import com.astra.astraotoshop.user.order.provider.OrderListPresenter;
import com.astra.astraotoshop.utils.base.BaseActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderHistoryActivity extends BaseActivity implements OrderHistoryAdapter.OrderHistoryListener, OrderContract.OrderListVIew, ListListener, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.orderList)
    RecyclerView orderList;
    @BindView(R.id.itemCount)
    TextView itemCount;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    private OrderContract.OrderListPresenter presenter;
    private OrderHistoryAdapter adapter;
    private LinearLayoutManager layoutManager;
    private boolean isLoad = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        ButterKnife.bind(this);
        actionBarSetup();
        presenter = new OrderListPresenter(this, getStoreCode());
        requestProducts();

        adapter = new OrderHistoryAdapter(this);
        layoutManager = new LinearLayoutManager(this);

        orderList.setLayoutManager(layoutManager);
        orderList.setAdapter(adapter);
        orderList.addOnScrollListener(onScrollListener);
        swipeRefresh.setOnRefreshListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick() {
        startActivity(new Intent(this, HistoryOrderDetailActivity.class));
    }

    @Override
    public void showOrderList(List<OrderHistoriesItem> ordersItems, int totalCount) {
        adapter.setOrdersItems(ordersItems, presenter.isAddMore());
        if (totalCount > 0) itemCount.setText(totalCount + " pesanan ditemukan");
        else itemCount.setText("Pesanan tidak ditemukan");
        isLoad = false;
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        isLoad = false;
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        Intent intent = getIntent(this, HistoryOrderDetailActivity.class);
        startActivity((intent).putExtra("data", (OrderHistoriesItem) data));
    }

    @Override
    public void onRefresh() {
        presenter.reset();
        isLoad = true;
    }

    RecyclerView.OnScrollListener onScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (layoutManager.findFirstVisibleItemPosition() + recyclerView.getChildCount() == layoutManager.getItemCount()) {
                if (!isLoad) {
                    requestProducts();
                    isLoad = true;
                    swipeRefresh.setRefreshing(true);
                }
            }
        }
    };

    private void requestProducts() {
        presenter.requestOrderList();
    }

    @Override
    protected void onDestroy() {
        orderList.removeOnScrollListener(onScrollListener);
        onScrollListener = null;
        super.onDestroy();
    }
}
