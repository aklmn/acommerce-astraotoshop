package com.astra.astraotoshop.order.checkout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.checkout.provider.ItemMultiWarehouse;
import com.astra.astraotoshop.order.checkout.provider.ShippingMethodEntity;
import com.astra.astraotoshop.order.checkout.provider.ShippingPresenter;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.Loading;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.Contract;

import java.lang.reflect.Type;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 2/4/2018.
 */

public class ShippingMethodFragment extends DialogFragment implements ListListener {

    @BindView(R.id.multiWarehouseContainer)
    LinearLayout multiWarehouseContainer;
    @BindView(R.id.shippingMethod)
    RecyclerView shippingMethod;
    @BindView(R.id.productItems)
    RecyclerView productItems;
    Unbinder unbinder;
    @BindView(R.id.isMulti)
    Switch isMulti;
    private ShippingAdapter adapter;
    private ShippingProductAdapter productAdapter;
    private LinearLayoutManager layoutManage;
    private List<ShippingMethodEntity> shippingMethods;
    private int selectedPosition = -1;
    private CheckoutContract.ShippingPresenter presenter;
    ShippingMethodEntity shippingMethodEntity;

    public static ShippingMethodFragment newInstance() {

        Bundle args = new Bundle();

        ShippingMethodFragment fragment = new ShippingMethodFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.FullScreenDialog);
        adapter = new ShippingAdapter(this);
        productAdapter = new ShippingProductAdapter(getContext());
        presenter = new ShippingPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_multi_warehouse, container, false);
        unbinder = ButterKnife.bind(this, view);
        layoutManage = new LinearLayoutManager(getContext());
        shippingMethod.setLayoutManager(layoutManage);
        productItems.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isMultiWarehouse(shippingMethods)) {
            multiWarehouseContainer.setVisibility(View.VISIBLE);
            productItems.setVisibility(View.VISIBLE);
        }
        productItems.setAdapter(productAdapter);
        adapter.setMulti(isMulti.isChecked());
        if (selectedPosition != -1) {
            adapter.setShippingMethods(shippingMethods, selectedPosition);
        } else
            adapter.setShippingMethods(shippingMethods);
        shippingMethod.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.close)
    public void onCloseClicked() {
        dismiss();
    }

    @OnClick(R.id.save)
    public void onSaveClicked() {
        ShippingMethodEntity entity = adapter.getItemSelected();
        if (entity != null) {
            ((Checkout2Activity) getActivity()).setShippingMethod(adapter.getItemSelected(), adapter.getPositionSelected());
        }
    }

    @OnCheckedChanged(R.id.isMulti)
    public void multiWarehouses(CompoundButton button, boolean isChecked) {
        adapter.setMulti(isChecked);
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        try {
            shippingMethodEntity = (ShippingMethodEntity) data;
            if (shippingMethodEntity.getMethodDescription() != null) {
                productAdapter.setItemMultiWarehouses(
                        presenter.getProductList(shippingMethodEntity.getMethodDescription())
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setShippingMethods(List<ShippingMethodEntity> shippingMethods) {
        this.shippingMethods = shippingMethods;
    }

    public void setShippingMethods(List<ShippingMethodEntity> shippingMethods, int selectedPosition) {
        this.shippingMethods = shippingMethods;
        this.selectedPosition = selectedPosition;
    }

    @Contract("null -> false")
    private boolean isMultiWarehouse(List<ShippingMethodEntity> shippingMethods) {
        if (shippingMethods != null) {
            for (ShippingMethodEntity entity : shippingMethods) {
                if (entity.getCarrierCode().equalsIgnoreCase("acommultiwarehouseshipping"))
                    return true;
            }
        }
        return false;
    }

    private List<ItemMultiWarehouse> createProductList(String productJSON) {
        Type collectionType = new TypeToken<List<ItemMultiWarehouse>>() {
        }.getType();
        return new Gson().fromJson(productJSON, collectionType);
    }
}
