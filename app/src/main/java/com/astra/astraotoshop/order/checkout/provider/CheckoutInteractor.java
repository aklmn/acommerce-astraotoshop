package com.astra.astraotoshop.order.checkout.provider;

import com.astra.astraotoshop.order.checkout.CheckoutContract;
import com.astra.astraotoshop.order.checkout.provider.address.AddressShippingBody;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodBody;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/31/2018.
 */

public class CheckoutInteractor extends BaseInteractor implements CheckoutContract.CheckoutInteractor {

    private CheckoutContract.CheckoutPresenter presenter;

    public CheckoutInteractor(CheckoutContract.CheckoutPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestShippingMethod(String storeCode, AddressBody addressBody) {
        getGeneralNetworkManager()
                .requestShippingMethod(
                        new NetworkHandler(),
                        storeCode,
                        addressBody)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        shippingMethodEntities -> presenter.onShippingMethodReceived(shippingMethodEntities),
                        error -> presenter.onFailedReceivingShippingMethod(error)
                );
    }

    @Override
    public void saveShippingMethod(String storeCode, AddressShippingBody addressShippingBody) {
        getGeneralNetworkManager()
                .saveShippingMethod(
                        new NetworkHandler(),
                        storeCode
                        ,
                        addressShippingBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        paymentMethod -> presenter.onAddressSaved(paymentMethod),
                        error -> presenter.onFailedToSaveAddress()
                );
    }

    @Override
    public void savePaymentMethod(String storeCode, PaymentMethodBody paymentMethodBody) {
        getGeneralNetworkManager()
                .savePaymentMethod(
                        new NetworkHandler(),
                        storeCode,
                        paymentMethodBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        orderCode -> presenter.onPaymentSaved(orderCode),
                        error -> presenter.onFailedToSavePayment(error)
                );
    }

    @Override
    public void saveVa(String storeCode, VaBody vaBody) {
        getGeneralNetworkManager()
                .saveVa(
                        new NetworkHandler(),
                        storeCode,
                        vaBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.onVaSaved(),
                        e -> presenter.onSaveFailed(e)
                );
    }

    @Override
    public void getPaymentDesc(String storeCode) {
        getGeneralNetworkManager()
                .getPaymentDesc(new NetworkHandler(),storeCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        paymentDesc -> presenter.onPaymentDescReceived(paymentDesc),
                        error->presenter.onPaymentDescFailed(error)
                );
    }
}
