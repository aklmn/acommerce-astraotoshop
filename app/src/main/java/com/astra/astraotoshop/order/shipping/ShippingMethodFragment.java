package com.astra.astraotoshop.order.shipping;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 12/21/2017.
 */

public class ShippingMethodFragment extends DialogFragment {

    @BindView(R.id.productList)
    RecyclerView productList;
    Unbinder unbinder;
    @BindView(R.id.container)
    NestedScrollView container;

    public static ShippingMethodFragment newInstance() {

        Bundle args = new Bundle();

        ShippingMethodFragment fragment = new ShippingMethodFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shipping_method, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ShippingAdapter adapter = new ShippingAdapter();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        productList.setLayoutManager(layoutManager);
        productList.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        container.fullScroll(View.FOCUS_UP);
    }

    @OnClick(R.id.close)
    public void close() {
        this.dismiss();
    }

    @OnTouch(R.id.editShippingOption)
    public boolean editShippingOption(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            ShippingOption shippingOption = ShippingOption.newInstance();
            shippingOption.setStyle(STYLE_NO_TITLE, R.style.FullScreenDialog);
            shippingOption.show(getChildFragmentManager(), "shipping option");
        }
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
