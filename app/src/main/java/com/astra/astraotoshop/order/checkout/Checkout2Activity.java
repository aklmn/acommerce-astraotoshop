package com.astra.astraotoshop.order.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.provider.totals.CartTotalEntity;
import com.astra.astraotoshop.order.checkout.doku.DokuActivity;
import com.astra.astraotoshop.order.checkout.provider.CheckoutPresenter;
import com.astra.astraotoshop.order.checkout.provider.ShippingMethodEntity;
import com.astra.astraotoshop.order.checkout.provider.address.AddressBodyBuilder;
import com.astra.astraotoshop.order.checkout.provider.installment.PaymentDesc;
import com.astra.astraotoshop.order.checkout.provider.installment.TenorsItem;
import com.astra.astraotoshop.order.checkout.provider.payment.AvailablePaymentMethod;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodsItem;
import com.astra.astraotoshop.order.order.SuccessPageActivity;
import com.astra.astraotoshop.user.address.AddressListActivity;
import com.astra.astraotoshop.user.profile.provider.Addresses;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.pref.Pref;
import com.astra.astraotoshop.utils.view.Loading;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;

public class Checkout2Activity extends BaseActivity implements CheckoutContract.CheckoutView, AdapterView.OnItemSelectedListener {

    private final int RC_ADDRESS = 3;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.subTotal)
    TextView subTotal;
    @BindView(R.id.discountPrice)
    TextView discountPrice;
    @BindView(R.id.grandTotal)
    TextView grandTotal;
    @BindView(R.id.checkoutBtn)
    Button checkoutBtn;
    @BindView(R.id.shippingPrice)
    TextView shippingPrice;
    @BindView(R.id.priceContainer)
    ConstraintLayout priceContainer;
    @BindView(R.id.addressLabelContainer)
    RelativeLayout addressLabelContainer;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.addressContentContainer)
    LinearLayout addressContentContainer;
    @BindView(R.id.shippingLabelContainer)
    RelativeLayout shippingLabelContainer;
    @BindView(R.id.deliverName)
    TextView deliverName;
    @BindView(R.id.deliveryPrice)
    TextView deliveryPrice;
    @BindView(R.id.shippingContentContainer)
    LinearLayout shippingContentContainer;
    @BindView(R.id.shippingContainer)
    LinearLayout shippingContainer;
    @BindView(R.id.paymentLabelContainer)
    RelativeLayout paymentLabelContainer;
    @BindView(R.id.paymentMethod)
    TextView paymentMethod;
    @BindView(R.id.paymentImage)
    ImageView paymentImage;
    @BindView(R.id.paymentContentContainer)
    LinearLayout paymentContentContainer;
    @BindView(R.id.installmentContainer)
    LinearLayout installmentContainer;
    @BindView(R.id.bank)
    MaterialSpinner bankSpinner;
    @BindView(R.id.tenor)
    MaterialSpinner tenorSpinner;
    @BindView(R.id.scroll)
    NestedScrollView scroll;
    @BindView(R.id.discountCode)
    TextView discountCode;

    private boolean isFirst = true;

    private String paymentCode;
    private boolean isAddressSet = false;
    private CheckoutContract.CheckoutPresenter presenter;
    private ShippingMethodFragment shippingMethodFragment;
    private PaymentMethodFragment paymentMethodFragment;
    private ShippingMethodEntity shippingMethod;
    private int itemSelected = -1;
    Addresses addresses;
    private Loading loading;
    private List<PaymentMethodsItem> paymentMethods;
    private PaymentMethodsItem payment;
    private String bankName;
    private String tenor;
    private ArrayAdapter bankAdapter;
    private ArrayAdapter tenorAdapter;
    private PaymentDesc paymentDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout3);
        ButterKnife.bind(this);
        actionBarSetup();
        setTotal();
        presenter = new CheckoutPresenter(this, getStoreCode());
        shippingMethodFragment = ShippingMethodFragment.newInstance();
        paymentMethodFragment = PaymentMethodFragment.newInstance();
        loading = new Loading(this, getLayoutInflater());
        setDefaultAddress();

        if (getState() == getResources().getInteger(R.integer.state_product)) {
            shippingContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showLoading() {
        try {
            loading.show(getString(R.string.message_please_wait));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void hideLoading() {
        try {
            loading.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showShippingMethod(List<ShippingMethodEntity> shippingMethod) {
        hideLoading();
        if (isFirst) {
            if (shippingMethod.size() > 0) {
                setShippingMethod(shippingMethod.get(0), 0);
                return;
            }
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("shipping_fragment");
        if (fragment != null) {
            transaction.remove(fragment);
        }
        transaction.commit();
        if (itemSelected != -1) {
            shippingMethodFragment.setShippingMethods(shippingMethod, itemSelected);
        } else
            shippingMethodFragment.setShippingMethods(shippingMethod);
        shippingMethodFragment.show(getSupportFragmentManager(), "shipping_fragment");
    }

    @Override
    public void showPaymentMethod(AvailablePaymentMethod paymentMethod) {
        hideLoading();
        if (isFirst && paymentMethod != null) {
            if (paymentMethod.getPaymentMethods().size() > 0) {
                setPaymentMethod(paymentMethod.getPaymentMethods().get(0));
                isFirst = false;
            }
        }

        if (paymentMethod != null) {
            if (!isProductService()) {
                try {
                    shippingMethodFragment.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            paymentMethods = paymentMethod.getPaymentMethods();
            StringFormat.applyCurrencyFormat(subTotal, paymentMethod.getTotals().getSubtotal());
            StringFormat.applyCurrencyFormat(grandTotal, paymentMethod.getTotals().getGrandTotal());
            StringFormat.applyCurrencyFormat(discountPrice, paymentMethod.getTotals().getDiscountAmount());
            StringFormat.applyCurrencyFormat(shippingPrice, paymentMethod.getTotals().getShippingAmount());
        }
    }

    @Override
    public void showSuccessPage(String orderCode) {
        if (payment.getCode().contains("_va")) {
            presenter.saveVa(orderCode);
            return;
        }
        if (paymentCode.contains("installment")) {
            toDokuPage(orderCode);
            return;
        } else if (paymentCode.contains("cc")) {
            toDokuPage(orderCode);
            return;
        }

        loading.dismiss();
        toSuccessPage();
    }

    private void toDokuPage(String orderCode) {
        Intent intent = getIntent(this, DokuActivity.class);
        intent.putExtra("orderId", presenter.getOrderCode());
        intent.putExtra("address", addresses);
        intent.putExtra("shipping", shippingMethod);
        intent.putExtra("payment", payment);
        intent.putExtra("orderCode", orderCode);
        if (bankName != null) intent.putExtra("bankSpinner", bankName);
        if (tenor != null) intent.putExtra("tennor", tenor);
        startActivity(intent);
        finish();
    }

    @Override
    public void showMessageError(String message) {
        loading.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Ok", (dialog, which) -> {
                    dialog.dismiss();
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onVaSaved() {
        hideLoading();
        toSuccessPage();
    }

    @Override
    public void onSaveFailed() {
        loading.dismiss();
    }

    @Override
    public void showPaymentDesc(PaymentDesc paymentDesc) {
        this.paymentDesc = paymentDesc;
        installmentContainer.setVisibility(View.VISIBLE);
        bankAdapter.clear();
        bankAdapter.addAll(paymentDesc.getCcInstallmentHosted().getInstallmentBankList());
        bankSpinner.setAdapter(bankAdapter);
        tenorSpinner.post(() -> scroll.fullScroll(View.FOCUS_DOWN));
        hideLoading();
    }

    @Override
    public void showPaymentDescError() {
        hideLoading();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == RC_ADDRESS) {
            Addresses addresses = data.getParcelableExtra("data");
            if (addresses != null) {
                setAddress(addresses);
                itemSelected = -1;
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position >= 0) {
            if (parent.getId() == R.id.bank && parent.getSelectedItem() != null) {
                bankName = String.valueOf(parent.getSelectedItem());
                for (TenorsItem item : paymentDesc.getCcInstallmentHosted().getInstallmentTennorMapping()) {
                    if (item.getName().equalsIgnoreCase(String.valueOf(parent.getSelectedItem()))) {
                        tenorAdapter.clear();
                        tenorAdapter.addAll(item.getValue());
                        break;
                    }
                }
                checkoutBtn.setEnabled(false);
                tenor = null;
                tenorSpinner.setAdapter(tenorAdapter);
            } else {
                tenor = String.valueOf(parent.getSelectedItem());
                checkoutBtn.setEnabled(true);
            }
        } else {
            if (parent.getId() == R.id.bank) {
                bankName = null;
                tenor = null;
            } else
                tenor = null;

            checkoutBtn.setEnabled(false);
        }
    }

    private void applySpinnerAdapter(MaterialSpinner spinner, ArrayList<String> data) {
        spinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, data));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @OnClick({R.id.addressLabelContainer, R.id.addressContentContainer})
    public void setAddress() {
        startActivityForResult((getIntent(this, AddressListActivity.class)).putExtra("isFromCheckout", true), RC_ADDRESS);
    }

    @OnClick({R.id.shippingLabelContainer, R.id.shippingContentContainer})
    public void setShipping() {
        if (isAddressSet)
            presenter.getShippingMethod();
    }

    @OnClick({R.id.paymentLabelContainer, R.id.paymentContentContainer})
    public void setPaymentMethod() {
        hideLoading();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("payment_fragment");
        if (fragment != null) {
            transaction.remove(fragment);
        }
        transaction.commit();
        paymentMethodFragment.setMethodsItems(paymentMethods);
        paymentMethodFragment.show(getSupportFragmentManager(), "payment_fragment");
    }

    @OnClick(R.id.checkoutBtn)
    public void pay() {
        loading.show("Harap Tunggu");
        presenter.savePaymentMethod(paymentCode);
    }

    private void setDefaultAddress() {
        String address = Pref.getPreference().getString("address");
        if (address != null) {
            Addresses addresses = new Gson().fromJson(address, Addresses.class);
            if (addresses != null) {
                setAddress(addresses);
            }
        }
    }

    private void setAddress(Addresses addresses) {
        presenter.setAddressId(addresses.getId());
        setAddressValue(addresses);
        this.addresses = addresses;
        shippingMethod = null;
        shippingContentContainer.setVisibility(View.GONE);
        paymentMethods = new ArrayList<>();
        paymentContentContainer.setVisibility(View.GONE);
        isAddressSet = true;

        if (isFirst) {
            showLoading();
            presenter.getShippingMethod();
        }

        if (getState() == getResources().getInteger(R.integer.state_product_service)) {
            showLoading();
            requestPaymentMethod("acommerce", "noshippment");
        }
    }

    private void setTotal() {
        CartTotalEntity cartTotalEntity = getIntent().getParcelableExtra("total");
        if (cartTotalEntity != null) {
            StringFormat.applyCurrencyFormat(subTotal, cartTotalEntity.getBaseSubtotal());
            StringFormat.applyCurrencyFormat(grandTotal, cartTotalEntity.getBaseGrandTotal());
            StringFormat.applyCurrencyFormat(discountPrice, cartTotalEntity.getBaseDiscountAmount());
            showDiscountCode(cartTotalEntity.getCouponCode());
        }
    }

    private void showDiscountCode(String couponCode) {
        if (couponCode != null) {
            discountCode.setText(String.format(getString(R.string.label_discount_code) + "(%s)", couponCode));
        } else {
            discountCode.setText(getString(R.string.label_discount_code));
        }
    }

    private void setAddressValue(Addresses addresses) {
        this.addresses = addresses;
        StringFormat.setText(name, addresses.getFirstname() + " " + addresses.getLastname());
        StringFormat.setText(address, StringFormat.getAddress(addresses));
        addressContentContainer.setVisibility(View.VISIBLE);
    }

    public void setShippingMethod(ShippingMethodEntity shippingMethod, int itemSelected) {
        showLoading();
        this.shippingMethod = shippingMethod;
        this.itemSelected = itemSelected;
        shippingContentContainer.setVisibility(View.VISIBLE);
        deliverName.setText(shippingMethod.getMethodTitle());
        StringFormat.applyCurrencyFormat(deliveryPrice, shippingMethod.getAmount());
        requestPaymentMethod(shippingMethod.getCarrierCode(), shippingMethod.getMethodCode());
    }

    private void requestPaymentMethod(String carrierCode, String methodCode) {
        presenter.saveShippingMethod(AddressBodyBuilder.getAddressBody(
                addresses, carrierCode, methodCode));
    }

    public void setPaymentMethod(@NonNull PaymentMethodsItem mPaymentMethod) {
        this.payment = mPaymentMethod;
        paymentCode = mPaymentMethod.getCode();
        try {
            paymentMethodFragment.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        paymentContentContainer.setVisibility(View.VISIBLE);
        paymentMethod.setText(mPaymentMethod.getTitle());
        if (mPaymentMethod.getCode().contains("installment")) {
            presenter.getPaymentDesc();
            loading.show("Harap Tunggu");
            if (bankAdapter == null)
                bankAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
            if (tenorAdapter == null)
                tenorAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
            bankSpinner.setAdapter(bankAdapter);
            bankSpinner.setOnItemSelectedListener(this);
            tenorSpinner.setAdapter(tenorAdapter);
            tenorSpinner.setOnItemSelectedListener(this);
        } else {
            installmentContainer.setVisibility(View.GONE);
            checkoutBtn.setEnabled(true);
        }
    }

    private void toSuccessPage() {
        Intent intent = getIntent(this, SuccessPageActivity.class);
        intent.putExtra("storeCode", getStoreCode());
        intent.putExtra("orderId", presenter.getOrderCode());
        intent.putExtra("address", addresses);
        intent.putExtra("shipping", shippingMethod);
        intent.putExtra("payment", payment);
        startActivity(intent);
    }
}
