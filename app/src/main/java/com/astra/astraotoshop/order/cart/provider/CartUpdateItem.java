package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

public class CartUpdateItem {

	@SerializedName("item_id")
	private int itemId;

	@SerializedName("quote_id")
	private int quoteId;

	@SerializedName("qty")
	private int qty;

	@SerializedName("sku")
	private String sku;

	@SerializedName("product_option")
	private ProductOption productOption;

	public CartUpdateItem(int itemId, int quoteId, int qty, String sku, ProductOption productOption) {
		this.itemId = itemId;
		this.quoteId = quoteId;
		this.qty = qty;
		this.sku = sku;
		this.productOption = productOption;
	}



	public CartUpdateItem() {
    }

    public void setQuoteId(int quoteId){
		this.quoteId = quoteId;
	}

	public int getQuoteId(){
		return quoteId;
	}

	public void setQty(int qty){
		this.qty = qty;
	}

	public int getQty(){
		return qty;
	}

	public void setSku(String sku){
		this.sku = sku;
	}

	public String getSku(){
		return sku;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	@Override
 	public String toString(){
		return 
			"cart_item{" +
			"item_id : '" + itemId + '\'' +
			",quote_id : '" + quoteId + '\'' +
			",qty : '" + qty + '\'' +
			",sku : '" + sku + '\'' +
			"}";
		}
}