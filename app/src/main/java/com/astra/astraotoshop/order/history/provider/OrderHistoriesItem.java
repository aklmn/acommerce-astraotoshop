package com.astra.astraotoshop.order.history.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderHistoriesItem implements Parcelable{

    @SerializedName("tax_amount")
    private int taxAmount;

    @SerializedName("customer_note_notify")
    private int customerNoteNotify;

    @SerializedName("shipping_discount_amount")
    private int shippingDiscountAmount;

    @SerializedName("customer_firstname")
    private String customerFirstname;

    @SerializedName("discount_amount")
    private int discountAmount;

    @SerializedName("remote_ip")
    private String remoteIp;

    @SerializedName("increment_id")
    private String incrementId;

    @SerializedName("state")
    private String state;

    @SerializedName("base_shipping_tax_amount")
    private int baseShippingTaxAmount;

    @SerializedName("base_grand_total")
    private int baseGrandTotal;

    @SerializedName("billing_address_id")
    private int billingAddressId;

    @SerializedName("customer_lastname")
    private String customerLastname;

    @SerializedName("quote_id")
    private int quoteId;

    @SerializedName("discount_tax_compensation_amount")
    private int discountTaxCompensationAmount;

    @SerializedName("weight")
    private double weight;

    @SerializedName("entity_id")
    private int entityId;

    @SerializedName("base_shipping_amount")
    private int baseShippingAmount;

    @SerializedName("subtotal_incl_tax")
    private int subtotalInclTax;

    @SerializedName("subtotal")
    private int subtotal;

    @SerializedName("base_shipping_incl_tax")
    private int baseShippingInclTax;

    @SerializedName("customer_email")
    private String customerEmail;

    @SerializedName("base_to_global_rate")
    private int baseToGlobalRate;

    @SerializedName("customer_is_guest")
    private int customerIsGuest;

    @SerializedName("items")
    private List<OrderHistoriesItem> items;

    @SerializedName("global_currency_code")
    private String globalCurrencyCode;

    @SerializedName("status")
    private String status;

    @SerializedName("is_virtual")
    private int isVirtual;

    @SerializedName("email_sent")
    private int emailSent;

    @SerializedName("store_currency_code")
    private String storeCurrencyCode;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("total_item_count")
    private int totalItemCount;

    @SerializedName("shipping_tax_amount")
    private int shippingTaxAmount;

    @SerializedName("store_to_base_rate")
    private int storeToBaseRate;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("base_shipping_discount_amount")
    private int baseShippingDiscountAmount;

    @SerializedName("store_name")
    private String storeName;

    @SerializedName("grand_total")
    private int grandTotal;

    @SerializedName("base_currency_code")
    private String baseCurrencyCode;

    @SerializedName("base_tax_amount")
    private int baseTaxAmount;

    @SerializedName("store_id")
    private int storeId;

    @SerializedName("shipping_discount_tax_compensation_amount")
    private int shippingDiscountTaxCompensationAmount;

    @SerializedName("total_due")
    private int totalDue;

    @SerializedName("total_qty_ordered")
    private int totalQtyOrdered;

    @SerializedName("base_discount_amount")
    private int baseDiscountAmount;

    @SerializedName("shipping_description")
    private String shippingDescription;

    @SerializedName("store_to_order_rate")
    private int storeToOrderRate;

    @SerializedName("shipping_amount")
    private int shippingAmount;

    @SerializedName("base_discount_tax_compensation_amount")
    private int baseDiscountTaxCompensationAmount;

    @SerializedName("base_to_order_rate")
    private int baseToOrderRate;

    @SerializedName("base_subtotal")
    private int baseSubtotal;

    @SerializedName("protect_code")
    private String protectCode;

    @SerializedName("base_total_due")
    private int baseTotalDue;

    @SerializedName("base_subtotal_incl_tax")
    private int baseSubtotalInclTax;

    @SerializedName("customer_id")
    private int customerId;

    @SerializedName("customer_group_id")
    private int customerGroupId;

    @SerializedName("order_currency_code")
    private String orderCurrencyCode;

    @SerializedName("customer_gender")
    private int customerGender;

    @SerializedName("shipping_incl_tax")
    private int shippingInclTax;

    protected OrderHistoriesItem(Parcel in) {
        taxAmount = in.readInt();
        customerNoteNotify = in.readInt();
        shippingDiscountAmount = in.readInt();
        customerFirstname = in.readString();
        discountAmount = in.readInt();
        remoteIp = in.readString();
        incrementId = in.readString();
        state = in.readString();
        baseShippingTaxAmount = in.readInt();
        baseGrandTotal = in.readInt();
        billingAddressId = in.readInt();
        customerLastname = in.readString();
        quoteId = in.readInt();
        discountTaxCompensationAmount = in.readInt();
        weight = in.readDouble();
        entityId = in.readInt();
        baseShippingAmount = in.readInt();
        subtotalInclTax = in.readInt();
        subtotal = in.readInt();
        baseShippingInclTax = in.readInt();
        customerEmail = in.readString();
        baseToGlobalRate = in.readInt();
        customerIsGuest = in.readInt();
        items = in.createTypedArrayList(OrderHistoriesItem.CREATOR);
        globalCurrencyCode = in.readString();
        status = in.readString();
        isVirtual = in.readInt();
        emailSent = in.readInt();
        storeCurrencyCode = in.readString();
        createdAt = in.readString();
        totalItemCount = in.readInt();
        shippingTaxAmount = in.readInt();
        storeToBaseRate = in.readInt();
        updatedAt = in.readString();
        baseShippingDiscountAmount = in.readInt();
        storeName = in.readString();
        grandTotal = in.readInt();
        baseCurrencyCode = in.readString();
        baseTaxAmount = in.readInt();
        storeId = in.readInt();
        shippingDiscountTaxCompensationAmount = in.readInt();
        totalDue = in.readInt();
        totalQtyOrdered = in.readInt();
        baseDiscountAmount = in.readInt();
        shippingDescription = in.readString();
        storeToOrderRate = in.readInt();
        shippingAmount = in.readInt();
        baseDiscountTaxCompensationAmount = in.readInt();
        baseToOrderRate = in.readInt();
        baseSubtotal = in.readInt();
        protectCode = in.readString();
        baseTotalDue = in.readInt();
        baseSubtotalInclTax = in.readInt();
        customerId = in.readInt();
        customerGroupId = in.readInt();
        orderCurrencyCode = in.readString();
        customerGender = in.readInt();
        shippingInclTax = in.readInt();
    }

    public static final Creator<OrderHistoriesItem> CREATOR = new Creator<OrderHistoriesItem>() {
        @Override
        public OrderHistoriesItem createFromParcel(Parcel in) {
            return new OrderHistoriesItem(in);
        }

        @Override
        public OrderHistoriesItem[] newArray(int size) {
            return new OrderHistoriesItem[size];
        }
    };

    public void setTaxAmount(int taxAmount) {
        this.taxAmount = taxAmount;
    }

    public int getTaxAmount() {
        return taxAmount;
    }

    public void setCustomerNoteNotify(int customerNoteNotify) {
        this.customerNoteNotify = customerNoteNotify;
    }

    public int getCustomerNoteNotify() {
        return customerNoteNotify;
    }

    public void setShippingDiscountAmount(int shippingDiscountAmount) {
        this.shippingDiscountAmount = shippingDiscountAmount;
    }

    public int getShippingDiscountAmount() {
        return shippingDiscountAmount;
    }

    public void setCustomerFirstname(String customerFirstname) {
        this.customerFirstname = customerFirstname;
    }

    public String getCustomerFirstname() {
        return customerFirstname;
    }

    public void setDiscountAmount(int discountAmount) {
        this.discountAmount = discountAmount;
    }

    public int getDiscountAmount() {
        return discountAmount;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setIncrementId(String incrementId) {
        this.incrementId = incrementId;
    }

    public String getIncrementId() {
        return incrementId;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public void setBaseShippingTaxAmount(int baseShippingTaxAmount) {
        this.baseShippingTaxAmount = baseShippingTaxAmount;
    }

    public int getBaseShippingTaxAmount() {
        return baseShippingTaxAmount;
    }

    public void setBaseGrandTotal(int baseGrandTotal) {
        this.baseGrandTotal = baseGrandTotal;
    }

    public int getBaseGrandTotal() {
        return baseGrandTotal;
    }

    public void setBillingAddressId(int billingAddressId) {
        this.billingAddressId = billingAddressId;
    }

    public int getBillingAddressId() {
        return billingAddressId;
    }

    public void setCustomerLastname(String customerLastname) {
        this.customerLastname = customerLastname;
    }

    public String getCustomerLastname() {
        return customerLastname;
    }

    public void setQuoteId(int quoteId) {
        this.quoteId = quoteId;
    }

    public int getQuoteId() {
        return quoteId;
    }

    public void setDiscountTaxCompensationAmount(int discountTaxCompensationAmount) {
        this.discountTaxCompensationAmount = discountTaxCompensationAmount;
    }

    public int getDiscountTaxCompensationAmount() {
        return discountTaxCompensationAmount;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setBaseShippingAmount(int baseShippingAmount) {
        this.baseShippingAmount = baseShippingAmount;
    }

    public int getBaseShippingAmount() {
        return baseShippingAmount;
    }

    public void setSubtotalInclTax(int subtotalInclTax) {
        this.subtotalInclTax = subtotalInclTax;
    }

    public int getSubtotalInclTax() {
        return subtotalInclTax;
    }

    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public void setBaseShippingInclTax(int baseShippingInclTax) {
        this.baseShippingInclTax = baseShippingInclTax;
    }

    public int getBaseShippingInclTax() {
        return baseShippingInclTax;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setBaseToGlobalRate(int baseToGlobalRate) {
        this.baseToGlobalRate = baseToGlobalRate;
    }

    public int getBaseToGlobalRate() {
        return baseToGlobalRate;
    }

    public void setCustomerIsGuest(int customerIsGuest) {
        this.customerIsGuest = customerIsGuest;
    }

    public int getCustomerIsGuest() {
        return customerIsGuest;
    }

    public void setItems(List<OrderHistoriesItem> items) {
        this.items = items;
    }

    public List<OrderHistoriesItem> getItems() {
        return items;
    }

    public void setGlobalCurrencyCode(String globalCurrencyCode) {
        this.globalCurrencyCode = globalCurrencyCode;
    }

    public String getGlobalCurrencyCode() {
        return globalCurrencyCode;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setIsVirtual(int isVirtual) {
        this.isVirtual = isVirtual;
    }

    public int getIsVirtual() {
        return isVirtual;
    }

    public void setEmailSent(int emailSent) {
        this.emailSent = emailSent;
    }

    public int getEmailSent() {
        return emailSent;
    }

    public void setStoreCurrencyCode(String storeCurrencyCode) {
        this.storeCurrencyCode = storeCurrencyCode;
    }

    public String getStoreCurrencyCode() {
        return storeCurrencyCode;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setTotalItemCount(int totalItemCount) {
        this.totalItemCount = totalItemCount;
    }

    public int getTotalItemCount() {
        return totalItemCount;
    }

    public void setShippingTaxAmount(int shippingTaxAmount) {
        this.shippingTaxAmount = shippingTaxAmount;
    }

    public int getShippingTaxAmount() {
        return shippingTaxAmount;
    }

    public void setStoreToBaseRate(int storeToBaseRate) {
        this.storeToBaseRate = storeToBaseRate;
    }

    public int getStoreToBaseRate() {
        return storeToBaseRate;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setBaseShippingDiscountAmount(int baseShippingDiscountAmount) {
        this.baseShippingDiscountAmount = baseShippingDiscountAmount;
    }

    public int getBaseShippingDiscountAmount() {
        return baseShippingDiscountAmount;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setGrandTotal(int grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getGrandTotal() {
        return grandTotal;
    }

    public void setBaseCurrencyCode(String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }

    public String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    public void setBaseTaxAmount(int baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }

    public int getBaseTaxAmount() {
        return baseTaxAmount;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setShippingDiscountTaxCompensationAmount(int shippingDiscountTaxCompensationAmount) {
        this.shippingDiscountTaxCompensationAmount = shippingDiscountTaxCompensationAmount;
    }

    public int getShippingDiscountTaxCompensationAmount() {
        return shippingDiscountTaxCompensationAmount;
    }

    public void setTotalDue(int totalDue) {
        this.totalDue = totalDue;
    }

    public int getTotalDue() {
        return totalDue;
    }

    public void setTotalQtyOrdered(int totalQtyOrdered) {
        this.totalQtyOrdered = totalQtyOrdered;
    }

    public int getTotalQtyOrdered() {
        return totalQtyOrdered;
    }

    public void setBaseDiscountAmount(int baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }

    public int getBaseDiscountAmount() {
        return baseDiscountAmount;
    }

    public void setShippingDescription(String shippingDescription) {
        this.shippingDescription = shippingDescription;
    }

    public String getShippingDescription() {
        return shippingDescription;
    }

    public void setStoreToOrderRate(int storeToOrderRate) {
        this.storeToOrderRate = storeToOrderRate;
    }

    public int getStoreToOrderRate() {
        return storeToOrderRate;
    }

    public void setShippingAmount(int shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    public int getShippingAmount() {
        return shippingAmount;
    }

    public void setBaseDiscountTaxCompensationAmount(int baseDiscountTaxCompensationAmount) {
        this.baseDiscountTaxCompensationAmount = baseDiscountTaxCompensationAmount;
    }

    public int getBaseDiscountTaxCompensationAmount() {
        return baseDiscountTaxCompensationAmount;
    }

    public void setBaseToOrderRate(int baseToOrderRate) {
        this.baseToOrderRate = baseToOrderRate;
    }

    public int getBaseToOrderRate() {
        return baseToOrderRate;
    }

    public void setBaseSubtotal(int baseSubtotal) {
        this.baseSubtotal = baseSubtotal;
    }

    public int getBaseSubtotal() {
        return baseSubtotal;
    }

    public void setProtectCode(String protectCode) {
        this.protectCode = protectCode;
    }

    public String getProtectCode() {
        return protectCode;
    }

    public void setBaseTotalDue(int baseTotalDue) {
        this.baseTotalDue = baseTotalDue;
    }

    public int getBaseTotalDue() {
        return baseTotalDue;
    }

    public void setBaseSubtotalInclTax(int baseSubtotalInclTax) {
        this.baseSubtotalInclTax = baseSubtotalInclTax;
    }

    public int getBaseSubtotalInclTax() {
        return baseSubtotalInclTax;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerGroupId(int customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public int getCustomerGroupId() {
        return customerGroupId;
    }

    public void setOrderCurrencyCode(String orderCurrencyCode) {
        this.orderCurrencyCode = orderCurrencyCode;
    }

    public String getOrderCurrencyCode() {
        return orderCurrencyCode;
    }

    public void setCustomerGender(int customerGender) {
        this.customerGender = customerGender;
    }

    public int getCustomerGender() {
        return customerGender;
    }

    public void setShippingInclTax(int shippingInclTax) {
        this.shippingInclTax = shippingInclTax;
    }

    public int getShippingInclTax() {
        return shippingInclTax;
    }

    @Override
    public String toString() {
        return
                "OrderHistoriesItem{" +
                        "tax_amount = '" + taxAmount + '\'' +
                        ",customer_note_notify = '" + customerNoteNotify + '\'' +
                        ",shipping_discount_amount = '" + shippingDiscountAmount + '\'' +
                        ",customer_firstname = '" + customerFirstname + '\'' +
                        ",discount_amount = '" + discountAmount + '\'' +
                        ",remote_ip = '" + remoteIp + '\'' +
                        ",increment_id = '" + incrementId + '\'' +
                        ",state = '" + state + '\'' +
                        ",base_shipping_tax_amount = '" + baseShippingTaxAmount + '\'' +
                        ",base_grand_total = '" + baseGrandTotal + '\'' +
                        ",billing_address_id = '" + billingAddressId + '\'' +
                        ",customer_lastname = '" + customerLastname + '\'' +
                        ",quote_id = '" + quoteId + '\'' +
                        ",discount_tax_compensation_amount = '" + discountTaxCompensationAmount + '\'' +
                        ",weight = '" + weight + '\'' +
                        ",entity_id = '" + entityId + '\'' +
                        ",base_shipping_amount = '" + baseShippingAmount + '\'' +
                        ",subtotal_incl_tax = '" + subtotalInclTax + '\'' +
                        ",subtotal = '" + subtotal + '\'' +
                        ",base_shipping_incl_tax = '" + baseShippingInclTax + '\'' +
                        ",customer_email = '" + customerEmail + '\'' +
                        ",base_to_global_rate = '" + baseToGlobalRate + '\'' +
                        ",customer_is_guest = '" + customerIsGuest + '\'' +
                        ",items = '" + items + '\'' +
                        ",global_currency_code = '" + globalCurrencyCode + '\'' +
                        ",status = '" + status + '\'' +
                        ",is_virtual = '" + isVirtual + '\'' +
                        ",email_sent = '" + emailSent + '\'' +
                        ",store_currency_code = '" + storeCurrencyCode + '\'' +
                        ",created_at = '" + createdAt + '\'' +
                        ",total_item_count = '" + totalItemCount + '\'' +
                        ",shipping_tax_amount = '" + shippingTaxAmount + '\'' +
                        ",store_to_base_rate = '" + storeToBaseRate + '\'' +
                        ",updated_at = '" + updatedAt + '\'' +
                        ",base_shipping_discount_amount = '" + baseShippingDiscountAmount + '\'' +
                        ",store_name = '" + storeName + '\'' +
                        ",grand_total = '" + grandTotal + '\'' +
                        ",base_currency_code = '" + baseCurrencyCode + '\'' +
                        ",base_tax_amount = '" + baseTaxAmount + '\'' +
                        ",store_id = '" + storeId + '\'' +
                        ",shipping_discount_tax_compensation_amount = '" + shippingDiscountTaxCompensationAmount + '\'' +
                        ",total_due = '" + totalDue + '\'' +
                        ",total_qty_ordered = '" + totalQtyOrdered + '\'' +
                        ",base_discount_amount = '" + baseDiscountAmount + '\'' +
                        ",shipping_description = '" + shippingDescription + '\'' +
                        ",store_to_order_rate = '" + storeToOrderRate + '\'' +
                        ",shipping_amount = '" + shippingAmount + '\'' +
                        ",base_discount_tax_compensation_amount = '" + baseDiscountTaxCompensationAmount + '\'' +
                        ",base_to_order_rate = '" + baseToOrderRate + '\'' +
                        ",base_subtotal = '" + baseSubtotal + '\'' +
                        ",protect_code = '" + protectCode + '\'' +
                        ",base_total_due = '" + baseTotalDue + '\'' +
                        ",base_subtotal_incl_tax = '" + baseSubtotalInclTax + '\'' +
                        ",customer_id = '" + customerId + '\'' +
                        ",customer_group_id = '" + customerGroupId + '\'' +
                        ",order_currency_code = '" + orderCurrencyCode + '\'' +
                        ",customer_gender = '" + customerGender + '\'' +
                        ",shipping_incl_tax = '" + shippingInclTax + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(taxAmount);
        dest.writeInt(customerNoteNotify);
        dest.writeInt(shippingDiscountAmount);
        dest.writeString(customerFirstname);
        dest.writeInt(discountAmount);
        dest.writeString(remoteIp);
        dest.writeString(incrementId);
        dest.writeString(state);
        dest.writeInt(baseShippingTaxAmount);
        dest.writeInt(baseGrandTotal);
        dest.writeInt(billingAddressId);
        dest.writeString(customerLastname);
        dest.writeInt(quoteId);
        dest.writeInt(discountTaxCompensationAmount);
        dest.writeDouble(weight);
        dest.writeInt(entityId);
        dest.writeInt(baseShippingAmount);
        dest.writeInt(subtotalInclTax);
        dest.writeInt(subtotal);
        dest.writeInt(baseShippingInclTax);
        dest.writeString(customerEmail);
        dest.writeInt(baseToGlobalRate);
        dest.writeInt(customerIsGuest);
        dest.writeTypedList(items);
        dest.writeString(globalCurrencyCode);
        dest.writeString(status);
        dest.writeInt(isVirtual);
        dest.writeInt(emailSent);
        dest.writeString(storeCurrencyCode);
        dest.writeString(createdAt);
        dest.writeInt(totalItemCount);
        dest.writeInt(shippingTaxAmount);
        dest.writeInt(storeToBaseRate);
        dest.writeString(updatedAt);
        dest.writeInt(baseShippingDiscountAmount);
        dest.writeString(storeName);
        dest.writeInt(grandTotal);
        dest.writeString(baseCurrencyCode);
        dest.writeInt(baseTaxAmount);
        dest.writeInt(storeId);
        dest.writeInt(shippingDiscountTaxCompensationAmount);
        dest.writeInt(totalDue);
        dest.writeInt(totalQtyOrdered);
        dest.writeInt(baseDiscountAmount);
        dest.writeString(shippingDescription);
        dest.writeInt(storeToOrderRate);
        dest.writeInt(shippingAmount);
        dest.writeInt(baseDiscountTaxCompensationAmount);
        dest.writeInt(baseToOrderRate);
        dest.writeInt(baseSubtotal);
        dest.writeString(protectCode);
        dest.writeInt(baseTotalDue);
        dest.writeInt(baseSubtotalInclTax);
        dest.writeInt(customerId);
        dest.writeInt(customerGroupId);
        dest.writeString(orderCurrencyCode);
        dest.writeInt(customerGender);
        dest.writeInt(shippingInclTax);
    }
}