package com.astra.astraotoshop.order.checkout;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.user.address.CreateAddressActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/22/2017.
 */

public class CheckoutAddressFragment extends Fragment {

    @BindView(R.id.addressList)
    RecyclerView addressList;
    Unbinder unbinder;
    CheckoutPageListener pageListener;

    public static CheckoutAddressFragment newInstance() {

        Bundle args = new Bundle();

        CheckoutAddressFragment fragment = new CheckoutAddressFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_checkout_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CheckoutAddressAdapter adapter = new CheckoutAddressAdapter(getContext());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());

        addressList.setLayoutManager(layoutManager);
        addressList.setAdapter(adapter);
    }

    @OnClick(R.id.addAddress)
    public void addAddress(){
        startActivity(new Intent(getContext(), CreateAddressActivity.class));
    }

    @OnClick(R.id.nextPaymentMethod)
    public void nextPaymentMethod(){
        try {
            pageListener.changePagePosition(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setPageListener(CheckoutPageListener pageListener) {
        this.pageListener = pageListener;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public interface CheckoutPageListener{
        void changePagePosition(int position);
    }
}
