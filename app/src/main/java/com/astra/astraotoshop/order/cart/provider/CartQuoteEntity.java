package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Henra Setia Nugraha on 1/17/2018.
 */

public class CartQuoteEntity {
    @SerializedName("customer_id")
    String userId;

    public CartQuoteEntity(String userId) {
        this.userId = userId;
    }
}
