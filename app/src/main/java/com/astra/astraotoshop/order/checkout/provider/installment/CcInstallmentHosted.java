package com.astra.astraotoshop.order.checkout.provider.installment;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CcInstallmentHosted {

    @SerializedName("instructions")
    private String instructions;

    @SerializedName("is_installment_active")
    private String isInstallmentActive;

    @SerializedName("installment_bank_list")
    private List<String> installmentBankList;

    @SerializedName("installment_tennor_mapping")
    List<TenorsItem> installmentTennorMapping;

    public List<TenorsItem> getInstallmentTennorMapping() {
        return installmentTennorMapping;
    }

    public void setInstallmentTennorMapping(List<TenorsItem> installmentTennorMapping) {
        this.installmentTennorMapping = installmentTennorMapping;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setIsInstallmentActive(String isInstallmentActive) {
        this.isInstallmentActive = isInstallmentActive;
    }

    public String getIsInstallmentActive() {
        return isInstallmentActive;
    }

    public void setInstallmentBankList(List<String> installmentBankList) {
        this.installmentBankList = installmentBankList;
    }

    public List<String> getInstallmentBankList() {
        return installmentBankList;
    }

    @Override
    public String toString() {
        return
                "CcInstallmentHosted{" +
                        "instructions = '" + instructions + '\'' +
                        ",installment_tennor_mapping = '" + installmentTennorMapping + '\'' +
                        ",is_installment_active = '" + isInstallmentActive + '\'' +
                        ",installment_bank_list = '" + installmentBankList + '\'' +
                        "}";
    }
}