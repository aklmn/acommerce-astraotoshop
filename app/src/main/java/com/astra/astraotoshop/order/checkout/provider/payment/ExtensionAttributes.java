package com.astra.astraotoshop.order.checkout.provider.payment;

import com.google.gson.annotations.SerializedName;

public class ExtensionAttributes {

    @SerializedName("reward_currency_amount")
    private int rewardCurrencyAmount;

    @SerializedName("reward_points_balance")
    private int rewardPointsBalance;

    @SerializedName("base_reward_currency_amount")
    private int baseRewardCurrencyAmount;

    public void setRewardCurrencyAmount(int rewardCurrencyAmount) {
        this.rewardCurrencyAmount = rewardCurrencyAmount;
    }

    public int getRewardCurrencyAmount() {
        return rewardCurrencyAmount;
    }

    public void setRewardPointsBalance(int rewardPointsBalance) {
        this.rewardPointsBalance = rewardPointsBalance;
    }

    public int getRewardPointsBalance() {
        return rewardPointsBalance;
    }

    public void setBaseRewardCurrencyAmount(int baseRewardCurrencyAmount) {
        this.baseRewardCurrencyAmount = baseRewardCurrencyAmount;
    }

    public int getBaseRewardCurrencyAmount() {
        return baseRewardCurrencyAmount;
    }

    @Override
    public String toString() {
        return
                "AppExtensionAttributes{" +
                        "reward_currency_amount = '" + rewardCurrencyAmount + '\'' +
                        ",reward_points_balance = '" + rewardPointsBalance + '\'' +
                        ",base_reward_currency_amount = '" + baseRewardCurrencyAmount + '\'' +
                        "}";
    }
}