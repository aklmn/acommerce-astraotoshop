package com.astra.astraotoshop.order.cart;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartListItem;
import com.astra.astraotoshop.order.cart.provider.CartPresenter;
import com.astra.astraotoshop.order.cart.provider.totals.CartPricePresenter;
import com.astra.astraotoshop.order.cart.provider.totals.CartTotalEntity;
import com.astra.astraotoshop.order.cart.provider.voucher.CartVoucherPresenter;
import com.astra.astraotoshop.order.checkout.Checkout2Activity;
import com.astra.astraotoshop.order.checkout.revamp.CheckoutActivity;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.base.BaseDialogFragment;
import com.astra.astraotoshop.utils.view.FontIcon;
import com.astra.astraotoshop.utils.view.FontIconic;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Subscriber;

/**
 * Created by Henra Setia Nugraha on 12/30/2017.
 */

public class CartFragment extends BaseDialogFragment implements CartContract.CartView, ListListener, CartContract.CartPriceView, CartContract.CartVoucherView {

    @BindView(R.id.itemList)
    RecyclerView itemList;
    @BindView(R.id.itemCount)
    TextView itemCount;
    Unbinder unbinder;
    @BindView(R.id.subtotal)
    TextView subtotal;
    @BindView(R.id.discountPrice)
    TextView discountPrice;
    @BindView(R.id.voucherCode)
    TextView voucherCode;
    @BindView(R.id.grandTotal)
    TextView grandTotal;
    @BindView(R.id.voucher)
    EditText voucher;
    @BindView(R.id.deleteVoucherBtn)
    Button deleteVoucherBtn;
    @BindView(R.id.pay)
    Button pay;
    @BindView(R.id.voucherContainer)
    TextInputLayout voucherContainer;
    @BindView(R.id.loading)
    ProgressBar loading;
    @BindView(R.id.shippingAmount)
    TextView shippingAmount;
    @BindView(R.id.discountContainer)
    LinearLayout discountContainer;
    @BindView(R.id.iconCollapseDC)
    FontIcon iconCollapseDC;
    private CartContract.CartPresenter presenter;
    private CartContract.CartPricePresenter pricePresenter;
    private CartContract.CartVoucherPresenter voucherPresenter;
    private CartAdapter cartAdapter;
    int itemPosition = -1;
    private CartAppointmentAdapter cartServiceAdapter;
    private Dialog dialog;

    public static CartFragment newInstance(int state) {

        Bundle args = new Bundle();
        args.putInt(STATE, state);
        CartFragment fragment = new CartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.new_checkout, container, false);
        unbinder = ButterKnife.bind(this, view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        itemList.setLayoutManager(layoutManager);
        itemList.setNestedScrollingEnabled(false);
        presenter = new CartPresenter(this, null, "product");
        pricePresenter = new CartPricePresenter(this);
        voucherPresenter = new CartVoucherPresenter(this,
                getResources().getStringArray(R.array.storeCode)[getState()]);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getState() == getResources().getInteger(R.integer.state_product_service))
            setupProductServiceList();
        else if ((getState() == getResources().getInteger(R.integer.state_product)))
            setupProductServiceList();

        presenter.requestCartList();
        pricePresenter.getTotal(getStoreCode());
    }

    @Override
    public void onResume() {
        super.onResume();
        loading.setVisibility(View.VISIBLE);
        presenter.requestCartList();
        pricePresenter.getTotal(getStoreCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            getDialog().getWindow().setWindowAnimations(R.style.DialogAnimation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupProductServiceList() {
        cartAdapter = new CartAdapter(getContext(), this);
        itemList.setHasFixedSize(true);
        itemList.setAdapter(cartAdapter);
        cartServiceAdapter = new CartAppointmentAdapter(getContext(), this);
    }

    private void setupProductList() {
        CartAppointmentAdapter productAdapter = new CartAppointmentAdapter(getContext(), null);
        itemList.setAdapter(productAdapter);
    }

    @OnClick(R.id.pay)
    public void checkout() {
        Intent intent = new Intent(getActivity(), CheckoutActivity.class);
        intent.putExtra(STATE, getState());
        intent.putExtra("total", pricePresenter.getTotal());
        getActivity().startActivity(intent);
        this.dismiss();
    }

    @OnClick(R.id.voucherBtn)
    public void addVoucher() {
        voucherPresenter.addVoucher(voucher.getText().toString());
    }

    @OnClick(R.id.deleteVoucherBtn)
    public void deleteVoucher() {
        voucherPresenter.deleteVoucher();
    }

    @OnClick(R.id.close)
    public void close() {
        dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setCartList(List<CartListItem> items) {
        loading.setVisibility(View.GONE);
//        if (getState() == getResources().getInteger(R.integer.state_product_service)) {
//            cartServiceAdapter.setCarts(items);
//            itemList.setAdapter(cartServiceAdapter);
//        } else {
        cartAdapter.setCartItems(items);
        itemList.setAdapter(cartAdapter);
//        }
        if (items.size() > 0) {
            itemCount.setText(items.size() + " produk ditemukan");
        } else {
            itemCount.setText("Produk tidak ditemukan");
        }

        if (cartAdapter.getItemCount() > 0 || cartServiceAdapter.getItemCount() > 0)
            pay.setEnabled(true);
        else pay.setEnabled(false);
    }

    @Override
    public void onCartItemDeleted() {
        if (isProductService()) cartServiceAdapter.deleteItem(itemPosition);
        else cartAdapter.deleteItem(itemPosition);

        itemCount.setText(cartAdapter.getItemCount() + " produk ditemukan");
        pricePresenter.getTotal(getResources().getStringArray(R.array.storeCode)[getState()]);
        if (cartAdapter.getItemCount() > 0) pay.setEnabled(true);
        else {
            pay.setEnabled(false);
            BaseActivity baseActivity = (BaseActivity) getActivity();
            if (baseActivity != null) {
                baseActivity.setCartBadgeVisibility(false);
            }
        }
    }

    @Override
    public void onCartUpdated() {
//        CartAdapter.CartViewHolder viewHolder = (CartAdapter.CartViewHolder) itemList.findViewHolderForAdapterPosition(itemPosition);
//        TextView textView = viewHolder.update;
//        textView.setVisibility(View.GONE);
        pricePresenter.getTotal("product");
        cartAdapter.itemUpdated(itemPosition);
    }

    @Override
    public void setTotalCart(CartTotalEntity cart) {
        if (cart.getCouponCode() == null) {
            voucherCode.setText(getString(R.string.label_discount_code));
            deleteVoucherBtn.setVisibility(View.GONE);
        } else {
            voucherCode.setText(String.format(getString(R.string.label_discount_code) + "(%s)", cart.getCouponCode()));
            voucher.setText(cart.getCouponCode());
            deleteVoucherBtn.setVisibility(View.VISIBLE);
        }

        StringFormat.applyCurrencyFormat(shippingAmount, cart.getShippingAmount());
        StringFormat.applyCurrencyFormat(subtotal, cart.getBaseSubtotal());
        StringFormat.applyCurrencyFormat(discountPrice, cart.getBaseDiscountAmount());
        StringFormat.applyCurrencyFormat(grandTotal, cart.getBaseGrandTotal());
    }

    @Override
    public void showError() {
        loading.setVisibility(View.GONE);
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        switch (viewId) {
            case R.id.delete: {
                showAlert(data, position);
                break;
            }
            case R.id.update: {
                itemPosition = position;
                presenter.updateCart((CartListItem) data);
                break;
            }
            case R.id.edit: {
                CartListItem cartListItem = (CartListItem) data;
                Intent intent = new Intent(getContext(), ProductDetailActivity.class);
                intent.putExtra(STATE, getState());
                intent.putExtra(getString(R.string.intent_edit_appointment), true);
                intent.putExtra(getString(R.string.intent_item_id), cartListItem.getItemId());
                intent.putExtra("id", ((CartListItem) data).getExtensionAttributes().getProductId());
                getActivity().startActivity(intent);
//                dismiss();
//                if (getActivity() instanceof ProductDetailActivity)
//                    getActivity().finish();
                break;
            }

        }
    }

    private void showAlert(Object data, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setMessage("Apakah anda yakin untuk menghapus")
                .setNegativeButton("Batal", (dialog, which) -> {
                    dialog.dismiss();
                })
                .setPositiveButton("Ya", (dialog, which) -> {
                    itemPosition = position;
                    presenter.deleteCart(String.valueOf(data));
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showMessage(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton("OK", (dialog, which) -> dialog.dismiss());
        builder.show();
    }

    @Override
    public void setVoucherView(boolean isAdd) {
        if (isAdd) {
            voucher.setEnabled(false);
            showMessage(getString(R.string.cupon_success));
        } else {
            voucher.setEnabled(true);
            showMessage("Kupon berhasil dihapus");
        }
        pricePresenter.getTotal(getStoreCode());
        voucherContainer.setError("");
    }

    @Override
    public void showVoucherError(boolean isAdd) {
        if (isAdd) {
            voucher.setEnabled(true);
            voucherContainer.setError("Kupon tidak tersedia");
            showMessage(getString(R.string.cupon_fail));
        } else voucher.setEnabled(false);
    }

    @OnClick(R.id.DCContainer)
    public void DCContainerClicked() {
        if (discountContainer.getVisibility() == View.GONE) {
            discountContainer.setVisibility(View.VISIBLE);
            iconCollapseDC.setRotation(180f);
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_up);
            discountContainer.startAnimation(animation);
        } else {
            discountContainer.setVisibility(View.GONE);
            iconCollapseDC.setRotation(180f);
        }
    }
}
