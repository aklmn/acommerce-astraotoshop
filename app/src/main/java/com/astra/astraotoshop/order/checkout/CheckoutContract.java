package com.astra.astraotoshop.order.checkout;

import android.support.annotation.StringRes;

import com.astra.astraotoshop.order.checkout.provider.AddressBody;
import com.astra.astraotoshop.order.checkout.provider.ItemMultiWarehouse;
import com.astra.astraotoshop.order.checkout.provider.ShippingMethodEntity;
import com.astra.astraotoshop.order.checkout.provider.VaBody;
import com.astra.astraotoshop.order.checkout.provider.address.AddressShippingBody;
import com.astra.astraotoshop.order.checkout.provider.installment.PaymentDesc;
import com.astra.astraotoshop.order.checkout.provider.payment.AvailablePaymentMethod;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethod;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodBody;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/31/2018.
 */

public interface CheckoutContract {
    interface CheckoutView{
        void showLoading();
        void showShippingMethod(List<ShippingMethodEntity> shippingMethod);
        void showPaymentMethod(AvailablePaymentMethod paymentMethod);
        void showSuccessPage(String orderCode);
        void showMessageError(String message);
        void hideLoading();
        void onVaSaved();
        void onSaveFailed();
        void showPaymentDesc(PaymentDesc paymentDesc);
        void showPaymentDescError();
    }

    interface CheckoutPresenter{
        void getShippingMethod();
        void onShippingMethodReceived(List<ShippingMethodEntity> shippingMethod);
        void onFailedReceivingShippingMethod(Throwable throwable);
        void setAddressId(int addressId);

        void saveShippingMethod(AddressShippingBody addressShippingBody);
        void onAddressSaved(AvailablePaymentMethod paymentMethod);
        void onFailedToSaveAddress();
        ShippingMethodEntity getShipping();

        void savePaymentMethod(String paymentCode);
        void onPaymentSaved(String orderCode);
        void onFailedToSavePayment(Throwable error);
        PaymentMethod getPayment();
        String getOrderCode();

        void saveVa(String orderId);
        void onVaSaved();
        void onSaveFailed(Throwable e);

        void getPaymentDesc();
        void onPaymentDescReceived(PaymentDesc paymentDesc);
        void onPaymentDescFailed(Throwable throwable);
    }

    interface CheckoutInteractor{
        void requestShippingMethod(String storeCode, AddressBody addressBody);
        void saveShippingMethod(String storeCode,AddressShippingBody addressShippingBody);
        void savePaymentMethod(String storeCode,PaymentMethodBody paymentMethodBody);
        void saveVa(String storeCode, VaBody vaBody);
        void getPaymentDesc(String storeCode);
    }

    interface ShippingPresenter{
        List<ItemMultiWarehouse> getProductList(String productJSON);
    }
}
