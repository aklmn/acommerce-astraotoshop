package com.astra.astraotoshop.order.checkout.provider.address;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BillingAddress {

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("city")
    private String city;

    @SerializedName("postcode")
    private String postcode;

    @SerializedName("saveInAddressBook")
    private Object saveInAddressBook;

    @SerializedName("telephone")
    private String telephone;

    @SerializedName("countryId")
    private String countryId;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("customerAddressId")
    private String customerAddressId;

    @SerializedName("regionCode")
    private String regionCode;

    @SerializedName("regionId")
    private String regionId;

    @SerializedName("street")
    private List<String> street;

    @SerializedName("customerId")
    private String customerId;

    @SerializedName("region")
    private String region;

    @SerializedName("customAttributes")
    private CustomAttributes customAttributes;

    public BillingAddress(String firstname, String city, String postcode, Object saveInAddressBook, String telephone, String countryId, String lastname, String customerAddressId, String regionCode, String regionId, List<String> street, String customerId, String region, CustomAttributes customAttributes) {
        this.firstname = firstname;
        this.city = city;
        this.postcode = postcode;
        this.saveInAddressBook = saveInAddressBook;
        this.telephone = telephone;
        this.countryId = countryId;
        this.lastname = lastname;
        this.customerAddressId = customerAddressId;
        this.regionCode = regionCode;
        this.regionId = regionId;
        this.street = street;
        this.customerId = customerId;
        this.region = region;
        this.customAttributes = customAttributes;
    }

    public BillingAddress() {
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setSaveInAddressBook(Object saveInAddressBook) {
        this.saveInAddressBook = saveInAddressBook;
    }

    public Object getSaveInAddressBook() {
        return saveInAddressBook;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setCustomerAddressId(String customerAddressId) {
        this.customerAddressId = customerAddressId;
    }

    public String getCustomerAddressId() {
        return customerAddressId;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setStreet(List<String> street) {
        this.street = street;
    }

    public List<String> getStreet() {
        return street;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setCustomAttributes(CustomAttributes customAttributes) {
        this.customAttributes = customAttributes;
    }

    public CustomAttributes getCustomAttributes() {
        return customAttributes;
    }

    @Override
    public String toString() {
        return
                "BillingAddress{" +
                        "firstname = '" + firstname + '\'' +
                        ",city = '" + city + '\'' +
                        ",postcode = '" + postcode + '\'' +
                        ",saveInAddressBook = '" + saveInAddressBook + '\'' +
                        ",telephone = '" + telephone + '\'' +
                        ",countryId = '" + countryId + '\'' +
                        ",lastname = '" + lastname + '\'' +
                        ",customerAddressId = '" + customerAddressId + '\'' +
                        ",regionCode = '" + regionCode + '\'' +
                        ",regionId = '" + regionId + '\'' +
                        ",street = '" + street + '\'' +
                        ",customerId = '" + customerId + '\'' +
                        ",region = '" + region + '\'' +
                        ",customAttributes = '" + customAttributes + '\'' +
                        "}";
    }
}