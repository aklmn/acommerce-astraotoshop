package com.astra.astraotoshop.order.checkout.revamp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.utils.view.CustomFontFace;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PaymentActivity extends AppCompatActivity {

    @BindView(R.id.pointCotainer)
    LinearLayout pointCotainer;
    @BindView(R.id.paymentList)
    RecyclerView paymentList;
    @BindView(R.id.coupon)
    EditText coupon;
    @BindView(R.id.currentPoint)
    CustomFontFace currentPoint;
    @BindView(R.id.point)
    EditText point;
    @BindView(R.id.allPoint)
    CheckBox allPoint;
    private PaymentListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        adapter = new PaymentListAdapter();
        paymentList.setLayoutManager(new LinearLayoutManager(this));
        paymentList.setAdapter(adapter);
    }
}
