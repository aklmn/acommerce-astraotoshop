package com.astra.astraotoshop.order.appointment;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.astra.astraotoshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 12/11/2017.
 */

public class AppointmentAdapter_old extends RecyclerView.Adapter<AppointmentAdapter_old.AppointmentViewHolder> {

    AppointmentListener listener;

    public AppointmentAdapter_old() {
    }

    public AppointmentAdapter_old(AppointmentListener listener) {
        this.listener = listener;
    }

    @Override
    public AppointmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AppointmentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_appointment, parent, false));
    }

    @Override
    public void onBindViewHolder(AppointmentViewHolder holder, int position) {
        if (position == 0) {
            holder.container.setVisibility(View.VISIBLE);
            holder.textContainer.setVisibility(View.GONE);
        }else {
            holder.textContainer.setVisibility(View.VISIBLE);
            holder.container.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    class AppointmentViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.container)
        ConstraintLayout container;
        @BindView(R.id.textContainer)
        LinearLayout textContainer;

        AppointmentViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @OnClick(R.id.editAppointment)
        public void editAppointment(){
            try {
                listener.onItemClick();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface AppointmentListener{
        void onItemClick();
    }
}
