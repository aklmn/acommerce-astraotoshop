package com.astra.astraotoshop.order.appointment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.appointment.provider.AppointmentPresenter;
import com.astra.astraotoshop.order.appointment.provider.SlotItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.FontIcon;
import com.astra.astraotoshop.utils.view.FontIconic;
import com.astra.astraotoshop.utils.view.KeyboardEventListener;
import com.astra.astraotoshop.utils.view.Loading;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AppointmentAddressFragment extends BottomSheetDialogFragment implements ListListener, AppointmentContract.AppointmentView, AppointmentContract.AppointmentExternal, ViewTreeObserver.OnGlobalLayoutListener, KeyboardEventListener.KeyboardListener {

    @BindView(R.id.title)
    CustomFontFace title;
    @BindView(R.id.close)
    FontIconic close;
    @BindView(R.id.searchIcon)
    FontIcon searchIcon;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.addressList)
    RecyclerView addressList;
    Unbinder unbinder;
    @BindView(R.id.test)
    LinearLayout test;
    @BindView(R.id.saveAppointment)
    Button saveAppointment;

    private AppointmentAdapter adapter;
    private String productId;
    private AppointmentPresenter presenter;
    private View view;
    private BottomSheetBehavior<FrameLayout> behavior;
    private String outlet;
    private Loading loading;
    private AppointmentContract.MainAppointment callback;


    public static AppointmentAddressFragment newInstance(String productId) {

        Bundle args = new Bundle();
        args.putString("productId", productId);
        AppointmentAddressFragment fragment = new AppointmentAddressFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productId = getArguments().getString("productId");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_appointment_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        view.getViewTreeObserver().addOnGlobalLayoutListener(this);
        listSetup();
        presenter = new AppointmentPresenter(this, productId, this);
        presenter.getAppointment("");
        KeyboardEventListener.setKeyboardVisibilityListener(getActivity(), this);
        loading = new Loading(getContext(), getLayoutInflater());
        return view;
    }

    private void listSetup() {
        adapter = new AppointmentAdapter(AppointmentAdapter.ADDRESS_TYPE, this);
        addressList.setLayoutManager(new LinearLayoutManager(getContext()));
        addressList.setAdapter(adapter);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        KeyboardEventListener.unSubsribe(getActivity());
        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        super.onDismiss(dialog);
    }

    @OnClick(R.id.close)
    public void onCloseClicked() {
        dismiss();
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        if (!presenter.getData().get(0).contains("||")) {
            presenter.getAppointment((String) data);
        } else {
            outlet = (String) data;
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
            try {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDataAvailable() {
        if (presenter.getData().size() > 0) {
            if (presenter.getData().size() > 0 && presenter.getData().get(0).contains("||")) {
                adapter.setCurrentType(AppointmentAdapter.OUTLET_TYPE);
                saveAppointment.setVisibility(View.VISIBLE);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else adapter.setCurrentType(AppointmentAdapter.ADDRESS_TYPE);
            adapter.setData(presenter.getData());
            try {
                test.setMinimumHeight(7000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void showError(String message) {

    }

    @Override
    public void showButton(boolean isShow) {

    }

    @Override
    public void onSlotAvailable(List<SlotItem> slotItems) {
        AppointmentFragment frag = (AppointmentFragment) getActivity().getSupportFragmentManager().findFragmentByTag(ProductDetailActivity.class.getName() + AppointmentFragment.class.getName());
        if (frag != null) {
            frag.setOutlet(slotItems, outlet, presenter.getAppointment());
        }
    }

    @Override
    public void setSlot(List<SlotItem> slots) {
        loading.dismiss();
        dismiss();
    }

    @Override
    public void showSlotErrorMessage() {
        loading.dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onGlobalLayout() {
        BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
        FrameLayout bottomSheet = (FrameLayout)
                dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
        bottomSheet.setBackgroundColor(getResources().getColor(android.R.color.transparent));
        behavior = BottomSheetBehavior.from(bottomSheet);
    }

    @Override
    public void onKeyboardStatusChange(boolean isShown) {
        if (isShown) {
            try {
                test.setMinimumHeight(5000);
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                behavior.setPeekHeight(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                test.setMinimumHeight(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.gap)
    public void onGapClick() {
        dismiss();
    }

    @OnClick(R.id.saveAppointment)
    public void onSaveAppointmentClick() {
        if (loading != null) loading.show(R.string.message_please_wait);
        presenter.getAppointment(outlet);
    }

    @OnTextChanged(R.id.search)
    public void onSearch(CharSequence charSequence, int start, int count, int after) {
        if (presenter.getData().size() > 0 && presenter.getData().get(0).contains("||"))
            adapter.setCurrentType(AppointmentAdapter.OUTLET_TYPE);
        else adapter.setCurrentType(AppointmentAdapter.ADDRESS_TYPE);
        if (charSequence.length() > 2) {
            searchData(charSequence.toString());
        } else if (charSequence.length() == 0) {
            adapter.setData(presenter.getData());
        }
    }

    private void searchData(String value) {
        Observable.from(presenter.getData())
                .filter(s -> s.toLowerCase().contains(value.toLowerCase()))
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        strings -> adapter.setData(strings),
                        er -> {
                        }
                );


    }
}