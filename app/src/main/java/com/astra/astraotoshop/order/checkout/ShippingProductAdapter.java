package com.astra.astraotoshop.order.checkout;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.checkout.provider.ItemMultiWarehouse;
import com.astra.astraotoshop.user.order.provider.ProductssItem;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 2/8/2018.
 */

public class ShippingProductAdapter extends RecyclerView.Adapter<ShippingProductAdapter.ShippingProductViewHolder> {

    private List<ItemMultiWarehouse> itemMultiWarehouses;
    private Context context;

    public ShippingProductAdapter(Context context) {
        this.context = context;
        itemMultiWarehouses = new ArrayList<>();
    }

    @Override
    public ShippingProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ShippingProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_shipping2, parent, false));
    }

    @Override
    public void onBindViewHolder(ShippingProductViewHolder holder, int position) {
        holder.name.setText(itemMultiWarehouses.get(position).getName());
        holder.qtyWarehouse.setText(String.valueOf(itemMultiWarehouses.get(position).getQty()));
        holder.estimateWarehouse.setText(itemMultiWarehouses.get(position).getEstimate() + " hari kerja");
//        if (itemMultiWarehouses.get(position).getMultiEstimate() != null) {
//            try {
//                holder.estimateMulti.setText(itemMultiWarehouses.get(position).getMultiEstimate() + " hari kerja");
//                holder.qtyMulti.setText(String.valueOf(itemMultiWarehouses.get(position).getMultiQty()));
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            holder.multiWarehouseProductContainer.setVisibility(View.VISIBLE);
//        } else {
//            holder.multiWarehouseProductContainer.setVisibility(View.GONE);
//        }

        if (itemMultiWarehouses.get(position).getImage() != null) {
            if (itemMultiWarehouses.get(position).getImage().startsWith("http")) {
                Glide.with(context).load(itemMultiWarehouses.get(position).getImage()).into(holder.image);
            } else {
                Glide.with(context).load(BuildConfig.MEDIA + itemMultiWarehouses.get(position).getImage()).into(holder.image);
            }
        }
//        if (itemMultiWarehouses.get(position).getSpecialPrice() != null) {
//            StringFormat.applyCurrencyFormat(holder.firstPrice, itemMultiWarehouses.get(position).getSpecialPrice());
//            StringFormat.applyCurrencyFormat(holder.secondPrice, itemMultiWarehouses.get(position).getPrice());
//            holder.secondPrice.setVisibility(View.VISIBLE);
//        } else if (itemMultiWarehouses.get(position).getPrice() != null) {
//            StringFormat.applyCurrencyFormat(holder.firstPrice, itemMultiWarehouses.get(position).getPrice());
//            holder.secondPrice.setVisibility(View.GONE);
//        }
    }

    @Override
    public int getItemCount() {
        return itemMultiWarehouses.size();
    }

    public void setItemMultiWarehouses(List<ItemMultiWarehouse> itemMultiWarehouses) {
        this.itemMultiWarehouses = itemMultiWarehouses;
        notifyDataSetChanged();
    }

    public void updateItems(List<ProductssItem> items) {
        try {
            for (ProductssItem item : items) {
                for (ItemMultiWarehouse multiWarehouse : itemMultiWarehouses) {
                    if (item.getItemId().equals(multiWarehouse.getItemId())) {
                        multiWarehouse.setImage(item.getImageUrl());
                        multiWarehouse.setPrice(item.getOriginalPrice());
                        multiWarehouse.setSpecialPrice(item.getPrice());
                    }
                }
            }
            notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class ShippingProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.name)
        CustomFontFace name;
        @BindView(R.id.qty)
        TextView qty;
        @BindView(R.id.qtyWarehouse)
        TextView qtyWarehouse;
        @BindView(R.id.estimateWarehouse)
        TextView estimateWarehouse;

        public ShippingProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
