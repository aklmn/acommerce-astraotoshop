package com.astra.astraotoshop.order.history;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.CartAdapter;
import com.astra.astraotoshop.order.checkout.ShippingProductAdapter;
import com.astra.astraotoshop.order.checkout.provider.ItemMultiWarehouse;
import com.astra.astraotoshop.order.history.provider.OrderAddressEntity;
import com.astra.astraotoshop.order.history.provider.OrderHistoriesItem;
import com.astra.astraotoshop.user.order.OrderContract;
import com.astra.astraotoshop.user.order.provider.OrderDetail;
import com.astra.astraotoshop.user.order.provider.OrderDetailPresenter;
import com.astra.astraotoshop.user.order.provider.ProductssItem;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryOrderDetailActivity extends BaseActivity implements OrderContract.OrderDetailView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.itemList)
    RecyclerView itemList;
    @BindView(R.id.scroll)
    NestedScrollView scroll;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.paymentMethod)
    TextView paymentMethod;
    @BindView(R.id.subtotal)
    TextView subtotal;
    @BindView(R.id.discount)
    TextView discount;
    @BindView(R.id.grandTotal)
    TextView grandTotal;
    @BindView(R.id.incrementId)
    TextView incrementId;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.deliverName)
    TextView deliverName;
    @BindView(R.id.deliveryPrice)
    TextView deliveryPrice;
    @BindView(R.id.shippingCost)
    TextView shippingCost;

    private OrderContract.OrderDetailPresenter presenter;
    private CartAdapter cartAdapter;
    private ShippingProductAdapter shippingAdapter;
    private OrderHistoriesItem ordersItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_order_detail);
        ButterKnife.bind(this);
        actionBarSetup();

        presenter = new OrderDetailPresenter(this);
        ordersItem = getIntent().getParcelableExtra("data");
        if (ordersItem.getEntityId() != -1)
            presenter.requestOrderDetail(String.valueOf(ordersItem.getEntityId()), getStoreCode());

        shippingAdapter = new ShippingProductAdapter(getApplicationContext());
        cartAdapter = new CartAdapter(this, "summary");
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        itemList.setLayoutManager(layoutManager);
        itemList.setAdapter(shippingAdapter);
        setCustomTitle("Pesanan #" + ordersItem.getIncrementId());
    }


    @Override
    public void onResume() {
        super.onResume();
        try {
//            profileListMenu.setFocusableInTouchMode(false);
//            profileListMenu.setFocusable(false);
//            profileListMenu.clearFocus();
            scroll.fullScroll(View.FOCUS_UP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showOrder(OrderDetail orderDetail) {
        incrementId.setText("#" + orderDetail.getIncrementId());
        String htmlValue = orderDetail.getShippingDescription() + "  <b>" +
                StringFormat.addCurrencyFormat(orderDetail.getShippingAmount()) + "</b>";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            deliverName.setText(Html.fromHtml(htmlValue, Html.FROM_HTML_MODE_LEGACY));
        } else deliverName.setText(Html.fromHtml(htmlValue));
        date.setText(orderDetail.getCreatedAt());
        status.setText(orderDetail.getStatus());
        name.setText(orderDetail.getCustomerFirstname() + " " + orderDetail.getCustomerLastname());
        paymentMethod.setText(orderDetail.getPaymentMethod());
        StringFormat.applyCurrencyFormat(subtotal, orderDetail.getBaseSubtotal());
        StringFormat.applyCurrencyFormat(discount, orderDetail.getDiscountAmount());
        StringFormat.applyCurrencyFormat(grandTotal, orderDetail.getGrandTotal());
//        StringFormat.applyCurrencyFormat(deliveryPrice, orderDetail.getShippingAmount());
        StringFormat.applyCurrencyFormat(shippingCost, orderDetail.getShippingAmount());
        address.setText(setAddressValue(orderDetail.getAddresses()));
        mappingList(orderDetail.getItems());

    }

    private String setAddressValue(OrderAddressEntity addresses) {
        String address = "";
        address = StringFormat.addressBuilder(address, addresses.getStreet());
        address = StringFormat.addressBuilder(address, addresses.getRtRw());
        address = StringFormat.addressBuilder(address, addresses.getCity());
        address = StringFormat.addressBuilder(address, addresses.getRegion());
        address = StringFormat.addressBuilder(address, addresses.getPostcode());
        address = StringFormat.addressBuilder(address, addresses.getMobilePhone());
        return address;
    }

    private void mappingList(List<ProductssItem> items) {
        List<ItemMultiWarehouse> multiWarehouses = new ArrayList<>();
        ItemMultiWarehouse warehouse;
        for (ProductssItem item : items) {
            warehouse = new ItemMultiWarehouse();
            warehouse.setImage(item.getImageUrl());
            warehouse.setName(item.getName());
            warehouse.setSKU(item.getSku());
            warehouse.setPrice(item.getOriginalPrice());
            warehouse.setSpecialPrice(item.getBasePrice());
            if (getState() == getResources().getInteger(R.integer.state_product) && item.getMethodDescriptions().size() > 0) {
                if (item.getMethodDescriptions().size() > 1) {
                    warehouse.setQty(item.getMethodDescriptions().get(0).getQTY());
                    warehouse.setEstimate(item.getMethodDescriptions().get(0).getESTIMATE());
                    warehouse.setMultiEstimate(item.getMethodDescriptions().get(1).getESTIMATE());
                    warehouse.setMultiQty(item.getMethodDescriptions().get(1).getQTY());
                } else {
                    warehouse.setQty(item.getMethodDescriptions().get(0).getQTY());
                    warehouse.setEstimate(item.getMethodDescriptions().get(0).getESTIMATE());
                }
            }
            multiWarehouses.add(warehouse);
        }
        shippingAdapter.setItemMultiWarehouses(multiWarehouses);
    }

    @Override
    public void onError() {

    }
}
