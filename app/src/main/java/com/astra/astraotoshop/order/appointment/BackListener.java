package com.astra.astraotoshop.order.appointment;

/**
 * Created by Henra Setia Nugraha on 3/13/2018.
 */

public interface BackListener {
    void onBackPressed();
}
