package com.astra.astraotoshop.order.cart;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.provider.AppointmentsItemCart;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.FontIcon;

import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 11/21/2017.
 */

public class CartAppointmentDetailAdapter extends RecyclerView.Adapter<CartAppointmentDetailAdapter.AppDetailViewHolder> {

    private AppointmentsItemCart item;
    private boolean isShowOption;
    private ListListener listener;

    public CartAppointmentDetailAdapter(boolean isShowOption, ListListener listener) {
        this.isShowOption = isShowOption;
        this.listener = listener;
    }

    @Override
    public AppDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AppDetailViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_appointment, parent, false), isShowOption);
    }

    @Override
    public void onBindViewHolder(AppDetailViewHolder holder, int position) {
        holder.count.setText(String.valueOf(position + 1));
        String desc = ((
                item.getAddress().get(position).substring(
                        0, item.getAddress().get(position).indexOf("|")
                ))) + " - "
                + item.getApHours().get(position) + ", "
                + item.getApDate().get(position);
        holder.description.setText(desc);
    }

    @Override
    public int getItemCount() {
        return item == null ? 0 : item.getAddress().size();
    }

    public void setItem(AppointmentsItemCart item) {
        this.item = item;
        notifyDataSetChanged();
    }

    public void updateItem(int position, AppointmentsItemCart updateValue) {
        item.getRegion().set(position, updateValue.getRegion().get(0));
        item.getCity().set(position, updateValue.getCity().get(0));
        item.getAddress().set(position, updateValue.getAddress().get(0));
        item.getSlotName().set(position, updateValue.getSlotName().get(0));
        item.getWhsId().set(position, updateValue.getWhsId().get(0));
        item.getApDate().set(position, updateValue.getApDate().get(0));
        item.getApHours().set(position, updateValue.getApHours().get(0));
        item.getMechanicComeToHome().set(position, updateValue.getMechanicComeToHome().get(0));
        notifyItemChanged(position);
    }

    class AppDetailViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.count)
        TextView count;
        @BindView(R.id.description)
        TextView description;
        @BindView(R.id.deleteIcon)
        FontIcon deleteIcon;
        @BindView(R.id.editIcon)
        FontIcon editIcon;

        public AppDetailViewHolder(View itemView, boolean isShowOption) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (isShowOption) {
                deleteIcon.setVisibility(View.VISIBLE);
                editIcon.setVisibility(View.VISIBLE);
            }
        }

        @OnClick(R.id.deleteIcon)
        public void onItemDelete() {
            if (listener != null) {
                listener.onItemClick(R.id.deleteIcon, getAdapterPosition(), item.getAddress().size() - 1);
                notifyItemRemoved(getAdapterPosition());
                notifyDataSetChanged();
            }
        }

        @OnClick(R.id.editIcon)
        public void onItemEdit() {
            if (listener != null) {
                AppointmentsItemCart appItem = new AppointmentsItemCart();
                appItem.setRegion(Collections.singletonList(item.getRegion().get(getAdapterPosition())));
                appItem.setCity(Collections.singletonList(item.getCity().get(getAdapterPosition())));
                appItem.setAddress(Collections.singletonList(item.getAddress().get(getAdapterPosition())));
                appItem.setSlotName(Collections.singletonList(item.getSlotName().get(getAdapterPosition())));
                appItem.setWhsId(Collections.singletonList(item.getWhsId().get(getAdapterPosition())));
                appItem.setApDate(Collections.singletonList(item.getApDate().get(getAdapterPosition())));
                appItem.setApHours(Collections.singletonList(item.getApHours().get(getAdapterPosition())));
                listener.onItemClick(R.id.editIcon, getAdapterPosition(), item);
            }
        }
    }
}
