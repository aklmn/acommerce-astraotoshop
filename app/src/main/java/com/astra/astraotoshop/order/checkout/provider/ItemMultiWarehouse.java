package com.astra.astraotoshop.order.checkout.provider;

import com.google.gson.annotations.SerializedName;

public class ItemMultiWarehouse {

    @SerializedName("COST")
    private int cost;

    @SerializedName("IMAGE")
    private String image;

    @SerializedName("WAREHOUSE")
    private String warehouse;

    @SerializedName("QTY")
    private int qty;

    private int multiQty;

    @SerializedName("ITEM_ID")
    private String itemId;

    @SerializedName("SKU")
    private String sku;

    @SerializedName("ESTIMATE")
    private String estimate;

    private String multiEstimate;

    @SerializedName("NAME")
    private String name;

    private String price;
    private String specialPrice;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSpecialPrice() {
        return specialPrice;
    }

    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    public int getMultiQty() {
        return multiQty;
    }

    public void setMultiQty(int multiQty) {
        this.multiQty = multiQty;
    }

    public String getMultiEstimate() {
        return multiEstimate;
    }

    public void setMultiEstimate(String multiEstimate) {
        this.multiEstimate = multiEstimate;
    }

    public void setCost(int cOST) {
        this.cost = cOST;
    }

    public int getCost() {
        return cost;
    }

    public void setImage(String iMAGE) {
        this.image = iMAGE;
    }

    public String getImage() {
        return image;
    }

    public void setWarehouse(String wAREHOUSE) {
        this.warehouse = wAREHOUSE;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setQty(int qTY) {
        this.qty = qTY;
    }

    public int getQty() {
        return qty;
    }

    public void setItemId(String iTEMID) {
        this.itemId = iTEMID;
    }

    public String getItemId() {
        return itemId;
    }

    public void setSKU(String sKU) {
        this.sku = sKU;
    }

    public String getSKU() {
        return sku;
    }

    public void setEstimate(String eSTIMATE) {
        this.estimate = eSTIMATE;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setName(String nAME) {
        this.name = nAME;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return
                "ItemMultiWarehouse{" +
                        "cost = '" + cost + '\'' +
                        ",image = '" + image + '\'' +
                        ",warehouse = '" + warehouse + '\'' +
                        ",qty = '" + qty + '\'' +
                        ",iTEM_ID = '" + itemId + '\'' +
                        ",sku = '" + sku + '\'' +
                        ",estimate = '" + estimate + '\'' +
                        ",name = '" + name + '\'' +
                        "}";
    }
}