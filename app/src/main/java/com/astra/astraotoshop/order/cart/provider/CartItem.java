package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

public class CartItem{

	@SerializedName("quote_id")
	private int quoteId;

	@SerializedName("qty")
	private int qty;

	@SerializedName("sku")
	private String sku;

	@SerializedName("product_option")
	private ProductOption productOption;

	public CartItem(int quoteId, int qty, String sku, ProductOption productOption) {
		this.quoteId = quoteId;
		this.qty = qty;
		this.sku = sku;
		this.productOption = productOption;
	}

	public ProductOption getProductOption() {
		return productOption;
	}

	public void setProductOption(ProductOption productOption) {
		this.productOption = productOption;
	}

	public CartItem() {
    }

    public void setQuoteId(int quoteId){
		this.quoteId = quoteId;
	}

	public int getQuoteId(){
		return quoteId;
	}

	public void setQty(int qty){
		this.qty = qty;
	}

	public int getQty(){
		return qty;
	}

	public void setSku(String sku){
		this.sku = sku;
	}

	public String getSku(){
		return sku;
	}

	@Override
 	public String toString(){
		return 
			"cart_item{" +
			"quote_id : '" + quoteId + '\'' +
			",qty : '" + qty + '\'' +
			",sku : '" + sku + '\'' +
			"}";
		}
}