package com.astra.astraotoshop.order.checkout.provider.installment;

import com.google.gson.annotations.SerializedName;

public class PaymentDesc {

    @SerializedName("cc_hosted")
    private CcHosted ccHosted;

    @SerializedName("danamon_va")
    private DanamonVa danamonVa;

    @SerializedName("permata_va")
    private PermataVa permataVa;

    @SerializedName("other_va")
    private OtherVa otherVa;

    @SerializedName("mandiri_va")
    private MandiriVa mandiriVa;

    @SerializedName("cimb_va")
    private CimbVa cimbVa;

    @SerializedName("cc_installment_hosted")
    private CcInstallmentHosted ccInstallmentHosted;

    public void setCcHosted(CcHosted ccHosted) {
        this.ccHosted = ccHosted;
    }

    public CcHosted getCcHosted() {
        return ccHosted;
    }

    public void setDanamonVa(DanamonVa danamonVa) {
        this.danamonVa = danamonVa;
    }

    public DanamonVa getDanamonVa() {
        return danamonVa;
    }

    public void setPermataVa(PermataVa permataVa) {
        this.permataVa = permataVa;
    }

    public PermataVa getPermataVa() {
        return permataVa;
    }

    public void setOtherVa(OtherVa otherVa) {
        this.otherVa = otherVa;
    }

    public OtherVa getOtherVa() {
        return otherVa;
    }

    public void setMandiriVa(MandiriVa mandiriVa) {
        this.mandiriVa = mandiriVa;
    }

    public MandiriVa getMandiriVa() {
        return mandiriVa;
    }

    public void setCimbVa(CimbVa cimbVa) {
        this.cimbVa = cimbVa;
    }

    public CimbVa getCimbVa() {
        return cimbVa;
    }

    public void setCcInstallmentHosted(CcInstallmentHosted ccInstallmentHosted) {
        this.ccInstallmentHosted = ccInstallmentHosted;
    }

    public CcInstallmentHosted getCcInstallmentHosted() {
        return ccInstallmentHosted;
    }

    @Override
    public String toString() {
        return
                "PaymentDesc{" +
                        "cc_hosted = '" + ccHosted + '\'' +
                        ",danamon_va = '" + danamonVa + '\'' +
                        ",permata_va = '" + permataVa + '\'' +
                        ",other_va = '" + otherVa + '\'' +
                        ",mandiri_va = '" + mandiriVa + '\'' +
                        ",cimb_va = '" + cimbVa + '\'' +
                        ",cc_installment_hosted = '" + ccInstallmentHosted + '\'' +
                        "}";
    }
}