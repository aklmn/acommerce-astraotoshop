package com.astra.astraotoshop.order.checkout.provider.payment;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Totals {

    @SerializedName("tax_amount")
    private int taxAmount;

    @SerializedName("shipping_discount_amount")
    private int shippingDiscountAmount;

    @SerializedName("discount_amount")
    private Double discountAmount;

    @SerializedName("items_qty")
    private int itemsQty;

    @SerializedName("quote_currency_code")
    private String quoteCurrencyCode;

    @SerializedName("base_subtotal_with_discount")
    private int baseSubtotalWithDiscount;

    @SerializedName("weee_tax_applied_amount")
    private Object weeeTaxAppliedAmount;

    @SerializedName("shipping_tax_amount")
    private int shippingTaxAmount;

    @SerializedName("base_shipping_discount_amount")
    private int baseShippingDiscountAmount;

    @SerializedName("grand_total")
    private int grandTotal;

    @SerializedName("base_currency_code")
    private String baseCurrencyCode;

    @SerializedName("base_tax_amount")
    private int baseTaxAmount;

    @SerializedName("base_shipping_tax_amount")
    private int baseShippingTaxAmount;

    @SerializedName("base_grand_total")
    private int baseGrandTotal;

    @SerializedName("base_discount_amount")
    private int baseDiscountAmount;

    @SerializedName("extension_attributes")
    private ExtensionAttributes extensionAttributes;

    @SerializedName("shipping_amount")
    private int shippingAmount;

    @SerializedName("base_shipping_amount")
    private int baseShippingAmount;

    @SerializedName("subtotal_incl_tax")
    private int subtotalInclTax;

    @SerializedName("subtotal_with_discount")
    private int subtotalWithDiscount;

    @SerializedName("subtotal")
    private int subtotal;

    @SerializedName("base_subtotal")
    private int baseSubtotal;

    @SerializedName("base_shipping_incl_tax")
    private int baseShippingInclTax;

    @SerializedName("items")
    private List<ItemsItem> items;

    @SerializedName("total_segments")
    private List<TotalSegmentsItem> totalSegments;

    @SerializedName("shipping_incl_tax")
    private int shippingInclTax;

    public void setTaxAmount(int taxAmount) {
        this.taxAmount = taxAmount;
    }

    public int getTaxAmount() {
        return taxAmount;
    }

    public void setShippingDiscountAmount(int shippingDiscountAmount) {
        this.shippingDiscountAmount = shippingDiscountAmount;
    }

    public int getShippingDiscountAmount() {
        return shippingDiscountAmount;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setItemsQty(int itemsQty) {
        this.itemsQty = itemsQty;
    }

    public int getItemsQty() {
        return itemsQty;
    }

    public void setQuoteCurrencyCode(String quoteCurrencyCode) {
        this.quoteCurrencyCode = quoteCurrencyCode;
    }

    public String getQuoteCurrencyCode() {
        return quoteCurrencyCode;
    }

    public void setBaseSubtotalWithDiscount(int baseSubtotalWithDiscount) {
        this.baseSubtotalWithDiscount = baseSubtotalWithDiscount;
    }

    public int getBaseSubtotalWithDiscount() {
        return baseSubtotalWithDiscount;
    }

    public void setWeeeTaxAppliedAmount(Object weeeTaxAppliedAmount) {
        this.weeeTaxAppliedAmount = weeeTaxAppliedAmount;
    }

    public Object getWeeeTaxAppliedAmount() {
        return weeeTaxAppliedAmount;
    }

    public void setShippingTaxAmount(int shippingTaxAmount) {
        this.shippingTaxAmount = shippingTaxAmount;
    }

    public int getShippingTaxAmount() {
        return shippingTaxAmount;
    }

    public void setBaseShippingDiscountAmount(int baseShippingDiscountAmount) {
        this.baseShippingDiscountAmount = baseShippingDiscountAmount;
    }

    public int getBaseShippingDiscountAmount() {
        return baseShippingDiscountAmount;
    }

    public void setGrandTotal(int grandTotal) {
        this.grandTotal = grandTotal;
    }

    public int getGrandTotal() {
        return grandTotal;
    }

    public void setBaseCurrencyCode(String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }

    public String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    public void setBaseTaxAmount(int baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }

    public int getBaseTaxAmount() {
        return baseTaxAmount;
    }

    public void setBaseShippingTaxAmount(int baseShippingTaxAmount) {
        this.baseShippingTaxAmount = baseShippingTaxAmount;
    }

    public int getBaseShippingTaxAmount() {
        return baseShippingTaxAmount;
    }

    public void setBaseGrandTotal(int baseGrandTotal) {
        this.baseGrandTotal = baseGrandTotal;
    }

    public int getBaseGrandTotal() {
        return baseGrandTotal;
    }

    public void setBaseDiscountAmount(int baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }

    public int getBaseDiscountAmount() {
        return baseDiscountAmount;
    }

    public void setExtensionAttributes(ExtensionAttributes extensionAttributes) {
        this.extensionAttributes = extensionAttributes;
    }

    public ExtensionAttributes getExtensionAttributes() {
        return extensionAttributes;
    }

    public void setShippingAmount(int shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    public int getShippingAmount() {
        return shippingAmount;
    }

    public void setBaseShippingAmount(int baseShippingAmount) {
        this.baseShippingAmount = baseShippingAmount;
    }

    public int getBaseShippingAmount() {
        return baseShippingAmount;
    }

    public void setSubtotalInclTax(int subtotalInclTax) {
        this.subtotalInclTax = subtotalInclTax;
    }

    public int getSubtotalInclTax() {
        return subtotalInclTax;
    }

    public void setSubtotalWithDiscount(int subtotalWithDiscount) {
        this.subtotalWithDiscount = subtotalWithDiscount;
    }

    public int getSubtotalWithDiscount() {
        return subtotalWithDiscount;
    }

    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public void setBaseSubtotal(int baseSubtotal) {
        this.baseSubtotal = baseSubtotal;
    }

    public int getBaseSubtotal() {
        return baseSubtotal;
    }

    public void setBaseShippingInclTax(int baseShippingInclTax) {
        this.baseShippingInclTax = baseShippingInclTax;
    }

    public int getBaseShippingInclTax() {
        return baseShippingInclTax;
    }

    public void setItems(List<ItemsItem> items) {
        this.items = items;
    }

    public List<ItemsItem> getItems() {
        return items;
    }

    public void setTotalSegments(List<TotalSegmentsItem> totalSegments) {
        this.totalSegments = totalSegments;
    }

    public List<TotalSegmentsItem> getTotalSegments() {
        return totalSegments;
    }

    public void setShippingInclTax(int shippingInclTax) {
        this.shippingInclTax = shippingInclTax;
    }

    public int getShippingInclTax() {
        return shippingInclTax;
    }

    @Override
    public String toString() {
        return
                "Totals{" +
                        "tax_amount = '" + taxAmount + '\'' +
                        ",shipping_discount_amount = '" + shippingDiscountAmount + '\'' +
                        ",discount_amount = '" + discountAmount + '\'' +
                        ",items_qty = '" + itemsQty + '\'' +
                        ",quote_currency_code = '" + quoteCurrencyCode + '\'' +
                        ",base_subtotal_with_discount = '" + baseSubtotalWithDiscount + '\'' +
                        ",weee_tax_applied_amount = '" + weeeTaxAppliedAmount + '\'' +
                        ",shipping_tax_amount = '" + shippingTaxAmount + '\'' +
                        ",base_shipping_discount_amount = '" + baseShippingDiscountAmount + '\'' +
                        ",grand_total = '" + grandTotal + '\'' +
                        ",base_currency_code = '" + baseCurrencyCode + '\'' +
                        ",base_tax_amount = '" + baseTaxAmount + '\'' +
                        ",base_shipping_tax_amount = '" + baseShippingTaxAmount + '\'' +
                        ",base_grand_total = '" + baseGrandTotal + '\'' +
                        ",base_discount_amount = '" + baseDiscountAmount + '\'' +
                        ",extension_attributes = '" + extensionAttributes + '\'' +
                        ",shipping_amount = '" + shippingAmount + '\'' +
                        ",base_shipping_amount = '" + baseShippingAmount + '\'' +
                        ",subtotal_incl_tax = '" + subtotalInclTax + '\'' +
                        ",subtotal_with_discount = '" + subtotalWithDiscount + '\'' +
                        ",subtotal = '" + subtotal + '\'' +
                        ",base_subtotal = '" + baseSubtotal + '\'' +
                        ",base_shipping_incl_tax = '" + baseShippingInclTax + '\'' +
                        ",items = '" + items + '\'' +
                        ",total_segments = '" + totalSegments + '\'' +
                        ",shipping_incl_tax = '" + shippingInclTax + '\'' +
                        "}";
    }
}