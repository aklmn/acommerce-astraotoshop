package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

public class ProductOption {

    @SerializedName("extension_attributes")
    private AppExtensionAttributes appExtensionAttributes;

    public ProductOption(AppExtensionAttributes appExtensionAttributes) {
        this.appExtensionAttributes = appExtensionAttributes;
    }

    public void setAppExtensionAttributes(AppExtensionAttributes appExtensionAttributes) {
        this.appExtensionAttributes = appExtensionAttributes;
    }

    public AppExtensionAttributes getAppExtensionAttributes() {
        return appExtensionAttributes;
    }

    @Override
    public String toString() {
        return
                "ProductOption{" +
                        "extension_attributes = '" + appExtensionAttributes + '\'' +
                        "}";
    }
}