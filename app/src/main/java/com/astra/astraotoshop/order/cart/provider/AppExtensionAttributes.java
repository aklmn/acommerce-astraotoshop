package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppExtensionAttributes {

    @SerializedName("appointment_param")
    private List<AppointmentParamItem> appointmentParam;

    public AppExtensionAttributes(List<AppointmentParamItem> appointmentParam) {
        this.appointmentParam = appointmentParam;
    }

    public void setAppointmentParam(List<AppointmentParamItem> appointmentParam) {
        this.appointmentParam = appointmentParam;
    }

    public List<AppointmentParamItem> getAppointmentParam() {
        return appointmentParam;
    }

    @Override
    public String toString() {
        return
                "AppExtensionAttributes{" +
                        "appointment_param = '" + appointmentParam + '\'' +
                        "}";
    }
}