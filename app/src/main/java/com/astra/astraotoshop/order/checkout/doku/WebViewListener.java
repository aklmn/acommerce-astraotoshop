package com.astra.astraotoshop.order.checkout.doku;

/**
 * Created by Henra Setia Nugraha on 3/7/2018.
 */

public interface WebViewListener {
    void onSuccess();
    void onFailed();
    void onLoaded();
}
