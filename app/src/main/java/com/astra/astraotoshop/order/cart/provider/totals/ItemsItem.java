package com.astra.astraotoshop.order.cart.provider.totals;

import com.google.gson.annotations.SerializedName;

public class ItemsItem {

    @SerializedName("tax_amount")
    private int taxAmount;

    @SerializedName("item_id")
    private int itemId;

    @SerializedName("discount_amount")
    private Double discountAmount;

    @SerializedName("base_discount_amount")
    private Double baseDiscountAmount;

    @SerializedName("row_total")
    private int rowTotal;

    @SerializedName("weee_tax_applied_amount")
    private Object weeeTaxAppliedAmount;

    @SerializedName("tax_percent")
    private int taxPercent;

    @SerializedName("base_row_total_incl_tax")
    private int baseRowTotalInclTax;

    @SerializedName("base_price_incl_tax")
    private int basePriceInclTax;

    @SerializedName("price_incl_tax")
    private int priceInclTax;

    @SerializedName("price")
    private int price;

    @SerializedName("weee_tax_applied")
    private Object weeeTaxApplied;

    @SerializedName("qty")
    private int qty;

    @SerializedName("base_price")
    private int basePrice;

    @SerializedName("options")
    private String options;

    @SerializedName("name")
    private String name;

    @SerializedName("base_row_total")
    private int baseRowTotal;

    @SerializedName("discount_percent")
    private int discountPercent;

    @SerializedName("row_total_incl_tax")
    private int rowTotalInclTax;

    @SerializedName("base_tax_amount")
    private int baseTaxAmount;

    public void setTaxAmount(int taxAmount) {
        this.taxAmount = taxAmount;
    }

    public int getTaxAmount() {
        return taxAmount;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setDiscountAmount(Double discountAmount) {
        this.discountAmount = discountAmount;
    }

    public Double getDiscountAmount() {
        return discountAmount;
    }

    public void setBaseDiscountAmount(Double baseDiscountAmount) {
        this.baseDiscountAmount = baseDiscountAmount;
    }

    public Double getBaseDiscountAmount() {
        return baseDiscountAmount;
    }

    public void setRowTotal(int rowTotal) {
        this.rowTotal = rowTotal;
    }

    public int getRowTotal() {
        return rowTotal;
    }

    public void setWeeeTaxAppliedAmount(Object weeeTaxAppliedAmount) {
        this.weeeTaxAppliedAmount = weeeTaxAppliedAmount;
    }

    public Object getWeeeTaxAppliedAmount() {
        return weeeTaxAppliedAmount;
    }

    public void setTaxPercent(int taxPercent) {
        this.taxPercent = taxPercent;
    }

    public int getTaxPercent() {
        return taxPercent;
    }

    public void setBaseRowTotalInclTax(int baseRowTotalInclTax) {
        this.baseRowTotalInclTax = baseRowTotalInclTax;
    }

    public int getBaseRowTotalInclTax() {
        return baseRowTotalInclTax;
    }

    public void setBasePriceInclTax(int basePriceInclTax) {
        this.basePriceInclTax = basePriceInclTax;
    }

    public int getBasePriceInclTax() {
        return basePriceInclTax;
    }

    public void setPriceInclTax(int priceInclTax) {
        this.priceInclTax = priceInclTax;
    }

    public int getPriceInclTax() {
        return priceInclTax;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public void setWeeeTaxApplied(Object weeeTaxApplied) {
        this.weeeTaxApplied = weeeTaxApplied;
    }

    public Object getWeeeTaxApplied() {
        return weeeTaxApplied;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQty() {
        return qty;
    }

    public void setBasePrice(int basePrice) {
        this.basePrice = basePrice;
    }

    public int getBasePrice() {
        return basePrice;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getOptions() {
        return options;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setBaseRowTotal(int baseRowTotal) {
        this.baseRowTotal = baseRowTotal;
    }

    public int getBaseRowTotal() {
        return baseRowTotal;
    }

    public void setDiscountPercent(int discountPercent) {
        this.discountPercent = discountPercent;
    }

    public int getDiscountPercent() {
        return discountPercent;
    }

    public void setRowTotalInclTax(int rowTotalInclTax) {
        this.rowTotalInclTax = rowTotalInclTax;
    }

    public int getRowTotalInclTax() {
        return rowTotalInclTax;
    }

    public void setBaseTaxAmount(int baseTaxAmount) {
        this.baseTaxAmount = baseTaxAmount;
    }

    public int getBaseTaxAmount() {
        return baseTaxAmount;
    }

    @Override
    public String toString() {
        return
                "ProductssItem{" +
                        "tax_amount = '" + taxAmount + '\'' +
                        ",item_id = '" + itemId + '\'' +
                        ",discount_amount = '" + discountAmount + '\'' +
                        ",base_discount_amount = '" + baseDiscountAmount + '\'' +
                        ",row_total = '" + rowTotal + '\'' +
                        ",weee_tax_applied_amount = '" + weeeTaxAppliedAmount + '\'' +
                        ",tax_percent = '" + taxPercent + '\'' +
                        ",base_row_total_incl_tax = '" + baseRowTotalInclTax + '\'' +
                        ",base_price_incl_tax = '" + basePriceInclTax + '\'' +
                        ",price_incl_tax = '" + priceInclTax + '\'' +
                        ",price = '" + price + '\'' +
                        ",weee_tax_applied = '" + weeeTaxApplied + '\'' +
                        ",qty = '" + qty + '\'' +
                        ",base_price = '" + basePrice + '\'' +
                        ",options = '" + options + '\'' +
                        ",name = '" + name + '\'' +
                        ",base_row_total = '" + baseRowTotal + '\'' +
                        ",discount_percent = '" + discountPercent + '\'' +
                        ",row_total_incl_tax = '" + rowTotalInclTax + '\'' +
                        ",base_tax_amount = '" + baseTaxAmount + '\'' +
                        "}";
    }
}