package com.astra.astraotoshop.order.history.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderHistory {

    @SerializedName("total_count")
    private int totalCount;

    @SerializedName("items")
    private List<OrderHistoriesItem> items;

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setItems(List<OrderHistoriesItem> items) {
        this.items = items;
    }

    public List<OrderHistoriesItem> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return
                "OrderHistory{" +
                        "total_count = '" + totalCount + '\'' +
                        ",items = '" + items + '\'' +
                        "}";
    }
}