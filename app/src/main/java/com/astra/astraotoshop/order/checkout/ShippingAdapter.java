package com.astra.astraotoshop.order.checkout;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.checkout.provider.ShippingMethodEntity;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

/**
 * Created by Henra Setia Nugraha on 2/4/2018.
 */

public class ShippingAdapter extends RecyclerView.Adapter<ShippingAdapter.ShippingViewHolder> {

    private List<ShippingMethodEntity> shippingMethods;
    private ListListener listener;
    private boolean isMulti = false;
    private int positionSelected = -1;
    private RadioButton itemState;

    public ShippingAdapter(ListListener listener) {
        shippingMethods = new ArrayList<>();
        this.listener = listener;
    }

    @Override
    public ShippingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ShippingViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.part_radio_button, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(ShippingViewHolder holder, int position) {
        String shipping = shippingMethods.get(position).getMethodTitle() + " " + StringFormat.addCurrencyFormat(shippingMethods.get(position).getAmount());
        holder.method.setText(Html.fromHtml(shipping));
        holder.method.setChecked(shippingMethods.get(position).isSelected());
    }

    @Override
    public int getItemCount() {
        return shippingMethods.size();
    }

    public void setShippingMethods(List<ShippingMethodEntity> shippingMethods) {
        if (shippingMethods != null) {
            this.shippingMethods = shippingMethods;
            notifyDataSetChanged();
        }
    }

    public void setShippingMethods(List<ShippingMethodEntity> shippingMethods, int position) {
        this.shippingMethods = shippingMethods;
        positionSelected = position;
        if (this.shippingMethods.size() > 0)
            this.shippingMethods.get(position).setSelected(true);
        notifyDataSetChanged();
    }

    public void setMulti(boolean multi) {
        isMulti = multi;
        notifyDataSetChanged();
    }

    public int getPositionSelected() {
        return positionSelected;
    }

    public ShippingMethodEntity getItemSelected() {
        if (positionSelected != -1) {
            return shippingMethods.get(positionSelected);
        }
        return null;
    }

    class ShippingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.radioItem)
        RadioButton method;

        public ShippingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                method.setChecked(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnCheckedChanged(R.id.radioItem)
        public void checked(CompoundButton button, boolean isChecked) {
            if (itemState != null)
                itemState.setChecked(false);
            itemState = method;

            if (positionSelected != -1)
                shippingMethods.get(positionSelected).setSelected(false);
            positionSelected = getAdapterPosition();
            shippingMethods.get(getAdapterPosition()).setSelected(true);
            listener.onItemClick(android.R.id.text1, getAdapterPosition(), shippingMethods.get(getAdapterPosition()));
        }
    }
}
