package com.astra.astraotoshop.order.cart.provider.voucher;

import android.content.Context;

import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/29/2018.
 */

public class CartVoucherInteractor extends BaseInteractor implements CartContract.CartVoucherInteractor {

    private CartContract.CartVoucherPresenter presenter;

    public CartVoucherInteractor(CartContract.CartVoucherPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void addVoucher(String storeCode, String voucherCode) {
        getGeneralNetworkManager()
                .updateVoucher(
                        new NetworkHandler(),
                        storeCode,
                        voucherCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        aBoolean -> presenter.onVoucherAdded(),
                        error->presenter.onAddVoucherFailed()
                );
    }

    @Override
    public void deleteVoucher(String storeCode) {
        getGeneralNetworkManager()
                .deleteVoucher(
                        new NetworkHandler(),
                        storeCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        aBoolean -> presenter.onVoucherDeleted(),
                        error->presenter.onDeleteVoucherFailed()
                );
    }
}
