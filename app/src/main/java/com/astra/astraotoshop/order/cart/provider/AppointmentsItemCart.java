package com.astra.astraotoshop.order.cart.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AppointmentsItemCart implements Parcelable{

    @SerializedName("address")
    private List<String> address;

    @SerializedName("city")
    private List<String> city;

    @SerializedName("whs_id")
    private List<String> whsId;

    @SerializedName("ap_hours")
    private List<String> apHours;

    @SerializedName("ap_date")
    private List<String> apDate;

    @SerializedName("mechanic_come_to_home")
    private List<String> mechanicComeToHome;

    @SerializedName("region")
    private List<String> region;

    @SerializedName("slot_name")
    private List<String> slotName;

    public AppointmentsItemCart() {
        address = new ArrayList<>();
        city = new ArrayList<>();
        whsId = new ArrayList<>();
        apHours = new ArrayList<>();
        apDate = new ArrayList<>();
        mechanicComeToHome = new ArrayList<>();
        region = new ArrayList<>();
        slotName = new ArrayList<>();
    }

    protected AppointmentsItemCart(Parcel in) {
        address = in.createStringArrayList();
        city = in.createStringArrayList();
        whsId = in.createStringArrayList();
        apHours = in.createStringArrayList();
        apDate = in.createStringArrayList();
        mechanicComeToHome = in.createStringArrayList();
        region = in.createStringArrayList();
        slotName = in.createStringArrayList();
    }

    public static final Creator<AppointmentsItemCart> CREATOR = new Creator<AppointmentsItemCart>() {
        @Override
        public AppointmentsItemCart createFromParcel(Parcel in) {
            return new AppointmentsItemCart(in);
        }

        @Override
        public AppointmentsItemCart[] newArray(int size) {
            return new AppointmentsItemCart[size];
        }
    };

    public void setAddress(List<String> address) {
        this.address = address;
    }

    public List<String> getAddress() {
        return address;
    }

    public void setCity(List<String> city) {
        this.city = city;
    }

    public List<String> getCity() {
        return city;
    }

    public void setWhsId(List<String> whsId) {
        this.whsId = whsId;
    }

    public List<String> getWhsId() {
        return whsId;
    }

    public void setApHours(List<String> apHours) {
        this.apHours = apHours;
    }

    public List<String> getApHours() {
        return apHours;
    }

    public void setApDate(List<String> apDate) {
        this.apDate = apDate;
    }

    public List<String> getApDate() {
        return apDate;
    }

    public void setMechanicComeToHome(List<String> mechanicComeToHome) {
        this.mechanicComeToHome = mechanicComeToHome;
    }

    public List<String> getMechanicComeToHome() {
        return mechanicComeToHome;
    }

    public void setRegion(List<String> region) {
        this.region = region;
    }

    public List<String> getRegion() {
        return region;
    }

    public void setSlotName(List<String> slotName) {
        this.slotName = slotName;
    }

    public List<String> getSlotName() {
        return slotName;
    }

    @Override
    public String toString() {
        return
                "AppointmentsItemCart{" +
                        "address = '" + address + '\'' +
                        ",city = '" + city + '\'' +
                        ",whs_id = '" + whsId + '\'' +
                        ",ap_hours = '" + apHours + '\'' +
                        ",ap_date = '" + apDate + '\'' +
                        ",mechanic_come_to_home = '" + mechanicComeToHome + '\'' +
                        ",region = '" + region + '\'' +
                        ",slot_name = '" + slotName + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(address);
        dest.writeStringList(city);
        dest.writeStringList(whsId);
        dest.writeStringList(apHours);
        dest.writeStringList(apDate);
        dest.writeStringList(mechanicComeToHome);
        dest.writeStringList(region);
        dest.writeStringList(slotName);
    }
}