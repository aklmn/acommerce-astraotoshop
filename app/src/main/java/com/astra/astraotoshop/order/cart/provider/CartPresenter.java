package com.astra.astraotoshop.order.cart.provider;

import android.view.View;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.utils.pref.Pref;

import java.util.ArrayList;
import java.util.List;

import retrofit2.HttpException;
import rx.functions.Action1;

/**
 * Created by Henra Setia Nugraha on 1/17/2018.
 */

public class CartPresenter implements CartContract.CartPresenter {

    private CartContract.CartInteractor interactor;
    private CartContract.CartView view;
    private CartContract.ExternalCart external;
    private String storeCode;
    private boolean isEditAppointment = false;
    private ArrayList appointmentItems;
    private List<CartListItem> cartListItems;


    public CartPresenter(CartContract.CartView view, CartContract.ExternalCart external, String storeCode) {
        this.view = view;
        this.external = external;
        this.storeCode = storeCode;
        interactor = new CartInteractor(this);

    }

    public CartPresenter(CartContract.CartView view, CartContract.ExternalCart external, String storeCode, boolean isEditAppointment) {
        this.view = view;
        this.external = external;
        this.storeCode = storeCode;
        this.isEditAppointment = isEditAppointment;
        interactor = new CartInteractor(this);
    }

    @Override
    public void requestCartList() {
        if (Pref.getPreference().getInt("quoteId" + storeCode) == -1)
            requestQuoteId(quoteId -> {
                saveToPref(quoteId);
                interactor.requestCartList(storeCode);
            }, error -> {
                System.out.println();
            });
        else interactor.requestCartList(storeCode);
    }

    @Override
    public void onCartListReceived(CartListEntity carts) {
        if (isEditAppointment) cartListItems = carts.getItems();
        view.setCartList(carts.getItems());
        if (carts.getItems().size() > 0)
            Pref.getPreference().putInt("cartVisibility", View.VISIBLE);
        else Pref.getPreference().putInt("cartVisibility", View.GONE);
    }

    @Override
    public void addToCart(String sku, int qty) {
//        if (Pref.getPreference().getInt("quoteId" + storeCode) == -1)
//            requestQuoteId(quoteId -> {
//                saveToPref(quoteId);
//                mAddToCart(sku, qty);
//            }, error -> {
//            });
//        else mAddToCart(sku, qty);
        addToCart(sku, qty, null);
    }

    @Override
    public void addToCart(String sku, int qty, List<AppointmentParamItem> appointmentItem) {
        if (Pref.getPreference().getInt("quoteId" + storeCode) == -1)
            requestQuoteId(quoteId -> {
                saveToPref(quoteId);
                mAddToCart(sku, qty, appointmentItem);
            }, error -> {
                System.out.println();
            });
        else mAddToCart(sku, qty, appointmentItem);
    }

    private void mAddToCart(String sku, int qty, List<AppointmentParamItem> appointmentItem) {
//        List<AppointmentParamItem> appointmentItems = new ArrayList<>();
//        if (appointmentItem != null) {
//            appointmentItems.add(appointmentItem);
//        }

        interactor.addToCart(
                storeCode,
                new AddCartBody(
                        new CartItem(
                                Pref.getPreference().getInt("quoteId" + storeCode),
                                qty,
                                sku,
                                new ProductOption(new AppExtensionAttributes(appointmentItem))
                        )
                )
        );
    }

    @Override
    public void onAdded() {
        external.onItemAdded();
    }

    @Override
    public void onFailedToAdd(Throwable throwable) {
        Pref.getPreference().remove("quoteId" + storeCode);
        requestQuoteId(quoteId -> {
            saveToPref(quoteId);
            external.onItemFailedToAdd("Gagal menabahkan, Silakan coba lagi");
        }, error -> {
            external.onItemFailedToAdd("Terjadi kesalahan");
        });
    }

    @Override
    public void updateCart(CartListItem data) {
        List<AppointmentParamItem> paramItems = null;
        if (data.getExtensionAttributes().getAppointmentsItemCart() != null) {
            paramItems = new ArrayList<>();
            int size = data.getExtensionAttributes().getAppointmentsItemCart().get(0).getRegion().size();
            if (size > 0) {
                AppointmentsItemCart appItem = data.getExtensionAttributes().getAppointmentsItemCart().get(0);
                AppointmentParamItem paramItem;
                for (int i = 0; i < size; i++) {
                    paramItem = new AppointmentParamItem(
                            appItem.getAddress().get(i),
                            appItem.getCity().get(i),
                            appItem.getWhsId().get(i),
                            0,
                            appItem.getApHours().get(i),
                            appItem.getApDate().get(i),
                            appItem.getMechanicComeToHome().get(i),
                            appItem.getRegion().get(i),
                            appItem.getSlotName().get(i)
                    );
                    paramItems.add(paramItem);
                }
            }
        }

        interactor.updateCart(
                storeCode,
                data.getItemId(),
                data.getQuoteId(),
                new UpdateCartBody(
                        new CartUpdateItem(
                                data.getItemId(),
                                data.getQuoteId(),
                                data.getQty(),
                                data.getSku(),
                                new ProductOption(new AppExtensionAttributes(paramItems)))
                )
        );
    }

    @Override
    public void onUpdated() {
        if (view != null)
            view.onCartUpdated();
        if (external != null)
            external.onItemUpdated();
    }

    @Override
    public void onFailedToUpdate(Throwable throwable) {
        if (((HttpException) throwable).response().code() == 404) {
            Pref.getPreference().remove("quoteId" + storeCode);
            requestQuoteId(this::saveToPref, error -> {
            });
        } else if (external != null) {
            external.onItemFailedToUpdate("Terjadi Kesalahan");
        }
    }

    @Override
    public void deleteCart(String itemId) {
        interactor.deleteCart(storeCode, itemId);
    }

    @Override
    public void onDeleted() {
        if (external != null) {
            external.onItemDeleted();
        } else if (view != null) {
            view.onCartItemDeleted();
        }
    }

    @Override
    public void onFailedToDelete(Throwable throwable) {
        if (external != null) {
            external.onItemFailedToDelete();
        }
    }

    @Override
    public void onFailedReceiving(Throwable throwable) {
        System.out.println();
    }

    @Override
    public List<CartListItem> getCartsItem() {
        return cartListItems != null ? cartListItems : new ArrayList<>();
    }

    private void requestQuoteId(Action1<String> success, Action1<Throwable> error) {
        int quoteId = Pref.getPreference().getInt("quoteId" + storeCode);
        String userId = Pref.getPreference().getString(BuildConfig.UID);

        if (quoteId == -1 && userId != null) {
            interactor.requestQuoteId(new CartQuoteEntity(userId), storeCode, success, error);
        }
    }

    private void saveToPref(String value) {
        Pref.getPreference().putInt("quoteId" + storeCode, Integer.valueOf(value));
        System.out.println();
    }
}
