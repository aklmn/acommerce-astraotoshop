package com.astra.astraotoshop.order.cart;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.provider.CartItem;
import com.astra.astraotoshop.order.cart.provider.CartListItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.ImageSquared;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import butterknife.OnTextChanged;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Henra Setia Nugraha on 11/21/2017.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

    private Context context;
    private String state;
    private List<CartListItem> cartItems;
    private ListListener listener;

    public CartAdapter(Context context, ListListener listener) {
        this.context = context;
        this.listener = listener;
        cartItems = new ArrayList<>();
    }

    public CartAdapter(Context context, String state) {
        this.context = context;
        this.state = state;
        cartItems = new ArrayList<>();
    }

    public CartAdapter(Context context) {
        this.context = context;
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CartViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart3, parent, false));
    }

    @Override
    public void onBindViewHolder(CartViewHolder holder, int position) {
        setText(holder.productName, cartItems.get(position).getName());
        setText(holder.firstPrice, StringFormat.addCurrencyFormat(cartItems.get(position).getPrice()));
        holder.setQuantity(cartItems.get(position).getQty());

        if (cartItems.get(position).getExtensionAttributes().getImageUrl() != null) {
            Glide.with(context).load(cartItems.get(position).getExtensionAttributes().getImageUrl()).into(holder.image);
        }
    }

    private void setText(TextView textView, Object value) {
        if (value != null)
            textView.setText(String.valueOf(value));
    }

    @Override
    public int getItemCount() {
        return cartItems.size();
    }

    public Context getContext() {
        return context;
    }

    public void setCartItems(List<CartListItem> cartItems) {
        this.cartItems = cartItems;
        notifyDataSetChanged();
    }

    public void deleteItem(int position) {
        cartItems.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, cartItems.size());
    }

    public void updateItem(int position, int qty) {
        cartItems.get(position).setQty(qty);
    }

    public void itemUpdated(int position) {
        cartItems.get(position).setNeedUpdate(false);
    }

    class CartViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageSquared image;
        @BindView(R.id.productName)
        TextView productName;
        @BindView(R.id.firstPrice)
        TextView firstPrice;
        @BindView(R.id.qty)
        EditText qty;

        public CartViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.qtyMin)
        public void minus() {
            int quantity = Integer.parseInt(qty.getText().toString());
            if (--quantity >= 1) setQuantity(quantity);
        }

        @OnClick(R.id.qtyPlus)
        public void plus() {
            int quantity = Integer.parseInt(qty.getText().toString());
            setQuantity(++quantity);
        }

        private void setQuantity(int quantity) {
            qty.setText(String.valueOf(quantity));
            observerSetup();
        }

        @OnTextChanged(R.id.qty)
        public void updateQty(CharSequence charSequence, int i, int l, int j) {
            if (!charSequence.toString().equals("")) {
                int q = Integer.parseInt(charSequence.toString());
                if (q <= 0) {
                    setQuantity(1);
                }
            }
        }

        @OnFocusChange(R.id.qty)
        public void focus(View view, boolean isFocus) {
            if (!isFocus) {
                if (((EditText) view).getText().toString().equals("")) {
                    setQuantity(1);
                }
            }
        }

        @OnClick(R.id.delete)
        public void delete() {
            deleteItem(getAdapterPosition());
//            listener.onItemClick(R.id.delete, getAdapterPosition(), cartItems.get(getAdapterPosition()).getItemId());
        }

        private void observerSetup() {
            Observable.unsafeCreate((Observable.OnSubscribe<String>) subscriber -> {
                qty.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() > 0) {
                            subscriber.onNext(qty.getText().toString());
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
            })
                    .filter(s -> s.length() > 0)
                    .distinctUntilChanged()
                    .debounce(1000, TimeUnit.MILLISECONDS)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(s -> {
                        try {
                            CartListItem cartItem = cartItems.get(getAdapterPosition());
                            cartItem.setQty(Integer.parseInt(s));
                            listener.onItemClick(R.id.update, getAdapterPosition(), cartItem);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
        }
    }
}
