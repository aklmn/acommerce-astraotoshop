package com.astra.astraotoshop.order.cart;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.provider.CartListItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.FontIcon;
import com.astra.astraotoshop.utils.view.FontIconic;
import com.astra.astraotoshop.utils.view.ImageSquared;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 11/21/2017.
 */

public class CartAppointmentAdapter extends RecyclerView.Adapter<CartAppointmentAdapter.CartProductViewHolder> {

    private Context context;
    private String state;
    private List<CartListItem> carts;
    private boolean showOption = true;
    private ListListener listener;

    public CartAppointmentAdapter(Context context, ListListener listener) {
        this.context = context;
        this.listener = listener;
        carts = new ArrayList<>();
    }

    public CartAppointmentAdapter(Context context, boolean showOption, ListListener listener) {
        this.context = context;
        this.showOption = showOption;
        this.listener = listener;
        carts = new ArrayList<>();
    }

    @Override
    public CartProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CartProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart3, parent, false), showOption);
    }

    @Override
    public void onBindViewHolder(CartProductViewHolder holder, int position) {
        holder.name.setText(carts.get(position).getName());
        StringFormat.applyCurrencyFormat(holder.firstPrice, carts.get(position).getPrice());
        Glide.with(context).load(carts.get(position).getExtensionAttributes().getImageUrl()).into(holder.image);
        holder.qty.setText(carts.get(position).getQty());
        try {
            holder.adapter.setItem(carts.get(position).getExtensionAttributes().getAppointmentsItemCart().get(0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return carts.size();
    }

    public void setCarts(List<CartListItem> carts) {
        if (carts != null) {
            this.carts = carts;
            notifyDataSetChanged();
        }
    }

    public void deleteItem(int position) {
        carts.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, carts.size());
    }

    class CartProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageSquared image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.firstPrice)
        TextView firstPrice;
        @BindView(R.id.delete)
        FontIcon delete;
        @BindView(R.id.detailLabel)
        Switch detailLabel;
        @BindView(R.id.detailIcon)
        FontIcon detailIcon;
        @BindView(R.id.edit)
        FontIcon edit;
        @BindView(R.id.detailList)
        RecyclerView detailList;
        @BindView(R.id.qtyMin)
        FontIconic qtyMin;
        @BindView(R.id.qty)
        EditText qty;
        @BindView(R.id.qtyPlus)
        FontIconic qtyPlus;
        private final CartAppointmentDetailAdapter adapter;

        public CartProductViewHolder(View itemView, boolean showOption) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            adapter = new CartAppointmentDetailAdapter(false, null);
            detailList.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
            detailList.setAdapter(adapter);

            if (!showOption) {
                edit.setVisibility(View.GONE);
                delete.setVisibility(View.GONE);
            }
        }

        @OnClick(R.id.qtyMin)
        public void onMinClick() {
            int currentQty = Integer.parseInt(qtyMin.getText().toString());
            if ((currentQty - 1) > 0) {
                qty.setText(--currentQty);
            }
        }

        @OnClick(R.id.qtyPlus)
        public void onPlusClick(){

        }

        @OnClick(R.id.detailIcon)
        public void onViewClicked() {
            detailLabel.setChecked(!detailLabel.isChecked());
        }

        @OnCheckedChanged(R.id.detailLabel)
        public void onDetailChecked(CompoundButton button, boolean isChecked) {
            if (isChecked) detailList.setVisibility(View.VISIBLE);
            else detailList.setVisibility(View.GONE);
        }

        @OnClick(R.id.delete)
        public void onDelete() {
            try {
                listener.onItemClick(R.id.delete, getAdapterPosition(), carts.get(getAdapterPosition()).getItemId());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @OnClick(R.id.edit)
        public void onEdit() {
            try {
                listener.onItemClick(R.id.edit, getAdapterPosition(), carts.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
