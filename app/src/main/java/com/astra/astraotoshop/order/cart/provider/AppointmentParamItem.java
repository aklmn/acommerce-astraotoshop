package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

public class AppointmentParamItem {

    @SerializedName("address")
    private String address;

    @SerializedName("city")
    private String city;

    @SerializedName("whs_id")
    private String whsId;

    @SerializedName("home_service")
    private int homeService;

    @SerializedName("ap_hours")
    private String apHours;

    @SerializedName("ap_date")
    private String apDate;

    @SerializedName("mechanic_come_to_home")
    private String mechanicComeToHome;

    @SerializedName("region")
    private String region;

    @SerializedName("slot_name")
    private String slotName;

    public AppointmentParamItem(String address, String city, String whsId, int homeService, String apHours, String apDate, String mechanicComeToHome, String region, String slotName) {
        this.address = address;
        this.city = city;
        this.whsId = whsId;
        this.homeService = homeService;
        this.apHours = apHours;
        this.apDate = apDate;
        this.mechanicComeToHome = mechanicComeToHome;
        this.region = region;
        this.slotName = slotName;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setWhsId(String whsId) {
        this.whsId = whsId;
    }

    public String getWhsId() {
        return whsId;
    }

    public void setHomeService(int homeService) {
        this.homeService = homeService;
    }

    public int getHomeService() {
        return homeService;
    }

    public void setApHours(String apHours) {
        this.apHours = apHours;
    }

    public String getApHours() {
        return apHours;
    }

    public void setApDate(String apDate) {
        this.apDate = apDate;
    }

    public String getApDate() {
        return apDate;
    }

    public void setMechanicComeToHome(String mechanicComeToHome) {
        this.mechanicComeToHome = mechanicComeToHome;
    }

    public String getMechanicComeToHome() {
        return mechanicComeToHome;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getRegion() {
        return region;
    }

    public void setSlotName(String slotName) {
        this.slotName = slotName;
    }

    public String getSlotName() {
        return slotName;
    }

    @Override
    public String toString() {
        return
                "AppointmentParamItem{" +
                        "address = '" + address + '\'' +
                        ",city = '" + city + '\'' +
                        ",whs_id = '" + whsId + '\'' +
                        ",home_service = '" + homeService + '\'' +
                        ",ap_hours = '" + apHours + '\'' +
                        ",ap_date = '" + apDate + '\'' +
                        ",mechanic_come_to_home = '" + mechanicComeToHome + '\'' +
                        ",region = '" + region + '\'' +
                        ",slot_name = '" + slotName + '\'' +
                        "}";
    }
}