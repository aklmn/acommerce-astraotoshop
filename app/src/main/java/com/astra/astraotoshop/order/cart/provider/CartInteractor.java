package com.astra.astraotoshop.order.cart.provider;

import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/17/2018.
 */

public class CartInteractor extends BaseInteractor implements CartContract.CartInteractor {

    private CartContract.CartPresenter presenter;

    public CartInteractor(CartContract.CartPresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestQuoteId(CartQuoteEntity userId, String storeCode, Action1<String> success, Action1<Throwable> error) {
        getGeneralNetworkManager()
                .requestQuoteId(
                        new NetworkHandler(),
                        storeCode,
                        userId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success, error);
//                .subscribe(sss->{
//                    System.out.println();
//                }, ddd->{
//                    System.out.println();
//                });
    }

    @Override
    public void requestCartList(String storeCode) {
        getGeneralNetworkManager()
                .requestCartList(
                        new NetworkHandler(),
                        storeCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        carts -> {
                            presenter.onCartListReceived(carts);
                        },
                        error -> {
                            presenter.onFailedReceiving(error);
                        }
                );
    }

    @Override
    public void addToCart(String storeCode, AddCartBody cart) {
        getGeneralNetworkManager()
                .addCart(
                        new NetworkHandler(),
                        storeCode,
                        cart)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        o -> presenter.onAdded(),
                        error -> presenter.onFailedToAdd(error)
                );
    }

    @Override
    public void updateCart(String storeCode, int itemId, int quoteId, UpdateCartBody cartBody) {
        getGeneralNetworkManager()
                .updateItemCart(
                        new NetworkHandler(),
                        storeCode,
                        itemId,
                        quoteId,
                        cartBody)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        o -> {
                            System.out.println();
                            presenter.onUpdated();
                        },
                        error -> {
                            presenter.onFailedToUpdate(error);
                        }
                );
    }

    @Override
    public void deleteCart(String storeCode, String itemId) {
        getGeneralNetworkManager()
                .deleteItemCart(
                        new NetworkHandler(),
                        storeCode,
                        itemId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        o -> {
                            presenter.onDeleted();
                        },
                        error -> {
                            presenter.onFailedToDelete(error);
                        }
                );
    }
}
