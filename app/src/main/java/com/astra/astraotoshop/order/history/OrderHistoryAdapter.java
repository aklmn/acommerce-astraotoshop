package com.astra.astraotoshop.order.history;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.history.provider.OrderHistoriesItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 11/30/2017.
 */

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.OrderHistoryViewHolder> {

    ListListener listener;
    private List<OrderHistoriesItem> ordersItems;

    public OrderHistoryAdapter(ListListener listener) {
        this.listener = listener;
        ordersItems = new ArrayList<>();
    }

    @Override
    public OrderHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderHistoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_history, parent, false));
    }

    @Override
    public void onBindViewHolder(OrderHistoryViewHolder holder, int position) {
        holder.incrementId.setText("#" + ordersItems.get(position).getIncrementId());
        holder.date.setText(ordersItems.get(position).getCreatedAt());
        holder.status.setText(ordersItems.get(position).getStatus());
        StringFormat.applyCurrencyFormat(holder.grandTotal, ordersItems.get(position).getGrandTotal());
    }

    @Override
    public int getItemCount() {
        return ordersItems.size();
    }

    public void setOrdersItems(List<OrderHistoriesItem> ordersItems, boolean addMore) {
        if (addMore) {
            int lastPos = this.ordersItems.size();
            this.ordersItems.addAll(ordersItems);
            notifyItemRangeInserted(lastPos, ordersItems.size());
        } else {
            this.ordersItems = ordersItems;
            notifyDataSetChanged();
        }
    }

    class OrderHistoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.incrementId)
        TextView incrementId;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.grandTotal)
        TextView grandTotal;
        @BindView(R.id.status)
        TextView status;

        public OrderHistoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.container)
        public void containerClick() {
            try {
                listener.onItemClick(0, getAdapterPosition(), ordersItems.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    interface OrderHistoryListener {
        void onItemClick();
    }
}
