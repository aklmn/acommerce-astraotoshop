package com.astra.astraotoshop.order.cart;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.astra.astraotoshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 11/21/2017.
 */

public class CartVendorAddressAdapter extends RecyclerView.Adapter<CartVendorAddressAdapter.CartVendorViewHolder> {

    Context context;

    public CartVendorAddressAdapter(Context context) {
        this.context = context;
    }

    @Override
    public CartVendorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CartVendorViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_vendor_address, parent, false));
    }

    @Override
    public void onBindViewHolder(CartVendorViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    class CartVendorViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.markerIcon)
        TextView markerIcon;
        @BindView(R.id.timeIcon)
        TextView timeIcon;

        public CartVendorViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/fontawesome.ttf");
            markerIcon.setTypeface(typeface);
            timeIcon.setTypeface(typeface);
        }
    }
}
