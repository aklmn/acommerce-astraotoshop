package com.astra.astraotoshop.order.checkout.provider.address;

import com.google.gson.annotations.SerializedName;

public class MobilePhone {

    @SerializedName("value")
    private String value;

    @SerializedName("attribute_code")
    private String attributeCode="mobile_phone";

    public MobilePhone() {
    }

    public MobilePhone(String value) {

        this.value = value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setAttributeCode(String attributeCode) {
        this.attributeCode = attributeCode;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    @Override
    public String toString() {
        return
                "MobilePhone{" +
                        "value = '" + value + '\'' +
                        ",attribute_code = '" + attributeCode + '\'' +
                        "}";
    }
}