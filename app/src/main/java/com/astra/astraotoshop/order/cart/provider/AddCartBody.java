package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

public class AddCartBody {

    @SerializedName("cart_item")
    private CartItem cartItem;


    public AddCartBody(CartItem cartItem) {
        this.cartItem = cartItem;
    }

    public AddCartBody() {
    }

    public void setCartItem(CartItem cartItem) {
        this.cartItem = cartItem;
    }

    public CartItem getCartItem() {
        return cartItem;
    }

    @Override
    public String toString() {
        return
                "AddCartBody{" +
                        "cart_item = '" + cartItem + '\'' +
                        "}";
    }
}