package com.astra.astraotoshop.order.appointment.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class SlotItem implements Parcelable{

	@SerializedName("whs_id")
	private String whsId;

	@SerializedName("clock")
	private String clock;

	@SerializedName("slot_name")
	private String slotName;

	public SlotItem() {
	}

	protected SlotItem(Parcel in) {
		whsId = in.readString();
		clock = in.readString();
		slotName = in.readString();
	}

	public static final Creator<SlotItem> CREATOR = new Creator<SlotItem>() {
		@Override
		public SlotItem createFromParcel(Parcel in) {
			return new SlotItem(in);
		}

		@Override
		public SlotItem[] newArray(int size) {
			return new SlotItem[size];
		}
	};

	public void setWhsId(String whsId){
		this.whsId = whsId;
	}

	public String getWhsId(){
		return whsId;
	}

	public void setClock(String clock){
		this.clock = clock;
	}

	public String getClock(){
		return clock;
	}

	public void setSlotName(String slotName){
		this.slotName = slotName;
	}

	public String getSlotName(){
		return slotName;
	}

	@Override
 	public String toString(){
		return 
			"SlotItem{" + 
			"whs_id = '" + whsId + '\'' + 
			",clock = '" + clock + '\'' + 
			",slot_name = '" + slotName + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(whsId);
		dest.writeString(clock);
		dest.writeString(slotName);
	}
}