package com.astra.astraotoshop.order.appointment.provider;

import com.google.gson.annotations.SerializedName;

public class AppointmentEntity {

    @SerializedName("items")
    private AppointmentsItem appointmentsItem;

    public void setAppointmentsItem(AppointmentsItem appointmentsItem) {
        this.appointmentsItem = appointmentsItem;
    }

    public AppointmentsItem getAppointmentsItem() {
        return appointmentsItem;
    }

    @Override
    public String toString() {
        return
                "AppointmentEntity{" +
                        "appointmentsItem = '" + appointmentsItem + '\'' +
                        "}";
    }
}