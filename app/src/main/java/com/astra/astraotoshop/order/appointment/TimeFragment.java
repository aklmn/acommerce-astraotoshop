package com.astra.astraotoshop.order.appointment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.appointment.provider.SlotItem;
import com.astra.astraotoshop.utils.base.BaseDialogFragment;
import com.astra.astraotoshop.utils.view.FontIconic;
import com.astra.astraotoshop.utils.view.StringFormat;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarView;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

/**
 * Created by Henra Setia Nugraha on 3/20/2018.
 */

public class TimeFragment extends BaseDialogFragment implements AdapterView.OnItemClickListener {

    @BindView(R.id.close)
    FontIconic close;
    @BindView(R.id.clock)
    ListView clock;
    Unbinder unbinder;
    @BindView(R.id.date)
    HorizontalCalendarView date;

    private List<SlotItem> slotItems;
    private TimeAdapter adapter;
    private HorizontalCalendar horizontalCalendar;
    private Calendar selectedCalendar;
    private SlotItem slotItem;

    public static TimeFragment newInstance() {

        Bundle args = new Bundle();

        TimeFragment fragment = new TimeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_schedule, container, false);
        unbinder = ButterKnife.bind(this, view);
        adapter = new TimeAdapter();
        Calendar startCal = Calendar.getInstance();
        startCal.add(Calendar.DAY_OF_MONTH, 1);
        Calendar endCal = Calendar.getInstance();
        endCal.add(Calendar.MONTH, 2);
        clock.setOnItemClickListener(this);
        selectedCalendar = startCal;
        horizontalCalendar = new HorizontalCalendar.Builder(view, R.id.date)
                .range(startCal, endCal)
                .build();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        clock.setAdapter(adapter);
        if (slotItems != null) adapter.setSlotItems(slotItems);
        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar calendar, int i) {
                selectedCalendar = calendar;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        adapter.setCheckItem(view.findViewById(R.id.checkIcon), position);
        slotItem = (SlotItem) adapter.getItem(position);
    }

    @OnClick(R.id.choose)
    public void onChooseClicked() {
        AppointmentActivity activity = (AppointmentActivity) getActivity();
        if (slotItem == null) {
            Toast.makeText(getContext(), "Pilih jam kedatangan", Toast.LENGTH_SHORT).show();
            return;
        }

        if (activity != null) {
            activity.setTime(slotItem, selectedCalendar);
        }
        dismiss();
    }

    @OnClick(R.id.close)
    public void onCloseClicked() {
        dismiss();
    }

    public void setSlotItems(List<SlotItem> slotItems) {
        this.slotItems = slotItems;
    }
}
