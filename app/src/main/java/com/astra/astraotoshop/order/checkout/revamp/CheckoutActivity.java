package com.astra.astraotoshop.order.checkout.revamp;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.provider.totals.CartTotalEntity;
import com.astra.astraotoshop.order.checkout.CheckoutContract;
import com.astra.astraotoshop.order.checkout.PaymentMethodFragment;
import com.astra.astraotoshop.order.checkout.ShippingAdapter;
import com.astra.astraotoshop.order.checkout.ShippingMethodFragment;
import com.astra.astraotoshop.order.checkout.ShippingProductAdapter;
import com.astra.astraotoshop.order.checkout.provider.CheckoutPresenter;
import com.astra.astraotoshop.order.checkout.provider.ShippingMethodEntity;
import com.astra.astraotoshop.order.checkout.provider.ShippingPresenter;
import com.astra.astraotoshop.order.checkout.provider.address.AddressBodyBuilder;
import com.astra.astraotoshop.order.checkout.provider.installment.PaymentDesc;
import com.astra.astraotoshop.order.checkout.provider.payment.AvailablePaymentMethod;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodsItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.user.address.AddressListActivity;
import com.astra.astraotoshop.user.profile.provider.Addresses;
import com.astra.astraotoshop.utils.base.BaseActivity;
import com.astra.astraotoshop.utils.pref.Pref;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.ImageSquared;
import com.astra.astraotoshop.utils.view.Loading;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckoutActivity extends BaseActivity implements CheckoutContract.CheckoutView, ListListener {

    private static final int RC_ADDRESS = 3;
    @BindView(R.id.subTotal)
    CustomFontFace subTotal;
    @BindView(R.id.grandTotal)
    CustomFontFace grandTotal;
    @BindView(R.id.shippingPrice)
    CustomFontFace shippingPrice;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.multiWarehouseContainer)
    LinearLayout multiWarehouseContainer;
    @BindView(R.id.shippingGroup)
    RecyclerView shippingGroup;
    @BindView(R.id.productList)
    RecyclerView productList;
    @BindView(R.id.shippingContainer)
    LinearLayout shippingContainer;
    @BindView(R.id.pruductImage)
    ImageSquared pruductImage;
    @BindView(R.id.pruductName)
    CustomFontFace pruductName;
    @BindView(R.id.shopName)
    CustomFontFace shopName;
    @BindView(R.id.shopAddress)
    TextView shopAddress;
    @BindView(R.id.schedule)
    CustomFontFace schedule;
    @BindView(R.id.productAppointment)
    ConstraintLayout productAppointment;
    @BindView(R.id.checkoutBtn)
    Button checkoutBtn;
    private CheckoutPresenter presenter;
    private ShippingMethodFragment shippingMethodFragment;
    private PaymentMethodFragment paymentMethodFragment;
    private Loading loading;
    private Addresses addresses;
    private ShippingMethodEntity shippingMethod;
    private List<PaymentMethodsItem> paymentMethods;
    private boolean isAddressSet;
    private boolean isFirst = true;
    private int itemSelected = -1;
    private ShippingAdapter shippingAdapter;
    private ShippingPresenter shippingPresenter;
    private ShippingProductAdapter appointmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_revamp);
        ButterKnife.bind(this);
        initialize();
    }

    private void initialize() {
        actionBarSetup();
        setTotal();
        presenter = new CheckoutPresenter(this, getStoreCode());
        shippingMethodFragment = ShippingMethodFragment.newInstance();
        paymentMethodFragment = PaymentMethodFragment.newInstance();
        loading = new Loading(this, getLayoutInflater());
        shippingAdapter = new ShippingAdapter(this);
        shippingGroup.setLayoutManager(new LinearLayoutManager(this));
        shippingGroup.setAdapter(shippingAdapter);
        shippingGroup.setNestedScrollingEnabled(false);

        appointmentAdapter = new ShippingProductAdapter(this);
        productList.setLayoutManager(new LinearLayoutManager(this));
        productList.setAdapter(appointmentAdapter);
        shippingPresenter = new ShippingPresenter();
        productList.setNestedScrollingEnabled(false);
        setDefaultAddress();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == RC_ADDRESS) {
            Addresses addresses = data.getParcelableExtra("data");
            if (addresses != null) {
                setAddress(addresses);
                itemSelected = -1;
            }
        }
    }

    @Override
    public void showLoading() {
        try {
            loading.show(getString(R.string.message_please_wait));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showShippingMethod(List<ShippingMethodEntity> shippingMethod) {
        if (shippingMethod.size() > 0) {
            shippingAdapter.setShippingMethods(shippingMethod, 0);
            shippingContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showPaymentMethod(AvailablePaymentMethod paymentMethod) {
        System.out.println();
    }

    @Override
    public void showSuccessPage(String orderCode) {
        System.out.println();
    }

    @Override
    public void showMessageError(String message) {
        System.out.println();
    }

    @Override
    public void hideLoading() {
        try {
            loading.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onVaSaved() {
        System.out.println();
    }

    @Override
    public void onSaveFailed() {
        System.out.println();
    }

    @Override
    public void showPaymentDesc(PaymentDesc paymentDesc) {
        System.out.println();
    }

    @Override
    public void showPaymentDescError() {
        System.out.println();
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        if (viewId == android.R.id.text1) {
            checkoutBtn.setEnabled(true);
            ShippingMethodEntity item = (ShippingMethodEntity) data;
            if (item.getMethodDescription() != null && !item.getMethodDescription().equals("")) {
                productList.setVisibility(View.VISIBLE);
                appointmentAdapter.setItemMultiWarehouses(shippingPresenter.getProductList(item.getMethodDescription()));
            } else appointmentAdapter.setItemMultiWarehouses(new ArrayList<>());
        }
    }

    @OnClick(R.id.checkoutBtn)
    public void onCheckoutBtnClicked() {
        startActivity(new Intent(this, PaymentActivity.class));
    }

    @OnClick(R.id.changeAddress)
    public void onChangeAddressClicked() {
        startActivityForResult((getIntent(this, AddressListActivity.class)).putExtra("isFromCheckout", true), RC_ADDRESS);
    }

    private void setTotal() {
        CartTotalEntity cartTotalEntity = getIntent().getParcelableExtra("total");
        if (cartTotalEntity != null) {
            StringFormat.applyCurrencyFormat(subTotal, cartTotalEntity.getBaseSubtotal());
            StringFormat.applyCurrencyFormat(grandTotal, cartTotalEntity.getBaseGrandTotal());
            StringFormat.applyCurrencyFormat(shippingPrice, cartTotalEntity.getShippingAmount());
        }
    }

    private void setDefaultAddress() {
        String address = Pref.getPreference().getString("address");
        if (address != null) {
            Addresses addresses = new Gson().fromJson(address, Addresses.class);
            if (addresses != null) {
                setAddress(addresses);
            }
        }
    }

    private void setAddress(Addresses addresses) {
        presenter.setAddressId(addresses.getId());
        setAddressValue(addresses);
        shippingMethod = null;
        shippingContainer.setVisibility(View.GONE);
        paymentMethods = new ArrayList<>();
        isAddressSet = true;

        if (isFirst) {
            showLoading();
            presenter.getShippingMethod();
        }

        if (getState() == getResources().getInteger(R.integer.state_product_service)) {
            showLoading();
            requestPaymentMethod("acommerce", "noshippment");
        }
    }

    private void setAddressValue(Addresses addresses) {
        this.addresses = addresses;
        String addressFormat = "<b>" + addresses.getFirstname() + " " + addresses.getLastname() + "</b><br>" + StringFormat.getAddress(addresses) + "<br>" + addresses.getTelephone();
        address.setText(Html.fromHtml(addressFormat));
    }

    private void requestPaymentMethod(String carrierCode, String methodCode) {
        presenter.saveShippingMethod(AddressBodyBuilder.getAddressBody(
                addresses, carrierCode, methodCode));
    }

    class RadioViewHolder {
        @BindView(R.id.radioItem)
        RadioButton radioItem;
        View view;

        RadioViewHolder() {
            view = LayoutInflater.from(CheckoutActivity.this).inflate(R.layout.part_radio_button, null);
            ButterKnife.bind(this, view);
        }

        public void bind(String value) {
            radioItem.setText(value);
        }

        public View getView() {
            return view;
        }
    }
}
