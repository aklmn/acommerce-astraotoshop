package com.astra.astraotoshop.order.checkout.provider;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartInteractor;
import com.astra.astraotoshop.order.cart.provider.CartQuoteEntity;
import com.astra.astraotoshop.order.checkout.CheckoutContract;
import com.astra.astraotoshop.order.checkout.provider.address.AddressShippingBody;
import com.astra.astraotoshop.order.checkout.provider.installment.PaymentDesc;
import com.astra.astraotoshop.order.checkout.provider.payment.AvailablePaymentMethod;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethod;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodBody;
import com.astra.astraotoshop.utils.pref.Pref;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/31/2018.
 */

public class CheckoutPresenter implements CheckoutContract.CheckoutPresenter {

    private CheckoutContract.CheckoutView view;
    private CheckoutContract.CheckoutInteractor interactor;
    private CartContract.CartInteractor cartInteractor;
    private String storeCode;
    private int addressId;
    private List<ShippingMethodEntity> shippingMethod;
    private String orderCode;

    public CheckoutPresenter(CheckoutContract.CheckoutView view, String storeCode) {
        this.view = view;
        this.storeCode = storeCode;
        interactor = new CheckoutInteractor(this);
        cartInteractor = new CartInteractor(null);
    }

    @Override
    public void getShippingMethod() {
        view.showLoading();
        interactor.requestShippingMethod(storeCode, new AddressBody(addressId));
    }

    @Override
    public void onShippingMethodReceived(List<ShippingMethodEntity> shippingMethod) {
        try {
            view.showShippingMethod(shippingMethod);
            this.shippingMethod = shippingMethod;
            view.hideLoading();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailedReceivingShippingMethod(Throwable throwable) {
        view.hideLoading();
    }

    @Override
    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    @Override
    public void saveShippingMethod(AddressShippingBody addressShippingBody) {
        interactor.saveShippingMethod(storeCode, addressShippingBody);
    }

    @Override
    public void onAddressSaved(AvailablePaymentMethod paymentMethod) {
        view.showPaymentMethod(paymentMethod);
    }

    @Override
    public void onFailedToSaveAddress() {
        view.hideLoading();
    }

    @Override
    public ShippingMethodEntity getShipping() {
        return null;
    }

    @Override
    public void savePaymentMethod(String paymentCode) {
        interactor.savePaymentMethod(storeCode, new PaymentMethodBody(new PaymentMethod(paymentCode)));
    }

    @Override
    public void onPaymentSaved(String orderCode) {
        String userId = Pref.getPreference().getString(BuildConfig.UID);
        cartInteractor.requestQuoteId(new CartQuoteEntity(userId), storeCode,
                quoteId -> {
                    Pref.getPreference().putInt("quoteId" + storeCode, Integer.valueOf(quoteId));
                    view.showSuccessPage(orderCode);
                    this.orderCode = orderCode;
                },
                error -> {
                    view.showSuccessPage(orderCode);
                    this.orderCode = orderCode;
                });
    }

    @Override
    public void onFailedToSavePayment(Throwable error) {
        view.showMessageError("Stok barang tidak tersedia");
    }

    @Override
    public PaymentMethod getPayment() {
        return null;
    }

    @Override
    public String getOrderCode() {
        return orderCode;
    }

    @Override
    public void saveVa(String orderId) {
        interactor.saveVa(storeCode, new VaBody(orderId));
    }

    @Override
    public void onVaSaved() {
        view.onVaSaved();
    }

    @Override
    public void onSaveFailed(Throwable e) {
        view.onSaveFailed();
    }

    @Override
    public void getPaymentDesc() {
        interactor.getPaymentDesc(storeCode);
    }

    @Override
    public void onPaymentDescReceived(PaymentDesc paymentDesc) {
        view.showPaymentDesc(paymentDesc);
    }

    @Override
    public void onPaymentDescFailed(Throwable throwable) {
        view.showPaymentDescError();
    }
}
