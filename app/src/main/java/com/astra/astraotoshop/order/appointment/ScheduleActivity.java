package com.astra.astraotoshop.order.appointment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.astra.astraotoshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScheduleActivity extends AppCompatActivity implements  AdapterView.OnItemClickListener {

    @BindView(R.id.clock)
    ListView clock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        ButterKnife.bind(this);

        String[] time = new String[5];
        for (int i = 0; i < time.length; i++) time[i] = "13:" + String.valueOf((i + 1) * 10);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_single_choice, time);
        clock.setAdapter(adapter);
        clock.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        finish();
    }
}
