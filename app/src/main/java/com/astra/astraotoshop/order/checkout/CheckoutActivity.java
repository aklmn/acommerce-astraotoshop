package com.astra.astraotoshop.order.checkout;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.PagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CheckoutActivity extends AppCompatActivity implements CheckoutAddressFragment.CheckoutPageListener {

    @BindView(R.id.divider)
    TextView divider;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pager)
    ViewPager pager;

    CheckoutAddressFragment addressFragment=CheckoutAddressFragment.newInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        ButterKnife.bind(this);
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/fontawesome.ttf");
        divider.setTypeface(typeface);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addressFragment.setPageListener(this);

        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(addressFragment,"");
        pagerAdapter.addFragment(CheckoutPaymentMethodFragment.newInstance(), "");
        pager.setAdapter(pagerAdapter);
    }

    @Override
    public void onBackPressed() {
        if (pager.getCurrentItem()!=0) {
            pager.setCurrentItem(pager.getCurrentItem()-1);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void changePagePosition(int position) {
        pager.setCurrentItem(position);
    }
}
