package com.astra.astraotoshop.order.checkout.provider.address;

import com.google.gson.annotations.SerializedName;

public class AddressShippingBody {

    @SerializedName("addressInformation")
    private AddressInformation addressInformation;

    public AddressShippingBody(AddressInformation addressInformation) {
        this.addressInformation = addressInformation;
    }

    public AddressShippingBody() {
    }

    public void setAddressInformation(AddressInformation addressInformation) {
        this.addressInformation = addressInformation;
    }

    public AddressInformation getAddressInformation() {
        return addressInformation;
    }

    @Override
    public String toString() {
        return
                "AddressShippingBody{" +
                        "addressInformation = '" + addressInformation + '\'' +
                        "}";
    }
}