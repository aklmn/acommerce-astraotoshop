package com.astra.astraotoshop.order.checkout.provider.payment;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AvailablePaymentMethod implements Parcelable{

    @SerializedName("payment_methods")
    private List<PaymentMethodsItem> paymentMethods;

    @SerializedName("totals")
    private Totals totals;

    protected AvailablePaymentMethod(Parcel in) {
    }

    public static final Creator<AvailablePaymentMethod> CREATOR = new Creator<AvailablePaymentMethod>() {
        @Override
        public AvailablePaymentMethod createFromParcel(Parcel in) {
            return new AvailablePaymentMethod(in);
        }

        @Override
        public AvailablePaymentMethod[] newArray(int size) {
            return new AvailablePaymentMethod[size];
        }
    };

    public void setPaymentMethods(List<PaymentMethodsItem> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public List<PaymentMethodsItem> getPaymentMethods() {
        return paymentMethods;
    }

    public void setTotals(Totals totals) {
        this.totals = totals;
    }

    public Totals getTotals() {
        return totals;
    }

    @Override
    public String toString() {
        return
                "AvailablePaymentMethod{" +
                        "payment_methods = '" + paymentMethods + '\'' +
                        ",totals = '" + totals + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}