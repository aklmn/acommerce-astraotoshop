package com.astra.astraotoshop.order.checkout.provider.installment;

import com.google.gson.annotations.SerializedName;

public class CcHosted {

    @SerializedName("instructions")
    private Object instructions;

    public void setInstructions(Object instructions) {
        this.instructions = instructions;
    }

    public Object getInstructions() {
        return instructions;
    }

    @Override
    public String toString() {
        return
                "CcHosted{" +
                        "instructions = '" + instructions + '\'' +
                        "}";
    }
}