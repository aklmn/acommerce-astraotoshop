package com.astra.astraotoshop.order.appointment;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.CustomFontFace;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.BaseAppointmentViewHolder> {

    public static final int ADDRESS_TYPE = 768;
    public static final int OUTLET_TYPE = 876;

    private List<String> data;
    private int currentType;
    private ListListener listener;
    private RadioButton currentSelected;
    private String itemSelected;

    public AppointmentAdapter(int currentType, ListListener listener) {
        this.currentType = currentType;
        this.listener = listener;
        data = new ArrayList<>();
    }

    @NonNull
    @Override
    public BaseAppointmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == ADDRESS_TYPE)
            return new AddressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_addrs_appointment, parent, false));
        else
            return new OutletViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_addrs_appointment2, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseAppointmentViewHolder holder, int position) {
        holder.bind(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return currentType;
    }

    public void setData(List<String> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void setCurrentType(int currentType) {
        this.currentType = currentType;
    }

    class AddressViewHolder extends BaseAppointmentViewHolder {

        @BindView(R.id.title)
        CustomFontFace title;

        AddressViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void bind(Object data) {
            title.setText((String) data);
        }

        @OnClick(R.id.itemContainer)
        public void onItemClick() {
            try {
                listener.onItemClick(R.id.itemContainer, getAdapterPosition(), data.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    class OutletViewHolder extends BaseAppointmentViewHolder {

        @BindView(R.id.title)
        CustomFontFace title;
        @BindView(R.id.check)
        RadioButton check;
        @BindView(R.id.completeAddress)
        TextView completeAddress;

        OutletViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        @Override
        public void bind(Object data) {
            String[] outlet = (((String)data).replace(" || ","%%")).split("%%");
            title.setText(outlet[0]);
            completeAddress.setText(outlet[1]);
        }

        @OnClick(R.id.itemContainer)
        public void onItemClick() {
            if (currentSelected != null) {
                currentSelected.setChecked(false);
            }
            currentSelected = check;
            currentSelected.setChecked(true);
            itemSelected = data.get(getAdapterPosition());
            try {
                listener.onItemClick(R.id.itemContainer, getAdapterPosition(), data.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    abstract class BaseAppointmentViewHolder extends RecyclerView.ViewHolder {
        BaseAppointmentViewHolder(View itemView) {
            super(itemView);
        }

        public abstract void bind(Object data);
    }
}
