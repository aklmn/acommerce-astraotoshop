package com.astra.astraotoshop.order.appointment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.appointment.provider.AppointmentsItem;
import com.astra.astraotoshop.order.appointment.provider.SlotItem;
import com.astra.astraotoshop.order.cart.provider.AppointmentParamItem;
import com.astra.astraotoshop.order.cart.provider.AppointmentsItemCart;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartPresenter;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AppointmentFragment extends DialogFragment implements AppointmentContract.MainAppointment, DatePickerDialog.OnDateSetListener, CartContract.ExternalCart {

    @BindView(R.id.title)
    CustomFontFace title;
    @BindView(R.id.homeService)
    CheckBox homeService;
    @BindView(R.id.outletTitle)
    CustomFontFace outletTitle;
    @BindView(R.id.outletName)
    CustomFontFace outletName;
    @BindView(R.id.outletAddress)
    CustomFontFace outletAddress;
    @BindView(R.id.resultContainer)
    LinearLayout resultContainer;
    @BindView(R.id.outletContainer)
    ConstraintLayout outletContainer;
    @BindView(R.id.scheduleTitle)
    CustomFontFace scheduleTitle;
    @BindView(R.id.containerSchedule)
    ConstraintLayout containerSchedule;
    Unbinder unbinder;
    @BindView(R.id.order)
    Button order;
    private String productId;
    private List<SlotItem> slotItems;
    private AppointmentsItem selectedAppointment;
    private View addressContainer;
    private DatePickerDialog pickerDialog;
    private Calendar selectedCalendar;
    private SlotItem selectedSchedule;
    private String selectedTime;
    private CartContract.CartPresenter cartPresenter;

    public static AppointmentFragment newInstance(String productid, String sku) {

        Bundle args = new Bundle();
        args.putString("productId", productid);
        args.putString("sku", sku);
        AppointmentFragment fragment = new AppointmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productId = getArguments().getString("productId");
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.FullScreenDialog);
        setCancelable(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_appointment, container, false);
        unbinder = ButterKnife.bind(this, view);
        cartPresenter = new CartPresenter(null, this, "productservice");
        initCalendar();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void addToCart(List<AppointmentParamItem> params) {

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        selectedCalendar = Calendar.getInstance();
        selectedCalendar.set(year, monthOfYear, dayOfMonth);

        ScheduleFragment scheduleFragment = new ScheduleFragment();
        scheduleFragment.setSlotItems(slotItems);
        scheduleFragment.show(getChildFragmentManager(), "schedule");
    }

    @OnClick(R.id.close)
    public void onCloseClicked() {
        dismiss();
    }

    @OnClick(R.id.order)
    public void onOrderClicked() {
        cartPresenter.addToCart(getArguments().getString("sku"), 1, appointmentMap());
    }

    @OnClick(R.id.outletContainer)
    public void onOutletClick() {
        AppointmentAddressFragment addressFragment = AppointmentAddressFragment.newInstance(productId);
        addressFragment.show(getChildFragmentManager(), AppointmentFragment.class.getName() + AppointmentAddressFragment.class.getName());
    }

    @OnClick(R.id.containerSchedule)
    public void onScheduleClick() {
        pickerDialog.show(Objects.requireNonNull(getActivity()).getFragmentManager(), "selectedCalendar");
    }

    private void initCalendar() {
        Calendar calendar = Calendar.getInstance();
        pickerDialog = DatePickerDialog.newInstance(
                this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );

        pickerDialog.setAccentColor(getResources().getColor(R.color.mainRed));

        Calendar dateTerm = Calendar.getInstance();
        dateTerm.add(Calendar.DAY_OF_MONTH, 1);
        pickerDialog.setMinDate(dateTerm);
        dateTerm.add(Calendar.DAY_OF_MONTH, 60);
        pickerDialog.setMaxDate(dateTerm);
    }

    public void setOutlet(List<SlotItem> slotItems, String outlet, AppointmentsItem appointment) {
        this.slotItems = slotItems;
        selectedAppointment = appointment;
        String[] outletData = (outlet.replace(" || ", "%%")).split("%%");
        outletName.setText(outletData[0]);
        outletAddress.setText(outletData[1]);
        outletTitle.setVisibility(View.GONE);
        resultContainer.setVisibility(View.VISIBLE);
        resetTime();
    }

    public void setSchedule(SlotItem selectedSchedule) {
        this.selectedSchedule = selectedSchedule;
        order.setEnabled(true);
        selectedTime = StringFormat.getCalendarString(selectedCalendar, null);
        if (selectedSchedule != null) {
            scheduleTitle.setText(StringFormat.getCalendarString(selectedCalendar, "EEEE, dd MMM") + " " + selectedSchedule.getClock().substring(0, 5));
        }
    }

    private void resetTime() {
        containerSchedule.setVisibility(View.VISIBLE);
        order.setEnabled(false);
        selectedCalendar = null;
        selectedSchedule = null;
        selectedTime = null;
    }

    private List<AppointmentParamItem> appointmentMap() {
        List<AppointmentParamItem> paramItems = new ArrayList<>();
        AppointmentParamItem paramItem;
        paramItem = new AppointmentParamItem(
                selectedAppointment.getAddress().get(0),
                selectedAppointment.getCity().get(0),
                selectedSchedule.getWhsId(),
                0,
                selectedSchedule.getClock(),
                StringFormat.getCalendarString(selectedCalendar, "yyyy-MM-dd"),
                "0",
                selectedAppointment.getRegion().get(0),
                selectedSchedule.getSlotName()
        );
        paramItems.add(paramItem);
        return paramItems;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onItemAdded() {

    }

    @Override
    public void onItemFailedToAdd(String message) {

    }

    @Override
    public void onItemUpdated() {

    }

    @Override
    public void onItemFailedToUpdate(String message) {

    }

    @Override
    public void onItemDeleted() {

    }

    @Override
    public void onItemFailedToDelete() {

    }
}
