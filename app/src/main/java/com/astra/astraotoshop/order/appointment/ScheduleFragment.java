package com.astra.astraotoshop.order.appointment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.appointment.provider.SlotItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.FontIconic;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ScheduleFragment extends BottomSheetDialogFragment implements ListListener {

    @BindView(R.id.gap)
    View gap;
    @BindView(R.id.title)
    CustomFontFace title;
    @BindView(R.id.close)
    FontIconic close;
    @BindView(R.id.search)
    EditText search;
    @BindView(R.id.addressList)
    RecyclerView scheduleList;
    Unbinder unbinder;
    @BindView(R.id.sub)
    CustomFontFace sub;
    @BindView(R.id.topLine)
    View topLine;
    @BindView(R.id.searchContainer)
    RelativeLayout searchContainer;
    @BindView(R.id.bottomLine)
    View bottomLine;
    @BindView(R.id.saveAppointment)
    Button saveSchedule;
    private View view;
    private List<SlotItem> slotItems;
    private ScheduleAdapter adapter;
    private BottomSheetBehavior<FrameLayout> behavior;
    private SlotItem selectedSchedule;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_appointment_address, container, false);
        unbinder = ButterKnife.bind(this, view);
        init();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
        FrameLayout bottomSheet = (FrameLayout)
                dialog.findViewById(android.support.design.R.id.design_bottom_sheet);
        Objects.requireNonNull(bottomSheet).setBackgroundColor(getResources().getColor(android.R.color.transparent));
        behavior = BottomSheetBehavior.from(bottomSheet);
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        selectedSchedule = (SlotItem) data;
        saveSchedule.setVisibility(View.VISIBLE);
        behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @OnClick(R.id.saveAppointment)
    public void onSaveClick() {
        AppointmentFragment fragment = (AppointmentFragment) getActivity().getSupportFragmentManager().findFragmentByTag(ProductDetailActivity.class.getName() + AppointmentFragment.class.getName());
        if (fragment != null) {
            fragment.setSchedule(selectedSchedule);
            dismiss();
        }
    }

    private void init() {
        topLine.setVisibility(View.GONE);
        searchContainer.setVisibility(View.GONE);
        bottomLine.setVisibility(View.GONE);
        title.setText("TENTUKAN WAKTU");
        sub.setText("Pilih waktu service");
        saveSchedule.setText("KONFIRMASI WAKTU");
        adapter = new ScheduleAdapter(this);
        adapter.setSlotItems(slotItems);
        scheduleList.setLayoutManager(new LinearLayoutManager(getContext()));
        scheduleList.setAdapter(adapter);
    }

    public void setSlotItems(List<SlotItem> slotItems) {
        this.slotItems = slotItems;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
