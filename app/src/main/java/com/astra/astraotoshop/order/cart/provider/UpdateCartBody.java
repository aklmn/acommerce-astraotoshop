package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

public class UpdateCartBody {

	@SerializedName("cart_item")
	private CartUpdateItem cartItem;

	public UpdateCartBody(CartUpdateItem cartItem) {
		this.cartItem = cartItem;
	}

	public UpdateCartBody() {
	}

	public void setCartItem(CartUpdateItem cartItem){
		this.cartItem = cartItem;
	}

	public CartUpdateItem getCartItem(){
		return cartItem;
	}

	@Override
 	public String toString(){
		return 
			"AddCartBody{" + 
			"cart_item = '" + cartItem + '\'' + 
			"}";
		}
}