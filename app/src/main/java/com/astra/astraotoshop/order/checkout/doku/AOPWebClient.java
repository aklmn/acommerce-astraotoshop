package com.astra.astraotoshop.order.checkout.doku;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.astra.astraotoshop.BuildConfig;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by Henra Setia Nugraha on 3/7/2018.
 */

public class AOPWebClient extends WebViewClient {

    private SSLContext sslContext;
    private Context context;
    private WebViewListener listener;

    public AOPWebClient(Context context, WebViewListener listener) {
        this.context = context;
        this.listener = listener;
        prepareSslPinning();
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (String.valueOf(request.getUrl()).contains("onepage/success")) {
                listener.onSuccess();
            } else if (String.valueOf(request.getUrl()).contains("onepage/failed")) {
                listener.onFailed();
            }
        }
        return super.shouldInterceptRequest(view, request);
    }

    @Override
    public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
        if (url.contains("onepage/success")) {
            listener.onSuccess();
        } else if (url.contains("onepage/failed")) {
            listener.onFailed();
        }
        return super.shouldInterceptRequest(view, url);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        Uri uri;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            uri = Uri.parse(String.valueOf(request.getUrl()));
            view.loadUrl(String.valueOf(uri));
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        listener.onLoaded();
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//        handler.proceed();
    }

    private WebResourceResponse processRequest(Uri uri) {
        Log.d("SSL_PINNING_WEBVIEWS", "GET: " + uri.toString());

        try {
            // Setup connection
            URL url = new URL(uri.toString());
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();

            // Set SSL Socket Factory for this request
            urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());

            // Get content, contentType and encoding
            InputStream is = urlConnection.getInputStream();
            String contentType = urlConnection.getContentType();
            String encoding = urlConnection.getContentEncoding();

            // If got a contentType header
            if (contentType != null) {

                String mimeType = contentType;

                // Parse mime type from contenttype string
                if (contentType.contains(";")) {
                    mimeType = contentType.split(";")[0].trim();
                }

                Log.d("SSL_PINNING_WEBVIEWS", "Mime: " + mimeType);

                // Return the response
                return new WebResourceResponse(mimeType, encoding, is);
            }

        } catch (SSLHandshakeException e) {
            Log.d("SSL_PINNING_WEBVIEWS", e.getLocalizedMessage());
        } catch (Exception e) {
            Log.d("SSL_PINNING_WEBVIEWS", e.getLocalizedMessage());
        }

        // Return empty response for this request
        return new WebResourceResponse(null, null, null);
    }

    private void prepareSslPinning() {
        // Create keystore
        KeyStore keyStore = initKeyStore();

        // Setup trustmanager factory
        String algorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = null;
        try {
            tmf = TrustManagerFactory.getInstance(algorithm);
            tmf.init(keyStore);

            // Set SSL context
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

    private KeyStore initKeyStore() {
        try {
            // Create keystore
            KeyStore keyStore = KeyStore.getInstance("PKCS12");
            keyStore.load(null, new char[]{});
            CertificateFactory cf = CertificateFactory.getInstance("X.509");

            addTrustedCertificates(keyStore, cf);

            return keyStore;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(BuildConfig.DEBUG ? "Not able to load the certificates : NoSuchAlgorithmException" : "");
        } catch (CertificateException e) {
            throw new RuntimeException(BuildConfig.DEBUG ? "Not able to load the certificates : CertificateException" : "");
        } catch (IOException e) {
            throw new RuntimeException(BuildConfig.DEBUG ? "Not able to load the certificates : IOException" : "");
        } catch (KeyStoreException e1) {
            throw new RuntimeException(BuildConfig.DEBUG ? "Not able to instantiate the keystore:" + e1.getMessage() : "");
        }
    }

    private void addTrustedCertificates(KeyStore keyStore, CertificateFactory cf) {
        try {
            // Add cert
            byte[] derIng = Base64.decode(TrustedServerCertificates.INFO_SUPPORT_COM, Base64.NO_WRAP);
            ByteArrayInputStream bais = new ByteArrayInputStream(derIng);
            X509Certificate cert = (X509Certificate) cf.generateCertificate(bais);
            keyStore.setCertificateEntry("astraotoshop.acomindo.com", cert);

        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }
}
