package com.astra.astraotoshop.order.cart.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ExtensionAttributes {

    @SerializedName("image_url")
    private String imageUrl;

    @SerializedName("appointment_data")
    List<AppointmentsItemCart> appointmentsItemCart;

    @SerializedName("product_id")
    private String productId;

    public ExtensionAttributes(List<AppointmentsItemCart> appointmentsItemCart) {
        this.appointmentsItemCart = appointmentsItemCart;
    }

    public ExtensionAttributes() {
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<AppointmentsItemCart> getAppointmentsItemCart() {
        return appointmentsItemCart;
    }

    public void setAppointmentsItemCart(List<AppointmentsItemCart> appointmentsItemCart) {
        this.appointmentsItemCart = appointmentsItemCart;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    @Override
    public String toString() {
        return
                "ExtensionAttributes{" +
                        "image_url = '" + imageUrl + '\'' +
                        "}";
    }
}