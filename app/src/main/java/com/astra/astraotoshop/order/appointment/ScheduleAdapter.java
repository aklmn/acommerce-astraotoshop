package com.astra.astraotoshop.order.appointment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.appointment.provider.SlotItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.FontIconic;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder> {
    private List<SlotItem> slotItems;
    private FontIconic currentPos;
    private ListListener listener;


    public ScheduleAdapter(ListListener listener) {
        this.listener = listener;
        slotItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public ScheduleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ScheduleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_time, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ScheduleViewHolder holder, int position) {
        holder.hour.setText(slotItems.get(position).getClock());
    }

    @Override
    public int getItemCount() {
        return slotItems.size();
    }

    public void setSlotItems(List<SlotItem> slotItems) {
        this.slotItems = slotItems;
        notifyDataSetChanged();
    }

    class ScheduleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.hour)
        TextView hour;
        @BindView(R.id.checkIcon)
        FontIconic checkIcon;

        ScheduleViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (currentPos != null) {
                currentPos.setVisibility(View.GONE);
            }
            checkIcon.setVisibility(View.VISIBLE);
            currentPos = checkIcon;

            listener.onItemClick(0, getAdapterPosition(), slotItems.get(getAdapterPosition()));
        }
    }
}
