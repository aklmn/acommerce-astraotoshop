package com.astra.astraotoshop.order.checkout.provider.address;

import com.astra.astraotoshop.user.profile.provider.Addresses;

/**
 * Created by Henra Setia Nugraha on 2/4/2018.
 */

public class AddressBodyBuilder {
    public static AddressShippingBody getAddressBody(Addresses addresses, String shippingCarrier, String shippingMethod) {
        MobilePhone mobilePhone = new MobilePhone(
                addresses.getTelephone() != null ? addresses.getTelephone() : ""
        );

        Kelurahan kelurahan = new Kelurahan();
        if (addresses.getCustomAttributes().get(0) != null) {
            kelurahan = new Kelurahan(
                    addresses.getCustomAttributes().get(0).getValue()
            );
        }

        BillingAddress billingAddress = new BillingAddress(
                addresses.getFirstname(),
                addresses.getCity(),
                addresses.getPostcode(),
                null,
                addresses.getTelephone(),
                addresses.getCountryId(),
                addresses.getLastname(),
                String.valueOf(addresses.getId()),
                addresses.getRegion().getRegionCode(),
                String.valueOf(addresses.getRegionId()),
                addresses.getStreet(),
                String.valueOf(addresses.getCustomerId()),
                addresses.getRegion().getRegion(),
                new CustomAttributes(mobilePhone, kelurahan)
        );

        AddressInformation information = new AddressInformation(
                shippingMethod,
                shippingCarrier,
                billingAddress
        );

        return new AddressShippingBody(information);
    }
}
