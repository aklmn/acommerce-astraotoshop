package com.astra.astraotoshop.order.checkout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by Henra Setia Nugraha on 11/23/2017.
 */

public class CheckoutPaymentMethodFragment extends Fragment {

    Unbinder unbinder;

    public static CheckoutPaymentMethodFragment newInstance() {

        Bundle args = new Bundle();

        CheckoutPaymentMethodFragment fragment = new CheckoutPaymentMethodFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_payment_method, container, false);
        unbinder= ButterKnife.bind(this,view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.orderSummary)
    public void orderSummary(){
        getActivity().finish();
    }

    @Override
    public void onDetach() {
        unbinder.unbind();
        super.onDetach();
    }
}
