package com.astra.astraotoshop.order.shipping;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.utils.base.BaseRecyclerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 12/21/2017.
 */

public class ShippingAdapter extends BaseRecyclerAdapter<ShippingAdapter.ShippingViewHolder> {

    @Override
    protected ShippingViewHolder viewHolder(ViewGroup parent, int viewType) {
        return new ShippingViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_shipping, parent, false));
    }

    @Override
    protected void bind(ShippingViewHolder holder, int position) {

    }

    @Override
    protected int itemCount() {
        return 3;
    }

    @Override
    protected void onEndPosition(ShippingViewHolder holder, int position) {
        holder.divider.setVisibility(View.GONE);
    }

    class ShippingViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.divider)
        ImageView divider;
        ShippingViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
