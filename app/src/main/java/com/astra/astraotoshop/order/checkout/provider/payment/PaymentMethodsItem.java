package com.astra.astraotoshop.order.checkout.provider.payment;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class PaymentMethodsItem implements Parcelable{

    @SerializedName("code")
    private String code;

    @SerializedName("title")
    private String title;

    protected PaymentMethodsItem(Parcel in) {
        code = in.readString();
        title = in.readString();
    }

    public static final Creator<PaymentMethodsItem> CREATOR = new Creator<PaymentMethodsItem>() {
        @Override
        public PaymentMethodsItem createFromParcel(Parcel in) {
            return new PaymentMethodsItem(in);
        }

        @Override
        public PaymentMethodsItem[] newArray(int size) {
            return new PaymentMethodsItem[size];
        }
    };

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return
                "PaymentMethodsItem{" +
                        "code = '" + code + '\'' +
                        ",title = '" + title + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(code);
        dest.writeString(title);
    }
}