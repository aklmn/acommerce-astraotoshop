package com.astra.astraotoshop.order.appointment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.appointment.provider.SlotItem;
import com.astra.astraotoshop.utils.view.FontIconic;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Henra Setia Nugraha on 3/20/2018.
 */

public class TimeAdapter extends BaseAdapter {

    private List<SlotItem> slotItems;
    private int currentPos = -1;
    private FontIconic viewTemp;

    public TimeAdapter() {
        slotItems = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return slotItems.size();
    }

    @Override
    public Object getItem(int position) {
        return slotItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setSlotItems(List<SlotItem> slotItems) {
        this.slotItems = slotItems;
        notifyDataSetChanged();
    }

    public void setCheckItem(FontIconic viewTemp, int position) {
        if (viewTemp != null) {
            if (this.viewTemp != null)
                this.viewTemp.setVisibility(View.GONE);
            currentPos = position;
            this.viewTemp = viewTemp;
            viewTemp.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_time, parent, false);
        }

        TimeViewHolder holder = new TimeViewHolder(convertView);
        holder.hour.setText(slotItems.get(position).getClock());
        if (currentPos > -1) {
            holder.checkIcon.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    static class TimeViewHolder {
        @BindView(R.id.hour)
        TextView hour;
        @BindView(R.id.checkIcon)
        FontIconic checkIcon;

        TimeViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
