package com.astra.astraotoshop.order.checkout.provider;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ShippingMethodEntity implements Parcelable{

    @SerializedName("error_message")
    private String errorMessage;

    @SerializedName("price_excl_tax")
    private int priceExclTax;

    @SerializedName("amount")
    private int amount;

    @SerializedName("price_incl_tax")
    private int priceInclTax;

    @SerializedName("method_description")
    private String methodDescription;

    @SerializedName("carrier_title")
    private String carrierTitle;

    @SerializedName("available")
    private boolean available;

    @SerializedName("base_amount")
    private int baseAmount;

    @SerializedName("method_code")
    private String methodCode;

    @SerializedName("method_title")
    private String methodTitle;

    @SerializedName("carrier_code")
    private String carrierCode;

    private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    protected ShippingMethodEntity(Parcel in) {
        errorMessage = in.readString();
        priceExclTax = in.readInt();
        amount = in.readInt();
        priceInclTax = in.readInt();
        methodDescription = in.readString();
        carrierTitle = in.readString();
        available = in.readByte() != 0;
        baseAmount = in.readInt();
        methodCode = in.readString();
        methodTitle = in.readString();
        carrierCode = in.readString();
    }

    public static final Creator<ShippingMethodEntity> CREATOR = new Creator<ShippingMethodEntity>() {
        @Override
        public ShippingMethodEntity createFromParcel(Parcel in) {
            return new ShippingMethodEntity(in);
        }

        @Override
        public ShippingMethodEntity[] newArray(int size) {
            return new ShippingMethodEntity[size];
        }
    };

    public ShippingMethodEntity(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setPriceExclTax(int priceExclTax) {
        this.priceExclTax = priceExclTax;
    }

    public int getPriceExclTax() {
        return priceExclTax;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setPriceInclTax(int priceInclTax) {
        this.priceInclTax = priceInclTax;
    }

    public int getPriceInclTax() {
        return priceInclTax;
    }

    public void setMethodDescription(String methodDescription) {
        this.methodDescription = methodDescription;
    }

    public String getMethodDescription() {
        return methodDescription;
    }

    public void setCarrierTitle(String carrierTitle) {
        this.carrierTitle = carrierTitle;
    }

    public String getCarrierTitle() {
        return carrierTitle;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setBaseAmount(int baseAmount) {
        this.baseAmount = baseAmount;
    }

    public int getBaseAmount() {
        return baseAmount;
    }

    public void setMethodCode(String methodCode) {
        this.methodCode = methodCode;
    }

    public String getMethodCode() {
        return methodCode;
    }

    public void setMethodTitle(String methodTitle) {
        this.methodTitle = methodTitle;
    }

    public String getMethodTitle() {
        return methodTitle;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    @Override
    public String toString() {
        return
                "ShippingMethodEntity{" +
                        "error_message = '" + errorMessage + '\'' +
                        ",price_excl_tax = '" + priceExclTax + '\'' +
                        ",amount = '" + amount + '\'' +
                        ",price_incl_tax = '" + priceInclTax + '\'' +
                        ",method_description = '" + methodDescription + '\'' +
                        ",carrier_title = '" + carrierTitle + '\'' +
                        ",available = '" + available + '\'' +
                        ",base_amount = '" + baseAmount + '\'' +
                        ",method_code = '" + methodCode + '\'' +
                        ",method_title = '" + methodTitle + '\'' +
                        ",carrier_code = '" + carrierCode + '\'' +
                        "}";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(errorMessage);
        dest.writeInt(priceExclTax);
        dest.writeInt(amount);
        dest.writeInt(priceInclTax);
        dest.writeString(methodDescription);
        dest.writeString(carrierTitle);
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeInt(baseAmount);
        dest.writeString(methodCode);
        dest.writeString(methodTitle);
        dest.writeString(carrierCode);
    }
}