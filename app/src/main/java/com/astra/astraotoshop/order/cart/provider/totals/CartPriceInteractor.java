package com.astra.astraotoshop.order.cart.provider.totals;

import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.utils.base.BaseInteractor;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Henra Setia Nugraha on 1/28/2018.
 */

public class CartPriceInteractor extends BaseInteractor implements CartContract.CartPriceInteractor {

    private CartContract.CartPricePresenter presenter;

    public CartPriceInteractor(CartContract.CartPricePresenter presenter) {
        super(null);
        this.presenter = presenter;
    }

    @Override
    public void requestTotal(String storeCode) {
        getGeneralNetworkManager()
                .requestTotalCart(
                        new NetworkHandler(),
                        storeCode)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(
                        cartTotalEntity -> presenter.onTotalReceived(cartTotalEntity),
                        error->presenter.onFailedReceivingTotal(error)
                );
    }
}
