package com.astra.astraotoshop.order.cart.provider.voucher;

import com.astra.astraotoshop.order.cart.provider.CartContract;

/**
 * Created by Henra Setia Nugraha on 1/29/2018.
 */

public class CartVoucherPresenter implements CartContract.CartVoucherPresenter {

    private CartContract.CartVoucherInteractor interactor;
    private CartContract.CartVoucherView view;
    private String storeCode;

    public CartVoucherPresenter(CartContract.CartVoucherView view, String storeCode) {
        interactor=new CartVoucherInteractor(this);
        this.view = view;
        this.storeCode = storeCode;
    }

    @Override
    public void addVoucher(String voucherCode) {
        interactor.addVoucher(storeCode,voucherCode);
    }

    @Override
    public void deleteVoucher() {
        interactor.deleteVoucher(storeCode);
    }

    @Override
    public void onVoucherAdded() {
        view.setVoucherView(true);
    }

    @Override
    public void onVoucherDeleted() {
        view.setVoucherView(false);
    }

    @Override
    public void onAddVoucherFailed() {
        view.showVoucherError(true);
    }

    @Override
    public void onDeleteVoucherFailed() {
        view.showVoucherError(false);
    }
}
