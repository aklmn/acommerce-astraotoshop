package com.astra.astraotoshop.order.checkout.provider;

import com.google.gson.annotations.SerializedName;

public class VaBody {

    @SerializedName("order_id")
    private String orderId;

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderId() {
        return orderId;
    }

    public VaBody(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return
                "VaBody{" +
                        "order_id = '" + orderId + '\'' +
                        "}";
    }
}