package com.astra.astraotoshop.order.appointment.provider;

import com.astra.astraotoshop.order.appointment.AppointmentContract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Henra Setia Nugraha on 3/6/2018.
 */

public class AppointmentPresenter implements AppointmentContract.AppointmentPresenter {

    private int CURRENT_STATE = -1;
    private final int REGION_STATE = 0;
    private final int CITY_STATE = 1;
    private final int ADDRESS_STATE = 2;

    private AppointmentContract.AppointmentView view;
    private AppointmentInterator interator;
    private Map<String, String> query;
    private AppointmentsItem appointment;
    private AppointmentsItem selectedAppointment = new AppointmentsItem();
    private String productId;
    private AppointmentContract.AppointmentExternal external;

    public AppointmentPresenter(AppointmentContract.AppointmentView view, String productId) {
        this.view = view;
        this.productId = productId;
        interator = new AppointmentInterator(this);
        setDefaultQuery();
    }

    public AppointmentPresenter(AppointmentContract.AppointmentView view, String productId, AppointmentContract.AppointmentExternal external) {
        this.view = view;
        this.productId = productId;
        this.external = external;
        interator = new AppointmentInterator(this);
        setDefaultQuery();
    }

    @Override
    public void getAppointment(String data) {
        interator.getAppointment(productId, getMap(data));
    }

    @Override
    public void onAppointmentReceived(AppointmentEntity entity) {
        if (appointment != null) {
            if (appointment.getSlot().size() == 0 && entity.getAppointmentsItem().getSlot().size() > 0) {
                appointment.setSlot(entity.getAppointmentsItem().getSlot());
                if (view != null)
                    view.onSlotAvailable(entity.getAppointmentsItem().getSlot());
                if (external != null) {
                    external.setSlot(entity.getAppointmentsItem().getSlot());
                    setDefaultQuery();
                    clearData();
                }
            } else if (appointment.getAddress().size() == 0 && entity.getAppointmentsItem().getAddress().size() > 0) {
                appointment.setAddress(entity.getAppointmentsItem().getAddress());
                CURRENT_STATE = ADDRESS_STATE;
            } else if (appointment.getCity().size() == 0 && entity.getAppointmentsItem().getCity().size() > 0) {
                appointment.setCity(entity.getAppointmentsItem().getCity());
                CURRENT_STATE = CITY_STATE;
            } else if (appointment.getRegion().size() == 0) {
                appointment.setRegion(entity.getAppointmentsItem().getRegion());
                CURRENT_STATE = REGION_STATE;
            }
        } else {
            appointment = entity.getAppointmentsItem();
            appointment.setCity(new ArrayList<>());
            CURRENT_STATE = REGION_STATE;
        }
        if (view != null) {
            view.showButton(appointment.getAddress().size() > 0);
            view.onDataAvailable();
        }
    }

    @Override
    public void onFailedReceiveData(Throwable e) {
        if (external != null) {
            external.showSlotErrorMessage();
        }
    }

    @Override
    public void getSlotItems(String region, String city, String address) {
        if (appointment == null) {
            appointment = new AppointmentsItem();
        }
        query.put("region", region);
        query.put("city", city);

        appointment.setRegion(Collections.singletonList(region));
        appointment.setCity(Collections.singletonList(city));
        appointment.setSlot(new ArrayList<>());
        CURRENT_STATE = ADDRESS_STATE;
        getAppointment(address);
    }

    @Override
    public List<String> getData() {
        switch (CURRENT_STATE) {
            case ADDRESS_STATE:
                return appointment.getAddress();
            case CITY_STATE:
                return appointment.getCity();
            case REGION_STATE:
                return appointment.getRegion();
        }
        return new ArrayList<>();
    }

    @Override
    public List<SlotItem> getSlot() {
        if (appointment != null)
            return appointment.getSlot();
        return new ArrayList<>();
    }

    @Override
    public void clearPrevious() {
        switch (CURRENT_STATE) {
            case ADDRESS_STATE: {
                appointment.setAddress(new ArrayList<>());
                CURRENT_STATE = CITY_STATE;
                query.put("address", "");
                break;
            }
            case CITY_STATE: {
                appointment.setCity(new ArrayList<>());
                CURRENT_STATE = REGION_STATE;
                query.put("city", "");
                break;
            }
            case REGION_STATE: {
                appointment = null;
                CURRENT_STATE = -1;
                query.put("region", "");
                break;
            }
        }
        if (appointment != null) {
            view.showButton(appointment.getAddress().size() > 0);
        }
    }

    @Override
    public void clearData() {
        if (appointment != null) {
            appointment.setCity(new ArrayList<>());
            appointment.setAddress(new ArrayList<>());
            appointment.setSlot(new ArrayList<>());
            CURRENT_STATE = REGION_STATE;
        }
    }

    @Override
    public AppointmentsItem getAppointment() {
        return selectedAppointment;
    }

    private void setDefaultQuery() {
        query = new HashMap<>();
        query.put("region", "");
        query.put("city", "");
        query.put("address", "");
    }

    private Map<String, String> getMap(String data) {
        if (appointment != null) {
            switch (CURRENT_STATE) {
                case REGION_STATE: {
                    query.put("region", data);
                    selectedAppointment.setRegion(new ArrayList<>(Collections.singletonList(data)));
                    break;
                }
                case CITY_STATE: {
                    query.put("city", data);
                    selectedAppointment.setCity(new ArrayList<>(Collections.singletonList(data)));
                    break;
                }
                case ADDRESS_STATE: {
                    query.put("address", data);
                    selectedAppointment.setAddress(new ArrayList<>(Collections.singletonList(data)));
                    break;
                }
            }
        }
        return query;
    }
}
