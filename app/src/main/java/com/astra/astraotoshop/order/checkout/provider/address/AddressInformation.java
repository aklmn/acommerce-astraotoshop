package com.astra.astraotoshop.order.checkout.provider.address;

import com.google.gson.annotations.SerializedName;

public class AddressInformation {

    @SerializedName("shipping_method_code")
    private String shippingMethodCode = "noshippment";

    @SerializedName("shipping_carrier_code")
    private String shippingCarrierCode = "acommerce";

    @SerializedName("billing_address")
    private BillingAddress billingAddress;

    @SerializedName("shipping_address")
    private BillingAddress shippingAddress;

    public AddressInformation(String shippingMethodCode, String shippingCarrierCode, BillingAddress billingAddress) {
        this.shippingMethodCode = shippingMethodCode;
        this.shippingCarrierCode = shippingCarrierCode;
        this.shippingAddress = billingAddress;
        this.billingAddress = billingAddress;
    }

    public AddressInformation() {
    }

    public void setShippingMethodCode(String shippingMethodCode) {
        this.shippingMethodCode = shippingMethodCode;
    }

    public String getShippingMethodCode() {
        return shippingMethodCode;
    }

    public void setShippingCarrierCode(String shippingCarrierCode) {
        this.shippingCarrierCode = shippingCarrierCode;
    }

    public String getShippingCarrierCode() {
        return shippingCarrierCode;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setShippingAddress(BillingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public BillingAddress getShippingAddress() {
        return shippingAddress;
    }

    @Override
    public String toString() {
        return
                "AddressInformation{" +
                        "shipping_method_code = '" + shippingMethodCode + '\'' +
                        ",shipping_carrier_code = '" + shippingCarrierCode + '\'' +
                        ",billing_address = '" + billingAddress + '\'' +
                        ",shipping_address = '" + shippingAddress + '\'' +
                        "}";
    }
}