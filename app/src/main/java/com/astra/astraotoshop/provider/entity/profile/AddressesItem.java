package com.astra.astraotoshop.provider.entity.profile;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class AddressesItem{

	@SerializedName("firstname")
	private String firstname;

	@SerializedName("city")
	private String city;

	@SerializedName("region_id")
	private int regionId;

	@SerializedName("postcode")
	private String postcode;

	@SerializedName("telephone")
	private String telephone;

	@SerializedName("lastname")
	private String lastname;

	@SerializedName("custom_attributes")
	private List<CustomAttributesItem> customAttributes;

	@SerializedName("street")
	private List<String> street;

	@SerializedName("company")
	private String company;

	@SerializedName("id")
	private int id;

	@SerializedName("customer_id")
	private int customerId;

	@SerializedName("region")
	private Region region;

	@SerializedName("country_id")
	private String countryId;

	public void setFirstname(String firstname){
		this.firstname = firstname;
	}

	public String getFirstname(){
		return firstname;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setRegionId(int regionId){
		this.regionId = regionId;
	}

	public int getRegionId(){
		return regionId;
	}

	public void setPostcode(String postcode){
		this.postcode = postcode;
	}

	public String getPostcode(){
		return postcode;
	}

	public void setTelephone(String telephone){
		this.telephone = telephone;
	}

	public String getTelephone(){
		return telephone;
	}

	public void setLastname(String lastname){
		this.lastname = lastname;
	}

	public String getLastname(){
		return lastname;
	}

	public void setCustomAttributes(List<CustomAttributesItem> customAttributes){
		this.customAttributes = customAttributes;
	}

	public List<CustomAttributesItem> getCustomAttributes(){
		return customAttributes;
	}

	public void setStreet(List<String> street){
		this.street = street;
	}

	public List<String> getStreet(){
		return street;
	}

	public void setCompany(String company){
		this.company = company;
	}

	public String getCompany(){
		return company;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setCustomerId(int customerId){
		this.customerId = customerId;
	}

	public int getCustomerId(){
		return customerId;
	}

	public void setRegion(Region region){
		this.region = region;
	}

	public Region getRegion(){
		return region;
	}

	public void setCountryId(String countryId){
		this.countryId = countryId;
	}

	public String getCountryId(){
		return countryId;
	}

	@Override
 	public String toString(){
		return 
			"AddressesItem{" + 
			"firstname = '" + firstname + '\'' + 
			",city = '" + city + '\'' + 
			",region_id = '" + regionId + '\'' + 
			",postcode = '" + postcode + '\'' + 
			",telephone = '" + telephone + '\'' + 
			",lastname = '" + lastname + '\'' + 
			",custom_attributes = '" + customAttributes + '\'' + 
			",street = '" + street + '\'' + 
			",company = '" + company + '\'' + 
			",id = '" + id + '\'' + 
			",customer_id = '" + customerId + '\'' + 
			",region = '" + region + '\'' + 
			",country_id = '" + countryId + '\'' + 
			"}";
		}
}