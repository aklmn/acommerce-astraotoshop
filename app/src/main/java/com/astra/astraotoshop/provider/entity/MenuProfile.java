package com.astra.astraotoshop.provider.entity;

import android.graphics.drawable.Drawable;

/**
 * Created by Henra Setia Nugraha on 11/2/2017.
 */

public class MenuProfile {
    private String title;
    private Drawable image;

    public MenuProfile() {
    }

    public MenuProfile(String title, Drawable image) {
        this.title = title;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }
}
