package com.astra.astraotoshop.provider.model;

import com.google.gson.annotations.SerializedName;

public class ProductsItem{

	@SerializedName("image")
	private String image;

	@SerializedName("sold")
	private String sold;

	@SerializedName("serial")
	private String serial;

	@SerializedName("review")
	private String review;

	@SerializedName("qty")
	private String qty;

	@SerializedName("name")
	private String name;

	@SerializedName("vendorId")
	private String vendorId;

	@SerializedName("storeName")
	private String storeName;

	@SerializedName("id")
	private String id;

	@SerializedName("sku")
	private String sku;

	@SerializedName("category")
	private String category;

	@SerializedName("brand")
	private String brand;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setSold(String sold){
		this.sold = sold;
	}

	public String getSold(){
		return sold;
	}

	public void setSerial(String serial){
		this.serial = serial;
	}

	public String getSerial(){
		return serial;
	}

	public void setReview(String review){
		this.review = review;
	}

	public String getReview(){
		return review;
	}

	public void setQty(String qty){
		this.qty = qty;
	}

	public String getQty(){
		return qty;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setVendorId(String vendorId){
		this.vendorId = vendorId;
	}

	public String getVendorId(){
		return vendorId;
	}

	public void setStoreName(String storeName){
		this.storeName = storeName;
	}

	public String getStoreName(){
		return storeName;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setSku(String sku){
		this.sku = sku;
	}

	public String getSku(){
		return sku;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public String getCategory(){
		return category;
	}

	public void setBrand(String brand){
		this.brand = brand;
	}

	public String getBrand(){
		return brand;
	}

	@Override
 	public String toString(){
		return 
			"ProductsItem{" + 
			"image = '" + image + '\'' + 
			",sold = '" + sold + '\'' + 
			",serial = '" + serial + '\'' + 
			",review = '" + review + '\'' + 
			",qty = '" + qty + '\'' + 
			",name = '" + name + '\'' + 
			",vendorId = '" + vendorId + '\'' + 
			",storeName = '" + storeName + '\'' + 
			",id = '" + id + '\'' + 
			",sku = '" + sku + '\'' + 
			",category = '" + category + '\'' + 
			",brand = '" + brand + '\'' + 
			"}";
		}
}