package com.astra.astraotoshop.provider.entity.profile;

import com.google.gson.annotations.SerializedName;

public class Region{

	@SerializedName("region_id")
	private int regionId;

	@SerializedName("region")
	private String region;

	@SerializedName("region_code")
	private String regionCode;

	public void setRegionId(int regionId){
		this.regionId = regionId;
	}

	public int getRegionId(){
		return regionId;
	}

	public void setRegion(String region){
		this.region = region;
	}

	public String getRegion(){
		return region;
	}

	public void setRegionCode(String regionCode){
		this.regionCode = regionCode;
	}

	public String getRegionCode(){
		return regionCode;
	}

	@Override
 	public String toString(){
		return 
			"Region{" + 
			"region_id = '" + regionId + '\'' + 
			",region = '" + region + '\'' + 
			",region_code = '" + regionCode + '\'' + 
			"}";
		}
}