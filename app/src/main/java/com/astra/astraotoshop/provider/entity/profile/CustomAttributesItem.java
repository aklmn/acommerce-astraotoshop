package com.astra.astraotoshop.provider.entity.profile;

import com.google.gson.annotations.SerializedName;

public class CustomAttributesItem{

	@SerializedName("value")
	private String value;

	@SerializedName("attribute_code")
	private String attributeCode;

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	public void setAttributeCode(String attributeCode){
		this.attributeCode = attributeCode;
	}

	public String getAttributeCode(){
		return attributeCode;
	}

	@Override
 	public String toString(){
		return 
			"CustomAttributesItem{" + 
			"value = '" + value + '\'' + 
			",attribute_code = '" + attributeCode + '\'' + 
			"}";
		}
}