package com.astra.astraotoshop.provider.entity.profile;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileResponse {

    @SerializedName("store_id")
    private int storeId;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("addresses")
    private List<AddressesItem> addresses;

    @SerializedName("default_shipping")
    private String defaultShipping;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("custom_attributes")
    private List<CustomAttributesItem> customAttributes;

    @SerializedName("disable_auto_group_change")
    private int disableAutoGroupChange;

    @SerializedName("group_id")
    private int groupId;

    @SerializedName("id")
    private int id;

    @SerializedName("default_billing")
    private String defaultBilling;

    @SerializedName("website_id")
    private int websiteId;

    @SerializedName("email")
    private String email;

    @SerializedName("created_in")
    private String createdIn;

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setAddresses(List<AddressesItem> addresses) {
        this.addresses = addresses;
    }

    public List<AddressesItem> getAddresses() {
        return addresses;
    }

    public void setDefaultShipping(String defaultShipping) {
        this.defaultShipping = defaultShipping;
    }

    public String getDefaultShipping() {
        return defaultShipping;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setCustomAttributes(List<CustomAttributesItem> customAttributes) {
        this.customAttributes = customAttributes;
    }

    public List<CustomAttributesItem> getCustomAttributes() {
        return customAttributes;
    }

    public void setDisableAutoGroupChange(int disableAutoGroupChange) {
        this.disableAutoGroupChange = disableAutoGroupChange;
    }

    public int getDisableAutoGroupChange() {
        return disableAutoGroupChange;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setDefaultBilling(String defaultBilling) {
        this.defaultBilling = defaultBilling;
    }

    public String getDefaultBilling() {
        return defaultBilling;
    }

    public void setWebsiteId(int websiteId) {
        this.websiteId = websiteId;
    }

    public int getWebsiteId() {
        return websiteId;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setCreatedIn(String createdIn) {
        this.createdIn = createdIn;
    }

    public String getCreatedIn() {
        return createdIn;
    }

    @Override
    public String toString() {
        return
                "ProfileResponse{" +
                        "store_id = '" + storeId + '\'' +
                        ",firstname = '" + firstname + '\'' +
                        ",addresses = '" + addresses + '\'' +
                        ",default_shipping = '" + defaultShipping + '\'' +
                        ",lastname = '" + lastname + '\'' +
                        ",custom_attributes = '" + customAttributes + '\'' +
                        ",disable_auto_group_change = '" + disableAutoGroupChange + '\'' +
                        ",group_id = '" + groupId + '\'' +
                        ",id = '" + id + '\'' +
                        ",default_billing = '" + defaultBilling + '\'' +
                        ",website_id = '" + websiteId + '\'' +
                        ",email = '" + email + '\'' +
                        ",created_in = '" + createdIn + '\'' +
                        "}";
    }
}