package com.astra.astraotoshop.provider.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response{

	@SerializedName("products")
	private List<ProductsItem> products;

	public void setProducts(List<ProductsItem> products){
		this.products = products;
	}

	public List<ProductsItem> getProducts(){
		return products;
	}
}