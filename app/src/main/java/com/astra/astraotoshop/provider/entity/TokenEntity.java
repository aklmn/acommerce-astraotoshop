package com.astra.astraotoshop.provider.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Henra Setia Nugraha on 1/4/2018.
 */

public class TokenEntity {
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;

    public TokenEntity(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
