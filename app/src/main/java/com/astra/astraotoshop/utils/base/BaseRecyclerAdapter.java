package com.astra.astraotoshop.utils.base;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;

/**
 * Created by Henra Setia Nugraha on 12/18/2017.
 */

public abstract class BaseRecyclerAdapter<T extends ViewHolder> extends RecyclerView.Adapter<T> {

    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        return viewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(T holder, int position) {
        bind(holder, position);
        if (position == 0)
            onStartPosition(holder, position);

        if (position == getItemCount() - 1)
            onEndPosition(holder, position);
    }

    @Override
    public int getItemCount() {
        return itemCount();
    }

    protected abstract T viewHolder(ViewGroup parent, int viewType);

    protected abstract void bind(T holder, int position);

    protected abstract int itemCount();

    protected void onStartPosition(T holder, int position) {
    }

    protected void onEndPosition(T holder, int position) {
    }
}
