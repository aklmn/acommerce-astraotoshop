package com.astra.astraotoshop.utils.network.handler;

import retrofit2.Response;

/**
 * Created by Henra Setia Nugraha on 10/25/2017.
 * network pattern by : https://github.com/mexanjuadha/CodelabsEvent
 * do not delete this comment
 */

public interface NetworkHandlerContract {
    void onNoInternetConnection();
    void onNetworkProblem();
    void onFailedProcessRequest(Response response);
}
