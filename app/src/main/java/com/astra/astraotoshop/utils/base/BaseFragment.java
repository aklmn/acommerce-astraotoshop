package com.astra.astraotoshop.utils.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.astra.astraotoshop.BuildConfig;
import com.astra.astraotoshop.R;
import com.astra.astraotoshop.utils.pref.Pref;

/**
 * Created by Henra Setia Nugraha on 12/20/2017.
 */

public class BaseFragment extends Fragment {
    protected static final String STATE = "state";
    private int state=-11;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        state = getState();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) state = savedInstanceState.getInt(STATE);
        super.onViewStateRestored(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE,state);
        super.onSaveInstanceState(outState);
    }

    protected boolean isLogin() {
        return Pref.getPreference().getString(BuildConfig.UID) != null;
    }

    protected int getState() {
        return state;
    }

    protected String getStoreCode(){
        return getResources().getStringArray(R.array.storeCode)[getState()];
    }

    public void setState(int state) {
        this.state = state;
    }
}
