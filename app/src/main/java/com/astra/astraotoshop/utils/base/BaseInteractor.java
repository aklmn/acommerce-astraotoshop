package com.astra.astraotoshop.utils.base;

import android.content.Context;

import com.astra.astraotoshop.utils.network.GeneralNetworkManager;
import com.astra.astraotoshop.utils.network.service.CategoryService;

/**
 * Created by Henra Setia Nugraha on 11/3/2017.
 */

public class BaseInteractor {
    private GeneralNetworkManager generalNetworkManager;
    private CategoryService categoryService;

    public BaseInteractor(Context context) {
        generalNetworkManager = new CategoryService();
    }

    public GeneralNetworkManager getGeneralNetworkManager() {
        return generalNetworkManager;
    }
}
