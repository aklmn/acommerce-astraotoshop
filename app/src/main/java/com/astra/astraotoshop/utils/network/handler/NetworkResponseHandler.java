package com.astra.astraotoshop.utils.network.handler;

import retrofit2.Response;

/**
 * Created by Henra Setia Nugraha on 10/25/2017.
 * network pattern by : https://github.com/mexanjuadha/CodelabsEvent
 * do not delete this comment
 */

public abstract class NetworkResponseHandler extends NetworkHandler {

    @Override
    public void onFailedProcessRequest(Response response) {
        super.onFailedProcessRequest(response);
        consumeStatusCode(response.code());
    }

    protected abstract void consumeStatusCode(int statusCode);
}
