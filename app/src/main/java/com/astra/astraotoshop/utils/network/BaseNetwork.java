package com.astra.astraotoshop.utils.network;

import android.content.Context;

import com.astra.astraotoshop.utils.network.handler.NetworkHandlerContract;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.net.UnknownHostException;

import retrofit2.HttpException;
import rx.Observable;
import rx.functions.Action1;

/**
 * Created by Henra Setia Nugraha on 10/25/2017.
 * network pattern by : https://github.com/mexanjuadha/CodelabsEvent
 * do not delete this comment
 */

public abstract class BaseNetwork<T> {

    private T services;
//    private Context context;

    public BaseNetwork() {
//        this.context = context;

        Gson gson = gsonHandler(new GsonBuilder().setPrettyPrinting()).create();
        services = NetworkFactory.createRestAdapter(gson, getBaseUrl(), getRestClass());
    }

    protected GsonBuilder gsonHandler(GsonBuilder builder) {
        return builder;
    }

    protected <T> Observable<T> addInterceptor(Observable<T> observable, NetworkHandlerContract networkHandlerContract) {
        return observable.doOnError(throwable -> onErrorNetwork(throwable, networkHandlerContract));
    }

    private void onErrorNetwork(Throwable throwable, NetworkHandlerContract networkHandlerContract) {
        if (throwable instanceof UnknownHostException)
            networkHandlerContract.onNoInternetConnection();
        else if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            networkHandlerContract.onFailedProcessRequest(httpException.response());
        }
    }

    public T getServices() {
        return services;
    }

//    public Context getContext() {
//        return context;
//    }

    protected abstract String getBaseUrl();

    protected abstract Class<T> getRestClass();
}
