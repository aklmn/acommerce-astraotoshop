package com.astra.astraotoshop.utils.view;

import android.app.Activity;
import android.support.design.widget.BottomSheetBehavior;
import android.text.method.KeyListener;
import android.view.View;
import android.view.ViewTreeObserver;

import java.lang.ref.WeakReference;

public class KeyboardEventListener {

    private static int currentOrientation = -1;
    private static int mAppHeight;
    private static ViewTreeObserver.OnGlobalLayoutListener observer;

    public static void setKeyboardVisibilityListener(Activity ac, KeyboardListener listener) {

        final View contentView = ac.findViewById(android.R.id.content);
        observer = new ViewTreeObserver.OnGlobalLayoutListener() {

            private int mPreviousHeight;

            @Override
            public void onGlobalLayout() {
                int newHeight = contentView.getHeight();
                if (newHeight == mPreviousHeight) {
                    return;
                }
                mPreviousHeight = newHeight;
                if (ac.getResources().getConfiguration().orientation != currentOrientation) {
                    currentOrientation = ac.getResources().getConfiguration().orientation;
                    mAppHeight = 0;
                }
                if (newHeight >= mAppHeight) {
                    mAppHeight = newHeight;
                }
                if (newHeight != 0) {
                    if (mAppHeight > newHeight)
                        listener.onKeyboardStatusChange(true);
                    else
                        listener.onKeyboardStatusChange(false);
                }
            }
        };
        contentView.getViewTreeObserver().addOnGlobalLayoutListener(observer);
    }

    public static void unSubsribe(Activity ac) {
        final View contentView = ac.findViewById(android.R.id.content);
        try {
            contentView.getViewTreeObserver().removeOnGlobalLayoutListener(observer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public interface KeyboardListener {
        void onKeyboardStatusChange(boolean isShown);
    }
}
