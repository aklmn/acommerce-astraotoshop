package com.astra.astraotoshop.utils.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.order.cart.CartFragment;
import com.astra.astraotoshop.order.cart.provider.CartContract;
import com.astra.astraotoshop.order.cart.provider.CartListItem;
import com.astra.astraotoshop.order.cart.provider.CartPresenter;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/25/2018.
 */

public class BaseActionActivity extends BaseActivity implements CartContract.CartView {

    protected CartContract.CartPresenter cartPresenter;
    private boolean isCartBadgeVisible = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cartPresenter = new CartPresenter(this, null, getStoreCode(), getIntent().getBooleanExtra(getString(R.string.intent_edit_appointment), false));
    }

    @Override
    protected void onStart() {
        super.onStart();
        cartPresenter.requestCartList();
    }

    protected void openCart() {
        if (isLogin()) {
            CartFragment.newInstance(getState()).show(getSupportFragmentManager(), "Cart");
        } else {
            showAccount();
        }
    }

    @Override
    public void setCartList(List<CartListItem> items) {
        if (items.size() > 0) {
            isCartBadgeVisible = true;
            setCartBadgeVisibility(isCartBadgeVisible);
        } else {
            isCartBadgeVisible = false;
            setCartBadgeVisibility(isCartBadgeVisible);
        }
        onCartLoaded();
    }

    @Override
    public void onCartItemDeleted() {

    }

    @Override
    public void onCartUpdated() {

    }

    @Override
    public void showError() {

    }

    protected void onCartLoaded() {

    }
}
