package com.astra.astraotoshop.utils.network.handler;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by Henra Setia Nugraha on 1/2/2018.
 */

public class ClientSSL {
    public static OkHttpClient getClientWithSSL() {
        return ClientSSL.getClientWithSSL(0);
    }

    public static OkHttpClient getClientWithSSL(int second) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (second > 0) {
            builder.readTimeout(second, TimeUnit.SECONDS)
                    .connectTimeout(second, TimeUnit.SECONDS);
        }

        injectSSLSocketFactory(builder);
        builder.hostnameVerifier((hostname, session) -> true);

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        builder.addInterceptor(logging);
        return builder.build();
    }

    public static SSLSocketFactory injectSSLSocketFactory(OkHttpClient.Builder okHttpClient) {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            if (okHttpClient != null) {
                okHttpClient.sslSocketFactory(sslSocketFactory,(X509TrustManager)trustAllCerts[0]);
                okHttpClient.hostnameVerifier((hostname, session) -> true);
            }
            return sslSocketFactory;
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            return null;
        }

    }
}
