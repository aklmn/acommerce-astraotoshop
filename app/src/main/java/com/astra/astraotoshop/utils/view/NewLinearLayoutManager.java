package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Henra Setia Nugraha on 1/29/2018.
 */

public class NewLinearLayoutManager extends LinearLayoutManager {

    int maxHeight=0;

    public NewLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
    }

    @Override
    public void onMeasure(RecyclerView.Recycler recycler, RecyclerView.State state, int widthSpec, int heightSpec) {
        if (getItemCount()>0) {
            if (getHeight()>maxHeight) {
                maxHeight=getHeight();

            }
        }
    }
}
