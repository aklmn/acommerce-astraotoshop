package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Henra Setia Nugraha on 11/29/2017.
 */

public class FontIcon extends TextView {
    public FontIcon(Context context) {
        super(context);
    }

    public FontIcon(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public FontIcon(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        try {
            Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/fontawesome.ttf");
            setTypeface(typeface);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
