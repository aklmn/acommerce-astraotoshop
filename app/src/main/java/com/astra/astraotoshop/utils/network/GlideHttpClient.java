package com.astra.astraotoshop.utils.network;

import android.content.Context;

import com.astra.astraotoshop.utils.network.handler.ClientSSL;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.GlideModule;

import java.io.InputStream;

import okhttp3.OkHttpClient;

/**
 * Created by Henra Setia Nugraha on 1/9/2018.
 */

public class GlideHttpClient implements GlideModule {
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {

    }

    @Override
    public void registerComponents(Context context, Glide glide) {

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        ClientSSL.injectSSLSocketFactory(builder);
        glide.register(GlideUrl.class, InputStream.class,
                new OkHttpUrlLoader.Factory(builder.build()));
    }
}
