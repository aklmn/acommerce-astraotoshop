package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.astra.astraotoshop.R;

/**
 * Created by Henra Setia Nugraha on 1/10/2018.
 */

public class ImageRatio extends AppCompatImageView {

    private float heightRatio = 1f;

    public ImageRatio(Context context) {
        super(context);
    }

    public ImageRatio(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.imageRatio, 0, 0);
        float value = typedArray.getFloat(R.styleable.imageRatio_height_ratio, 1f);
        if (value > 0)
            heightRatio = value;

        typedArray.recycle();
    }

    public ImageRatio(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int height = (int) (getMeasuredWidth()*9/21);
        setMeasuredDimension(getMeasuredWidth(), height);
    }
}
