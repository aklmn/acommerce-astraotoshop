package com.astra.astraotoshop.utils.base;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henra Setia Nugraha on 1/9/2018.
 */

public class BasePresenter {

    private int totalItem = 0;
    private int currentTotal = 0;
    private int page = 1;
    private Map<String, String> queries = new HashMap<>();
    private boolean addMore = false;

    protected BasePresenter() {
        getDefaultFilters();
    }

    protected BasePresenter(Map<String, String> defaultQueries) {
        queries = defaultQueries;
    }

    public int getTotalItem() {
        return totalItem;
    }

    public int getCurrentTotal() {
        return currentTotal;
    }

    protected void addCurrentTotal(int currentTotal) {
        this.currentTotal += currentTotal;
    }

    protected void setTotalItem(int totalItem) {
        this.totalItem = totalItem;
    }

    protected boolean isAvailableToLoad() {
        if (totalItem == 0 && page == 1) {
            if (queries.size() == 0)
                getDefaultFilters();
            return true;
        }
//        return currentTotal <= totalItem;
        return true;
    }

    public void setQueries(Map<String, String> queries) {
        this.queries = queries;
        addMore = false;
    }

    protected Map<String, String> getDefaultFilters() {
        queries = new HashMap<>();
        queries.put("order", "position");
        queries.put("dir", "desc");
        queries.put("limit", "10");
        queries.put("page", "1");
        page = 1;
        setAddMore(false);
        return queries;
    }

    protected void updateFilter(String key, String value) {
        queries.put(key, value);
        resetPage();
    }

    protected void deleteFilterItem(String key) {
        queries.remove(key);
        resetPage();
    }

    private void resetPage() {
        setAddMore(false);
        page = 1;
        totalItem = 0;
        currentTotal = 0;
        if (queries.containsKey("page")) queries.put("page", "1");
        else queries.put("searchCriteria[currentPage]", "1");
    }

    protected Map<String, String> getQueries() {
        return queries;
    }

    protected void increasePage() {
        if (queries.containsKey("page")) queries.put("page", String.valueOf(++page));
        if (queries.containsKey("searchCriteria[currentPage]"))
            queries.put("searchCriteria[currentPage]", String.valueOf(++page));
        setAddMore(true);
    }

    public boolean isAddMore() {
        return addMore;
    }

    public void setAddMore(boolean addMore) {
        this.addMore = addMore;
    }

    public static Map<String, String> getDefaultFilter() {
        return new BasePresenter().getDefaultFilters();
    }
}
