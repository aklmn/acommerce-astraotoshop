package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

/**
 * Created by Henra Setia Nugraha on 2/15/2018.
 */

public class AlertManager {

    public static void showSimpleMessage(Context context, String message, @Nullable DialogInterface.OnDismissListener dismissListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", (dialog, which) -> {
                    dialog.dismiss();
                });
        AlertDialog alertDialog = builder.create();
        if (dismissListener != null)
            alertDialog.setOnDismissListener(dismissListener);
        alertDialog.show();
    }
}
