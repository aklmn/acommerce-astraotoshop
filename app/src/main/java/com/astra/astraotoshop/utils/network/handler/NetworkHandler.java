package com.astra.astraotoshop.utils.network.handler;

import com.astra.astraotoshop.Application.AOPApplication;
import com.astra.astraotoshop.utils.pref.Pref;
import com.astra.astraotoshop.utils.view.Loading;

import retrofit2.Response;

/**
 * Created by Henra Setia Nugraha on 10/25/2017.
 * network pattern by : https://github.com/mexanjuadha/CodelabsEvent
 * do not delete this comment
 */

public class NetworkHandler implements NetworkHandlerContract {


    public NetworkHandler() {
    }

    @Override
    public void onNoInternetConnection() {
        System.out.println();
    }

    @Override
    public void onNetworkProblem() {
        System.out.println();
    }

    @Override
    public void onFailedProcessRequest(Response response) {
        if (response.code() == 401) {
            AOPApplication.forceLogout(Pref.getPreference().getString("currentStore"));
            AOPApplication.start();
        }
    }
}
