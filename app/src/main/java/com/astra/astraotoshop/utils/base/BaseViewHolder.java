package com.astra.astraotoshop.utils.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Henra Setia Nugraha on 12/18/2017.
 */

public class BaseViewHolder extends RecyclerView.ViewHolder {
    public BaseViewHolder(View itemView) {
        super(itemView);
    }
}
