package com.astra.astraotoshop.utils.network;

import com.astra.astraotoshop.home.provider.StaticBlock;
import com.astra.astraotoshop.home.provider.StaticCategory;
import com.astra.astraotoshop.order.appointment.provider.AppointmentEntity;
import com.astra.astraotoshop.order.cart.provider.AddCartBody;
import com.astra.astraotoshop.order.cart.provider.CartListEntity;
import com.astra.astraotoshop.order.cart.provider.CartQuoteEntity;
import com.astra.astraotoshop.order.cart.provider.UpdateCartBody;
import com.astra.astraotoshop.order.cart.provider.totals.CartTotalEntity;
import com.astra.astraotoshop.order.checkout.provider.AddressBody;
import com.astra.astraotoshop.order.checkout.provider.ShippingMethodEntity;
import com.astra.astraotoshop.order.checkout.provider.VaBody;
import com.astra.astraotoshop.order.checkout.provider.address.AddressShippingBody;
import com.astra.astraotoshop.order.checkout.provider.installment.PaymentDesc;
import com.astra.astraotoshop.order.checkout.provider.payment.AvailablePaymentMethod;
import com.astra.astraotoshop.order.checkout.provider.payment.PaymentMethodBody;
import com.astra.astraotoshop.order.history.provider.OrderHistory;
import com.astra.astraotoshop.product.catalog.provider.ProductListEntity;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.product.filter.provider.BrandFilterEntity;
import com.astra.astraotoshop.product.product.provider.ProductEntity;
import com.astra.astraotoshop.product.review.provider.ReviewEntity;
import com.astra.astraotoshop.product.search.provider.Suggestion;
import com.astra.astraotoshop.provider.entity.Response;
import com.astra.astraotoshop.provider.entity.TokenEntity;
import com.astra.astraotoshop.user.address.provider.AddAddressBody;
import com.astra.astraotoshop.user.address.provider.CityEntity;
import com.astra.astraotoshop.user.address.provider.RegionEntity;
import com.astra.astraotoshop.user.order.provider.OrderDetail;
import com.astra.astraotoshop.user.password.provider.ChangePassword;
import com.astra.astraotoshop.user.password.provider.IsEmailAvailable;
import com.astra.astraotoshop.user.password.provider.ResetPassword;
import com.astra.astraotoshop.user.profile.provider.ProfileEntity;
import com.astra.astraotoshop.user.profile.provider.UpdateProfile;
import com.astra.astraotoshop.user.review.provider.CustomerReview;
import com.astra.astraotoshop.user.signin.provider.SocialLogin;
import com.astra.astraotoshop.user.signup.provider.Register;
import com.astra.astraotoshop.user.signup.provider.RegisterBody;
import com.astra.astraotoshop.user.subsricption.provider.Subscription;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherDetail;
import com.astra.astraotoshop.user.voucher.provider.model.VoucherList;
import com.astra.astraotoshop.user.wishlist.provider.AddWishlistBody;
import com.astra.astraotoshop.user.wishlist.provider.WishlistEntity;

import java.util.List;
import java.util.Map;

import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by Henra Setia Nugraha on 10/26/2017.
 */

public interface ApiServices {

    @GET("productList")
    Observable<Response> getData();

    @Headers("Content-Type:application/json")
    @POST("V1/integration/{role}/token")
    Observable<String> getToken(
            @Path("role") String role,
            @Body TokenEntity tokenEntity);

    @Headers("Content-Type:application/json")
    @GET("V1/customers/me")
    Observable<ProfileEntity> getProfile(
            @Header("Authorization") String auth);

    @Headers("Content-Type:application/json")
    @PUT("V1/customers/me")
    Observable<ProfileEntity> updateProfile(
            @Header("Authorization") String auth,
            @Body UpdateProfile profile);

    @Headers("Content-Type:application/json")
    @GET("V1/acommerce-customapi/facebooklogin")
    Observable<Object> loginFB(
            @Header("Authorization") String auth,
            @Query("token") String fbToken,
            @Query("firstname") String firstName,
            @Query("lastname") String lastName,
            @Query("email") String email);

    @Headers("Content-Type:application/json")
    @GET("V1/acommerce-customapi/googlelogin")
    Observable<Object> loginG(
            @Header("Authorization") String auth,
            @Query("token") String gToken,
            @Query("firstname") String firstName,
            @Query("lastname") String lastName,
            @Query("email") String email);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/customer/product/review/")
    Observable<CustomerReview> getCustomerReview(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/appointment/voucher/list")
    Observable<VoucherList> getVoucherList(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/appointment/voucher/detail/{voucherId}")
    Observable<VoucherDetail> getVoucher(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("voucherId") String voucherId);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/acomapi/customer/newsletter/{state}")
    Observable<String> changeSubscription(
            @Header("Authorization") String auth,
            @Path("state") String state,
            @Path("storeCode") String storeCode);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/customer/newsletter")
    Observable<Subscription> getSubscription(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode);

    @Headers("Content-Type:application/json")
    @PUT("V1/customers/me/password")
    Observable<Object> changePassword(
            @Header("Authorization") String auth,
            @Body ChangePassword changePassword);

    @Headers("Content-Type:application/json")
    @PUT("V1/customers/password")
    Observable<Object> resetPassword(
            @Header("Authorization") String auth,
            @Body ResetPassword resetPassword);

    @Headers("Content-Type:application/json")
    @POST("V1/customers/isEmailAvailable")
    Observable<Boolean> isEmailAvailable(
            @Header("Authorization") String auth,
            @Body IsEmailAvailable emailAvailable);

    @Headers("Content-Type:application/json")
    @GET("V2/acomapi/categories/")
    Observable<CategoryEntity> getCategory(
            @Header("Authorization") String auth);

    @Headers("Content-Type:application/json")
    @GET("V2/acomapi/productbycategory/product/{catId}")
    Observable<ProductListEntity> getProductList(
            @Header("Authorization") String auth,
            @Path("catId") String catId,
            @QueryMap Map<String, String> filters);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V2/acomapi/search/result")
    Observable<ProductListEntity> getProductSearch(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Query("q") String querry,
            @QueryMap Map<String, String> filters);

    @Headers("Content-Type:application/json")
    @GET("V1/acomapi/product/{storeCode}/{id}")
    Observable<ProductEntity> getProduct(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCOde,
            @Path("id") String id);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/search/suggest")
    Observable<Suggestion> getSuggest(
            @Path("storeCode") String storeCOde,
            @Query("q") String query);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/products/bestselling/{storeCode}")
    Observable<ProductListEntity> getBestSelling(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCOde);

    @Headers("Content-Type:application/json")
    @GET("V1/acomapi/products/newest/{storeCode}")
    Observable<ProductListEntity> getNewestProduct(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCOde);

    @Headers("Content-Type:application/json")
    @GET("V1/acomapi/products/promo/{storeCode}")
    Observable<ProductListEntity> getPromoProduct(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCOde);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/cms/block/{identifier}")
    Observable<StaticBlock> getStaticBlock(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("identifier") String identifier);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/cms/block/{identifier}")
    Observable<StaticCategory> getStaticCategory(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("identifier") String identifier);

    @Headers("Content-Type:application/json")
    @GET("V1/products/attributes/brand/options")
    Observable<List<BrandFilterEntity>> getBrandFilter(
            @Header("Authorization") String auth);

    @Headers("Content-Type:application/json")
    @GET("V1/acomapi/productreview/product/{productId}")
    Observable<ReviewEntity> getProductReview(
            @Header("Authorization") String auth,
            @Path("productId") String productId,
            @QueryMap Map<String, String> filters);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/carts/mine")
    Observable<String> getQuoteId(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Body CartQuoteEntity userId);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/carts/mine/items")
    Observable<Object> addCart(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Body AddCartBody cart);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/carts/mine")
    Observable<CartListEntity> getCartList(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode);

    @Headers("Content-Type:application/json")
    @PUT("{storeCode}/V1/carts/{quoteId}/items/{itemId}")
    Observable<Object> updateCart(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("itemId") int itemId,
            @Path("quoteId") int quoteId,
            @Body UpdateCartBody cart);

    @Headers("Content-Type:application/json")
    @DELETE("{storeCode}/V1/carts/mine/items/{itemId}")
    Observable<Object> deleteItemCart(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("itemId") String itemId);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/carts/mine/totals")
    Observable<CartTotalEntity> getTotalCart(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode);

    @Headers("Content-Type:application/json")
    @PUT("{storeCode}/V1/carts/mine/coupons/{voucherCode}")
    Observable<Boolean> updateVoucher(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("voucherCode") String voucherCode);

    @Headers("Content-Type:application/json")
    @DELETE("{storeCode}/V1/carts/mine/coupons")
    Observable<Boolean> deleteVoucher(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/carts/mine/estimate-shipping-methods-by-address-id")
    Observable<List<ShippingMethodEntity>> getShippingMethod(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Body AddressBody addressBody);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/carts/mine/shipping-information")
    Observable<AvailablePaymentMethod> saveShippingMethod(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Body AddressShippingBody addressShippingBody);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/carts/mine/payment-information")
    Observable<String> savePaymentMethod(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Body PaymentMethodBody paymentMethodBody);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/payment/description")
    Observable<PaymentDesc> getPaymentDesc(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/acomapi/payment/va")
    Observable<Object> saveVa(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Body VaBody vaBody);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/customers")
    Observable<Register> register(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Body RegisterBody registerBody);

    @Headers("Content-Type:application/json")
    @GET("V1/orders")
    Observable<OrderHistory> getOrderList(
            @Header("Authorization") String auth,
            @QueryMap Map<String,String> queries);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/customer/orders/{orderId}")
    Observable<OrderDetail> getOrderDetail(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("orderId") String orderId);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/customer/wishlist")
    Observable<WishlistEntity> getWishlist(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/acomapi/customer/wishlist")
    Observable<Object> addWishlist(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Body AddWishlistBody wishlistBody);

    @Headers("Content-Type:application/json")
    @DELETE("{storeCode}/V1/acomapi/customer/wishlist/{itemId}")
    Observable<Object> deleteWishlist(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("itemId") String itemId);

    @Headers("Content-Type:application/json")
    @POST("{storeCode}/V1/acomapi/customer/address")
    Observable<Object> saveNewAddress(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Body AddAddressBody addressBody);

    @Headers("Content-Type:application/json")
    @PUT("{storeCode}/V1/acomapi/customer/address/{addressId}")
    Observable<Object> updateAddress(
            @Header("Authorization") String auth,
            @Path("addressId") String addressId,
            @Path("storeCode") String storeCode,
            @Body AddAddressBody addressBody);

    @Headers("Content-Type:application/json")
    @DELETE("{storeCode}/V1/acomapi/customer/address/{addressId}")
    Observable<Object> deleteAddress(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("addressId") String addressId);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/customer/address/region/{countryId}")
    Observable<RegionEntity> getRegion(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("countryId") String countryId);

    @Headers("Content-Type:application/json")
    @GET("{storeCode}/V1/acomapi/customer/address/city/{regionId}")
    Observable<CityEntity> getCity(
            @Header("Authorization") String auth,
            @Path("storeCode") String storeCode,
            @Path("regionId") String regionId);

    @Headers("Content-Type:application/json")
    @GET("productservice/V1/acomapi/appointment/available/{productId}")
    Observable<AppointmentEntity> getAvailable(
            @Path("productId") String productId,
            @QueryMap Map<String, String> query);
}
