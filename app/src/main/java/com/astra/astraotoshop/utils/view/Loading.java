package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.astra.astraotoshop.R;

/**
 * Created by Henra Setia Nugraha on 2/12/2018.
 */

public class Loading {
    private AlertDialog loadingDialog;
    private Context context;
    private LayoutInflater inflater;
    private View loadingView;
    private TextView loadingCaption;
    private boolean isShowing = false;

    public Loading newInstance(@NonNull Context context, @NonNull LayoutInflater layoutInflater) {
        return new Loading(context, layoutInflater);
    }

    public Loading(Context context, LayoutInflater inflater) {
        this.context = context;
        this.inflater = inflater;
        setupView();
    }

    private void setupView() {
        loadingView = inflater.inflate(R.layout.view_loading, null);
        loadingCaption = (TextView) loadingView.findViewById(R.id.text_loading);
    }

    public void show(String message) {
        if (!isShowing) {
            loadingCaption.setText(message == null ? "Harap Tunggu" : message);
            AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.FullScreenDialog).setView(loadingView);
            loadingDialog = builder.create();
            loadingDialog.getWindow().setBackgroundDrawable(new ColorDrawable(context.getResources().getColor(R.color.white_transparent)));
            loadingDialog.setCancelable(false);
            loadingDialog.show();
            isShowing = true;
        }
    }

    public void show(@StringRes int resId) {
        show(context.getString(resId));
    }

    public void dismiss() {
        if (loadingDialog != null) loadingDialog.dismiss();
        loadingView = inflater.inflate(R.layout.view_loading, null);
        isShowing = false;
    }

    public boolean isShowing() {
        return isShowing;
    }

}
