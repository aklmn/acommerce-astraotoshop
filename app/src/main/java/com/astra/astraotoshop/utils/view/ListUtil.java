package com.astra.astraotoshop.utils.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.astra.astraotoshop.provider.entity.MenuProfile;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henra Setia Nugraha on 11/2/2017.
 */

public class ListUtil {
    public static void setRealHeight(ListView listView) {
        ListAdapter mListAdapter = listView.getAdapter();
        if (mListAdapter == null) {
            return;
        }
        int height = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        for (int i = 0; i < mListAdapter.getCount(); i++) {
            View listItem = mListAdapter.getView(i, null, listView);
            listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            height += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = height + (listView.getDividerHeight() * (mListAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static void setRealHeight(RecyclerView.LayoutManager layoutManager, RecyclerView recyclerView) {
        if (recyclerView.getAdapter() == null) {
            return;
        }
        int height = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        for (int i = 0; i < recyclerView.getAdapter().getItemCount(); i++) {
//            height += layoutManager.getChildAt(i).getMeasuredHeight();
            System.out.println();
        }

        ViewGroup.LayoutParams layoutParams = recyclerView.getLayoutParams();
        layoutParams.height = height;
        recyclerView.setLayoutParams(layoutParams);
        recyclerView.requestLayout();
    }

    public static List<MenuProfile> getMenuList(Context context, int menuResource) {
        List<MenuProfile> profileList = new ArrayList<>();
        MenuProfile menuProfile;

        PopupMenu popupMenu = new PopupMenu(context, null);
        Menu menu = popupMenu.getMenu();

        try {
            ((AppCompatActivity) context).getMenuInflater().inflate(menuResource, menu);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < menu.size(); i++) {
            menuProfile = new MenuProfile(menu.getItem(i).getTitle().toString(), menu.getItem(i).getIcon());
            profileList.add(menuProfile);
        }

        return profileList;
    }
}
