package com.astra.astraotoshop.utils.view;

import android.support.annotation.Nullable;
import android.widget.TextView;

import com.astra.astraotoshop.user.profile.provider.Addresses;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Henra Setia Nugraha on 10/26/2017.
 */

public class StringFormat {

    public static String addCurrencyFormat(Object value) {
        String v = String.valueOf(value);
        String price = "Rp. ";
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        try {
            String temp = numberFormat.format(Double.parseDouble(v));
            price += temp.replace(",", ".");
            price = price.replace("-", "");
        } catch (Exception e) {
            e.printStackTrace();
            price += "0";
        }
        return price;
    }

    public static void applyCurrencyFormat(TextView view, Object value) {
        view.setText(addCurrencyFormat(value));
    }

    public static String clearCurrencyFormat(String value) {
        String price = "-1";
        try {
            price = value.replaceAll("\\D", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return price;
    }

    public static void setText(TextView textView, Object value) {
        if (value != null) {
            textView.setText((String) value);
        }
    }

    public static String getAddress(Addresses addresses) {
        String address = "";
        address = addressBuilder(address, addresses.getStreet().get(0));
        try {
            address = addressBuilder(address, addresses.getCustomAttributes().get(1).getValue());
            address = addressBuilder(address, addresses.getCustomAttributes().get(0).getValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        address = addressBuilder(address, addresses.getCity());
        address = addressBuilder(address, addresses.getRegion().getRegion());
        address = addressBuilder(address, addresses.getPostcode());
        try {
            address = addressBuilder(address, addresses.getCustomAttributes().get(2).getValue());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return address;
    }

    public static String addressBuilder(String exist, String value) {
        if (value != null) exist += value + ", ";
        return exist;
    }

    public static int getIntFromString(String value) {
        if (value != null) {
            try {
                return Integer.valueOf(value.replaceAll("\\D", ""));
            } catch (NumberFormatException e) {
                e.printStackTrace();
                return 0;
            }
        }
        return 0;
    }

    public static String getCalendarString(Calendar calendar, @Nullable String format) {
        if (calendar != null) {
            String defFormat = "yyyy-MM-dd";
            Locale locale = new Locale("id", "ID");
            SimpleDateFormat dateFormat = new SimpleDateFormat(
                    format == null ? defFormat : format,
                    locale);
            return dateFormat.format(calendar.getTime());
        }
        return "";
    }

    public static String timeConverter(String dateValue) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        String newDate = dateValue;
        try {
            Calendar calendar = Calendar.getInstance();
            Date date = format.parse(dateValue);
            calendar.setTime(date);
            newDate = StringFormat.getCalendarString(calendar, "dd MMMM yyyy");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return newDate;
    }

    public static Calendar timeConverter(String dateValue, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format, Locale.US);
        Date date;
        try {
            date = dateFormat.parse(dateValue);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
        Calendar calendar = Calendar.getInstance();
        if (date != null)
            calendar.setTime(date);
        return calendar;
    }
}
