package com.astra.astraotoshop.utils.view;

import android.widget.AbsListView;

/**
 * Created by henra.setia on 9/20/2017.
 */

public class InfiniteScroll implements AbsListView.OnScrollListener {

    private int visibleTreshold = 3;
    private int currentPage = 1;
    private int previousTotalItemCount = 0;
    private boolean loading = true;
    private int startingPageIndex = 1;
    private InfiniteScrollListener listener;

    public InfiniteScroll(InfiniteScrollListener scrollListener) {
        listener = scrollListener;
    }

    public InfiniteScroll(int visibleTreshold, InfiniteScrollListener scrollListener) {
        this.visibleTreshold = visibleTreshold;
        listener = scrollListener;
    }

    public InfiniteScroll(int visibleTreshold, int startingPageIndex, InfiniteScrollListener scrollListener) {
        this.visibleTreshold = visibleTreshold;
        this.startingPageIndex = startingPageIndex;
        currentPage = startingPageIndex;
        listener = scrollListener;
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (totalItemCount < previousTotalItemCount) {
            currentPage = startingPageIndex;
            previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) loading = true;
        }

        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
        }

        if (!loading && (firstVisibleItem + visibleItemCount >= totalItemCount) && visibleItemCount < totalItemCount) {
            loading = listener.onLoadMore(++currentPage, totalItemCount);
        }
    }

    public void resetState() {
        currentPage = 1;
        previousTotalItemCount = 0;
        loading=true;
    }

    public interface InfiniteScrollListener {
        boolean onLoadMore(int page, int totalItemCount);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }
}
