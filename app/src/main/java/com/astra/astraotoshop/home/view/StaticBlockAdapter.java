package com.astra.astraotoshop.home.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.home.provider.StaticBlocksItem;
import com.astra.astraotoshop.home.revamp.HomePageActivity;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.ImageRatio;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by Henra Setia Nugraha on 1/10/2018.
 */

public class StaticBlockAdapter extends RecyclerView.Adapter<StaticBlockAdapter.StaticBlockViewHolder> {

    private List<StaticBlocksItem> data;
    private Context context;
    private int imageWidth = 0;
    private ListListener listener;

    public StaticBlockAdapter(Context context, int imageWidth, ListListener listener) {
        this.context = context;
        this.imageWidth = imageWidth;
        this.listener = listener;
        data = new ArrayList<>();

    }

    public StaticBlockAdapter(Context context, ListListener listener) {
        this.context = context;
        this.listener = listener;
        data = new ArrayList<>();
    }

    @Override
    public StaticBlockViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StaticBlockViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(StaticBlockViewHolder holder, int position) {
//        ViewGroup.LayoutParams layoutParams = holder.image.getLayoutParams();
//        layoutParams.width = imageWidth;
//        layoutParams.height = WRAP_CONTENT;
//        holder.image.setLayoutParams(layoutParams);

        Glide.with(context).load(data.get(position).getImgUrl()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(List<StaticBlocksItem> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    class StaticBlockViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageRatio image;

        public StaticBlockViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.image)
        public void imageClick() {
            try {
                listener.onItemClick(R.id.bannerItem, getAdapterPosition(), data.get(getAdapterPosition()));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
