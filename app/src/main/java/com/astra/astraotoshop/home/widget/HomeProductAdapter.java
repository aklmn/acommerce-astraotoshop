package com.astra.astraotoshop.home.widget;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 12/14/2017.
 */

public class HomeProductAdapter extends RecyclerView.Adapter<HomeProductAdapter.HomeProductViewHolder> {

    private List<ProductItemEntity> products;
    private Context context;
    private ListListener listener;

    public HomeProductAdapter(Context context,ListListener listener) {
        this.context = context;
        this.listener=listener;
        products = new ArrayList<>();
    }

    @Override
    public HomeProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HomeProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_best_selling, parent, false));
    }

    @Override
    public void onBindViewHolder(HomeProductViewHolder holder, int position) {
        setMargin(holder.container, position);
        holder.name.setText(products.get(position).getName());
        holder.rate.setRating((float) products.get(position).getRating());
        Glide.with(context).load(products.get(position).getImage()).into(holder.image);

        if (position==1) {
            holder.name.setText(products.get(position).getName());
        }

        if (products.get(position).getSpecialPrice() != null) {
            StringFormat.applyCurrencyFormat(holder.firstPrice, products.get(position).getSpecialPrice());
            StringFormat.applyCurrencyFormat(holder.secondPrice, products.get(position).getPrice());
            holder.discount.setText(discountCalculate(products.get(position).getPrice(), products.get(position).getSpecialPrice()));
            holder.discount.setVisibility(View.VISIBLE);
            holder.secondPrice.setVisibility(View.VISIBLE);
        } else {
            StringFormat.applyCurrencyFormat(holder.firstPrice, products.get(position).getPrice());
            holder.secondPrice.setVisibility(View.GONE);
            holder.discount.setVisibility(View.GONE);
        }
    }

    private void setMargin(ConstraintLayout container, int position) {
        float density = context.getResources().getDisplayMetrics().density;
        if (position == 0) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) container.getLayoutParams();
            params.setMarginStart((int) (16 * density));
            container.setLayoutParams(params);
        } else if (position == getItemCount() - 1) {
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) container.getLayoutParams();
            params.setMarginEnd((int) (16 * density));
            container.setLayoutParams(params);
        }
    }

    private String discountCalculate(String price, String specialPrice) {
        Double dPrice = Double.parseDouble(price);
        Double dSpecialPrice = Double.parseDouble(specialPrice);

        double cutPrice = (dPrice - dSpecialPrice) / dPrice;
        int discount = (int) (cutPrice * 100);
        return String.valueOf(discount + "%");
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public void setProducts(List<ProductItemEntity> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    class HomeProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.rate)
        RatingBar rate;
        @BindView(R.id.secondPrice)
        TextView secondPrice;
        @BindView(R.id.firstPrice)
        TextView firstPrice;
        @BindView(R.id.discount)
        TextView discount;
        @BindView(R.id.container)
        ConstraintLayout container;

        public HomeProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.container)
        public void itemClick(){
            try {
                listener.onItemClick(R.id.bestSellingItem,getAdapterPosition(),products.get(getAdapterPosition()));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
