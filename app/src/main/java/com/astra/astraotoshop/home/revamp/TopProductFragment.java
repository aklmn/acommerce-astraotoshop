package com.astra.astraotoshop.home.revamp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;
import com.astra.astraotoshop.utils.view.CustomFontFace;
import com.astra.astraotoshop.utils.view.ImageSquared;
import com.astra.astraotoshop.utils.view.StringFormat;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TopProductFragment extends Fragment {

    @BindView(R.id.productImage)
    ImageSquared productImage;
    @BindView(R.id.discount)
    CustomFontFace discount;
    @BindView(R.id.discountContainer)
    LinearLayout discountContainer;
    @BindView(R.id.productTitle)
    CustomFontFace productTitle;
    @BindView(R.id.specialPrice)
    CustomFontFace specialPrice;
    @BindView(R.id.priceCont)
    RelativeLayout priceCont;
    @BindView(R.id.price)
    CustomFontFace price;
    @BindView(R.id.sideLine)
    View sideLine;
    @BindView(R.id.bottomLine)
    View bottomLine;
    @BindView(R.id.item1)
    View item1;
    @BindView(R.id.item2)
    View item2;
    @BindView(R.id.item3)
    View item3;
    Unbinder unbinder;
    ItemViewHolder includeItem1, includeItem2, includeItem3;
    List<ItemViewHolder> viewHolders;
    List<View> views;
    private List<ProductItemEntity> products;
    private Unbinder unbinder1;
    private Unbinder unbinder2;
    private Unbinder unbinder3;

    public static TopProductFragment newInstance() {

        Bundle args = new Bundle();

        TopProductFragment fragment = new TopProductFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_product, container, false);
        unbinder = ButterKnife.bind(this, view);
        bindItems();
        return view;
    }

    private void bindItems() {
        views = new ArrayList<>();
        views.add(item1);
        views.add(item2);
        views.add(item3);
        viewHolders = new ArrayList<>();
        includeItem1 = new ItemViewHolder();
        includeItem2 = new ItemViewHolder();
        includeItem3 = new ItemViewHolder();
        viewHolders.add(includeItem1);
        viewHolders.add(includeItem2);
        viewHolders.add(includeItem3);
        unbinder1 = ButterKnife.bind(includeItem1, item1);
        unbinder2 = ButterKnife.bind(includeItem2, item2);
        unbinder3 = ButterKnife.bind(includeItem3, item3);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (products != null) {
            for (int i = 0; i < products.size(); i++) {
                if (products.get(i) != null) {
                    ProductItemEntity item = products.get(i);
                    views.get(i).setVisibility(View.VISIBLE);
                    viewHolders.get(i).productTitle.setText(item.getName());
                    viewHolders.get(i).price.setText(StringFormat.addCurrencyFormat(item.getPrice()));
                    Glide.with(getContext()).load(item.getImage()).into(viewHolders.get(i).productImage);
                    views.get(i).setOnClickListener(v -> {
                        try {
                            ((HomePageActivity) getActivity()).onTopProductClick(item);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                } else {
                    views.get(i).setVisibility(View.INVISIBLE);
                }
            }
        }
    }

    public void setProducts(List<ProductItemEntity> products) {
        this.products = products;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        unbinder1.unbind();
        unbinder2.unbind();
        unbinder3.unbind();
    }

    public class ItemViewHolder {
        @BindView(R.id.productImage)
        ImageSquared productImage;
        @BindView(R.id.productTitle)
        CustomFontFace productTitle;
        @BindView(R.id.discount)
        CustomFontFace discount;
        @BindView(R.id.price)
        CustomFontFace price;
        @BindView(R.id.discountContainer)
        ViewGroup discountContainer;
    }
}
