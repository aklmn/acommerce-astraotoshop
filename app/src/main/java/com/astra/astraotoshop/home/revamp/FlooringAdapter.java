package com.astra.astraotoshop.home.revamp;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FlooringAdapter extends RecyclerView.Adapter<FlooringAdapter.FlooringViewHolder> {

    @NonNull
    @Override
    public FlooringViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new FlooringViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_flooring, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FlooringViewHolder holder, int position) {
        holder.bind(position);
        holder.itemView.setOnClickListener(v -> {

        });
    }

    @Override
    public int getItemCount() {
        return 9;
    }

    class FlooringViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.sideLine)
        View sideLine;
        @BindView(R.id.bottomLine)
        View bottomLine;

        public FlooringViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        public void bind(int position) {
            if (position%2 == 0) {
                sideLine.setVisibility(View.VISIBLE);
                bottomLine.setVisibility(View.VISIBLE);
            } else {
                sideLine.setVisibility(View.VISIBLE);
                bottomLine.setVisibility(View.GONE);
            }
        }
    }
}
