package com.astra.astraotoshop.home.provider;

import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;
import com.astra.astraotoshop.product.catalog.provider.ProductListEntity;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/10/2018.
 */

public interface HomeContract {
    interface HomeView {
        void setStaticValue(StaticBlock data, String identifier);
        void setStaticCategoryValue(StaticCategory data,String identifier);
        void setBestSelling(ProductListEntity productList);

        void showError(int viewId);

        void setPromoProduct(List<ProductItemEntity> productList);
    }

    interface HomePresenter {
        void requestStaticBlock(String storeCode, String identifier);
        void requestStaticCategory(String storeCode, String identifier);
        void getBestSelling(String storeCode);

        void onDataReceived(StaticBlock data, String identifier);
        void onCategoryReceived(StaticCategory data, String identifier);
        void onBestSellingReceived(ProductListEntity productList);

        void onFailedReceivingData(Throwable error);
        void onFailedReceivingCategory(Throwable error);
        void onFailedReceivingBestSelling(Throwable error);

        CategoryEntity getCategory();

        void requestNewestProduct(String product);
        ProductListEntity getNewestProduct();
        void onNewestProductReceived(ProductListEntity productList);
        void onFailedReceivingNewestProduct(Throwable error);
        void requestPromoProduct(String product);
        ProductListEntity getPromoProduct();
        void onPromoProductReceived(ProductListEntity productList);
        void onFailedReceivingPromoProduct(Throwable error);
    }

    interface HomeInteractor {
        void requestStaticBlock(NetworkHandler networkHandler, String storeCode, String identifier);
        void requestStaticCategory(NetworkHandler networkHandler, String storeCode, String identifier);
        void getBestSelling(String storeCode);

        void getNewestProduct(String product);

        void getPromoProduct(String product);
    }
}
