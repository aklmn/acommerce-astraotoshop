package com.astra.astraotoshop.home.revamp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.BrandAdapter;
import com.astra.astraotoshop.dummies.PagerAdapter;
import com.astra.astraotoshop.home.provider.HomeContract;
import com.astra.astraotoshop.home.provider.HomePresenter;
import com.astra.astraotoshop.home.provider.StaticBlock;
import com.astra.astraotoshop.home.provider.StaticCategory;
import com.astra.astraotoshop.home.view.StaticBlockAdapter;
import com.astra.astraotoshop.product.catalog.product.ProductsActivity;
import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;
import com.astra.astraotoshop.product.catalog.provider.ProductListEntity;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.product.search.SearchActivity;
import com.astra.astraotoshop.user.profile.ProfileMenuActivity;
import com.astra.astraotoshop.utils.base.BaseActionActivity;
import com.rd.PageIndicatorView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTouch;

public class HomePageActivity extends BaseActionActivity implements HomeContract.HomeView, ListListener, MenuListener {

    @BindView(R.id.topBanner)
    RecyclerView topBanner;
    @BindView(R.id.brandList)
    RecyclerView brandList;
    @BindView(R.id.bottomBanner)
    RecyclerView bottomBanner;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.productPager)
    ViewPager productPager;
    @BindView(R.id.pageIndicatorView)
    PageIndicatorView pageIndicatorView;
    @BindView(R.id.indicatorContainer)
    RelativeLayout indicatorContainer;
    @BindView(R.id.radioTopProduct)
    RadioGroup radioTopProduct;
    @BindView(R.id.sparepartList)
    RecyclerView sparepartList;
    @BindView(R.id.installationList)
    RecyclerView installationList;
    @BindView(R.id.menuPager)
    ViewPager menuPager;
    @BindView(R.id.frame)
    FrameLayout frame;
    private HomePresenter homePresenter;
    private StaticBlockAdapter topBannerAdapter;
    private BrandAdapter brandAdapter;
    private StaticBlockAdapter bottomBannerAdapter;
    private PagerAdapter pageAdapter;
    private PagerAdapter menuAdapter;
    private MenuFragment menuFragment;
    private FragmentTransaction trans;
    private HomePagerFragment fragPromo;
    private HomePagerFragment fragNewest;
    private int prevFrag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_revamp);
        ButterKnife.bind(this);
        actionBarSetup();
        homePresenter = new HomePresenter(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,
                drawerLayout, getToolbar(), R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        setupTopBanner();
        setupBrand();
        setupBottomBanner();
        setupPager();
        setupSparePart();
        setupInstallation();
        setupFragment();
        mock();
    }

    @Override
    protected void onStart() {
        super.onStart();
        getData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_page, menu);
        registerCustomMenu(menu, R.id.action_cart);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_cart: {
                openCart();
                break;
            }
            case R.id.action_profile: {
                if (isLogin()) {
                    Intent intent = getIntent(this, ProfileMenuActivity.class);
                    startActivity(intent);
                } else showAccount();
            }
        }
        return true;
    }

    @OnTouch({R.id.searchContainer, R.id.search})
    public boolean onSearchTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            Intent intent = getIntent(this, SearchActivity.class);
            startActivity(intent);
        }
        return true;
    }

    private void setupTopBanner() {
        topBannerAdapter = new StaticBlockAdapter(this, this);
        topBanner.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        topBanner.setAdapter(topBannerAdapter);
    }

    private void setupBrand() {
        brandAdapter = new BrandAdapter(this, this);
        brandList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        brandList.setAdapter(brandAdapter);
    }

    private void setupBottomBanner() {
        bottomBannerAdapter = new StaticBlockAdapter(this, this);
        bottomBanner.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        bottomBanner.setAdapter(bottomBannerAdapter);
    }

    private void setupPager() {
        pageAdapter = new PagerAdapter(getSupportFragmentManager());
        productPager.setAdapter(pageAdapter);
        radioTopProduct.check(R.id.radioPromo);

        trans = getSupportFragmentManager().beginTransaction();

        menuAdapter = new PagerAdapter(getSupportFragmentManager());
        menuPager.setAdapter(menuAdapter);
        menuFragment = MenuFragment.newInstance(true);
        menuFragment.setMenuListener(this);
        menuFragment.setCategory(homePresenter.getCategory());
        menuAdapter.addFragment(menuFragment);
    }

    private void setupSparePart() {
        FlooringAdapter flooringAdapter = new FlooringAdapter();
        sparepartList.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.HORIZONTAL, false));
        sparepartList.setAdapter(flooringAdapter);
    }

    private void setupInstallation() {
        FlooringAdapter flooringAdapter = new FlooringAdapter();
        installationList.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.HORIZONTAL, false));
        installationList.setAdapter(flooringAdapter);
    }

    private void setupFragment() {
        fragPromo = new HomePagerFragment();
        fragNewest = new HomePagerFragment();
    }

    private void getData() {
        homePresenter.requestStaticBlock("product", getString(R.string.identifier_banner));
        homePresenter.requestStaticBlock("product", getString(R.string.identifier_brand));
        homePresenter.requestStaticBlock("product", getString(R.string.identifier_promo));
        homePresenter.requestNewestProduct("product");
        homePresenter.requestPromoProduct("product");
        if (homePresenter.getCategory() != null) {
            menuFragment.setCategory(homePresenter.getCategory());
        }
    }

    @Override
    public void setStaticValue(StaticBlock data, String identifier) {
        if (identifier.equals(getString(R.string.identifier_banner))) {
            topBannerAdapter.setData(data.getItems());
        } else if (identifier.equals(getString(R.string.identifier_brand))) {
            brandAdapter.setImages(data.getItems());
        } else if (identifier.equals(getString(R.string.identifier_promo))) {
            bottomBannerAdapter.setData(data.getItems());
        }
    }

    @Override
    public void setStaticCategoryValue(StaticCategory data, String identifier) {

    }

    @Override
    public void setBestSelling(ProductListEntity productList) {

    }

    @Override
    public void showError(int viewId) {

    }

    @Override
    public void setPromoProduct(List<ProductItemEntity> items) {
        HomePagerFragment frag = new HomePagerFragment();
        frag.setProducts(items);
        trans.replace(R.id.frame, frag, "topProduct").commit();
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {

    }

    @OnClick({R.id.radioNewest, R.id.radioPromo, R.id.radioBestSeller})
    public void onRadioClick(View v) {
        switch (v.getId()) {
            case R.id.radioPromo: {
                if (homePresenter.getPromoProduct() != null && homePresenter.getPromoProduct().getItems().size() > 0 && prevFrag != R.id.radioPromo) {
                    fragPromo.setProducts(homePresenter.getPromoProduct().getItems());
                    replaceFragment(fragPromo);
                    prevFrag = R.id.radioPromo;
                }
                break;
            }
            case R.id.radioNewest: {
                if (homePresenter.getNewestProduct() != null && homePresenter.getNewestProduct().getItems().size() > 0 && prevFrag != R.id.radioNewest) {
                    fragNewest.setProducts(homePresenter.getNewestProduct().getItems());
                    replaceFragment(fragNewest);
                    prevFrag = R.id.radioNewest;
                }
                break;
            }
            case R.id.radioBestSeller: {
                if (pageAdapter.getCount() > 2)
                    productPager.setCurrentItem(2);
                break;
            }
        }
    }

    private void replaceFragment(HomePagerFragment fragTarget) {
        Fragment frag = getSupportFragmentManager().findFragmentByTag("topProduct");
        if (frag != null) getSupportFragmentManager().beginTransaction().remove(frag).commit();
        getSupportFragmentManager().beginTransaction().replace(R.id.frame, fragTarget, "topProduct").commit();
    }

    private void mock() {

    }

    public void onTopProductClick(ProductItemEntity product) {
        Intent intent = getIntent(this, ProductDetailActivity.class);
        intent.putExtra("id", product.getEntityId());
        startActivity(intent);
    }

    @Override
    public void onMenuSelected(CategoryEntity category) {
        if (category.getChildrenData().size() > 0) {
            category.getChildrenData().add(0, new CategoryEntity("Semua " + category.getName(), category.getId(), new ArrayList<>()));

            MenuFragment menuFragment = MenuFragment.newInstance(false);
            menuFragment.setMenuListener(this);
            menuFragment.setCategory(category);
            menuAdapter.addFragment(menuFragment);
            menuPager.setCurrentItem(menuAdapter.getCount(), true);
        } else {
            Intent intent = new Intent(this, ProductsActivity.class);
            intent.putExtra("catId", category.getId());
            intent.putExtra("name", category.getName());
            startActivity(intent);
        }
    }

    @Override
    public void onBackPress() {
        menuPager.setCurrentItem(menuAdapter.getCount() - 2, true);
        menuAdapter.removeFragment(menuAdapter.getCount() - 1);
    }
}
