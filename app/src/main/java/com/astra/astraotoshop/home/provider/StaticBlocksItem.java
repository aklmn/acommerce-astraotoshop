package com.astra.astraotoshop.home.provider;

import com.google.gson.annotations.SerializedName;

public class StaticBlocksItem {

    @SerializedName("page_url")
    private String pageUrl;

    @SerializedName("category_id")
    private String categoryId;

    @SerializedName("img_url")
    private String imgUrl;

    @SerializedName("product_id")
    private String productId;

    @SerializedName("alt")
    private String alt;

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    @Override
    public String toString() {
        return
                "StaticBlocksItem{" +
                        "page_url = '" + pageUrl + '\'' +
                        ",category_id = '" + categoryId + '\'' +
                        ",img_url = '" + imgUrl + '\'' +
                        ",product_id = '" + productId + '\'' +
                        "}";
    }
}