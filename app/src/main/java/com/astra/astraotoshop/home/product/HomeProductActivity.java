package com.astra.astraotoshop.home.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.BrandAdapter;
import com.astra.astraotoshop.home.provider.HomeContract;
import com.astra.astraotoshop.home.provider.HomePresenter;
import com.astra.astraotoshop.home.provider.StaticBlock;
import com.astra.astraotoshop.home.provider.StaticBlocksItem;
import com.astra.astraotoshop.home.provider.StaticCategory;
import com.astra.astraotoshop.home.view.StaticBlockActivity;
import com.astra.astraotoshop.home.view.StaticBlockAdapter;
import com.astra.astraotoshop.home.widget.CategoryAdapter;
import com.astra.astraotoshop.home.widget.HomeProductAdapter;
import com.astra.astraotoshop.home.widget.PromoAdapter;
import com.astra.astraotoshop.product.catalog.product.ProductsActivity;
import com.astra.astraotoshop.product.catalog.productservice.ProductServiceAdapter;
import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;
import com.astra.astraotoshop.product.catalog.provider.ProductListEntity;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.category.view.MainCategoryActivity;
import com.astra.astraotoshop.product.product.ProductDetailActivity;
import com.astra.astraotoshop.product.search.SearchActivity;
import com.astra.astraotoshop.user.profile.ProfileFragment;
import com.astra.astraotoshop.user.profile.ProfileMenuActivity;
import com.astra.astraotoshop.utils.base.BaseActionActivity;
import com.astra.astraotoshop.utils.pref.Pref;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeProductActivity extends BaseActionActivity implements

        ProductServiceAdapter.ItemClickListener, HomeContract.HomeView, ListListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.brand)
    RecyclerView brand;
    @BindView(R.id.promo)
    RecyclerView promo;
    @BindView(R.id.bestSellerList)
    RecyclerView bestSellerList;
    @BindView(R.id.category)
    RecyclerView category;

    HomeContract.HomePresenter presenter;

    @BindView(R.id.banner)
    RecyclerView banner;
    private StaticBlockAdapter bannerAdapter;
    private HomeProductAdapter homeProductAdapter;
    private BrandAdapter brandAdapter;
    private PromoAdapter promoAdapter;
    private CategoryAdapter categoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_product);
        actionBarSetup();
        ButterKnife.bind(this);

        presenter = new HomePresenter(this);
        presenter.requestStaticBlock(getStoreCode(), getString(R.string.identifier_banner));
        presenter.requestStaticBlock(getStoreCode(), getString(R.string.identifier_brand));
        presenter.requestStaticBlock(getStoreCode(), getString(R.string.identifier_promo));
        presenter.requestStaticBlock(getStoreCode(), getString(R.string.identifier_category));
        presenter.requestStaticCategory(getStoreCode(), getString(R.string.identifier_category));
        presenter.getBestSelling(getResources().getStringArray(R.array.storeCode)[getState()]);

        if (getState() == getResources().getInteger(R.integer.state_product_service))
            setCustomTitle("ASTRA OTO DRIVE");

        bannerListSetup();
        brandListSetup();
        categorySetup();
        promoListSetup();
        bestSellingListSetup();

        Pref.getPreference().putString("currentStore", getStoreCode());
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        registerCustomMenu(menu, R.id.action_cart);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }

            case R.id.action_profile: {
                Intent intent;
                if (isLogin()) {
                    intent = new Intent(this, ProfileMenuActivity.class);
                    mStartActivity(intent);
                } else {
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    Fragment fragment = getSupportFragmentManager().findFragmentByTag("account");
                    if (fragment != null) transaction.remove(fragment);

                    ProfileFragment profileFragment = ProfileFragment.newInstance(getState());
                    profileFragment.show(getSupportFragmentManager(), "account");
                }
                break;
            }

            case R.id.action_search: {
                Intent intent = new Intent(this, SearchActivity.class);
                intent.putExtra(STATE, getState());
                startActivity(intent);
                break;
            }
            case R.id.action_cart: {
                openCart();
            }
        }
        return true;
    }

    @OnClick(R.id.categoryContainer)
    public void category() {
        Intent intent = new Intent(this, MainCategoryActivity.class);
        intent.putExtra(STATE, getState());
        startActivity(intent);
    }

    private void bannerListSetup() {
        bannerAdapter = new StaticBlockAdapter(this, getWidthImage(), this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        banner.setLayoutManager(layoutManager);
        banner.setAdapter(bannerAdapter);

        (new GravitySnapHelper(Gravity.START)).attachToRecyclerView(banner);
    }

    private void brandListSetup() {
        brandAdapter = new BrandAdapter(this, this);
        LinearLayoutManager brandLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        brand.setLayoutManager(brandLayoutManager);
        brand.setAdapter(brandAdapter);

        (new GravitySnapHelper(Gravity.START)).attachToRecyclerView(brand);
    }

    private void categorySetup() {
        LinearLayoutManager categoryLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        categoryAdapter = new CategoryAdapter(this, this);
        category.setLayoutManager(categoryLayoutManager);
        category.setAdapter(categoryAdapter);
    }

    private void promoListSetup() {
        LinearLayoutManager promoLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        promoAdapter = new PromoAdapter(this, this, getWidthImage());
        promo.setLayoutManager(promoLayoutManager);
        promo.setAdapter(promoAdapter);
    }

    private void bestSellingListSetup() {
        homeProductAdapter = new HomeProductAdapter(this, this);
        LinearLayoutManager bestSellingLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        bestSellerList.setLayoutManager(bestSellingLayoutManager);
        bestSellerList.setAdapter(homeProductAdapter);
    }

    private int getWidthImage() {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
        windowManager.getDefaultDisplay().getMetrics(metrics);

        int widthInDP = Math.round(metrics.widthPixels / metrics.density);
        int widthExpect = widthInDP - 48;

        return (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                widthExpect,
                getResources().getDisplayMetrics()));
    }

    @Override
    public void onItemClick() {

    }

    @Override
    public void onItemClick(int viewId) {

    }

    @Override
    public void setStaticValue(StaticBlock data, String identifier) {
        if (identifier.equalsIgnoreCase(getString(R.string.identifier_banner))) {
            bannerAdapter.setData(data.getItems());
        } else if (identifier.equalsIgnoreCase(getString(R.string.identifier_brand))) {
            brandAdapter.setImages(data.getItems());
        } else if (identifier.equalsIgnoreCase(getString(R.string.identifier_category))) {
            categoryAdapter.setItems(data.getItems());
        } else if (identifier.equalsIgnoreCase(getString(R.string.identifier_promo))) {
            promoAdapter.setImages(data.getItems());
        }
    }

    @Override
    public void setStaticCategoryValue(StaticCategory data, String identifier) {
        categoryAdapter.setCategories(data.getItems());
    }

    @Override
    public void setBestSelling(ProductListEntity productList) {
        homeProductAdapter.setProducts(productList.getItems());
    }

    @Override
    public void showError(int viewId) {

    }

    @Override
    public void setPromoProduct(List<ProductItemEntity> productList) {

    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        if (viewId == R.id.bestSellingItem) {
            Intent intent = new Intent(this, ProductDetailActivity.class);
            intent.putExtra("id", ((ProductItemEntity) data).getEntityId());
            intent.putExtra("name", ((ProductItemEntity) data).getName());
            mStartActivity(intent);
        } else {
            staticManager(data);
        }
    }

    private void staticManager(Object data) {
        StaticBlocksItem staticBlock = (StaticBlocksItem) data;
        if (staticBlock != null) {
            if (!TextUtils.isEmpty(staticBlock.getCategoryId())) {
                Intent intent = getIntent(this, ProductsActivity.class);
                intent.putExtra("catId", Integer.valueOf(staticBlock.getCategoryId()));
                intent.putExtra("name", staticBlock.getAlt());
                startActivity(intent);
            } else if (!TextUtils.isEmpty(staticBlock.getProductId())) {
                Intent intent = getIntent(this, ProductDetailActivity.class);
                intent.putExtra("id", staticBlock.getProductId());
                startActivity(intent);
            } else if (!TextUtils.isEmpty(staticBlock.getPageUrl())) {
                Intent intent = getIntent(this, StaticBlockActivity.class);
                intent.putExtra("pageUrl", staticBlock.getPageUrl());
                intent.putExtra("alt", staticBlock.getAlt());
                startActivity(intent);
            }
        }
    }

    private void mStartActivity(Intent intent) {
        intent.putExtra(STATE, getState());
        startActivity(intent);
    }
}
