package com.astra.astraotoshop.home.revamp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.PagerAdapter;
import com.astra.astraotoshop.product.catalog.provider.ProductItemEntity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomePagerFragment extends Fragment {

    @BindView(R.id.mainPager)
    ViewPager mainPager;
    Unbinder unbinder;
    @BindView(R.id.indicatorContainer)
    RelativeLayout indicatorContainer;
    private PagerAdapter adapter;
    private List<ProductItemEntity> items;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_pager, container, false);
        unbinder = ButterKnife.bind(this, view);
        adapter = new PagerAdapter(getChildFragmentManager());
        mainPager.setAdapter(adapter);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (items != null && items.size() > 0) {
            List<ProductItemEntity> item;
            TopProductFragment frag;
            item = new ArrayList<>();
            for (int i = 0; i < items.size(); i++) {
                if (i % 3 == 0 && i != 0) {
                    frag = TopProductFragment.newInstance();
                    frag.setProducts(item);
                    adapter.addFragment(frag);
                    item = new ArrayList<>();
                }
                item.add(items.get(i));
            }
            if (item.size() % 3 != 0) {
                item = new ArrayList<>();
                int start = items.size() - (items.size() % 3);
                item.addAll(items.subList(start, items.size()));
                frag = TopProductFragment.newInstance();
                frag.setProducts(item);
                adapter.addFragment(frag);
            }
            indicatorContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setProducts(List<ProductItemEntity> items) {
        this.items = items;
    }
}
