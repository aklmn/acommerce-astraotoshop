package com.astra.astraotoshop.home.provider;

import com.astra.astraotoshop.product.catalog.provider.ProductListEntity;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.utils.network.handler.NetworkHandler;
import com.astra.astraotoshop.utils.pref.Pref;
import com.google.gson.Gson;

/**
 * Created by Henra Setia Nugraha on 1/10/2018.
 */

public class HomePresenter implements HomeContract.HomePresenter {

    private HomeContract.HomeView view;
    private HomeContract.HomeInteractor interactor;
    private NetworkHandler networkHandler;
    private CategoryEntity categories;
    private boolean isLoad = false;
    private ProductListEntity newestProduct;
    private ProductListEntity promoProduct;

    public HomePresenter(HomeContract.HomeView view) {
        this.view = view;
        interactor = new HomeInterator(null, this);
        networkHandler = new NetworkHandler();
        getCategories();
    }

    private void getCategories() {
        String json = Pref.getPreference().getString("cat-product");
        categories = new Gson().fromJson(json, CategoryEntity.class);
    }

    @Override
    public void requestStaticBlock(String storeCode, String identifier) {
        interactor.requestStaticBlock(networkHandler, storeCode, identifier);
    }

    @Override
    public void requestStaticCategory(String storeCode, String identifier) {
        interactor.requestStaticCategory(new NetworkHandler(), storeCode, identifier);
    }

    @Override
    public void getBestSelling(String storeCode) {
        interactor.getBestSelling(storeCode);
    }

    @Override
    public void onDataReceived(StaticBlock data, String identifier) {
        if (data != null) {
            view.setStaticValue(data, identifier);
        }
    }

    @Override
    public void onCategoryReceived(StaticCategory data, String identifier) {
        view.setStaticCategoryValue(data, identifier);
    }

    @Override
    public void onBestSellingReceived(ProductListEntity productList) {
        view.setBestSelling(productList);
    }

    @Override
    public void onFailedReceivingData(Throwable error) {
        System.out.println();
    }

    @Override
    public void onFailedReceivingCategory(Throwable error) {

    }

    @Override
    public void onFailedReceivingBestSelling(Throwable error) {
        System.out.println();
    }

    @Override
    public CategoryEntity getCategory() {
        if (categories == null) {
            getCategories();
        }
        return categories;
    }

    @Override
    public void requestNewestProduct(String product) {
        interactor.getNewestProduct(product);
    }

    @Override
    public ProductListEntity getNewestProduct() {
        return newestProduct;
    }

    @Override
    public void onNewestProductReceived(ProductListEntity productList) {
        newestProduct = productList;
    }

    @Override
    public void onFailedReceivingNewestProduct(Throwable error) {

    }

    @Override
    public void requestPromoProduct(String product) {
        interactor.getPromoProduct(product);
    }

    @Override
    public ProductListEntity getPromoProduct() {
        return promoProduct;
    }

    @Override
    public void onPromoProductReceived(ProductListEntity productList) {
        promoProduct = productList;
        view.setPromoProduct(productList.getItems());
    }

    @Override
    public void onFailedReceivingPromoProduct(Throwable error) {

    }
}
