package com.astra.astraotoshop.home.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StaticBlock {

    @SerializedName("items")
    private List<StaticBlocksItem> items;

    public void setItems(List<StaticBlocksItem> items) {
        this.items = items;
    }

    public List<StaticBlocksItem> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return
                "StaticBlock{" +
                        "items = '" + items + '\'' +
                        "}";
    }
}