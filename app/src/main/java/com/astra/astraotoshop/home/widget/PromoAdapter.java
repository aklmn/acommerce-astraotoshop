package com.astra.astraotoshop.home.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.home.provider.StaticBlocksItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 12/17/2017.
 */

public class PromoAdapter extends RecyclerView.Adapter<PromoAdapter.PromoVIewHolder> {

    private Context context;
    private List<StaticBlocksItem> images;
    private ListListener listener;
    private int imageWidth;

    public PromoAdapter(Context context, @Nullable ListListener listener, int imageWidth) {
        this.context = context;
        this.listener = listener;
        this.imageWidth = imageWidth;
        images = new ArrayList<>();
    }

    @Override
    public PromoVIewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PromoVIewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_image, parent, false));
    }

    @Override
    public void onBindViewHolder(PromoVIewHolder holder, int position) {
//        if (position == getItemCount() - 1) {
//            int padding = (int) (16 * context.getResources().getDisplayMetrics().density);
//            holder.image.setPadding(holder.image.getPaddingLeft(), 0, padding, 0);
//            holder.image.invalidate();
//        }
//

        ViewGroup.LayoutParams layoutParams = holder.image.getLayoutParams();
        layoutParams.width = imageWidth;
        layoutParams.height = imageWidth * 9 / 21;
        holder.image.setLayoutParams(layoutParams);
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) holder.image.getLayoutParams();
        if (position == 0) {
            params.setMargins(dpToPx(16), 0, dpToPx(16), dpToPx(16));
        } else {
            params.setMargins(0, 0, dpToPx(16), dpToPx(16));
        }
//        if (position == 0) {
//            holder.image.setPadding(dpToPx(16), dpToPx(16), dpToPx(16), dpToPx(16));
//        } else {
//            holder.image.setPadding(0, 0, 0, dpToPx(16));
//        }

        if (images.get(position) != null) {
            Glide.with(context).load(images.get(position).getImgUrl()).into(holder.image);
        }
    }

    private int dpToPx(int dp) {
        return (int) (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                context.getResources().getDisplayMetrics()));
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void setImages(List<StaticBlocksItem> images) {
        this.images = images;
        notifyDataSetChanged();
    }

    class PromoVIewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;

        public PromoVIewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.image)
        public void itemClick() {
            try {
                listener.onItemClick(R.id.promoItem, getAdapterPosition(), images.get(getAdapterPosition()));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
