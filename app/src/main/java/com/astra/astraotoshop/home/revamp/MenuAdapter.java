package com.astra.astraotoshop.home.revamp;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.utils.view.CustomFontFace;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

    private ListListener listener;
    private List<CategoryEntity> categories;


    public MenuAdapter(ListListener listener) {
        this.listener = listener;
        categories = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MenuViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_home, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MenuViewHolder holder, int position) {
        holder.name.setText(categories.get(position).getName().toUpperCase());
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void setCategories(List<CategoryEntity> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.name)
        CustomFontFace name;
        @BindView(R.id.container)
        ConstraintLayout container;

        public MenuViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            try {
                listener.onItemClick(R.id.container, getAdapterPosition(), categories.get(getAdapterPosition()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
