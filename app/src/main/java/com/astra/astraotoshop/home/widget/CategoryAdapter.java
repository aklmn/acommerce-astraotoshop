package com.astra.astraotoshop.home.widget;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.home.provider.CategoryItem;
import com.astra.astraotoshop.home.provider.StaticBlocksItem;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Henra Setia Nugraha on 12/14/2017.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private Context context;
    private List<CategoryItem> categories;
    private List<StaticBlocksItem> blocksItems;
    private ListListener listener;

    public CategoryAdapter(Context context, ListListener listener) {
        this.context = context;
        this.listener = listener;
        blocksItems = new ArrayList<>();
    }


    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false));
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        holder.name.setText(blocksItems.get(position).getAlt());
        if (blocksItems.get(position).getImgUrl() != null) {
            Glide.with(context).load(blocksItems.get(position).getImgUrl()).into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return blocksItems.size();
    }

    public void setCategories(List<CategoryItem> categories) {
        this.categories = categories;
        notifyDataSetChanged();
    }

    public void setItems(List<StaticBlocksItem> blocksItems) {
        this.blocksItems = blocksItems;
        notifyDataSetChanged();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.container)
        LinearLayout container;
        @BindView(R.id.name)
        TextView name;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.container)
        public void itemClick() {
            try {
                listener.onItemClick(R.id.categoryItem, getAdapterPosition(), blocksItems.get(getAdapterPosition()));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
    }
}
