package com.astra.astraotoshop.home.revamp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.product.catalog.view.ListListener;
import com.astra.astraotoshop.product.category.provider.CategoryEntity;
import com.astra.astraotoshop.utils.view.CustomFontFace;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MenuFragment extends Fragment implements ListListener {

    private static final String KEY = "isFirst";

    @BindView(R.id.menuList)
    RecyclerView menuList;
    @BindView(R.id.traceOrder)
    CustomFontFace traceOrder;
    @BindView(R.id.myVehicle)
    CustomFontFace myVehicle;
    @BindView(R.id.bulkOrder)
    CustomFontFace bulkOrder;
    @BindView(R.id.help)
    CustomFontFace help;
    Unbinder unbinder;
    @BindView(R.id.footer)
    LinearLayout footer;
    @BindView(R.id.backNav)
    RelativeLayout backNav;
    private MenuAdapter menuAdapter;
    private MenuListener menuListener;
    private CategoryEntity category;

    public static MenuFragment newInstance(boolean isFirst) {

        Bundle args = new Bundle();
        args.putBoolean(KEY, isFirst);
        MenuFragment fragment = new MenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (!getArguments().getBoolean(KEY)) {
            footer.setVisibility(View.GONE);
            backNav.setVisibility(View.VISIBLE);
        }
        menuListSetup();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (category != null) {
            menuAdapter.setCategories(category.getChildrenData());
        }
    }

    private void menuListSetup() {
        menuAdapter = new MenuAdapter(this);
        menuList.setLayoutManager(new LinearLayoutManager(getContext()));
        menuList.setAdapter(menuAdapter);
        menuList.setNestedScrollingEnabled(false);
    }

    public void setMenuListener(MenuListener menuListener) {
        this.menuListener = menuListener;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
        try {
            if (category != null) {
                menuAdapter.setCategories(category.getChildrenData());
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(int viewId, int position, Object data) {
        try {
            menuListener.onMenuSelected((CategoryEntity) data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.backNav)
    public void onBackPress(){
        try {
            menuListener.onBackPress();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
