package com.astra.astraotoshop.home.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StaticCategory {

    @SerializedName("items")
    private List<CategoryItem> items;

    public void setItems(List<CategoryItem> items) {
        this.items = items;
    }

    public List<CategoryItem> getItems() {
        return items;
    }

    @Override
    public String toString() {
        return
                "StaticCategory{" +
                        "items = '" + items + '\'' +
                        "}";
    }
}