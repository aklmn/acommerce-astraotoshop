package com.astra.astraotoshop.home.revamp;

import com.astra.astraotoshop.product.category.provider.CategoryEntity;

public interface MenuListener {
    void onMenuSelected(CategoryEntity category);
    void onBackPress();
}
