package com.astra.astraotoshop.home.provider;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Henra Setia Nugraha on 1/10/2018.
 */

public class StaticBlockEntity {
    @SerializedName("items")
    private List<String > items;

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
