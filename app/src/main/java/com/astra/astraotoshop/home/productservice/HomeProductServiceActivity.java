package com.astra.astraotoshop.home.productservice;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.astra.astraotoshop.R;
import com.astra.astraotoshop.dummies.BrandAdapter;
import com.astra.astraotoshop.dummies.PagerAdapter;
import com.astra.astraotoshop.home.widget.CategoryAdapter;
import com.astra.astraotoshop.home.widget.HomeProductAdapter;
import com.astra.astraotoshop.home.widget.PromoAdapter;
import com.astra.astraotoshop.order.cart.CartFragment;
import com.astra.astraotoshop.product.catalog.productservice.ProductServiceAdapter;
import com.astra.astraotoshop.product.category.view.MainCategoryActivity;
import com.astra.astraotoshop.product.search.SearchActivity;
import com.astra.astraotoshop.user.profile.ProfileFragment;
import com.astra.astraotoshop.utils.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeProductServiceActivity extends BaseActivity implements ProductServiceAdapter.ItemClickListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    //    @BindView(R.id.tabs)
//    TabLayout tabs;
//    @BindView(R.id.container)
//    ViewPager container;
//    @BindView(R.id.search)
//    EditText search;
    @BindView(R.id.brand)
    RecyclerView brand;
    @BindView(R.id.promo)
    RecyclerView promo;
    @BindView(R.id.bestSellerList)
    RecyclerView mostSold;
    @BindView(R.id.category)
    RecyclerView category;
    private PagerAdapter mSectionsPagerAdapter;
    int[] brandImage = {R.drawable.brand1, R.drawable.brand2, R.drawable.brand3, R.drawable.brand1, R.drawable.brand2, R.drawable.brand3};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_product);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        brandListSetup();
        promoListSetup();

//        profileFragment.setListener(this);
//        mSectionsPagerAdapter = new PagerAdapter(getSupportFragmentManager());
//        mSectionsPagerAdapter.addFragment(HomeFragment.newInstance(), "Beranda");
//        mSectionsPagerAdapter.addFragment(CategoryFragment.newInstance(getState()), "Katalog");
//        mSectionsPagerAdapter.addFragment(profileFragment, "Akun");
//
//        container.setAdapter(mSectionsPagerAdapter);
//        tabs.setupWithViewPager(container);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        try {
            MenuItem menuItem = menu.findItem(R.id.action_cart);
            View view = menuItem.getActionView();
            view.setOnClickListener((View -> onOptionsItemSelected(menuItem)));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:{
                finish();
                break;
            }
            case R.id.action_cart:{
                CartFragment.newInstance(getResources().getInteger(R.integer.state_product_service)).show(getSupportFragmentManager(), "Cart");
                break;
            }
            case R.id.action_search:{
                Intent intent = new Intent(this, SearchActivity.class);
                intent.putExtra(STATE, getResources().getInteger(R.integer.state_product));
                startActivity(intent);
                break;
            }
            case R.id.action_profile:{
                ProfileFragment.newInstance(getResources().getInteger(R.integer.state_product_service)).show(getSupportFragmentManager(),"Profile");
            }

        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.categoryContainer)
    public void category() {
        Intent intent = new Intent(this, MainCategoryActivity.class);
        intent.putExtra(STATE, getState());
        startActivity(intent);
    }

    private void brandListSetup() {
        BrandAdapter brandAdapter = new BrandAdapter(this,null);
        LinearLayoutManager brandLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        brand.setLayoutManager(brandLayoutManager);
        brand.setAdapter(brandAdapter);
    }

    private void promoListSetup() {
        HomeProductAdapter homeProductAdapter = new HomeProductAdapter(this,null);
        LinearLayoutManager promoLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager categoryLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager mostSoldLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        PromoAdapter promoAdapter = new PromoAdapter(this,null, 0);
        CategoryAdapter categoryAdapter = new CategoryAdapter(this,null);

        category.setLayoutManager(categoryLayoutManager);
        promo.setLayoutManager(promoLayoutManager);
        mostSold.setLayoutManager(mostSoldLayoutManager);

        category.setAdapter(categoryAdapter);
        promo.setAdapter(promoAdapter);
        mostSold.setAdapter(homeProductAdapter);
    }

    @Override
    public void onItemClick() {

    }

    @Override
    public void onItemClick(int viewId) {

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 3 && resultCode == RESULT_OK) {
//            changeProfilePage(null);
//        }
//    }
//
//    @OnTouch(R.id.search)
//    public boolean onSearchTouch(View view, MotionEvent motionEvent) {
//        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
//            startActivity(new Intent(this, SearchActivity.class));
//            return true;
//        } else
//            return false;
//    }
//
//    @Override
//    public void onSignOut() {
//        changeProfilePage("loggedIn");
//    }
//
//    private void changeProfilePage(String state) {
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        profileFragment.onDetach(state);
//        transaction.detach(profileFragment).attach(profileFragment).attach(profileFragment).commit();
//    }
}
