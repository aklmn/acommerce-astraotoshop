package com.astra.astraotoshop;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.transition.Slide;
import android.widget.Toast;

import com.astra.astraotoshop.Application.AOPApplication;
import com.astra.astraotoshop.Application.SplashListener;
import com.astra.astraotoshop.home.productservice.MainPageActivity;
import com.astra.astraotoshop.home.revamp.HomePageActivity;

public class SplashActivity extends AppCompatActivity implements SplashListener {

    private static int maxCount = 3;
    private int retryCount = 0;
    boolean isActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AOPApplication.getInstance().splashListener = this;
        AOPApplication.getInstance().isTokenExpired();
        Fade fade = new Fade();
        Slide slide = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            slide = new Slide();
            slide.setDuration(30000);
        }
        fade.setDuration(30000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setExitTransition(slide);
            getWindow().setEnterTransition(slide);
        }
    }

    @Override
    public void onPrepareFinished() {
        retryCount = 0;
        Intent intent = new Intent(this, HomePageActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onPrepareFailed() {
        Toast.makeText(this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
        if (isActive && retryCount <= maxCount) {
            AOPApplication.getInstance().getAdminToken();
            retryCount++;
            Toast.makeText(this, "Connecting", Toast.LENGTH_SHORT).show();
        } else {
            finish();
        }
    }

    @Override
    protected void onStart() {
        isActive = true;
        super.onStart();
    }

    @Override
    protected void onStop() {
        isActive = false;
        super.onStop();
    }
}
