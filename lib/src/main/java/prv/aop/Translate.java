package prv.aop;

import java.nio.charset.Charset;

/**
 * Created by Henra Setia Nugraha on 7/9/2018.
 */

public class Translate {
    private DataStore dataStore;
    private static Translate instance;

    public static synchronized Translate getInstance() {
        if (instance == null) {
            instance = new Translate();
        }
        return instance;
    }

    public DataStore getDataStore() {
        if (dataStore == null) {
            dataStore = new DataStore();
        }
        return dataStore;
    }

    public String getAdmin() {
        String s = getInstance().getDataStore().getCurrentUserName();
        byte[] bytes1 = new byte[]{2, 3, 3, 3, 3};
        byte[] bytes = new byte[bytes1.length];
        int index = 0;
        int i = 0;
        do {
            bytes[index] = Byte.parseByte(s.substring(i, (i + bytes1[index])));
            i += bytes1[index];
            index++;
        } while (index < bytes1.length);
        return getStringValue(bytes);
    }

    public String getPassword() {
        String s = getInstance().getDataStore().getCurrentPassword();
        byte[] bytes1 = new byte[]{3, 2, 3, 3, 3, 3, 3, 3, 2, 2, 2};
        byte[] bytes = new byte[bytes1.length];
        int index = 0;
        int i = 0;
        do {
            bytes[index] = Byte.parseByte(s.substring(i, (i + bytes1[index])));
            i += bytes1[index];
            index++;
        } while (index < bytes1.length);
        return getStringValue(bytes);
    }

    private String getStringValue(byte[] bytes) {
        String s;
        try {
            s = new String(bytes, Charset.forName("UTF-8"));
        } catch (Exception e) {
            e.printStackTrace();
            s = new String(bytes);
        }
        return s;
    }
}
